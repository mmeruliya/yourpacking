//
//  NewsLinkController.m
//  BabyPacking
//
//  Created by c79 on 01/10/13.
//
//

#import "NewsLinkController.h"
#import "Constant.h"

@interface NewsLinkController ()

@end

@implementation NewsLinkController

@synthesize newsLink;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    // Do any additional setup after loading the view from its nib.
    [newsLink loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:I_NEWS_LINK]]];
}

-(void)dealloc
{
    // Do any additional setup after loading the view from its nib.
    [newsLink stopLoading];
    [newsLink release];
    newsLink = nil;
    newsLink.delegate = nil;
    [super dealloc];
}

- (void)viewWillDisappear:(BOOL)animated{
    [newsLink stopLoading];
    newsLink = nil;
    newsLink.delegate = nil;
    [super viewWillDisappear:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

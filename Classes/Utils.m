//
//  Utils.m
//  BabyPacking
//
//  Created by Jay Lee on 10/13/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import "Utils.h"
#import <CommonCrypto/CommonDigest.h>

@interface Utils(Private)
+ (NSDate *)trimTimeOfDate:(NSDate *)theDate;
@end

@implementation Utils

+ (NSString*) md5:(NSString*)str length:(int)len
{
	const char *cStr = [str UTF8String];
	unsigned char result[len];
	
	CC_MD5( cStr, strlen(cStr), result );
	
	NSMutableString * strTemp = [[NSMutableString alloc] initWithCapacity: len*2];
	int i;
	for ( i = 0; i < len; i++ )
	{
		[strTemp appendFormat: @"%02x", result[i]];
	}
	NSString * output = [strTemp copy];
	[strTemp release];
	return ( [output autorelease] );
}

+ (NSString*) getCurrentTimeString {
	char time_string[260] = {0};
	
	long current_time = time(0);
	strftime(time_string, 14, "%Y%m%d%H%M%S", gmtime(&current_time));
	NSString* time = [NSString stringWithCString:time_string];
	
	return time;
}

#pragma mark NSDate processing functions

// trim hour & minute & second off "theDate"
+ (NSDate *)trimTimeOfDate:(NSDate *)theDate {
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	
	NSDateComponents *components = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit)
												fromDate: theDate];
	NSDate *targetDayOfWeek = [gregorian dateFromComponents:components];
	[gregorian release];
	
	return targetDayOfWeek;
}

+ (NSDate *)getTheBeginTimeOfDate:(NSDate *)fromDate {
	
	NSDate *date = [Utils trimTimeOfDate:fromDate];
	
	return date;
}

+ (NSDate *)getTheBeginTimeOfMonth:(NSDate *)fromDate {
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	
	NSDateComponents *components = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit)
												fromDate: fromDate];
	NSDate *date = [gregorian dateFromComponents:components];
	[gregorian release];
	
	return date;
}

+ (NSDate *)getTheBeginTimeOfYear:(NSDate *)fromDate {
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	
	NSDateComponents *components = [gregorian components:(NSYearCalendarUnit)
												fromDate: fromDate];
	NSDate *date = [gregorian dateFromComponents:components];
	[gregorian release];
	
	return date;
}

+ (NSDate *)getTheEndTimeOfDate:(NSDate *)fromDate {
	
	NSDate *date = [Utils trimTimeOfDate:fromDate];
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *componentsToAdd = [[NSDateComponents alloc] init];
	[componentsToAdd setHour:23];
	[componentsToAdd setMinute:59];
	[componentsToAdd setSecond:59];
	date = [gregorian dateByAddingComponents:componentsToAdd 
									  toDate:date options:0];
	[componentsToAdd release];
	[gregorian release];
	
	return date;
}

+ (NSDate *)getTheEndTimeOfMonth:(NSDate *)fromDate {
	
	NSDate *date = [Utils getMonthDay:30 ofDate:fromDate];
	date = [self getTheEndTimeOfDate:date];
	
	return date;
}

+ (NSDate *)getTheEndTimeOfYear:(NSDate *)fromDate {
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	
	NSDateComponents *components = [gregorian components:(NSYearCalendarUnit)
												fromDate: fromDate];
	NSDate *date = [gregorian dateFromComponents:components];
	
	NSDateComponents *componentsToAdd = [[NSDateComponents alloc] init];
	[componentsToAdd setMonth:11];
	[componentsToAdd setDay:30];
	[componentsToAdd setHour:23];
	[componentsToAdd setMinute:59];
	[componentsToAdd setSecond:59];
	date = [gregorian dateByAddingComponents:componentsToAdd toDate:date options:0];
	[componentsToAdd release];
	[gregorian release];
	
	return date;
}

// get the date in the week which contains "theDate"
// ordinal from Sunday = 0 to Saturday = 6 ...
+ (NSDate *)getWeekDay:(NSInteger)ordinal ofDate:(NSDate *)theDate {
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	
	// Get the weekday component of the current date
	NSDateComponents *weekdayComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:theDate];
	
	/*
	 Create a date components to represent the number of days to subtract from the current date.
	 The weekday value for Sunday in the Gregorian calendar is 1, so subtract 1 from the number of days to subtract from the date in question.  (If today's Sunday, subtract 0 days.)
	 */
	NSDateComponents *componentsToSubtract = [[NSDateComponents alloc] init];
	NSLog(@"%d",[weekdayComponents weekday]);
	[componentsToSubtract setDay: 0 - [weekdayComponents weekday] + 1 + ordinal];	// Sunday = 1,...,Saturday = 7
	NSDate *targetDayOfWeek = [gregorian dateByAddingComponents:componentsToSubtract 
														 toDate:theDate options:0];	
	
	NSDateComponents *components = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit)
												fromDate: targetDayOfWeek];
	targetDayOfWeek = [gregorian dateFromComponents:components];
	
	[componentsToSubtract release];
	[gregorian release];
	
	return targetDayOfWeek;
}

// get the date in the month which contains "theDate"
// ordinal from 1st = 0 to 31st = 30
+ (NSDate *)getMonthDay:(NSInteger)ordinal ofDate:(NSDate *)theDate {
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	
	// get YYYY-01-01 00:00:00
	NSDateComponents *components = [gregorian components:(NSYearCalendarUnit)
												fromDate: theDate];
	NSDate *tempDate = [gregorian dateFromComponents:components];
	
	// get [ordinal] days after YYYY-01-01 00:00:00
	NSDateComponents *componentsToAdd = [[NSDateComponents alloc] init];
	[componentsToAdd setDay:ordinal];
	tempDate = [gregorian dateByAddingComponents:componentsToAdd 
										  toDate:tempDate options:0];
	// reset day offset
	[componentsToAdd setDay:0];
	
	// get [months] after YYYY-01-DD 00:00:00
	components = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit)
							  fromDate: theDate];
	[componentsToAdd setMonth: [components month] - 1];
	NSDate *targetDayOfMonth = [gregorian dateByAddingComponents:componentsToAdd 
														  toDate:tempDate options:0];
	// release objects
	[componentsToAdd release];
	[gregorian release];
	
	return targetDayOfMonth;
}

+ (NSInteger)getMinutesFromDate:(NSDate *)from toDate:(NSDate *)to {
	
	return lround(ceil([to timeIntervalSinceDate:from]/60));
}

+ (NSDate *)getDateWithDays:(NSInteger)days afterDate:(NSDate *)date {
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	
	NSDateComponents *componentsToAdd = [[NSDateComponents alloc] init];
	[componentsToAdd setDay:days];
	NSDate* resDate = [gregorian dateByAddingComponents:componentsToAdd 
										  toDate:date options:0];

	[gregorian release];
	
	return resDate;
}

+ (NSDate *)getDateWithHours:(NSInteger)hours afterDate:(NSDate *)date {
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	
	NSDateComponents *componentsToAdd = [[NSDateComponents alloc] init];
	[componentsToAdd setHour:hours];
	NSDate* resDate = [gregorian dateByAddingComponents:componentsToAdd 
												 toDate:date options:0];
	
	[gregorian release];
	
	return resDate;
}

@end

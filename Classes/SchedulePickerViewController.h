//
//  SchedulePickerViewController.h
//  BabyPacking
//
//  Created by Jay Lee on 11/18/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LayoutManager.h"
#import "BabyPackingAppDelegate.h"

@interface SchedulePickerViewController : UIViewController {

	NSDate* mStartDate;
	NSDate* mEndDate;
	NSDate* mTempStartDate;
	NSDate* mTempEndDate;
	NSDateFormatter* mDateFormatter;
	BOOL mStartDateSelected;
    LayoutManager *layoutManager;

	IBOutlet UITableView* mTableView;
	IBOutlet UIDatePicker* mDatePicker;
	IBOutlet UILabel* mStartTimeLabel;
	IBOutlet UILabel* mEndTimeLabel;
	IBOutlet UITableViewCell* mStartsCell;
	IBOutlet UITableViewCell* mEndsCell;
	IBOutlet UITableViewCell* mSwitchCell;
	IBOutlet UIBarButtonItem* mBtnCancel;
	IBOutlet UIBarButtonItem* mBtnDone;
    IBOutlet UIButton *mBtnDonePicker;
    IBOutlet UIView *mPickerView;
}

@property(nonatomic, retain) NSDate* startDate;
@property(nonatomic, retain) NSDate* endDate;
@property(nonatomic, retain) UIView *mPickerView;
@property(nonatomic,retain) UIButton *mBtnDonePicker;

- (IBAction)buttonCancelDidClick:(id)sender;
- (IBAction)buttonDoneDidClick:(id)sender;
- (IBAction)switchValueDidChange:(id)sender;
- (IBAction)pickerValueDidChange:(id)sender;
- (IBAction)donePicker:(id)sender;

@end

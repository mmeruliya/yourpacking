//
//  CSTableViewCell.h
//  BabyPacking
//
//  Created by Gary He on 5/21/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum {
	CSTableViewCellStyleDefault,
	CSTableViewCellStyleValue1,
	CSTableViewCellStyleValue2,
	CSTableViewCellStyleSubtitle,
} CSTableViewCellStyle;

@interface CSTableViewCell : UITableViewCell {
	UIImageView *imageView;
	UILabel *textLabel;
	UILabel *detailTextLabel;
	CSTableViewCellStyle cellStyle;
	UIView *csAccessoryView;
	UIButton *btn;
}

@property(nonatomic, retain) UILabel *textLabel;
@property(nonatomic, retain) UILabel *detailTextLabel;
@property(nonatomic, retain) UIImageView *imageView;
@property(nonatomic, retain) UIView *csAccessoryView;
@property(nonatomic, retain) UIButton *btn;

- (id)initWithCSStyle:(CSTableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;
- (void) layoutContent;
- (void) showAccessoryView;
- (void) hideAccessoryView;
- (void) setPosition;
@end

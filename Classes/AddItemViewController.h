//
//  AddItemViewController.h
//  BabyPacking
//
//  Created by Gary He on 5/20/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PackingList.h"
#import "PackingItem.h"

@class BabyPackingAppDelegate;

@interface AddItemViewController : UITableViewController <UITextFieldDelegate,UITextViewDelegate> {
	
	BabyPackingAppDelegate *appdelegate;
	IBOutlet UIBarButtonItem *saveButton;
	IBOutlet UIBarButtonItem *cancelButton;
	IBOutlet UITableViewCell *itemNameCell;
	IBOutlet UITextField	*nameField;
	IBOutlet UITableViewCell *itemTipsCell;
	IBOutlet UITextField	*tipsField;
	IBOutlet UITableViewCell *itemQTYCell;
	IBOutlet UITextField     *itemQTYField;
	IBOutlet UITextView      *tipTextView;
	PackingList *currentList;
	PackingItem *editingItem;
	NSMutableArray *itemsInApp;
	NSMutableArray *itemArray;
	NSInteger indexth;
	BOOL isNewItem;
	BOOL isFromCheckView;
	
	NSString *category;
	PackingList *currentCategory;
}

@property(nonatomic, retain) PackingList *currentList;
@property(nonatomic, retain) PackingItem *editingItem;
@property(nonatomic, retain) NSMutableArray *itemsInApp;
@property(nonatomic) BOOL isFromCheckView;
@property(nonatomic) NSInteger indexth;
@property(nonatomic, retain) NSMutableArray *itemArray;

@property (nonatomic, retain) NSString *category;
@property (nonatomic, retain) PackingList *currentCategory;

- (IBAction) buttonAction:(id)sender;

@end

//
//  CustomeIconVC.h
//  BabyPacking
//
//  Created by Kirti Avaiya on 05/09/13.
//
//

#import <UIKit/UIKit.h>

@interface CustomeIconVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *tbl;
    NSArray *tblFill;
    NSMutableArray *imgArray;
}

@property (nonatomic,retain) NSArray *tblFill;
@property (nonatomic,retain) NSMutableArray *imgArray;

@end

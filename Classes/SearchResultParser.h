//
//  SearchResultParser.h
//  BabyPacking
//
//  Created by Jay Lee on 11/19/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import "XMLParserBase.h"

@interface SearchResultParser : XMLParserBase
{
	BOOL mSuccessful;
	NSMutableDictionary* mData;
	NSMutableDictionary* mCurItem;
}

@property (nonatomic, assign) BOOL successful;
@property (nonatomic, retain) NSMutableDictionary* data;

@end

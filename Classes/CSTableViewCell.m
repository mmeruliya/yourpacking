//
//  CSTableViewCell.m
//  BabyPacking
//
//  Created by Gary He on 5/21/09.
//  Copyright 2009 test. All rights reserved.
//

#import "BabyPackingAppDelegate.h"
#import "CSTableViewCell.h"
#import "Constant.h"
#define DEF_FONT_SIZE 18
#define TEXT_LABEL_FONT_SIZE DEF_FONT_SIZE - 1
#define DETAIL_LABEL_FONT_SIZE DEF_FONT_SIZE - 6
@interface CSTableViewCell(Private)

- (void)layoutContent;

@end

@implementation CSTableViewCell
@synthesize textLabel;
@synthesize detailTextLabel;
@synthesize imageView;
@synthesize csAccessoryView;
@synthesize btn;

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:NOTIFY_FONT_CHANGED
                                                  object:nil];
	
	[textLabel release];
	[detailTextLabel release];
	[imageView release];
	[csAccessoryView release];
	[btn release];
	
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithFrame:frame reuseIdentifier:reuseIdentifier]) {
        // Initialization code
		[[NSNotificationCenter defaultCenter] addObserver:self 
												 selector:@selector(fontChanged:) 
													 name:NOTIFY_FONT_CHANGED 
												   object:nil];
    }
    return self;
}
-(void)colorchanged
{
    UIView *selectionColor = [[UIView alloc] init];
    selectionColor.backgroundColor = [UIColor colorWithRed:126 green:198 blue:175 alpha:1];
  

}
- (void) fontChanged:(NSNotification*)notification {
	BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
	NSString *customFontName = [appDelegate getCustomFontName];
	int textLabeFontSize = TEXT_LABEL_FONT_SIZE;
	int detailLabelFontSize = DETAIL_LABEL_FONT_SIZE;
	
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		textLabeFontSize -= 8;
		detailLabelFontSize -= 8;
	}
	
	switch (cellStyle) {
		case CSTableViewCellStyleValue1:
			textLabel.font = [UIFont fontWithName:customFontName size:textLabeFontSize];
			detailTextLabel.font = [UIFont fontWithName:customFontName size:detailLabelFontSize];
			break;
		case CSTableViewCellStyleValue2:
			
			break;
		case CSTableViewCellStyleSubtitle:
			textLabel.font = [UIFont fontWithName:customFontName size:textLabeFontSize];
			detailTextLabel.font = [UIFont fontWithName:customFontName size:detailLabelFontSize];
			break;
		default: //CSTableViewCellStyleDefault:
			textLabel.font = [UIFont fontWithName:customFontName size:textLabeFontSize];
			break;
	}
}

- (id)initWithCSStyle:(CSTableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	CGRect frame = CGRectZero;
	if (self = [super initWithFrame:frame reuseIdentifier:reuseIdentifier]) {
		[[NSNotificationCenter defaultCenter] addObserver:self 
												 selector:@selector(fontChanged:) 
													 name:NOTIFY_FONT_CHANGED 
												   object:nil];
		
		imageView = [[UIImageView alloc] initWithImage:nil];
		imageView.frame = CGRectMake(8,2, LIST_ICON_SIZE, LIST_ICON_SIZE);
		imageView.contentMode = UIViewContentModeCenter;
		//imageView.contentMode = UIViewContentModeScaleAspectFit;
		//imageView.contentMode = UIViewContentModeScaleAspectFill;
		imageView.clipsToBounds = YES;
		[self.contentView addSubview:imageView];
		
		textLabel = [[UILabel alloc] initWithFrame:frame];
		textLabel.backgroundColor = [UIColor clearColor];
		//textLabel.backgroundColor = [UIColor lightGrayColor];
		detailTextLabel = [[UILabel alloc] initWithFrame:frame];
		detailTextLabel.backgroundColor = [UIColor clearColor];
		//detailTextLabel.backgroundColor = [UIColor darkGrayColor];
		cellStyle = style;
        
		btn = [[UIButton alloc] initWithFrame: CGRectMake(0, 8, 50, 27)];
		[btn setEnabled:NO];
		[self.contentView addSubview:btn];
		[self.contentView addSubview:textLabel];
		[self.contentView addSubview:detailTextLabel];
		
//		BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
//		NSString *customFontName = [appDelegate getCustomFontName];
//		int textLabeFontSize = TEXT_LABEL_FONT_SIZE;
//		int detailLabelFontSize = DETAIL_LABEL_FONT_SIZE;
//		
//		if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
//			textLabeFontSize -= 6;
//			detailLabelFontSize -= 6;
//		}
		// default point size 20
//		switch (cellStyle) {
//			case CSTableViewCellStyleValue1:
//				textLabel.font = [UIFont fontWithName:customFontName size:textLabeFontSize];
//				detailTextLabel.font = [UIFont fontWithName:customFontName size:detailLabelFontSize];
//				detailTextLabel.textAlignment = UITextAlignmentRight;
//				//detailTextLabel.textAlignment = UITextAlignmentLeft;
//				break;
//			case CSTableViewCellStyleValue2:
//				
//				break;
//			case CSTableViewCellStyleSubtitle:
//				textLabel.font = [UIFont fontWithName:customFontName size:textLabeFontSize];
//				detailTextLabel.font = [UIFont fontWithName:customFontName size:detailLabelFontSize];
//				detailTextLabel.textColor = [UIColor darkGrayColor];
//				break;
//			default: //CSTableViewCellStyleDefault:
//				textLabel.font = [UIFont fontWithName:customFontName size:textLabeFontSize];
//				break;
//		}
		
		switch (cellStyle) {
			case CSTableViewCellStyleValue1:
				//textLabel.font = [UIFont fontWithName:self.font.fontName size:self.font.pointSize - 1];
				//detailTextLabel.font = [UIFont fontWithName:self.font.fontName size:self.font.pointSize - 6];
				detailTextLabel.textAlignment = UITextAlignmentRight;
				//detailTextLabel.textAlignment = UITextAlignmentLeft;
				break;
			case CSTableViewCellStyleValue2:

				break;
			case CSTableViewCellStyleSubtitle:
				//textLabel.font = [UIFont fontWithName:self.font.fontName size:self.font.pointSize - 1];
				//detailTextLabel.font = [UIFont fontWithName:self.font.fontName size:self.font.pointSize - 6];
				detailTextLabel.textColor = [UIColor darkGrayColor];
				break;
			default: //CSTableViewCellStyleDefault:
				//textLabel.font = [UIFont fontWithName:self.font.fontName size:self.font.pointSize - 1];
				break;
		}
		//[self layoutContent];
		[self fontChanged:nil];
	}
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    
  //  [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setImage:(UIImage*) image {
	//NSLog(@"setImage");
	imageView.image = image;
	if (image.size.width < LIST_ICON_SIZE && image.size.height < LIST_ICON_SIZE) {
		imageView.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
	} else {
		imageView.bounds = CGRectMake(0, 0, LIST_ICON_SIZE, LIST_ICON_SIZE);
	}
	CGRect frame = imageView.frame;
	frame.origin.x = 10;
	imageView.frame = frame;
	//NSLog(@"imageView.frame = %f, %f, %f, %f", imageView.frame.origin.x, imageView.frame.origin.y, imageView.frame.size.width, imageView.frame.size.height);
}

- (void) layoutSubviews {
	[super layoutSubviews];
	if (self.showingDeleteConfirmation) {
		self.hidesAccessoryWhenEditing = YES;
	} else {
		self.hidesAccessoryWhenEditing = NO;
	}
	[self layoutContent];
}

#define DETAIL_LABEL_WIDTH_IN_VALUE1 30
#define TEXT_LABEL_HEIGHT_IN_SUBTITLE 24
- (void) layoutContent {
	CGRect bounds = self.contentView.bounds;
	//CGFloat contentX = self.contentView.frame.origin.x;
	//NSLog(@"contentX = %f", contentX);
	CGFloat contentWidth = bounds.size.width;
	CGFloat contentHeight = bounds.size.height;
	CGFloat contentOffsetX = 10.0;
	CGFloat cellImageWidth = 0;
	if (self.imageView.image != nil) {
		cellImageWidth = self.imageView.frame.size.width + contentOffsetX;
	} 
	
	CGFloat availableWidth = contentWidth - contentOffsetX * 2 - cellImageWidth;
	int x1 = 0, y1 = 0, w1 = 0, h1 = 0;
	int x2 = 0, y2 = 0, w2 = 0, h2 = 0;
	switch (cellStyle) {
		case CSTableViewCellStyleValue1:
		{
			//CGFloat percent = 0.75;
			x1 = contentOffsetX + cellImageWidth;
			y1 = 0;
			if ([detailTextLabel.text length] > 0) {
				w1 = availableWidth - DETAIL_LABEL_WIDTH_IN_VALUE1;
			} else {
				w1 = availableWidth;
			}
			
			h1 = contentHeight;
			if ([detailTextLabel.text length] > 0) {
				x2 = x1 + w1;
				y2 = 0;
				w2 = DETAIL_LABEL_WIDTH_IN_VALUE1;
				h2 = contentHeight;
			}
			
//			x1 = contentOffsetX + cellImageWidth;
//			y1 = 0;
//			w1 = 190;
//			h1 = contentHeight;
//			
//			x2 = 200;
//			y2 = 0;
//			w2 = 70;
//			h2 = contentHeight;

		}
			break;
		case CSTableViewCellStyleValue2:
			
			break;
		case CSTableViewCellStyleSubtitle:
		{
			//CGFloat percent = 0.55;
			x1 = contentOffsetX + cellImageWidth;
			y1 = 0;
			w1 = availableWidth;
			h1 = TEXT_LABEL_HEIGHT_IN_SUBTITLE;
			
			x2 = contentOffsetX + cellImageWidth-18;
			y2 = TEXT_LABEL_HEIGHT_IN_SUBTITLE;
			w2 = availableWidth;
			h2 = contentHeight - TEXT_LABEL_HEIGHT_IN_SUBTITLE;
			
//			x1 = contentOffsetX + cellImageWidth;
//			y1 = 0;
//			w1 = availableWidth;
//			h1 = 24;
//			
//			x2 = contentOffsetX + cellImageWidth;
//			y2 = 24;
//			w2 = availableWidth;
//			h2 = 20;
		}
			break;
		default: //case CSTableViewCellStyleDefault:
			x1 = contentOffsetX + cellImageWidth;
			y1 = 0;
			w1 = availableWidth;
			h1 = contentHeight;
			break;
	}
	
	textLabel.frame = CGRectMake(x1-8, y1, w1+20, h1);
	detailTextLabel.frame = CGRectMake(x2+10, y2, w2+10, h2);
}

- (void) showAccessoryView {
	self.accessoryView = csAccessoryView;
}
- (void) hideAccessoryView {
	self.accessoryView = nil;
}

- (void) setPosition{
	[self.imageView setFrame:CGRectMake(8,-1,30,30)];
	[self.textLabel setFrame:CGRectMake(40,18,200,30)];
}

@end

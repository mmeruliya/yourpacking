//
//  SettingsViewController.m
//  BabyPacking
//
//  Created by Gary He on 6/8/09.
//  Copyright 2009 test. All rights reserved.
//

#import "Constant.h"
#import "BackgroundTextSubmenuViewController.h"
#import "CustomizeCellViewController.h";
#import "CustomizeTopBarViewController.h"
#import "SettingsViewController.h"
#import "BabyPackingAppDelegate.h"
#import "LoginParser.h"
#import "RegistrationParser.h"
#import "DataManager.h"
#import "ImportParser.h"
#import "ExportParser.h"
#import "XMLWriter.h"
#import "Categories.h"
#import "Items.h"
#import "PackingItem.h"
#import "PackingList.h"
#import "BriefcaseItem.h"
#import "RecordItem.h"
#import "TaskItem.h"
#import "SetImageParser.h"

UIView *View_Login,*View_reg,*View_import,*View_export;
UITextField *txtUserName_Login,*txtPassword_Login,*txtUserName_Reg,*txtPassword_Reg,*txtEmail_Reg;


@implementation SettingsViewController

@synthesize importButton,exportButton,loginView,loginView1,regView,regView1,txtEmailReg,txtEmailReg1,txtPasswordLogin,loginView_L,regView_L,loginView_PDL,regView_PDL,txtPasswordLogin1,txtPasswordReg,txtPasswordReg1,txtUsernameLogin,txtUsernameLogin1,txtUsernameReg,txtUsernameReg1, txtPasswordLogin_L,txtUsernameLogin_L,txtUsernameReg_L,txtEmailReg_L,txtPasswordReg_L,txtUsernameLogin_PDL,txtUsernameReg_PDL,txtPasswordLogin_PDL,txtPasswordReg_PDL,txtEmailReg_PDL;
@synthesize babyXMLString,packingXMLString;
@synthesize iphoneLogout_ImportView,iphoneLogout_ExportView,ipadLogout_ExportView,ipadLogout_ImportView,iphoneLogout_ExportView_L,iphoneLogout_ImportView_L,ipadLogout_ImportView_L,ipadLogout_ExportView_L;
@synthesize btnSelectColorForTopBg;

- (void)dealloc {

	[iphoneLogout_ImportView release];
	[iphoneLogout_ExportView release];
	[ipadLogout_ExportView release];
	[ipadLogout_ImportView release];
    [ipadLogout_ExportView_L release];
	[ipadLogout_ImportView_L release];
	[txtUsernameLogin release];
	[txtPasswordLogin release];
    [txtUsernameLogin_PDL release];
	[txtPasswordLogin_PDL release];
	[txtUsernameLogin1 release];
	[txtPasswordLogin1 release];
    [txtUsernameLogin_L release];
	[txtPasswordLogin_L release];
	[txtUsernameReg release];
	[txtPasswordReg release];
    [txtUsernameReg_PDL release];
	[txtPasswordReg_PDL release];
	[txtUsernameReg1 release];
    [txtUsernameReg_L release];
	[txtPasswordReg1 release];
    [txtPasswordReg_L release];
	[txtEmailReg release];
    [txtEmailReg_PDL release];
	[txtEmailReg1 release];
    [txtEmailReg_L release];
	[babyXMLString release];
	[packingXMLString release];
	[cancelButton release];
	[loginView release];
	[loginView1 release];
    [loginView_L release];
    [loginView_PDL release];
	[regView release];
	[regView1 release];
    [regView_L release];
    [regView_PDL release];
	[doneButton release];
	[soundSwidth release];
    [soundProgressSwitch release];
	[colorBGSwidth release];
	[photoBGSwidth release];
	[addItemByGroupSwidth release];
	[readyMadeListShowSwidth release];
	[welcomePageShowSwidth release];
	[fontTypeLabel release];
	[taskSoundLabel release];
    [sound1 release];
    [sound2 release];
	[importButton release];
	[exportButton release];
	[btnSelectColorForTopBg release];
    
    [super dealloc];
}

/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if (self = [super initWithStyle:style]) {
    }
    return self;
}
*/

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        layoutManager = [[LayoutManager alloc] initWithPortraitView:self.view andLandscapeNIB:@"SettingsViewController_L"];
        //self = [super initWithNibName:@"RecalliPhoneView" bundle:nibBundleOrNil];
        
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    mAppDelegate = (id)[UIApplication sharedApplication].delegate;
	
    //Custom Title	
    
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor whiteColor];
    
	NSString *customFontName = [mAppDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
    //	label.font = [UIFont fontWithName:customFontName size:fontSize];
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];		
	}
	else {
		label.font=[UIFont fontWithName:customFontName size:fontSize];		
	}
	
//	label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;
    
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
            label.textColor=[UIColor whiteColor];
        self.navigationItem.backBarButtonItem.tintColor=[UIColor whiteColor];
        self.navigationItem.leftBarButtonItem.tintColor=[UIColor whiteColor];
        self.navigationItem.rightBarButtonItem.tintColor=[UIColor whiteColor];
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label.textColor = theColour1;
        self.navigationItem.backBarButtonItem.tintColor=theColour1;
        self.navigationItem.leftBarButtonItem.tintColor=theColour1;
        self.navigationItem.rightBarButtonItem.tintColor=theColour1;
	}
	// and finally set the colour of your label
	
    label.text=@"Settings";
    [label sizeToFit];
    
	count=0;
	
    
	self.navigationItem.titleView = label;	

	
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.

	[NSThread detachNewThreadSelector:@selector(startSync) toTarget:self withObject:nil];
//	[mAppDelegate startSynchronizing];
    
    UIButton *Editbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    Editbtn.frame = CGRectMake(0, 0, 30, 30);
    Editbtn.tag=2;
    [Editbtn setImage:[UIImage imageNamed:@"Done.png"] forState:UIControlStateNormal];
    // [Editbtn setBackgroundColor:[UIColor blackColor]];
    [Editbtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    doneButton=[[UIBarButtonItem alloc] initWithCustomView:Editbtn];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 30, 30);
    btn.tag=1;
    [btn setImage:[UIImage imageNamed:@"Cancel.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cancelButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
	self.navigationItem.rightBarButtonItem = doneButton;
	self.navigationItem.leftBarButtonItem = cancelButton;
	
	
//###########################
	[mAppDelegate.dataManager getAllData];
	NSLog(@"ITEM COUNT = %d",[mAppDelegate.allItemsList count]);
	NSLog(@"CATEGORY COUNT = %d",[mAppDelegate.allCategoriesList count]);
	//				ExportParser *eParser=[[ExportParser alloc] init];
	//				[eParser initWithData:[NSString stringWithFormat:@"%d",mAppDelegate.userID] withTableName:@"category"];
	//				[eParser release];
	//
	//				ExportParser *eParser1=[[ExportParser alloc] init];
	//				[eParser1 initWithData:[NSString stringWithFormat:@"%d",mAppDelegate.userID] withTableName:@"Items"];
	//				[eParser1 release];
	// allocate serializer
	XMLWriter* xmlWriter = [[XMLWriter alloc]init];
	
	// start writing XML elements
	for (Categories *cat in mAppDelegate.allCategoriesList) {
		
		[xmlWriter writeStartElement:@"categories"];
		
		[xmlWriter writeStartElement:@"pk"];
		[xmlWriter writeCharacters:[NSString stringWithFormat:@"%d",cat.pk]];
		[xmlWriter writeEndElement:@"pk"];
		
		[xmlWriter writeStartElement:@"name"];
		[xmlWriter writeCharacters:[NSString stringWithFormat:@"%@",cat.name]];
		[xmlWriter writeEndElement];
		
		[xmlWriter writeStartElement:@"deleted"];
		[xmlWriter writeCharacters:[NSString stringWithFormat:@"%d",cat.deleted]];
		[xmlWriter writeEndElement];
		
		[xmlWriter writeStartElement:@"type"];
		[xmlWriter writeCharacters:[NSString stringWithFormat:@"%d",cat.type]];
		[xmlWriter writeEndElement];
		
		[xmlWriter writeEndElement:@"categories"];					
	}
	
	// get the resulting XML string
	NSString* xml = [xmlWriter toString];
	
	NSLog(@"XML STRING = %@",xml);

	
	// allocate serializer
	XMLWriter* xmlWriter1 = [[XMLWriter alloc]init];
	
	// start writing XML elements
	for (Items *items in mAppDelegate.allItemsList) {

		[xmlWriter1 writeStartElement:@"Items"];
		
		[xmlWriter1 writeStartElement:@"pk"];
		[xmlWriter1 writeCharacters:[NSString stringWithFormat:@"%d",items.pk]];
		[xmlWriter1 writeEndElement:@"pk"];
		
		[xmlWriter1 writeStartElement:@"checked"];
		[xmlWriter1 writeCharacters:[NSString stringWithFormat:@"%d",items.checked]];
		[xmlWriter1 writeEndElement];
		
		[xmlWriter1 writeStartElement:@"name"];
		[xmlWriter1 writeCharacters:[NSString stringWithFormat:@"%@",items.name]];
		[xmlWriter1 writeEndElement];
		
		[xmlWriter1 writeStartElement:@"tip"];
		[xmlWriter1 writeCharacters:[NSString stringWithFormat:@"%@",items.tip]];
		[xmlWriter1 writeEndElement];

		[xmlWriter1 writeStartElement:@"custom"];
		[xmlWriter1 writeCharacters:[NSString stringWithFormat:@"%@",items.custom]];
		[xmlWriter1 writeEndElement];
		
		[xmlWriter1 writeStartElement:@"qty"];
		[xmlWriter1 writeCharacters:[NSString stringWithFormat:@"%@",items.qty]];
		[xmlWriter1 writeEndElement];
		
		[xmlWriter1 writeStartElement:@"deleted"];
		[xmlWriter1 writeCharacters:[NSString stringWithFormat:@"%d",items.deleted]];
		[xmlWriter1 writeEndElement];
		
		[xmlWriter1 writeStartElement:@"categoryId"];
		[xmlWriter1 writeCharacters:[NSString stringWithFormat:@"%d",items.categoryId]];
		[xmlWriter1 writeEndElement];
		
		[xmlWriter1 writeEndElement:@"Items"];					
	}
	
	// get the resulting XML string
	NSString* xml1 = [xmlWriter1 toString];
	
	NSLog(@"XML STRING = %@",xml1);

	
	
	
	// allocate serializer
	NSMutableArray *babyItemArray=[NSMutableArray arrayWithArray:[PackingItem findAll]];
	NSLog(@"Baby Packing ITEM List = %d",[babyItemArray count]);
	XMLWriter* xmlWriter2 = [[XMLWriter alloc]init];
	
	// start writing XML elements
	for (PackingItem *pItem in babyItemArray) {
		
		[xmlWriter2 writeStartElement:@"item"];
		
		[xmlWriter2 writeStartElement:@"pk"];
		[xmlWriter2 writeCharacters:[NSString stringWithFormat:@"%d",pItem.pk]];
		[xmlWriter2 writeEndElement:@"pk"];
		
		[xmlWriter2 writeStartElement:@"name"];
		[xmlWriter2 writeCharacters:[NSString stringWithFormat:@"%@",pItem.name]];
		[xmlWriter2 writeEndElement];
		
		[xmlWriter2 writeStartElement:@"qty"];
		[xmlWriter2 writeCharacters:[NSString stringWithFormat:@"%@",pItem.qty]];
		[xmlWriter2 writeEndElement];
		
		[xmlWriter2 writeStartElement:@"tip"];
		[xmlWriter2 writeCharacters:[NSString stringWithFormat:@"%@",pItem.tip]];
		[xmlWriter2 writeEndElement];
		
		[xmlWriter2 writeStartElement:@"selected"];
		[xmlWriter2 writeCharacters:[NSString stringWithFormat:@"%d",pItem.selected]];
		[xmlWriter2 writeEndElement];
		
		[xmlWriter2 writeStartElement:@"custom"];
		[xmlWriter2 writeCharacters:[NSString stringWithFormat:@"%@",pItem.custom]];
		[xmlWriter2 writeEndElement];
		
		[xmlWriter2 writeStartElement:@"checked"];
		[xmlWriter2 writeCharacters:[NSString stringWithFormat:@"%d",pItem.checked]];
		[xmlWriter2 writeEndElement];
		
		[xmlWriter2 writeStartElement:@"listId"];
		[xmlWriter2 writeCharacters:[NSString stringWithFormat:@"%d",pItem.listId]];
		[xmlWriter2 writeEndElement];
		
		[xmlWriter2 writeStartElement:@"predefineId"];
		[xmlWriter2 writeCharacters:[NSString stringWithFormat:@"%d",pItem.predefineId]];
		[xmlWriter2 writeEndElement];

		[xmlWriter2 writeStartElement:@"predefineForAge"];
		[xmlWriter2 writeCharacters:[NSString stringWithFormat:@"%d",pItem.predefineForAge]];
		[xmlWriter2 writeEndElement];
		
		[xmlWriter2 writeEndElement:@"item"];					
	}
	
	// get the resulting XML string
	NSString* xml2 = [xmlWriter2 toString];
	
	NSLog(@"XML STRING = %@",xml2);
	
	// allocate serializer
	NSMutableArray *babyListArray=[[NSMutableArray alloc] initWithArray:[PackingList findAllWithoutPhoto]];
	XMLWriter* xmlWriter3 = [[XMLWriter alloc]init];
	
	// start writing XML elements
	for (PackingList *list in babyListArray) {
		
		[xmlWriter3 writeStartElement:@"list"];
		
		[xmlWriter3 writeStartElement:@"pk"];
		[xmlWriter3 writeCharacters:[NSString stringWithFormat:@"%d",list.pk]];
		[xmlWriter3 writeEndElement:@"pk"];
		
		[xmlWriter3 writeStartElement:@"name"];
		[xmlWriter3 writeCharacters:[NSString stringWithFormat:@"%@",list.name]];
		[xmlWriter3 writeEndElement];
		
		[xmlWriter3 writeStartElement:@"sex"];
		[xmlWriter3 writeCharacters:[NSString stringWithFormat:@"%d",list.sex]];
		[xmlWriter3 writeEndElement];
		
		[xmlWriter3 writeStartElement:@"age"];
		[xmlWriter3 writeCharacters:[NSString stringWithFormat:@"%d",list.age]];
		[xmlWriter3 writeEndElement];

		if (list.icon==nil) {
			NSLog(@"NIL ICON");
			[xmlWriter3 writeStartElement:@"smallIcon"];
			[xmlWriter3 writeCharacters:@""];
			[xmlWriter3 writeEndElement];			
		}
		else {
//			[xmlWriter3 writeStartElement:@"smallIcon"];
//			UIImage* pic = list.icon;
//			NSData* pictureData = UIImagePNGRepresentation(list.icon);
//			NSString* pictureDataString = [pictureData base64Encoding];
//			[xmlWriter3 writeCharacters:pictureDataString];
//			[xmlWriter3 writeEndElement];			

//			[xmlWriter3 writeStartElement:@"smallIcon"];
//			NSData *pictureData = UIImagePNGRepresentation(list.icon);
//			NSString *pictureDataString = [[NSString alloc] initWithData:pictureData encoding:NSASCIIStringEncoding];
//			NSLog(@"STRING PICTURE");
//			NSLog(@"STRING PICTURE %@",pictureDataString);
//			[xmlWriter3 writeCharacters:[NSString stringWithFormat:@"%@",pictureDataString]];
//			[xmlWriter3 writeEndElement];						

//			[xmlWriter3 writeStartElement:@"smallIcon"];
//			//UIImage* pic = list.icon;
//			NSData* pictureData = UIImagePNGRepresentation(list.icon);
//			[xmlWriter3 writeCharacters:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%.*s", [pictureData length], [pictureData bytes]]]];
//			NSLog(@"BYTE LENGTH :  %d", [[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%.*s", [pictureData length], [pictureData bytes]]] length]);
//			[xmlWriter3 writeEndElement];			

//			[xmlWriter3 writeStartElement:@"smallIcon"];
//			//UIImage* pic = list.icon;
//			NSData* pictureData = UIImagePNGRepresentation(list.icon);
//			NSMutableString *binaryString = [NSMutableString stringWithCapacity:[pictureData length]];
//			unsigned char *bytes = [pictureData bytes];
//			for (int i = 0; i < [pictureData length]; i++) {
//				[binaryString appendFormat:@"%02x", bytes[i]];
//			}
//			NSLog(@"%@",binaryString);
//			[xmlWriter3 writeCharacters:binaryString];
//			[xmlWriter3 writeEndElement];			

			
			[xmlWriter3 writeStartElement:@"smallIcon"];
			//UIImage* pic = list.icon;
			[xmlWriter3 writeCharacters:@"1"];
			[xmlWriter3 writeEndElement];			
			
		}
//		
//		UIImageView *imgg=[[UIImageView alloc] initWithFrame:CGRectMake(20.0, 30.0, 100.0, 100.0)];
//		[imgg setImage:list.icon];
//		[self.view addSubview:imgg];
		
		[xmlWriter3 writeStartElement:@"lastUpdate"];
		[xmlWriter3 writeCharacters:[NSString stringWithFormat:@"%f",list.lastUpdate]];
		[xmlWriter3 writeEndElement];

		[xmlWriter3 writeStartElement:@"totalCount"];
		[xmlWriter3 writeCharacters:[NSString stringWithFormat:@"%d",list.totalCount]];
		[xmlWriter3 writeEndElement];

		[xmlWriter3 writeStartElement:@"checkedCount"];
		[xmlWriter3 writeCharacters:[NSString stringWithFormat:@"%d",list.checkedCount]];
		[xmlWriter3 writeEndElement];

		[xmlWriter3 writeStartElement:@"predefineId"];
		[xmlWriter3 writeCharacters:[NSString stringWithFormat:@"%d",list.predefineId]];
		[xmlWriter3 writeEndElement];
		
		[xmlWriter3 writeStartElement:@"predefineForAge"];
		[xmlWriter3 writeCharacters:[NSString stringWithFormat:@"%d",list.predefineForAge]];
		[xmlWriter3 writeEndElement];
		
		
		[xmlWriter3 writeEndElement:@"list"];					
	}
	
	// get the resulting XML string
	NSString* xml3 = [xmlWriter3 toString];
	
	NSLog(@"XML STRING = %@",xml3);
	
	// allocate serializer
	NSMutableArray *babyBriefcaseArray=[[NSMutableArray alloc] initWithArray:[BriefcaseItem findAll]];
	XMLWriter* xmlWriter4 = [[XMLWriter alloc]init];
	
	// start writing XML elements
	for (BriefcaseItem *briefcase in babyBriefcaseArray) {

		[xmlWriter4 writeStartElement:@"briefcase"];
		
		[xmlWriter4 writeStartElement:@"pk"];
		[xmlWriter4 writeCharacters:[NSString stringWithFormat:@"%d",briefcase.pk]];
		[xmlWriter4 writeEndElement:@"pk"];
		
		[xmlWriter4 writeStartElement:@"title"];
		[xmlWriter4 writeCharacters:[NSString stringWithFormat:@"%@",briefcase.title]];
		[xmlWriter4 writeEndElement];
		
		[xmlWriter4 writeStartElement:@"passport"];
		[xmlWriter4 writeCharacters:[NSString stringWithFormat:@"%@",briefcase.passport]];
		[xmlWriter4 writeEndElement];
		
		[xmlWriter4 writeStartElement:@"license"];
		[xmlWriter4 writeCharacters:[NSString stringWithFormat:@"%@",briefcase.license]];
		[xmlWriter4 writeEndElement];
		
		[xmlWriter4 writeStartElement:@"ssn"];
		[xmlWriter4 writeCharacters:[NSString stringWithFormat:@"%@",briefcase.ssn]];
		[xmlWriter4 writeEndElement];
		
		[xmlWriter4 writeStartElement:@"id_card"];
		[xmlWriter4 writeCharacters:[NSString stringWithFormat:@"%@",briefcase.id_card]];
		[xmlWriter4 writeEndElement];
		
		[xmlWriter4 writeStartElement:@"airline_name"];
		[xmlWriter4 writeCharacters:[NSString stringWithFormat:@"%@",briefcase.airline_name]];
		[xmlWriter4 writeEndElement];
		
		[xmlWriter4 writeStartElement:@"flight"];
		[xmlWriter4 writeCharacters:[NSString stringWithFormat:@"%@",briefcase.flight]];
		[xmlWriter4 writeEndElement];
		
		[xmlWriter4 writeStartElement:@"frequent_flyer"];
		[xmlWriter4 writeCharacters:[NSString stringWithFormat:@"%@",briefcase.frequent_flyer]];
		[xmlWriter4 writeEndElement];
		
		[xmlWriter4 writeStartElement:@"car_name"];
		[xmlWriter4 writeCharacters:[NSString stringWithFormat:@"%@",briefcase.car_name]];
		[xmlWriter4 writeEndElement];

		[xmlWriter4 writeStartElement:@"license_plate"];
		[xmlWriter4 writeCharacters:[NSString stringWithFormat:@"%@",briefcase.license_plate]];
		[xmlWriter4 writeEndElement];

		[xmlWriter4 writeStartElement:@"hotel_name"];
		[xmlWriter4 writeCharacters:[NSString stringWithFormat:@"%@",briefcase.hotel_name]];
		[xmlWriter4 writeEndElement];

		[xmlWriter4 writeStartElement:@"hotel_phone"];
		[xmlWriter4 writeCharacters:[NSString stringWithFormat:@"%@",briefcase.hotel_phone]];
		[xmlWriter4 writeEndElement];

		[xmlWriter4 writeStartElement:@"hotel_address"];
		[xmlWriter4 writeCharacters:[NSString stringWithFormat:@"%@",briefcase.hotel_address]];
		[xmlWriter4 writeEndElement];

		[xmlWriter4 writeStartElement:@"embassy_name"];
		[xmlWriter4 writeCharacters:[NSString stringWithFormat:@"%@",briefcase.embassy_name]];
		[xmlWriter4 writeEndElement];

		[xmlWriter4 writeStartElement:@"embassy_address"];
		[xmlWriter4 writeCharacters:[NSString stringWithFormat:@"%@",briefcase.embassy_address]];
		[xmlWriter4 writeEndElement];

		[xmlWriter4 writeStartElement:@"embassy_emergency"];
		[xmlWriter4 writeCharacters:[NSString stringWithFormat:@"%@",briefcase.embassy_emergency]];
		[xmlWriter4 writeEndElement];

		[xmlWriter4 writeStartElement:@"other"];
		[xmlWriter4 writeCharacters:[NSString stringWithFormat:@"%@",briefcase.other]];
		[xmlWriter4 writeEndElement];

		[xmlWriter4 writeEndElement:@"briefcase"];					
	}
	
	// get the resulting XML string
	NSString* xml4 = [xmlWriter4 toString];
	
	NSLog(@"XML STRING = %@",xml4);

	
	// allocate serializer
	NSMutableArray *babyRecordList=[[NSMutableArray alloc] initWithArray:[RecordItem findAll]];
	XMLWriter* xmlWriter5 = [[XMLWriter alloc]init];
	
	// start writing XML elements
	for (RecordItem *record in babyRecordList) {
		
		[xmlWriter5 writeStartElement:@"record"];
		
		[xmlWriter5 writeStartElement:@"id"];
		[xmlWriter5 writeCharacters:[NSString stringWithFormat:@"%d",record.pk]];
		[xmlWriter5 writeEndElement:@"id"];
		
		[xmlWriter5 writeStartElement:@"remote_id"];
		[xmlWriter5 writeCharacters:[NSString stringWithFormat:@"%d",record.remote_id]];
		[xmlWriter5 writeEndElement];
		
		[xmlWriter5 writeStartElement:@"tid"];
		[xmlWriter5 writeCharacters:[NSString stringWithFormat:@"%d",record.task_id]];
		[xmlWriter5 writeEndElement];
		
		[xmlWriter5 writeStartElement:@"title"];
		[xmlWriter5 writeCharacters:[NSString stringWithFormat:@"%@",record.title]];
		[xmlWriter5 writeEndElement];
		
		[xmlWriter5 writeStartElement:@"category"];
		[xmlWriter5 writeCharacters:[NSString stringWithFormat:@"%@",record.category]];
		[xmlWriter5 writeEndElement];
		
		[xmlWriter5 writeStartElement:@"repeat"];
		[xmlWriter5 writeCharacters:[NSString stringWithFormat:@"%@",record.repeat]];
		[xmlWriter5 writeEndElement];
		
		[xmlWriter5 writeStartElement:@"interval"];
		[xmlWriter5 writeCharacters:[NSString stringWithFormat:@"%d",record.interval]];
		[xmlWriter5 writeEndElement];
		
		[xmlWriter5 writeStartElement:@"start_date"];
		[xmlWriter5 writeCharacters:[NSString stringWithFormat:@"%@",record.start_date]];
		[xmlWriter5 writeEndElement];
		
		[xmlWriter5 writeStartElement:@"end_date"];
		[xmlWriter5 writeCharacters:[NSString stringWithFormat:@"%@",record.end_date]];
		[xmlWriter5 writeEndElement];
		
		[xmlWriter5 writeStartElement:@"text"];
		[xmlWriter5 writeCharacters:[NSString stringWithFormat:@"%@",record.text]];
		[xmlWriter5 writeEndElement];
		
		[xmlWriter5 writeStartElement:@"device_token"];
		[xmlWriter5 writeCharacters:[NSString stringWithFormat:@"%@",record.device_token]];
		[xmlWriter5 writeEndElement];
		
		[xmlWriter5 writeStartElement:@"create_date"];
		[xmlWriter5 writeCharacters:[NSString stringWithFormat:@"%@",record.create_date]];
		[xmlWriter5 writeEndElement];
		
		[xmlWriter5 writeStartElement:@"update_date"];
		[xmlWriter5 writeCharacters:[NSString stringWithFormat:@"%2",record.update_date]];
		[xmlWriter5 writeEndElement];
				
		[xmlWriter5 writeEndElement:@"record"];					
	}
	
	// get the resulting XML string
	NSString* xml5 = [xmlWriter5 toString];
	
	NSLog(@"XML STRING = %@",xml5);

	// allocate serializer
	NSMutableArray *babyTaskList=[[NSMutableArray alloc] initWithArray:[TaskItem findAll]];
	XMLWriter* xmlWriter6 = [[XMLWriter alloc]init];

	// start writing XML elements
	for (TaskItem *task in babyTaskList) {
		
		[xmlWriter6 writeStartElement:@"task"];
		
		[xmlWriter6 writeStartElement:@"id"];
		[xmlWriter6 writeCharacters:[NSString stringWithFormat:@"%d",task.pk]];
		[xmlWriter6 writeEndElement:@"id"];
		
		[xmlWriter6 writeStartElement:@"remote_id"];
		[xmlWriter6 writeCharacters:[NSString stringWithFormat:@"%d",task.remote_id]];
		[xmlWriter6 writeEndElement];
				
		[xmlWriter6 writeStartElement:@"title"];
		[xmlWriter6 writeCharacters:[NSString stringWithFormat:@"%@",task.title]];
		[xmlWriter6 writeEndElement];
		
		[xmlWriter6 writeStartElement:@"category"];
		[xmlWriter6 writeCharacters:[NSString stringWithFormat:@"%@",task.category]];
		[xmlWriter6 writeEndElement];
		
		[xmlWriter6 writeStartElement:@"repeat"];
		[xmlWriter6 writeCharacters:[NSString stringWithFormat:@"%@",task.repeat]];
		[xmlWriter6 writeEndElement];
		
		[xmlWriter6 writeStartElement:@"interval"];
		[xmlWriter6 writeCharacters:[NSString stringWithFormat:@"%d",task.interval]];
		[xmlWriter6 writeEndElement];
		
		[xmlWriter6 writeStartElement:@"start_date"];
		[xmlWriter6 writeCharacters:[NSString stringWithFormat:@"%@",task.start_date]];
		[xmlWriter6 writeEndElement];
		
		NSLog(@"DATE ========== %@",task.start_date);
		
		[xmlWriter6 writeStartElement:@"end_date"];
		[xmlWriter6 writeCharacters:[NSString stringWithFormat:@"%@",task.end_date]];
		[xmlWriter6 writeEndElement];
		
		[xmlWriter6 writeStartElement:@"text"];
		[xmlWriter6 writeCharacters:[NSString stringWithFormat:@"%@",task.text]];
		[xmlWriter6 writeEndElement];
		
		[xmlWriter6 writeStartElement:@"device_token"];
		[xmlWriter6 writeCharacters:[NSString stringWithFormat:@"%@",task.device_token]];
		[xmlWriter6 writeEndElement];
		
		[xmlWriter6 writeStartElement:@"create_date"];
		[xmlWriter6 writeCharacters:[NSString stringWithFormat:@"%@",task.create_date]];
		[xmlWriter6 writeEndElement];
		
		[xmlWriter6 writeStartElement:@"update_date"];
		[xmlWriter6 writeCharacters:[NSString stringWithFormat:@"%@",task.update_date]];
		[xmlWriter6 writeEndElement];
		
		[xmlWriter6 writeEndElement:@"task"];					
	}
	
	// get the resulting XML string
	NSString* xml6 = [xmlWriter6 toString];
	
	NSLog(@"XML STRING = %@",xml6);
	packingXMLString=[[NSMutableString alloc] initWithFormat:@"<packngo><categoriesdetail>%@</categoriesdetail><itemsdetail>%@</itemsdetail><itemdetail>%@</itemdetail><listdetail>%@</listdetail><briefcasedetail>%@</briefcasedetail><recorddetail>%@</recorddetail><taskdetail>%@</taskdetail></packngo>",xml,xml1,xml2,xml3,xml4,xml5,xml6];
	babyXMLString=[[NSMutableString alloc] initWithFormat:@"<packngo><categoriesdetail>%@</categoriesdetail><itemsdetail>%@</itemsdetail><itemdetail>%@</itemdetail><listdetail>%@</listdetail><briefcasedetail>%@</briefcasedetail><recorddetail>%@</recorddetail><taskdetail>%@</taskdetail></packngo>",xml,xml1,xml2,xml3,xml4,xml5,xml6];
	
//################################################	

	
	CGRect rect = CGRectMake(198, 5, 94, 27);

    //Custom Switch
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        soundSwidth = [[TTFadeSwitch alloc] initWithFrame:CGRectMake(220, 5, 70, 20)];
    else
        soundSwidth = [[TTFadeSwitch alloc] initWithFrame:CGRectMake(500, 5, 70, 27)];
    [self customSwitch:soundSwidth];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        soundProgressSwitch = [[TTFadeSwitch alloc] initWithFrame:CGRectMake(220, 5, 70, 27)];
    else
        soundProgressSwitch = [[TTFadeSwitch alloc] initWithFrame:CGRectMake(500, 5, 70, 27)];
    [self customSwitch:soundProgressSwitch];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        colorBGSwidth = [[TTFadeSwitch alloc] initWithFrame:CGRectMake(220, 5, 70, 27)];
    else
        colorBGSwidth = [[TTFadeSwitch alloc] initWithFrame:CGRectMake(500, 5, 70, 27)];
    [self customSwitch:colorBGSwidth];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        photoBGSwidth = [[TTFadeSwitch alloc] initWithFrame:CGRectMake(220, 5, 70, 27)];
    else
        photoBGSwidth = [[TTFadeSwitch alloc] initWithFrame:CGRectMake(500, 5, 70, 27)];
    [self customSwitch:photoBGSwidth];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        addItemByGroupSwidth = [[TTFadeSwitch alloc] initWithFrame:CGRectMake(220, 5, 70, 27)];
    else
        addItemByGroupSwidth = [[TTFadeSwitch alloc] initWithFrame:CGRectMake(500, 5, 70, 27)];
    [self customSwitch:addItemByGroupSwidth];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        readyMadeListShowSwidth = [[TTFadeSwitch alloc] initWithFrame:CGRectMake(220, 5, 70, 27)];
    else
        readyMadeListShowSwidth = [[TTFadeSwitch alloc] initWithFrame:CGRectMake(500, 5, 70, 27)];
    [self customSwitch:readyMadeListShowSwidth];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        welcomePageShowSwidth = [[TTFadeSwitch alloc] initWithFrame:CGRectMake(220, 5, 70, 27)];
    else
        welcomePageShowSwidth = [[TTFadeSwitch alloc] initWithFrame:CGRectMake(500, 5, 70, 27)];
    [self customSwitch:welcomePageShowSwidth];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        iCloudSwidth = [[TTFadeSwitch alloc] initWithFrame:CGRectMake(220, 5, 70, 27)];
    else
        iCloudSwidth = [[TTFadeSwitch alloc] initWithFrame:CGRectMake(500, 5, 70, 27)];
    [self customSwitch:iCloudSwidth];
//	soundSwidth = [[UISwitch alloc] initWithFrame:rect];
//  soundProgressSwitch = [[UISwitch alloc] initWithFrame:rect1];
//	colorBGSwidth = [[UISwitch alloc] initWithFrame:rect];
//	photoBGSwidth = [[UISwitch alloc] initWithFrame:rect];
//	addItemByGroupSwidth = [[UISwitch alloc] initWithFrame:rect];
//	readyMadeListShowSwidth = [[UISwitch alloc] initWithFrame:rect];
//	welcomePageShowSwidth = [[UISwitch alloc] initWithFrame:rect];

//	importButton=[[UIButton alloc] initWithFrame:CGRectMake(140.0, 8.0, 80.0, 27.0)];
//	exportButton=[[UIButton alloc] initWithFrame:CGRectMake(220.0, 8.0, 80.0, 27.0)];


	importButton=[[UIButton alloc] initWithFrame:rect];

	exportButton=[[UIButton alloc] initWithFrame:rect];

	btnSelectColorForTopBg=[[UIButton alloc] initWithFrame:rect];
	
	fontTypeLabel = [[UILabel alloc] initWithFrame:CGRectMake(95,1,172,40)];
	taskSoundLabel = [[UILabel alloc] initWithFrame:CGRectMake(150,1,117,40)];
	//fontTypeLabel.textColor = [UIColor blueColor];
	fontTypeLabel.textColor = [UIColor colorWithRed:0.19 green:0.30 blue:0.52 alpha:1.0];
	fontTypeLabel.textAlignment = UITextAlignmentRight;
    fontTypeLabel.backgroundColor=[UIColor clearColor];
	taskSoundLabel.textColor = [UIColor colorWithRed:0.19 green:0.30 blue:0.52 alpha:1.0];
	taskSoundLabel.textAlignment = UITextAlignmentRight;
    taskSoundLabel.backgroundColor=[UIColor clearColor];

	sound1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 400, 20)];
	sound1.textColor = [UIColor blackColor];
	sound1.textAlignment = UITextAlignmentLeft;
    sound1.backgroundColor=[UIColor clearColor];
    sound1.font=[UIFont boldSystemFontOfSize:16];

    sound2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 38, 400, 20)];
	sound2.textColor = [UIColor blackColor];
	sound2.textAlignment = UITextAlignmentLeft;
    sound2.backgroundColor=[UIColor clearColor];
    sound2.font=[UIFont boldSystemFontOfSize:16];    

    
    
	[soundSwidth addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
	[soundProgressSwitch addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
	[colorBGSwidth addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
	[photoBGSwidth addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
	[addItemByGroupSwidth addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
	[readyMadeListShowSwidth addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];	
	[welcomePageShowSwidth addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [iCloudSwidth addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
	
	[importButton addTarget:self action:@selector(importDB:) forControlEvents:UIControlEventTouchUpInside];
	[exportButton addTarget:self action:@selector(exportDB:) forControlEvents:UIControlEventTouchUpInside];
	
	[btnSelectColorForTopBg addTarget:self action:@selector(selectColor:) forControlEvents:UIControlEventTouchUpInside];
	
	soundSwidth.on = mAppDelegate.soundOn;
    soundProgressSwitch.on = mAppDelegate.soundProgressOn;
	colorBGSwidth.on = mAppDelegate.colorBgOn;
	photoBGSwidth.on =  mAppDelegate.photoBgOn;
	addItemByGroupSwidth.on = [mAppDelegate.addItemsByGroup boolValue];
	readyMadeListShowSwidth.on = mAppDelegate.readyMadeListShow;
    iCloudSwidth.on=mAppDelegate.iCloudShow;
	welcomePageShowSwidth.on = [mAppDelegate.welcomePageShow boolValue];
	fontTypeLabel.text = mAppDelegate.fontType;
    
	NSString* title = [SettingsViewController titleForValue:mAppDelegate.taskSound forSection:K_SET_TASK_SOUND];
	taskSoundLabel.text = title ? title : [SettingsViewController defaultTitleForSection:K_SET_TASK_SOUND];
	[NSThread detachNewThreadSelector:@selector(stopSync) toTarget:self withObject:nil];
//	[self performSelectorOnMainThread:@selector(stopSync) withObject:nil waitUntilDone:YES];
//	[NSThread exit];
//	[mAppDelegate stopSynchronizing];
    
    
    
    // Fade mode with Labels
    
//    soundSwidth.thumbImage = [UIImage imageNamed:@"switchToggle"];
//    soundSwidth.trackMaskImage = [UIImage imageNamed:@"switchMask"];
//    soundSwidth.thumbHighlightImage = [UIImage imageNamed:@"switchToggleHigh"];
//    soundSwidth.trackImageOn = [UIImage imageNamed:@"switchGreen"];
//    soundSwidth.trackImageOff = [UIImage imageNamed:@"switchRed"];
//    soundSwidth.onString = @"ON";
//    soundSwidth.offString = @"OFF";
//    soundSwidth.onLabel.font = [UIFont boldSystemFontOfSize:11];
//    soundSwidth.offLabel.font = [UIFont boldSystemFontOfSize:11];
//    soundSwidth.onLabel.textColor = [UIColor whiteColor];
//    soundSwidth.offLabel.textColor = [UIColor whiteColor];
//    soundSwidth.onLabel.shadowColor = [UIColor colorWithRed:0.121569 green:0.600000 blue:0.454902 alpha:1.0];
//    soundSwidth.offLabel.shadowColor = [UIColor colorWithRed:0.796078 green:0.211765 blue:0.156863 alpha:1.0];
//    soundSwidth.onLabel.shadowOffset = CGSizeMake(0, 1.0);
//    soundSwidth.offLabel.shadowOffset = CGSizeMake(0, 1.0);
//    soundSwidth.labelsEdgeInsets = UIEdgeInsetsMake(1.0, 10.0, 1.0, 10.0);
//    soundSwidth.thumbInsetX = -3.0;
//    soundSwidth.thumbOffsetY = 0.0;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
}

#pragma mark - Change switch UI

- (void) customSwitch : (TTFadeSwitch *) switchName{
    switchName.thumbImage = [UIImage imageNamed:@"switchToggle"];
    switchName.trackMaskImage = [UIImage imageNamed:@"switchMask"];
    switchName.thumbHighlightImage = [UIImage imageNamed:@"switchToggleHigh"];
    switchName.trackImageOn = [UIImage imageNamed:@"switchGreen"];
    switchName.trackImageOff = [UIImage imageNamed:@"switchRed"];
    switchName.onString = @"ON";
    switchName.offString = @"OFF";
    switchName.onLabel.font = [UIFont boldSystemFontOfSize:11];
    switchName.offLabel.font = [UIFont boldSystemFontOfSize:11];
    switchName.onLabel.textColor = [UIColor whiteColor];
    switchName.offLabel.textColor = [UIColor whiteColor];
    switchName.onLabel.shadowColor = [UIColor colorWithRed:0.121569 green:0.600000 blue:0.454902 alpha:1.0];
    switchName.offLabel.shadowColor = [UIColor colorWithRed:0.796078 green:0.211765 blue:0.156863 alpha:1.0];
    switchName.onLabel.shadowOffset = CGSizeMake(0, 1.0);
    switchName.offLabel.shadowOffset = CGSizeMake(0, 1.0);
    switchName.labelsEdgeInsets = UIEdgeInsetsMake(1.0, 10.0, 1.0, 10.0);
    switchName.thumbInsetX = -3.0;
    switchName.thumbOffsetY = 0.0;
}

- (void)startSync{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];	
	[mAppDelegate startSynchronizing];
	[pool release];
}

- (void)stopSync{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];	
	[mAppDelegate stopSynchronizing];
	[pool release];	
}

- (void) valueChanged:(id) sender {
	NSLog(@"valueChanged");
}

- (IBAction) buttonAction:(id) sender {
	if ([sender tag]== 1) {
		[self.navigationController popViewControllerAnimated:YES];
	} else if ([sender tag]== 2) {
		// save settings
		mAppDelegate.soundOn = soundSwidth.on;
		mAppDelegate.soundProgressOn = soundProgressSwitch.on;        
		mAppDelegate.colorBgOn = colorBGSwidth.on;
		mAppDelegate.photoBgOn = photoBGSwidth.on;
		mAppDelegate.addItemsByGroup =[NSString stringWithFormat:@"%d",addItemByGroupSwidth.on];
		mAppDelegate.readyMadeListShow = readyMadeListShowSwidth.on;
        mAppDelegate.iCloudShow = iCloudSwidth.on;
		mAppDelegate.welcomePageShow = [NSString stringWithFormat:@"%d", welcomePageShowSwidth.on];
		mAppDelegate.fontType = fontTypeLabel.text;
		NSString* sound = [SettingsViewController valueForTitle:taskSoundLabel.text forSection:K_SET_TASK_SOUND];
		mAppDelegate.taskSound = sound ? sound : [SettingsViewController defaultValueForSection:K_SET_TASK_SOUND];
		[mAppDelegate prefetchSoundID:mAppDelegate.taskSound];
		
		NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
		[userSettings setBool:mAppDelegate.soundOn forKey:K_SET_SOUND];
		[userSettings setBool:mAppDelegate.soundProgressOn forKey:K_SET_PROGRESS_SOUND];        
		[userSettings setBool:mAppDelegate.colorBgOn forKey:K_SET_COLOR_BG];
		[userSettings setBool:mAppDelegate.photoBgOn forKey:K_SET_PHOTO_BG];
		[userSettings setObject:mAppDelegate.addItemsByGroup forKey:K_SET_ADDITEMSBYGROUP];
		[userSettings setBool:mAppDelegate.readyMadeListShow forKey:K_SET_READYMADELISTSHOW];
        [userSettings setBool:mAppDelegate.iCloudShow forKey:K_SET_ICLOUDSHOW];
		[userSettings setObject:mAppDelegate.welcomePageShow forKey:K_SET_WELCOMEPAGESHOW];
		[userSettings setObject:mAppDelegate.fontType forKey:K_SET_FONT_TYPE];
		[userSettings setObject:mAppDelegate.taskSound forKey:K_SET_TASK_SOUND];
		[userSettings synchronize];
		
		[[NSNotificationCenter defaultCenter] postNotificationName:NOTIFY_FONT_CHANGED object:self];
		
		[self.navigationController popViewControllerAnimated:YES];
	}
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            View_Login = loginView;
            View_reg = regView;
            View_import = ipadLogout_ImportView;
            View_export = ipadLogout_ExportView;
            [layoutManager translateToLandscapeForView:self.view withAnimation:NO];
            
        }else
        {
            View_Login = loginView1;
            View_reg = regView1;
            View_import = iphoneLogout_ImportView;
            View_export = iphoneLogout_ExportView;
            [layoutManager translateToPortraitForView:self.view withAnimation:NO];
        }
    }
    else
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            
            View_Login = loginView_PDL;
            View_reg = regView_PDL;
            View_import = ipadLogout_ImportView_L;
            View_export = ipadLogout_ExportView_L;
            [layoutManager translateToLandscapeForView:self.view withAnimation:NO];
            
        }else{
            View_Login = loginView_L;
            View_reg = regView_L;
            View_import = iphoneLogout_ImportView_L;
            View_export = iphoneLogout_ExportView_L;
            [layoutManager translateToLandscapeForView:self.view withAnimation:NO];
        }
    }

//    NSString *customFontName = [mAppDelegate getCustomFontName];
//	int fontSize = 14;
//	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
//		fontSize -= 8;
//	}
    NSLog(@"%@",mAppDelegate.fontType);UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
       
        self.navigationItem.backBarButtonItem.tintColor=[UIColor whiteColor];
        self.navigationItem.leftBarButtonItem.tintColor=[UIColor whiteColor];
        self.navigationItem.rightBarButtonItem.tintColor=[UIColor whiteColor];
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		
        self.navigationItem.backBarButtonItem.tintColor=theColour1;
        self.navigationItem.leftBarButtonItem.tintColor=theColour1;
        self.navigationItem.rightBarButtonItem.tintColor=theColour1;
	}


    fontTypeLabel.font=[UIFont fontWithName:fontTypeLabel.text size:16];


}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation) == UIInterfaceOrientationPortrait)
        {
            [iCloudSwidth setFrame:CGRectMake(600,5 , 70, 27)];
            [soundSwidth setFrame:CGRectMake(600,5 , 70, 27)];
            [soundProgressSwitch setFrame:CGRectMake(600,40 , 70, 27)];
            [colorBGSwidth setFrame:CGRectMake(600,5 , 70, 27)];
            [photoBGSwidth setFrame:CGRectMake(600,5 , 70, 27)];
            [addItemByGroupSwidth setFrame:CGRectMake(600,5 , 70, 27)];
            [readyMadeListShowSwidth setFrame:CGRectMake(600,5 , 70, 27)];
            [welcomePageShowSwidth setFrame:CGRectMake(600,5 , 70, 27)];
            [btnImport setFrame:CGRectMake(570.0, 4.0, btnImport.frame.size.width, btnImport.frame.size.height)];
            [btnExport setFrame:CGRectMake(660.0, 4.0, btnImport.frame.size.width, btnImport.frame.size.height)];
            
        }
        else
        {
            [iCloudSwidth setFrame:CGRectMake(850,5 , 70, 27)];
            [soundSwidth setFrame:CGRectMake(850,5 , 70, 27)];
            [soundProgressSwitch setFrame:CGRectMake(850,40 , 70, 27)];
            [colorBGSwidth setFrame:CGRectMake(850,5 , 70, 27)];
            [photoBGSwidth setFrame:CGRectMake(850,5 , 70, 27)];
            [addItemByGroupSwidth setFrame:CGRectMake(850,5 , 70, 27)];
            [readyMadeListShowSwidth setFrame:CGRectMake(850,5 , 70, 27)];
            [welcomePageShowSwidth setFrame:CGRectMake(850,5 , 70, 27)];
            [btnImport setFrame:CGRectMake(820.0, 4.0, btnImport.frame.size.width, btnImport.frame.size.height)];
            [btnExport setFrame:CGRectMake(910.0, 4.0, btnImport.frame.size.width, btnImport.frame.size.height)];
        }
    }
}

/*
- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (IBAction) onMinusClicked:(id)sender{
	NSLog(@"NUMBEAlkhf lhaslf a ===== %d",[lblTextSize.text intValue]);
	if ([lblTextSize.text intValue]-1>0) {
		int i=[lblTextSize.text intValue];
		i--;
		lblTextSize.text=[NSString stringWithFormat:@"%d",i];		
	}
}

- (IBAction) onPlusClicked:(id)sender{
	if ([lblTextSize.text intValue]<30) {
		int i=[lblTextSize.text intValue];
		i++;
		lblTextSize.text=[NSString stringWithFormat:@"%d",i];			
	}
}

- (IBAction) colorButtonPressed:(id)sender{
	if ([selectColorView superview]) {
		[selectColorView removeFromSuperview];
	}
	
	if (colorSelectIndex==1) {
		UIButton *btn=(UIButton *)sender;
		
		UIColor *selectedColor = btn.backgroundColor;
		
		NSData *data = [NSKeyedArchiver archivedDataWithRootObject: selectedColor];	
		
		// write the data into the user defaults
		[[NSUserDefaults standardUserDefaults] setObject: data forKey: K_TOP_BAR_COLOR];
		
		//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
		UIColor *theColour;
		// read the data back from the user defaults
		NSData *data1= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_BAR_COLOR];
		// check whether you got anything
		if(data1 == nil) {
			// use this to set the colour the first time your app runs
		} else {
			// this recreates the colour you saved
//			theColour = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data1];
//			self.navigationController.navigationBar.tintColor = theColour;
		}
		// and finally set the colour of your label		
	}
	else {
		UIButton *btn=(UIButton *)sender;
		
		UIColor *selectedColor = btn.backgroundColor;
		
		NSData *data = [NSKeyedArchiver archivedDataWithRootObject: selectedColor];	
		
		// write the data into the user defaults
		[[NSUserDefaults standardUserDefaults] setObject: data forKey: K_TOP_TEXT_COLOR];
		
		//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
		UIColor *theColour;
		// read the data back from the user defaults
		NSData *data1= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
		// check whether you got anything
		if(data1 == nil) {
			// use this to set the colour the first time your app runs
		} else {
			// this recreates the colour you saved
		}
		// and finally set the colour of your label		
	}

	
}
- (IBAction) selectColor:(id)sender{
	if ([sender tag]==1) {
		colorSelectIndex=1;
	}
	else {
		colorSelectIndex=2;		
	}

	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
		[UIView beginAnimations:nil context:nil];
		[selectColorView setFrame:CGRectMake(0, 768, 1024, 768)];
		[self.view addSubview:selectColorView];
		
		[UIView setAnimationDuration:0.75f];
		[selectColorView setFrame:CGRectMake(0, 0, 768, 1024)];
		[UIView commitAnimations];	
	}
	else {
	[UIView beginAnimations:nil context:nil];
	[selectColorView setFrame:CGRectMake(0, 480, 320, 570)];
	[self.view addSubview:selectColorView];
		
	
	[UIView setAnimationDuration:0.75f];
	[selectColorView setFrame:CGRectMake(0, 0, 320, 570)];
	[UIView commitAnimations];	
	}
}

- (IBAction) importDB:(id) sender{
    
    View_reg.backgroundColor=[UIColor lightGrayColor];
    View_Login.backgroundColor=[UIColor lightGrayColor];
    View_import.backgroundColor=[UIColor lightGrayColor];
    
	NSLog(@"IMPORT");
	import=YES;
    
		if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"USERNAME"] isEqualToString:@""] || [[NSUserDefaults standardUserDefaults] objectForKey:@"USERNAME"] == nil) {
              
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
                {
                    View_Login = loginView;
                    View_reg = regView;
                }
                else
                {
                    View_Login = loginView_PDL;
                    View_reg = regView_PDL;
                   
                }


				if ([View_Login superview]) {
					[View_Login removeFromSuperview];
				}
					[self.view addSubview:View_reg];
                
			}
			else {
                if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
                {
                    View_Login = loginView1;
                    View_reg = regView1;
                }
                else
                {
                    View_Login = loginView_L;
                    View_reg = regView_L;
                    
                }

				if ([View_Login superview]) {
					[View_Login removeFromSuperview];
				}
                [self.view addSubview:View_reg];
			}		
		}
	else {
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
			if (![ipadLogout_ImportView superview]) {
                if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
                {
                    View_import = ipadLogout_ImportView;
                }
                else
                {
                    View_import = ipadLogout_ImportView_L;
                                       
                }

				[self.view addSubview:View_import];
			}
		}
		else {
            if (![iphoneLogout_ImportView superview]) {
                if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
                {
                    View_import = iphoneLogout_ImportView;
                }
                else
                {
                    View_import = iphoneLogout_ImportView_L;
                }

                [self.view addSubview:View_import];
			}			
		}
	}
}

- (IBAction) logoutClicked:(id)sender{
	[[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"USERNAME"];
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
		if ([View_import superview]) {
			[View_import removeFromSuperview];
		}
		if ([View_export superview]) {
			[View_export removeFromSuperview];
		}
	}
	else {
		if ([View_import superview]) {
			[View_import removeFromSuperview];
		}
		if ([View_export superview]) {
			[View_export removeFromSuperview];
		}
	}
	UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"You are successfully logged out of the system." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
	[alert release];
}

- (IBAction)cancelImportExport:(id)sender{
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
		if ([View_import superview]) {
			[View_import removeFromSuperview];
		}
		if ([View_export superview]) {
			[View_export removeFromSuperview];
		}
	}
	else {
		if ([View_import superview]) {
			[View_import removeFromSuperview];
		}
		if ([View_export superview]) {
			[View_export removeFromSuperview];
		}
	}	
}

- (IBAction) import:(id)sender{
    
//IMPORT WEB SERVICE CALLED
	[NSThread detachNewThreadSelector:@selector(startSync) toTarget:self withObject:nil];
	
	if (TARGET_VERSION == VERSION_BABY_PACKING) {
		//[mAppDelegate startSynchronizing];
		ImportParser *importParser=[[ImportParser alloc] init];
		[importParser initWithDBName:@"1"];
		[importParser release];		
		//[mAppDelegate stopSynchronizing];
		[NSThread detachNewThreadSelector:@selector(stopSync) toTarget:self withObject:nil];
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"IMPORT" message:@"Successful Import" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];			
	}
	else if (TARGET_VERSION == VERSION_FAMILY_PACKING) {
		
		/*ImportParser *importParser=[[ImportParser alloc] init];
		[importParser initWithDBName:@"1"];
		[importParser release];*/
	        [self ImList];
		[NSThread detachNewThreadSelector:@selector(stopSync) toTarget:self withObject:nil];
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"IMPORT" message:@"Successful Import" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];

	}
	else {
		//[mAppDelegate startSynchronizing];
		ImportParser *importParser=[[ImportParser alloc] init];
		[importParser initWithDBName:@"1"];
		[importParser release];
		//[mAppDelegate stopSynchronizing];
		NSMutableArray *babyListArray=[[NSMutableArray alloc] initWithArray:[PackingList findAllWithoutPhoto]];
		[NSThread detachNewThreadSelector:@selector(stopSync) toTarget:self withObject:nil];
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"IMPORT" message:@"Successful Import" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];			
	}	
//IMPORT OVER	
}

- (IBAction) export:(id)sender{
//EXPORT WEB SERVICE CALLED
	[NSThread detachNewThreadSelector:@selector(startSync) toTarget:self withObject:nil];
	if (TARGET_VERSION == VERSION_BABY_PACKING) {
		NSLog(@"EXPORT WEB SERVICE");
		ExportParser *exportData=[[ExportParser alloc] init];
		[exportData initWithDBName:@"1" withXMLString:babyXMLString];
		[exportData release];			
	}
	else if (TARGET_VERSION == VERSION_FAMILY_PACKING) {
//		ExportParser *exportData=[[ExportParser alloc] init];
//		[exportData initWithDBName:@"1" withXMLString:packingXMLString];
//		[exportData release];
        [self ExList];
	}
	else {
		ExportParser *exportData=[[ExportParser alloc] init];
		[exportData initWithDBName:@"1" withXMLString:packingXMLString];
		[exportData release];			
	}
	
	NSMutableArray *babyListArray=[[NSMutableArray alloc] initWithArray:[PackingList findAllWithoutPhoto]];
	for (PackingList *list in babyListArray) {
		if (list.icon!=nil) {
			NSData* pictureData = UIImagePNGRepresentation(list.icon);			
			SetImageParser *setImage=[[SetImageParser alloc] init];
			[setImage initWithData:[NSString stringWithFormat:@"%d",list.pk] withUserId:[[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"] withData:pictureData];
			[setImage release];
		}
	}	
	
	[NSThread detachNewThreadSelector:@selector(stopSync) toTarget:self withObject:nil];
	
	UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"EXPORT" message:@"Successful Export" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
	[alert release];
//EXPORT OVER		
}


- (IBAction) exportDB:(id) sender{
    
    View_reg.backgroundColor=[UIColor lightGrayColor];
    View_Login.backgroundColor=[UIColor lightGrayColor];
    View_export.backgroundColor=[UIColor lightGrayColor];
    
	NSLog(@"EXPORT");
	import=NO;
	if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"USERNAME"] isEqualToString:@""] || [[NSUserDefaults standardUserDefaults] objectForKey:@"USERNAME"] == nil) {
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            
            if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
            {
                View_Login = loginView;
                View_reg = regView;
            }
            else
            {
                View_Login = loginView_PDL;
                View_reg = regView_PDL;
            }

			if ([View_Login superview]) {
				[View_Login removeFromSuperview];
			}
			[self.view addSubview:View_reg];
		}
		else {
            if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
            {
                View_Login = loginView1;
                View_reg = regView1;
            }
            else
            {
                View_Login = loginView_L;
                View_reg = regView_L;
            }

			if ([View_Login superview]) {
				[View_Login removeFromSuperview];
			}
			[self.view addSubview:View_reg];
		}		
		
	}
	else {
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
            {
                View_export = ipadLogout_ExportView;
            }
            else
            {
                View_export = ipadLogout_ExportView_L;
            }

			if (![ipadLogout_ExportView superview]) {
				[self.view addSubview:View_export];
			}
		}
		else {
            if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
            {
                View_export = iphoneLogout_ExportView;
            }
            else
            {
                View_export = iphoneLogout_ExportView_L;
            }

			if (![iphoneLogout_ExportView superview]) {
				[self.view addSubview:View_export];
			}			
		}		
	}
}

- (IBAction) loginClicked:(id) sender{
     [self.tableView setScrollEnabled:YES];
	[txtPasswordLogin resignFirstResponder];
    [txtPasswordLogin_PDL resignFirstResponder];
	[txtPasswordLogin1 resignFirstResponder];
    [txtPasswordLogin_L resignFirstResponder];
	[txtPasswordReg resignFirstResponder];
    [txtPasswordReg_PDL resignFirstResponder];
	[txtPasswordReg1 resignFirstResponder];
    [txtPasswordReg1 resignFirstResponder];
	[txtUsernameLogin resignFirstResponder];
    [txtUsernameLogin_PDL resignFirstResponder];
	[txtUsernameLogin1 resignFirstResponder];
    [txtUsernameLogin_L resignFirstResponder];
	[txtUsernameReg resignFirstResponder];
    [txtUsernameReg_PDL resignFirstResponder];
	[txtUsernameReg1 resignFirstResponder];
    [txtUsernameReg_L resignFirstResponder];
	[txtEmailReg resignFirstResponder];
    [txtEmailReg_PDL resignFirstResponder];
	[txtEmailReg1 resignFirstResponder];
    [txtEmailReg_L resignFirstResponder];
	
//http://192.168.0.7/packngohelp/ws/user_login.php
//	Input - username, password, userType (1=>packing, 2=>baby), act (export)
//	
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            txtUserName_Login = txtUsernameLogin;
            txtPassword_Login = txtPasswordLogin;
        }
        else{
            txtUserName_Login = txtUsernameLogin1;
            txtPassword_Login = txtPasswordLogin1;
        }
    }
    else
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            txtUserName_Login = txtUsernameLogin_PDL;
            txtPassword_Login = txtPasswordLogin_PDL;
        }
        else{
            txtUserName_Login = txtUsernameLogin_L;
            txtPassword_Login = txtPasswordLogin_L;
        }
    }

	NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
	if ( [txtUserName_Login.text isEqualToString:@""] || [txtPassword_Login.text isEqualToString:@""] ) {
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Import/Export" message:@"Field Empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
	else {
		[NSThread detachNewThreadSelector:@selector(startSync) toTarget:self withObject:nil];
		if (TARGET_VERSION == VERSION_BABY_PACKING) {
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
				LoginParser *parsee=[[LoginParser alloc] init];
				[parsee initWithData:txtUserName_Login.text withAge:txtPassword_Login.text withtype:@"1" withAct:@""];
				[parsee release];
			}
			else {
				LoginParser *parsee=[[LoginParser alloc] init];
				[parsee initWithData:txtUserName_Login.text withAge:txtPassword_Login.text withtype:@"1" withAct:@""];
				[parsee release];
			}
		}
		else if (TARGET_VERSION == VERSION_FAMILY_PACKING) {		
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
				LoginParser *parsee=[[LoginParser alloc] init];
				[parsee initWithData:txtUserName_Login.text withAge:txtPassword_Login.text withtype:@"1" withAct:@""];
				[parsee release];
			}
			else {
				LoginParser *parsee=[[LoginParser alloc] init];
				[parsee initWithData:txtUserName_Login.text withAge:txtPassword_Login.text withtype:@"1" withAct:@""];
				[parsee release];
			}
		}
		else if (TARGET_VERSION == VERSION_YOU_PACKING) {		
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
				NSLog(@"appDELEGEATE STATUS = %d",mAppDelegate.loginStatus);
				LoginParser *parsee=[[LoginParser alloc] init];
				[parsee initWithData:txtUserName_Login.text withAge:txtUserName_Login.text withtype:@"1" withAct:@""];
				[parsee release];
				NSLog(@"appDELEGEATE STATUS = %d",mAppDelegate.loginStatus);
				
			}
			else {
				NSLog(@"appDELEGEATE STATUS = %d",mAppDelegate.loginStatus);
				LoginParser *parsee=[[LoginParser alloc] init];
				[parsee initWithData:txtUserName_Login.text withAge:txtPassword_Login.text withtype:@"1" withAct:@""];
				[parsee release];
				NSLog(@"appDELEGEATE STATUS = %d",mAppDelegate.loginStatus);
			}
		}
		if (mAppDelegate.loginStatus!=0) {
			//[NSThread detachNewThreadSelector:@selector(startSync) toTarget:self withObject:nil];
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
				[defaults setObject:[NSString stringWithFormat:@"%d",mAppDelegate.userID] forKey:@"USERID"];
				[defaults setObject:txtUserName_Login.text forKey:@"USERNAME"];
				[defaults setObject:txtPassword_Login.text forKey:@"PASSWORD"];
			}
			else {
				[defaults setObject:[NSString stringWithFormat:@"%d",mAppDelegate.userID] forKey:@"USERID"];
				[defaults setObject:txtUserName_Login.text forKey:@"USERNAME"];
				[defaults setObject:txtPassword_Login.text forKey:@"PASSWORD"];
			}

			if (TARGET_VERSION == VERSION_BABY_PACKING) {
				if (import) {
					//[mAppDelegate startSynchronizing];
					ImportParser *importParser=[[ImportParser alloc] init];
					[importParser initWithDBName:@"1"];
					[importParser release];
					//[mAppDelegate stopSynchronizing];
				}
				else {
					ExportParser *exportData=[[ExportParser alloc] init];
					[exportData initWithDBName:@"1" withXMLString:babyXMLString];
					[exportData release];
					NSMutableArray *babyListArray=[[NSMutableArray alloc] initWithArray:[PackingList findAllWithoutPhoto]];
					for (PackingList *list in babyListArray) {
						if (list.icon!=nil) {
							NSData* pictureData = UIImagePNGRepresentation(list.icon);			
							SetImageParser *setImage=[[SetImageParser alloc] init];
							[setImage initWithData:[NSString stringWithFormat:@"%d",list.pk] withUserId:[[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"] withData:pictureData];
							[setImage release];
						}
					}	
					
				}
			}
			else if (TARGET_VERSION == VERSION_FAMILY_PACKING) {
				if (import) {
					/*ImportParser *importParser=[[ImportParser alloc] init];
					[importParser initWithDBName:@"1"];
					[importParser release];*/
                    [self ImList];
				}
				else {
					/*ExportParser *exportData=[[ExportParser alloc] init];
					[exportData initWithDBName:@"1" withXMLString:packingXMLString];
					[exportData release];*/
                    [self ExList];
					NSMutableArray *babyListArray=[[NSMutableArray alloc] initWithArray:[PackingList findAllWithoutPhoto]];
					for (PackingList *list in babyListArray) {
						if (list.icon!=nil) {
							NSData* pictureData = UIImagePNGRepresentation(list.icon);			
							SetImageParser *setImage=[[SetImageParser alloc] init];
							[setImage initWithData:[NSString stringWithFormat:@"%d",list.pk] withUserId:[[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"] withData:pictureData];
							[setImage release];
						}
					}	
					
				}				
			}
			else {
				if (import) {
					//[mAppDelegate startSynchronizing];
					ImportParser *importParser=[[ImportParser alloc] init];
					[importParser initWithDBName:@"1"];
					[importParser release];
					//[mAppDelegate stopSynchronizing];
				}
				else {
					ExportParser *exportData=[[ExportParser alloc] init];
					[exportData initWithDBName:@"1" withXMLString:packingXMLString];
					[exportData release];	
					NSMutableArray *babyListArray=[[NSMutableArray alloc] initWithArray:[PackingList findAllWithoutPhoto]];
					for (PackingList *list in babyListArray) {
						if (list.icon!=nil) {
							NSData* pictureData = UIImagePNGRepresentation(list.icon);			
							SetImageParser *setImage=[[SetImageParser alloc] init];
							[setImage initWithData:[NSString stringWithFormat:@"%d",list.pk] withUserId:[[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"] withData:pictureData];
							[setImage release];
						}
					}	
					
				}								
			}

			[NSThread detachNewThreadSelector:@selector(stopSync) toTarget:self withObject:nil];
			if (import) {
				UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"IMPORT" message:@"Successful Import" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
				[alert show];
				[alert release];				
			}
			else {
				UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"EXPORT" message:@"Successful Export" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
				[alert show];
				[alert release];				
			}

		}	
		else {
			[NSThread detachNewThreadSelector:@selector(stopSync) toTarget:self withObject:nil];
			UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Login" message:@"Invalid login credentials" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
			[alert release];							
		}

		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
			if ([View_Login superview]) {
				[View_Login removeFromSuperview];
			}
		}
		else {
			if ([View_Login superview]) {
				[View_Login removeFromSuperview];
			}		
		}		
	}
}

- (IBAction) getRegisteredClicked:(id) sender{
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
		if ([View_reg superview]) {
			[View_reg removeFromSuperview];
            if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
            {
                View_Login = loginView;
            }
            else
            {
                View_Login = loginView_PDL;
            }
            

			[self.view addSubview:View_Login];
		}
	}
	else {
		if ([View_reg superview]) {
			[View_reg removeFromSuperview];
            if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
            {
                View_Login = loginView1;
            }
            else
            {
                View_Login = loginView_L;
            }

			[self.view addSubview:View_Login];
		}		
	}		
}

- (IBAction) submitClicked:(id) sender{
	[txtPasswordLogin resignFirstResponder];
    [txtPasswordLogin_PDL resignFirstResponder];
	[txtPasswordLogin1 resignFirstResponder];
    [txtPasswordLogin_L resignFirstResponder];
	[txtPasswordReg resignFirstResponder];
    [txtPasswordReg_PDL resignFirstResponder];
	[txtPasswordReg1 resignFirstResponder];
    [txtPasswordReg_L resignFirstResponder];
	[txtUsernameLogin resignFirstResponder];
    [txtUsernameLogin_PDL resignFirstResponder];
	[txtUsernameLogin1 resignFirstResponder];
    [txtUsernameLogin_L resignFirstResponder];
	[txtUsernameReg resignFirstResponder];
    [txtUsernameReg_PDL resignFirstResponder];
	[txtUsernameReg1 resignFirstResponder];
    [txtUsernameReg_L resignFirstResponder];
	[txtEmailReg resignFirstResponder];
    [txtEmailReg_PDL resignFirstResponder];
	[txtEmailReg1 resignFirstResponder];
    [txtEmailReg_L resignFirstResponder];
    
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            txtUserName_Reg = txtUsernameReg;
            txtPassword_Reg = txtPasswordReg;
            txtEmail_Reg = txtEmailReg;
        }
        else{
        txtUserName_Reg = txtUsernameReg1;
        txtPassword_Reg = txtPasswordReg1;
        txtEmail_Reg = txtEmailReg1;
        }
    }
    else
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            txtUserName_Reg = txtUsernameReg_PDL;
            txtPassword_Reg = txtPasswordReg_PDL;
            txtEmail_Reg = txtEmailReg_PDL;
        }
        else{
        txtUserName_Reg = txtUsernameReg_L;
        txtPassword_Reg = txtPasswordReg_L;
        txtEmail_Reg = txtEmailReg_L;
        }
    }

	
	//http://192.168.0.7/packngohelp/ws/user_registration.php
//	Input - username, password, type (1=>packing, 2=>baby)
//Output  - id, username, password
	if ([txtEmail_Reg.text isEqualToString:@""] || [txtUserName_Reg.text isEqualToString:@""] || [txtPassword_Reg.text isEqualToString:@""]) {
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Import/Export" message:@"Field Empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
	else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
		if ([txtPassword_Reg.text length]<6) {
			UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Import/Export" message:@"Pasword should be of atleast 6 characters" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
			[alert release];					
		}
		else if (![self validateEmail:txtEmail_Reg.text]) {
			UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Import/Export" message:@"Enter a valid Email-Id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
			[alert release];		
		}
		else {
			[NSThread detachNewThreadSelector:@selector(startSync) toTarget:self withObject:nil];		
			NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
			if (TARGET_VERSION == VERSION_BABY_PACKING) {			
				if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
					RegistrationParser *parser=[[RegistrationParser alloc] init];
					[parser initWithData:txtUserName_Reg.text withPassword:txtPassword_Reg.text withtype:@"1" withEmail:txtEmailReg.text];
					[parser release];
				}
				else {
					RegistrationParser *parser=[[RegistrationParser alloc] init];
					[parser initWithData:txtUserName_Reg.text withPassword:txtPassword_Reg.text withtype:@"1" withEmail:txtEmail_Reg.text];
					[parser release];				
				}
			}
			else if (TARGET_VERSION == VERSION_FAMILY_PACKING) {		
				if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
					RegistrationParser *parser=[[RegistrationParser alloc] init];
					[parser initWithData:txtUserName_Reg.text withPassword:txtPassword_Reg.text withtype:@"1" withEmail:txtEmailReg.text];
					[parser release];
				}
				else {
					RegistrationParser *parser=[[RegistrationParser alloc] init];
					[parser initWithData:txtUserName_Reg.text withPassword:txtPassword_Reg.text withtype:@"1" withEmail:txtEmail_Reg.text];
					[parser release];
				}
			}
			else if (TARGET_VERSION == VERSION_YOU_PACKING) {
				if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
					RegistrationParser *parser=[[RegistrationParser alloc] init];
					[parser initWithData:txtUserName_Reg.text withPassword:txtPassword_Reg.text withtype:@"1" withEmail:txtEmailReg.text];
					[parser release];
				}
				else {
					RegistrationParser *parser=[[RegistrationParser alloc] init];
					//[parser initWithData:txtUsernameReg1.text withPassword:txtPasswordReg1.text withtype:@"1" withEmail:txtEmailReg1.text];
					[parser initWithData:txtUserName_Reg.text withPassword:txtPassword_Reg.text withtype:@"1" withEmail:txtEmail_Reg.text];
					[parser release];
				}		
			}
			if (mAppDelegate.loginStatus!=0) {
				//			[NSThread detachNewThreadSelector:@selector(startSync) toTarget:self withObject:nil];
				
				if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
					[defaults setObject:[NSString stringWithFormat:@"%d",mAppDelegate.userID] forKey:@"USERID"];
					[defaults setObject:txtUserName_Reg.text forKey:@"USERNAME"];
					[defaults setObject:txtPassword_Reg.text forKey:@"PASSWORD"];
					[defaults setObject:txtEmail_Reg.text forKey:@"EMAIL"];
				}
				else {
					[defaults setObject:[NSString stringWithFormat:@"%d",mAppDelegate.userID] forKey:@"USERID"];
					[defaults setObject:txtUserName_Reg.text forKey:@"USERNAME"];
					[defaults setObject:txtPassword_Reg.text forKey:@"PASSWORD"];
					[defaults setObject:txtEmail_Reg.text forKey:@"EMAIL"];
				}
				if (TARGET_VERSION == VERSION_BABY_PACKING) {
					if (import) {
						//	[mAppDelegate startSynchronizing];
						ImportParser *importParser=[[ImportParser alloc] init];
						[importParser initWithDBName:@"1"];
						//	[importParser release];
						//[mAppDelegate stopSynchronizing];
					}
					else {
						ExportParser *exportData=[[ExportParser alloc] init];
						[exportData initWithDBName:@"1" withXMLString:babyXMLString];
						[exportData release];	
						NSMutableArray *babyListArray=[[NSMutableArray alloc] initWithArray:[PackingList findAllWithoutPhoto]];
						for (PackingList *list in babyListArray) {
							if (list.icon!=nil) {
								NSData* pictureData = UIImagePNGRepresentation(list.icon);			
								SetImageParser *setImage=[[SetImageParser alloc] init];
								[setImage initWithData:[NSString stringWithFormat:@"%d",list.pk] withUserId:[[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"] withData:pictureData];
								[setImage release];
							}
						}	
						
					}
				}
				else if (TARGET_VERSION == VERSION_FAMILY_PACKING) {
					if (import) {
						/*ImportParser *importParser=[[ImportParser alloc] init];
						[importParser initWithDBName:@"1"];
						[importParser release];*/
                        [self ImList];
					}
					else {
						/*ExportParser *exportData=[[ExportParser alloc] init];
						[exportData initWithDBName:@"1" withXMLString:packingXMLString];
						[exportData release];	*/
                        [self ExList];
						NSMutableArray *babyListArray=[[NSMutableArray alloc] initWithArray:[PackingList findAllWithoutPhoto]];
						for (PackingList *list in babyListArray) {
							if (list.icon!=nil) {
								NSData* pictureData = UIImagePNGRepresentation(list.icon);			
								SetImageParser *setImage=[[SetImageParser alloc] init];
								[setImage initWithData:[NSString stringWithFormat:@"%d",list.pk] withUserId:[[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"] withData:pictureData];
								[setImage release];
							}
						}	
						
					}				
				}
				else {
					if (import) {
						//	[mAppDelegate startSynchronizing];
						ImportParser *importParser=[[ImportParser alloc] init];
						[importParser initWithDBName:@"1"];
						[importParser release];
						//	[mAppDelegate stopSynchronizing];
					}
					else {
						ExportParser *exportData=[[ExportParser alloc] init];
						[exportData initWithDBName:@"1" withXMLString:packingXMLString];
						[exportData release];	
						NSMutableArray *babyListArray=[[NSMutableArray alloc] initWithArray:[PackingList findAllWithoutPhoto]];
						for (PackingList *list in babyListArray) {
							if (list.icon!=nil) {
								NSData* pictureData = UIImagePNGRepresentation(list.icon);			
								SetImageParser *setImage=[[SetImageParser alloc] init];
								[setImage initWithData:[NSString stringWithFormat:@"%d",list.pk] withUserId:[[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"] withData:pictureData];
								[setImage release];
							}
						}	
						
					}				
				}
				[NSThread detachNewThreadSelector:@selector(stopSync) toTarget:self withObject:nil];
				if (import) {
					UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"IMPORT" message:@"Successful Import" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
					[alert show];
					[alert release];
				}
				else {
					UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"EXPORT" message:@"Successful Export" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
					[alert show];
					[alert release];				
				}
				
			}
			else {
				[NSThread detachNewThreadSelector:@selector(stopSync) toTarget:self withObject:nil];
				UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Registration" message:@"User Already Exist" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
				[alert show];
				[alert release];							
			}
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
				if ([View_reg superview]) {
					[View_reg removeFromSuperview];
				}
			}
			else {
				if ([View_reg superview]) {
					[View_reg removeFromSuperview];
				}		
			}		
		}
	}
	else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
		if ([txtPassword_Reg.text length]<6) {
			UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Import/Export" message:@"Pasword should be of atleast 6 characters" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
			[alert release];					
		}		
		else if (![self validateEmail:txtEmail_Reg.text]) {
			UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Import/Export" message:@"Enter a valid Email-Id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
			[alert release];		
		}
		else {
			[NSThread detachNewThreadSelector:@selector(startSync) toTarget:self withObject:nil];		
			NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
			if (TARGET_VERSION == VERSION_BABY_PACKING) {			
				if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
					RegistrationParser *parser=[[RegistrationParser alloc] init];
					[parser initWithData:txtUserName_Reg.text withPassword:txtPassword_Reg.text withtype:@"1" withEmail:txtEmail_Reg.text];
					[parser release];
				}
				else {
					RegistrationParser *parser=[[RegistrationParser alloc] init];
					[parser initWithData:txtUserName_Reg.text withPassword:txtPassword_Reg.text withtype:@"1" withEmail:txtEmail_Reg.text];
					[parser release];				
				}
			}
			else if (TARGET_VERSION == VERSION_FAMILY_PACKING) {		
				if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
					RegistrationParser *parser=[[RegistrationParser alloc] init];
					[parser initWithData:txtUserName_Reg.text withPassword:txtPassword_Reg.text withtype:@"1" withEmail:txtEmail_Reg.text];
					[parser release];
				}
				else {
					RegistrationParser *parser=[[RegistrationParser alloc] init];
					[parser initWithData:txtUserName_Reg.text withPassword:txtPassword_Reg.text withtype:@"1" withEmail:txtEmail_Reg.text];
					[parser release];
				}
			}
			else if (TARGET_VERSION == VERSION_YOU_PACKING) {
				if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
					RegistrationParser *parser=[[RegistrationParser alloc] init];
					[parser initWithData:txtUserName_Reg.text withPassword:txtPassword_Reg.text withtype:@"1" withEmail:txtEmail_Reg.text];
					[parser release];
				}
				else {
					RegistrationParser *parser=[[RegistrationParser alloc] init];
					//[parser initWithData:txtUsernameReg1.text withPassword:txtPasswordReg1.text withtype:@"1" withEmail:txtEmailReg1.text];
					[parser initWithData:txtUserName_Reg.text withPassword:txtPassword_Reg.text withtype:@"1" withEmail:txtEmail_Reg.text];
					[parser release];
				}		
			}
			if (mAppDelegate.loginStatus!=0) {
				//			[NSThread detachNewThreadSelector:@selector(startSync) toTarget:self withObject:nil];
				
				if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
					[defaults setObject:[NSString stringWithFormat:@"%d",mAppDelegate.userID] forKey:@"USERID"];
					[defaults setObject:txtUserName_Reg.text forKey:@"USERNAME"];
					[defaults setObject:txtPassword_Reg.text forKey:@"PASSWORD"];
					[defaults setObject:txtPassword_Reg.text forKey:@"EMAIL"];				
				}
				else {
					[defaults setObject:[NSString stringWithFormat:@"%d",mAppDelegate.userID] forKey:@"USERID"];
					[defaults setObject:txtUserName_Reg.text forKey:@"USERNAME"];
					[defaults setObject:txtPassword_Reg.text forKey:@"PASSWORD"];
					[defaults setObject:txtEmail_Reg.text forKey:@"EMAIL"];
				}
				if (TARGET_VERSION == VERSION_BABY_PACKING) {
					if (import) {
						//	[mAppDelegate startSynchronizing];
						ImportParser *importParser=[[ImportParser alloc] init];
						[importParser initWithDBName:@"1"];
						//	[importParser release];
						//[mAppDelegate stopSynchronizing];
					}
					else {
						ExportParser *exportData=[[ExportParser alloc] init];
						[exportData initWithDBName:@"1" withXMLString:babyXMLString];
						[exportData release];
						NSMutableArray *babyListArray=[[NSMutableArray alloc] initWithArray:[PackingList findAllWithoutPhoto]];
						for (PackingList *list in babyListArray) {
							if (list.icon!=nil) {
								NSData* pictureData = UIImagePNGRepresentation(list.icon);			
								SetImageParser *setImage=[[SetImageParser alloc] init];
								[setImage initWithData:[NSString stringWithFormat:@"%d",list.pk] withUserId:[[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"] withData:pictureData];
								[setImage release];
							}
						}	
						
					}
				}
				else if (TARGET_VERSION == VERSION_FAMILY_PACKING) {
					if (import) {
						/*ImportParser *importParser=[[ImportParser alloc] init];
						[importParser initWithDBName:@"1"];
						[importParser release];*/
                        [self ImList];
					}
					else {
						/*ExportParser *exportData=[[ExportParser alloc] init];
						[exportData initWithDBName:@"1" withXMLString:packingXMLString];
						[exportData release];*/
                        [self ExList];
						NSMutableArray *babyListArray=[[NSMutableArray alloc] initWithArray:[PackingList findAllWithoutPhoto]];
						for (PackingList *list in babyListArray) {
							if (list.icon!=nil) {
								NSData* pictureData = UIImagePNGRepresentation(list.icon);			
								SetImageParser *setImage=[[SetImageParser alloc] init];
								[setImage initWithData:[NSString stringWithFormat:@"%d",list.pk] withUserId:[[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"] withData:pictureData];
								[setImage release];
							}
						}	
						
					}				
				}
				else {
					if (import) {
						//	[mAppDelegate startSynchronizing];
						ImportParser *importParser=[[ImportParser alloc] init];
						[importParser initWithDBName:@"1"];
						[importParser release];
						//	[mAppDelegate stopSynchronizing];
					}
					else {
						ExportParser *exportData=[[ExportParser alloc] init];
						[exportData initWithDBName:@"1" withXMLString:packingXMLString];
						[exportData release];
						NSMutableArray *babyListArray=[[NSMutableArray alloc] initWithArray:[PackingList findAllWithoutPhoto]];
						for (PackingList *list in babyListArray) {
							if (list.icon!=nil) {
								NSData* pictureData = UIImagePNGRepresentation(list.icon);			
								SetImageParser *setImage=[[SetImageParser alloc] init];
								[setImage initWithData:[NSString stringWithFormat:@"%d",list.pk] withUserId:[[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"] withData:pictureData];
								[setImage release];
							}
						}	
						
					}				
				}
				[NSThread detachNewThreadSelector:@selector(stopSync) toTarget:self withObject:nil];
				if (import) {
					UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"IMPORT" message:@"Successful Import" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
					[alert show];
					[alert release];
				}
				else {
					UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"EXPORT" message:@"Successful Export" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
					[alert show];
					[alert release];				
				}
				
			}
			else {
				[NSThread detachNewThreadSelector:@selector(stopSync) toTarget:self withObject:nil];
				UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Registration" message:@"User Already Exist" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
				[alert show];
				[alert release];							
			}
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
				if ([View_reg superview]) {
					[View_reg removeFromSuperview];
				}
			}
			else {
				if ([View_reg superview]) {
					[View_reg removeFromSuperview];
				}		
			}		
		}

	}
}

- (BOOL) validateEmail: (NSString *) candidate {
	NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"; 
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; 
	
	return [emailTest evaluateWithObject:candidate];
}


- (IBAction) cancelClicked:(id) sender{
     [self.tableView setScrollEnabled:YES];
	if ([loginView superview]) {
		[loginView removeFromSuperview];
	}
	if ([View_Login superview]) {
		[View_Login removeFromSuperview];
	}
	if ([View_reg superview]) {
		[View_reg removeFromSuperview];
	}	
	if ([regView superview]) {
		[regView removeFromSuperview];
	}	
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
   
  	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

	if (TARGET_VERSION == VERSION_FAMILY_PACKING)
		return 10;
	else if(TARGET_VERSION == VERSION_YOU_PACKING)
		return 8;
	else
		return 7;
}


// Customize the appearance of table view cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    
    for (UIView *view in [cell.contentView subviews]) {
        [view removeFromSuperview];
    }
    [cell setAccessoryView:UITableViewCellAccessoryNone];
    
    [cell.textLabel setText:@""];
    
		count++;
		NSLog(@"NIL ------------------------------------------------- %d",indexPath.row);		
    
		cell.selectionStyle = UITableViewCellSeparatorStyleNone;
		//count++;
        
		if (indexPath.row == 0)
        {
//			cell.textLabel.text = @"Sound";
            
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
				soundSwidth.frame = CGRectMake(600.0, 5.0, soundSwidth.frame.size.width, soundSwidth.frame.size.height);
				soundProgressSwitch.frame = CGRectMake(600.0, 40.0, soundProgressSwitch.frame.size.width, soundProgressSwitch.frame.size.height);
                sound1.frame = CGRectMake(10.0, 3.0, sound1.frame.size.width, fontTypeLabel.frame.size.height);
                sound2.frame = CGRectMake(10.0, 38.0, sound2.frame.size.width, fontTypeLabel.frame.size.height);
            }
            else
                soundProgressSwitch.frame = CGRectMake(220.0, 35.0, 70, 27);
            
            sound1.text=@"Checkbox Sound";
            sound2.text=@"Progress Bar Sound";
            [cell.contentView addSubview:sound1];
            [cell.contentView addSubview:sound2];            
            [cell.contentView addSubview:soundProgressSwitch];
			[cell.contentView addSubview:soundSwidth];
		}
        else if(indexPath.row == 1)
        {
			cell.textLabel.text = @"Background/Text Color";
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure.png"]];
		}
        else if(indexPath.row == 2)
        {
			cell.textLabel.text = @"Photo Background";
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				photoBGSwidth.frame = CGRectMake(600.0, 8.0, photoBGSwidth.frame.size.width, photoBGSwidth.frame.size.height);
			[cell.contentView addSubview:photoBGSwidth];
		}
        else if(indexPath.row == 3)
        {
			cell.textLabel.text = @"Font Type";
             fontTypeLabel.font=[UIFont fontWithName:fontTypeLabel.text size:14.0];
			//fontTypeLabel.frame = CGRectMake(500.0, 8.0, fontTypeLabel.frame.size.width, fontTypeLabel.frame.size.height);
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure.png"]];
			[cell.contentView addSubview:fontTypeLabel];
		}
        else if(indexPath.row == 4)
        {
			cell.textLabel.text = @"Task Alert Sound";
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure.png"]];
			[cell.contentView addSubview:taskSoundLabel];
		}
        else if(indexPath.row == 5)
        {
			cell.textLabel.text = @"Show Ready-Made List";
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				readyMadeListShowSwidth.frame = CGRectMake(600.0, 8.0, readyMadeListShowSwidth.frame.size.width, readyMadeListShowSwidth.frame.size.height);
			[cell.contentView addSubview:readyMadeListShowSwidth];
		}
//		else if(indexPath.row == 6){
//			cell.text = @"Show Welcome Page";
//			[cell.contentView addSubview:welcomePageShowSwidth];
//		}
		else if(indexPath.row == 6)
        {
			if (TARGET_VERSION == VERSION_BABY_PACKING) {
				cell.textLabel.text=@"Import/Export";
				UIButton *btnImport=[UIButton buttonWithType:UIButtonTypeRoundedRect];
				[btnImport setFrame:CGRectMake(125.0, 6.0, 80.0, 30.0)];

				if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
					btnImport.frame = CGRectMake(500.0, 8.0, btnImport.frame.size.width, btnImport.frame.size.height);
				
				[btnImport setTitle:@"Import" forState:UIControlStateNormal];
				[btnImport addTarget:self action:@selector(importDB:) forControlEvents:UIControlEventTouchUpInside];
				[cell.contentView addSubview:btnImport];
				
				UIButton *btnExport=[UIButton buttonWithType:UIButtonTypeRoundedRect];
				[btnExport setFrame:CGRectMake(210.0, 6.0, 80.0, 30.0)];
				if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
					btnExport.frame = CGRectMake(590, 8.0, btnImport.frame.size.width, btnImport.frame.size.height);
				
				[btnExport setTitle:@"Export" forState:UIControlStateNormal];
				[btnExport addTarget:self action:@selector(exportDB:) forControlEvents:UIControlEventTouchUpInside];
				[cell.contentView addSubview:btnExport];
				
			}
			else {
				cell.text = @"Group List Items ";
				if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
					addItemByGroupSwidth.frame = CGRectMake(600.0, 8.0, addItemByGroupSwidth.frame.size.width, addItemByGroupSwidth.frame.size.height);
				[cell.contentView addSubview:addItemByGroupSwidth];				
			}	
		}
        else if(indexPath.row == 7)
        {
			if (TARGET_VERSION == VERSION_YOU_PACKING) {
				cell.textLabel.text=@"Import/Export";
				UIButton *btnImport=[UIButton buttonWithType:UIButtonTypeRoundedRect];
				[btnImport setFrame:CGRectMake(125.0, 6.0, 80.0, 30.0)];
				
				if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
					btnImport.frame = CGRectMake(500.0, 8.0, btnImport.frame.size.width, btnImport.frame.size.height);
				
				[btnImport setTitle:@"Import" forState:UIControlStateNormal];
				[btnImport addTarget:self action:@selector(importDB:) forControlEvents:UIControlEventTouchUpInside];
				[cell.contentView addSubview:btnImport];
				
				UIButton *btnExport=[UIButton buttonWithType:UIButtonTypeRoundedRect];
				[btnExport setFrame:CGRectMake(210.0, 6.0, 80.0, 30.0)];
				if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
					btnExport.frame = CGRectMake(590, 8.0, btnImport.frame.size.width, btnImport.frame.size.height);
				
				[btnExport setTitle:@"Export" forState:UIControlStateNormal];
				[btnExport addTarget:self action:@selector(exportDB:) forControlEvents:UIControlEventTouchUpInside];
				[cell.contentView addSubview:btnExport];
			}
			else if (TARGET_VERSION == VERSION_BABY_PACKING) {
				cell.textLabel.text=@"Customize Top Bar";
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure.png"]];
			}			
			else {
				cell.text = @"Briefcase Password";
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure.png"]];
			}			
		}
		else if (indexPath.row == 8)
        {
			if (TARGET_VERSION == VERSION_YOU_PACKING) {
				cell.textLabel.text=@"Customize Top Bar";
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure.png"]];
			}			
			else if (TARGET_VERSION == VERSION_FAMILY_PACKING){

			cell.textLabel.text=@"Import/Export";
			btnImport=[UIButton buttonWithType:UIButtonTypeCustom];
			[btnImport setFrame:CGRectMake(155.0, 3.0, 67.0, 34.0)];
			
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				btnImport.frame = CGRectMake(570.0, 4.0, btnImport.frame.size.width, btnImport.frame.size.height);
			
//			[btnImport setTitle:@"Import" forState:UIControlStateNormal];
                [btnImport setBackgroundImage:[UIImage imageNamed:@"import.png"] forState:UIControlStateNormal];
                
			[btnImport addTarget:self action:@selector(importDB:) forControlEvents:UIControlEventTouchUpInside];
			[cell.contentView addSubview:btnImport];
			
			btnExport=[UIButton buttonWithType:UIButtonTypeCustom];
			[btnExport setFrame:CGRectMake(240.0, 3.0, 67.0, 34.0)];
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				btnExport.frame = CGRectMake(660.0, 4.0, btnImport.frame.size.width, btnImport.frame.size.height);
			
//			[btnExport setTitle:@"Export" forState:UIControlStateNormal];
                [btnExport setBackgroundImage:[UIImage imageNamed:@"export.png"] forState:UIControlStateNormal];
                
			[btnExport addTarget:self action:@selector(exportDB:) forControlEvents:UIControlEventTouchUpInside];
			[cell.contentView addSubview:btnExport];
			}
			else if	(TARGET_VERSION==VERSION_BABY_PACKING) {
				cell.textLabel.text=@"Customize Table Cell";
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure.png"]];
			}
	   }
	   else if	(indexPath.row==9) {
		   if (TARGET_VERSION==VERSION_FAMILY_PACKING) {
			 /*  cell.textLabel.text=@"Customize Top Bar";
			   cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
               cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure.png"]];*/
                cell.textLabel.text=@"iCloud sync";
               if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                   iCloudSwidth.frame = CGRectMake(600.0, 8.0, iCloudSwidth.frame.size.width, iCloudSwidth.frame.size.height);
               [cell.contentView addSubview:iCloudSwidth];

               
		   }
		   else if (TARGET_VERSION==VERSION_YOU_PACKING){
			   cell.textLabel.text=@"Customize Table Cell";
			   cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
               cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure.png"]];
		   }
		}
		else if (indexPath.row==10) {
			if (TARGET_VERSION==VERSION_FAMILY_PACKING) {
				cell.textLabel.text=@"Customize Table Cell";
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure.png"]];
			}
		}
		
	
	NSString *customFontName1 = [mAppDelegate getCustomFontName];
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
		cell.textLabel.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];			

        sound1.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];

        sound2.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];
        
	}
    else{
		cell.textLabel.font=[UIFont fontWithName:customFontName1 size:15];			        
        sound1.font=[UIFont fontWithName:customFontName1 size:15];			        
        sound2.font=[UIFont fontWithName:customFontName1 size:15];			        
    }
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		cell.backgroundColor = theColour1;
	}
	// and finally set the colour of your label	
	
	
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour21;
	// read the data back from the user defaults
	NSData *data211= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
	// check whether you got anything
	if(data211 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour21 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data211];
		cell.textLabel.textColor = theColour21;
		lblTextSize.textColor = theColour21;
		[lblTextSize setBackgroundColor:[UIColor clearColor]];
		fontTypeLabel.textColor = theColour21;
		taskSoundLabel.textColor = theColour21;
        sound1.textColor=theColour21;
        sound2.textColor=theColour21;
		[fontTypeLabel setBackgroundColor:[UIColor clearColor]];
		[taskSoundLabel setBackgroundColor:[UIColor clearColor]];
	}
	// and finally set the colour of your label
	
	
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        if (indexPath.row==0) {
            return 75;
        }
        else{
            return 45;
        }
    }else{
        if (indexPath.row==0) {
            return 70;
        }
        else{
            return 40;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
		BackgroundTextSubmenuViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[BackgroundTextSubmenuViewController alloc] initWithNibName:@"BackgroundTextSubmenuViewController_ipad" bundle:nil];
		else
			controller = [[BackgroundTextSubmenuViewController alloc] initWithNibName:@"BackgroundTextSubmenuViewController" bundle:nil];
        
        
        [self.navigationController pushViewController:controller animated:YES];
		[controller release];

    }else if (indexPath.row == 3) {
		TextPickerViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[TextPickerViewController alloc] initWithNibName:@"TextPickerViewController" bundle:nil];
		else
			controller = [[TextPickerViewController alloc] initWithNibName:@"TextPickerViewController1" bundle:nil];
		
		//[[TextPickerViewController alloc] initWithNibName:@"TextPickerViewController" bundle:nil];
		controller.title = @"Font Type";
		controller.selectedText = fontTypeLabel.text;
		controller.delegate = self;
		controller.choiceArray = [[SYSTEM_FONT_DICT allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
		[self.navigationController pushViewController:controller animated:YES];
		[controller release];
		
	} else if (indexPath.row == 4) {
		NSMutableDictionary* taskSoundDict = nil;
		for (NSMutableDictionary *dict in mAppDelegate.settingBundle) {
			if ([[dict objectForKey:@"Key"] isEqualToString:K_SET_TASK_SOUND]) {
				taskSoundDict = dict;
				break;
			}
		}
		if (taskSoundDict == nil) return;
		DetailedSettingsViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[DetailedSettingsViewController alloc] initWithNibName:@"DetailedSettingsViewController" bundle:nil];
		else {
            
            if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
            {
                controller = [[DetailedSettingsViewController alloc] initWithNibName:@"DetailedSettingsViewController1" bundle:nil];
            }
            else
            {
                controller = [[DetailedSettingsViewController alloc] initWithNibName:@"DetailedSettingsViewController_L" bundle:nil];
            }
        }
		//[[DetailedSettingsViewController alloc] initWithNibName:@"DetailedSettingsViewController" bundle:nil];
		controller.title = @"Task Alert Sound";
		controller.delegate = self;
		controller.data = taskSoundDict;
		controller.defaultValue = [SettingsViewController valueForTitle:taskSoundLabel.text forSection:K_SET_TASK_SOUND];
		[self.navigationController pushViewController:controller animated:YES];
        
		[controller release];
	} else if (indexPath.row == 7){
		if (TARGET_VERSION == VERSION_FAMILY_PACKING) {
			PasscodeViewController *controller = nil;
		
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				controller = [[PasscodeViewController alloc] initWithNibName:@"PasscodeViewController" bundle:nil];
			else
				controller = [[PasscodeViewController alloc] initWithNibName:@"PasscodeViewController1" bundle:nil];
		
			//[[PasscodeViewController alloc] initWithNibName:@"PasscodeViewController" bundle:nil];
			controller.title = @"Password";
			controller.delegate = self;
			[self.navigationController pushViewController:controller animated:YES];
			[controller release];
		}
		else if (TARGET_VERSION == VERSION_BABY_PACKING) {
			CustomizeTopBarViewController *controller = nil;
			
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				controller = [[CustomizeTopBarViewController alloc] initWithNibName:@"CustomizeTopBarViewController_ipad" bundle:nil];
			else
				controller = [[CustomizeTopBarViewController alloc] initWithNibName:@"CustomizeTopBarViewController" bundle:nil];
			
			[self.navigationController pushViewController:controller animated:YES];
			[controller release];						
		}

	}
	else if (indexPath.row==8){
		if (TARGET_VERSION==VERSION_BABY_PACKING) {
			CustomizeCellViewController *controller = nil;
			
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				controller = [[CustomizeCellViewController alloc] initWithNibName:@"CustomizeCellViewController_ipad" bundle:nil];
			else
				controller = [[CustomizeCellViewController alloc] initWithNibName:@"CustomizeCellViewController1" bundle:nil];
			
			//[[PasscodeViewController alloc] initWithNibName:@"PasscodeViewController" bundle:nil];
			[self.navigationController pushViewController:controller animated:YES];
			[controller release];			
		}
		else if (TARGET_VERSION == VERSION_YOU_PACKING) {
			CustomizeTopBarViewController *controller = nil;
			
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				controller = [[CustomizeTopBarViewController alloc] initWithNibName:@"CustomizeTopBarViewController_ipad" bundle:nil];
			else
				controller = [[CustomizeTopBarViewController alloc] initWithNibName:@"CustomizeTopBarViewController" bundle:nil];
			
			[self.navigationController pushViewController:controller animated:YES];
			[controller release];									
		}

	}
	else if (indexPath.row==9){
		if (TARGET_VERSION==VERSION_YOU_PACKING) {
			CustomizeCellViewController *controller = nil;
			
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				controller = [[CustomizeCellViewController alloc] initWithNibName:@"CustomizeCellViewController_ipad" bundle:nil];
			else
				controller = [[CustomizeCellViewController alloc] initWithNibName:@"CustomizeCellViewController" bundle:nil];
			
			//[[PasscodeViewController alloc] initWithNibName:@"PasscodeViewController" bundle:nil];
			[self.navigationController pushViewController:controller animated:YES];
			[controller release];			
		}
		else if (TARGET_VERSION == VERSION_FAMILY_PACKING) {
			/*CustomizeTopBarViewController *controller = nil;
			
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				controller = [[CustomizeTopBarViewController alloc] initWithNibName:@"CustomizeTopBarViewController_ipad" bundle:nil];
			else
				controller = [[CustomizeTopBarViewController alloc] initWithNibName:@"CustomizeTopBarViewController" bundle:nil];
			
			[self.navigationController pushViewController:controller animated:YES];
			[controller release];*/									
		}
		
	}
	else if (indexPath.row==10) {
		if (TARGET_VERSION==VERSION_FAMILY_PACKING) {
			CustomizeCellViewController *controller = nil;
			
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				controller = [[CustomizeCellViewController alloc] initWithNibName:@"CustomizeCellViewController_ipad" bundle:nil];
			else
				controller = [[CustomizeCellViewController alloc] initWithNibName:@"CustomizeCellViewController" bundle:nil];
			
			//[[PasscodeViewController alloc] initWithNibName:@"PasscodeViewController" bundle:nil];
			[self.navigationController pushViewController:controller animated:YES];
			[controller release];		
		}		
	}

	

}

//- (NSString *)base64Encoding
//{
//	NSTask *task = [[[NSTask alloc] init] autorelease];
//	NSPipe *inPipe = [NSPipe pipe], *outPipe = [NSPipe pipe];
//	NSFileHandle *inHandle = [inPipe fileHandleForWriting], *outHandle = [outPipe fileHandleForReading];
//	NSData *outData = nil;
//	
//	[task setLaunchPath:@"/usr/bin/openssl"];
//	[task setArguments:[NSArray arrayWithObjects:@"base64", @"-e", nil]];
//	[task setStandardInput:inPipe];
//	[task setStandardOutput:outPipe];
//	[task setStandardError:outPipe];
//	
//	[task launch];
//	
//	[inHandle writeData:self];
//	[inHandle closeFile];
//	
//	[task waitUntilExit];
//	
//	outData = [outHandle readDataToEndOfFile];
//	if (outData)
//	{
//		NSString *base64 = [[[NSString alloc] initWithData:outData encoding:NSUTF8StringEncoding] autorelease];
//		if (base64)
//			return base64;
//	}
//	
//	return nil;
//}

- (void) onSelectedFont:(NSString*) text {
	fontTypeLabel.text = text;
}

- (void) onSelectedAlertSound:(NSString*) text {
	taskSoundLabel.text = text;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

+ (NSString *) defaultTitleForSection:(NSString *)section {
	
	NSString *title = nil;
	
	BabyPackingAppDelegate *appDelegate = (id)[UIApplication sharedApplication].delegate;
	for (NSMutableDictionary *dict in appDelegate.settingBundle) {
		if ([section isEqualToString:[dict objectForKey:@"Key"]]) {
			NSString* value = [dict objectForKey:@"DefaultValue"];
			NSArray* values = [dict objectForKey:@"Values"];
			NSArray* titles = [dict objectForKey:@"Titles"];
			NSInteger index = [values indexOfObject:value];
			if (index == NSNotFound) return nil;
			title = [titles objectAtIndex:index];
			break;
		}
	}
	
	return title;
}

+ (NSString *) defaultValueForSection:(NSString *)section {
	
	NSString* value = nil;
	
	BabyPackingAppDelegate *appDelegate = (id)[UIApplication sharedApplication].delegate;
	for (NSMutableDictionary *dict in appDelegate.settingBundle) {
		if ([section isEqualToString:[dict objectForKey:@"Key"]]) {
			value = [dict objectForKey:@"DefaultValue"];
			break;
		}
	}
	
	return value;
}

+ (NSString *) titleForValue:(NSString *)value forSection:(NSString *)section {
	
	NSString *title = nil;
	
	BabyPackingAppDelegate *appDelegate = (id)[UIApplication sharedApplication].delegate;
	for (NSMutableDictionary *dict in appDelegate.settingBundle) {
		if ([section isEqualToString:[dict objectForKey:@"Key"]]) {
			NSArray* values = [dict objectForKey:@"Values"];
			NSArray* titles = [dict objectForKey:@"Titles"];
			NSInteger index = [values indexOfObject:value];
			if (index == NSNotFound) return nil;
			title = [titles objectAtIndex:index];
			break;
		}
	}
	
	return title;
}

+ (NSString *) valueForTitle:(NSString *)title forSection:(NSString *)section {
	
	NSString* value = nil;
	
	BabyPackingAppDelegate *appDelegate = (id)[UIApplication sharedApplication].delegate;
	for (NSMutableDictionary *dict in appDelegate.settingBundle) {
		if ([section isEqualToString:[dict objectForKey:@"Key"]]) {
			NSArray* values = [dict objectForKey:@"Values"];
			NSArray* titles = [dict objectForKey:@"Titles"];
			NSInteger index = [titles indexOfObject:title];
			if (index == NSNotFound) return nil;
			value = [values objectAtIndex:index];
			break;
		}
	}
	
	return value;
}

#pragma mark -
#pragma mark UITextField delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	[txtPasswordLogin resignFirstResponder];
    [txtPasswordLogin_PDL resignFirstResponder];
	[txtPasswordLogin1 resignFirstResponder];
    [txtPasswordLogin_L resignFirstResponder];
	[txtPasswordReg resignFirstResponder];
    [txtPasswordReg_PDL resignFirstResponder];
	[txtPasswordReg1 resignFirstResponder];
    [txtPasswordReg_L resignFirstResponder];
	[txtUsernameLogin resignFirstResponder];
    [txtUsernameLogin_PDL resignFirstResponder];
	[txtUsernameLogin1 resignFirstResponder];
    [txtUsernameLogin_L resignFirstResponder];
	[txtUsernameReg resignFirstResponder];
    [txtUsernameReg_PDL resignFirstResponder];
	[txtUsernameReg1 resignFirstResponder];
    [txtUsernameReg_L resignFirstResponder];
	[txtEmailReg resignFirstResponder];
    [txtEmailReg_PDL resignFirstResponder];
	[txtEmailReg1 resignFirstResponder];
    [txtEmailReg_L resignFirstResponder];
    
	
	return YES;
}

- (BOOL)textFieldDidEndEditing:(UITextField *)textField{
	[txtPasswordLogin resignFirstResponder];
    [txtPasswordLogin_PDL resignFirstResponder];
	[txtPasswordLogin1 resignFirstResponder];
    [txtPasswordLogin_L resignFirstResponder];
	[txtPasswordReg resignFirstResponder];
    [txtPasswordReg_PDL resignFirstResponder];
	[txtPasswordReg1 resignFirstResponder];
    [txtPasswordReg_L resignFirstResponder];
	[txtUsernameLogin resignFirstResponder];
    [txtUsernameLogin_PDL resignFirstResponder];
	[txtUsernameLogin1 resignFirstResponder];
    [txtUsernameLogin_L resignFirstResponder];
	[txtUsernameReg resignFirstResponder];
    [txtUsernameReg_PDL resignFirstResponder];
	[txtUsernameReg1 resignFirstResponder];
    [txtUsernameReg_L resignFirstResponder];
	[txtEmailReg resignFirstResponder];
    [txtEmailReg_PDL resignFirstResponder];
	[txtEmailReg1 resignFirstResponder];
    [txtEmailReg_L resignFirstResponder];
	
	return YES;
	
}

#pragma mark -
#pragma mark pickerView dataSource Methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
	return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{

		return 20;

}



#pragma mark pickerView Delegate Methods

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
	
	NSLog(@"%d",row);
	UILabel *pickerLabel = (UILabel *)view;
	NSString *text = nil;
	// Reuse the label if possible, otherwise create and configure a new one
	if ((pickerLabel == nil) || ([pickerLabel class] != [UILabel class])) { //newlabel
		CGRect frame = CGRectMake(0.0, 0.0, 50, 20.0);
		pickerLabel = [[[UILabel alloc] initWithFrame:frame] autorelease];
		pickerLabel.textAlignment = UITextAlignmentCenter;
		pickerLabel.font = [UIFont fontWithName:@"Helvetica" size:20];
		pickerLabel.backgroundColor=[UIColor clearColor];
	}
	pickerLabel.text=[NSString	stringWithFormat:@"%d",row+2];
	pickerLabel.textColor = [UIColor blackColor];
	return pickerLabel;
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
	
}

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{            return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    NSLog(@"%@",self.view.subviews);
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if (toInterfaceOrientation == UIInterfaceOrientationPortrait)
        {
            [iCloudSwidth setFrame:CGRectMake(600,5 , 70, 27)];
             [soundSwidth setFrame:CGRectMake(600,5 , 70, 27)];
             [soundProgressSwitch setFrame:CGRectMake(600,40 , 70, 27)];
             [colorBGSwidth setFrame:CGRectMake(600,5 , 70, 27)];
             [photoBGSwidth setFrame:CGRectMake(600,5 , 70, 27)];
             [addItemByGroupSwidth setFrame:CGRectMake(600,5 , 70, 27)];
            [readyMadeListShowSwidth setFrame:CGRectMake(600,5 , 70, 27)];
            [welcomePageShowSwidth setFrame:CGRectMake(600,5 , 70, 27)];
            [btnImport setFrame:CGRectMake(570.0, 4.0, btnImport.frame.size.width, btnImport.frame.size.height)];
            [btnExport setFrame:CGRectMake(660.0, 4.0, btnImport.frame.size.width, btnImport.frame.size.height)];

        }
        else
        {
            [iCloudSwidth setFrame:CGRectMake(850,5 , 70, 27)];
            [soundSwidth setFrame:CGRectMake(850,5 , 70, 27)];
            [soundProgressSwitch setFrame:CGRectMake(850,40 , 70, 27)];
            [colorBGSwidth setFrame:CGRectMake(850,5 , 70, 27)];
            [photoBGSwidth setFrame:CGRectMake(850,5 , 70, 27)];
            [addItemByGroupSwidth setFrame:CGRectMake(850,5 , 70, 27)];
            [readyMadeListShowSwidth setFrame:CGRectMake(850,5 , 70, 27)];
            [welcomePageShowSwidth setFrame:CGRectMake(850,5 , 70, 27)];
            [btnImport setFrame:CGRectMake(820.0, 4.0, btnImport.frame.size.width, btnImport.frame.size.height)];
            [btnExport setFrame:CGRectMake(910.0, 4.0, btnImport.frame.size.width, btnImport.frame.size.height)];
        }
    }
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        
    {
         [self.tableView setScrollEnabled:YES];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            
            for (UIView *exportview in self.view.subviews)
            {
                if (exportview ==ipadLogout_ExportView_L )
                {
                    [exportview removeFromSuperview];
                    View_export = ipadLogout_ExportView;
                    [self.view addSubview:View_export];
                }
            }
            for (UIView *importview in self.view.subviews)
            {
                if (importview ==ipadLogout_ImportView_L )
                {
                    [importview removeFromSuperview];
                    View_import = ipadLogout_ImportView;
                    [self.view addSubview:View_import];
                    
                }
            }
            for (UIView *loginview in self.view.subviews)
            {
                if (loginview ==loginView_PDL )
                {
                    [loginview removeFromSuperview];
                    View_Login = loginView;
                    [self.view addSubview:View_Login];
                }
            }
            for (UIView *regview in self.view.subviews)
            {
                if (regview ==regView_PDL )
                {
                    [regview removeFromSuperview];
                    View_reg = regView;
                    [self.view addSubview:View_reg];
                }
            }

        }
        else{
            for (UIView *exportview in self.view.subviews)
        {
            if (exportview ==iphoneLogout_ExportView_L )
            {
                [exportview removeFromSuperview];
                View_export = iphoneLogout_ExportView;
                [self.view addSubview:View_export];
            }
        }
        for (UIView *importview in self.view.subviews)
        {
            if (importview ==iphoneLogout_ImportView_L )
            {
                [importview removeFromSuperview];
                View_import = iphoneLogout_ImportView;
                [self.view addSubview:View_import];
              
            }
        }
        for (UIView *loginview in self.view.subviews)
        {
            if (loginview ==loginView_L )
            {
                [loginview removeFromSuperview];
                View_Login = loginView1;
                [self.view addSubview:View_Login];
            }
        }
        for (UIView *regview in self.view.subviews)
        {
            if (regview ==regView_L )
            {
                [regview removeFromSuperview];
                View_reg = regView1;
                [self.view addSubview:View_reg];
            }
        }
        }
        [layoutManager translateToPortraitForView:self.view withAnimation:YES];
       
        
    }
    else
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            for (UIView *exportview in self.view.subviews)
            {
                if (exportview ==ipadLogout_ExportView )
                {
                    [exportview removeFromSuperview];
                    View_export = ipadLogout_ExportView_L;
                    [self.view addSubview:View_export];
                    //[tblview setScrollEnabled:FALSE];
                    [self.tableView setScrollEnabled:NO];
                }
            }
            for (UIView *importview in self.view.subviews)
            {
                if (importview ==ipadLogout_ImportView )
                {
                    [importview removeFromSuperview];
                    View_import = ipadLogout_ImportView_L;
                    [self.view addSubview:View_import];
                    //[tblview setScrollEnabled:FALSE];
                    [self.tableView setScrollEnabled:NO];
                }
            }
            for (UIView *loginview in self.view.subviews)
            {
                if (loginview ==  loginView)
                {
                    [loginview removeFromSuperview];
                    View_Login = loginView_PDL;
                    [self.view addSubview:View_Login];
                    [self.tableView setScrollEnabled:NO];
                }
            }
            for (UIView *regview in self.view.subviews)
            {
                if (regview == regView )
                {
                    [regview removeFromSuperview];
                    View_reg = regView_PDL;
                    [self.view addSubview:View_reg];
                    [self.tableView setScrollEnabled:NO];
                }
            }

            
            
        }else
        {
        for (UIView *exportview in self.view.subviews)
        {
            if (exportview ==iphoneLogout_ExportView )
            {
                [exportview removeFromSuperview];
                View_export = iphoneLogout_ExportView_L;
                [self.view addSubview:View_export];
                //[tblview setScrollEnabled:FALSE];
                [self.tableView setScrollEnabled:NO];
                
                
                
            }
        }
        for (UIView *importview in self.view.subviews)
        {
            if (importview ==iphoneLogout_ImportView )
            {
                [importview removeFromSuperview];
                View_import = iphoneLogout_ImportView_L;
                [self.view addSubview:View_import];
                 //[tblview setScrollEnabled:FALSE];
                  [self.tableView setScrollEnabled:NO];
            }
        }
        for (UIView *loginview in self.view.subviews)
        {
            if (loginview ==loginView1 )
            {
                [loginview removeFromSuperview];
                View_Login = loginView_L;
                [self.view addSubview:View_Login];
                  [self.tableView setScrollEnabled:NO];
            }
        }
        for (UIView *regview in self.view.subviews)
        {
            if (regview ==regView1 )
            {
                [regview removeFromSuperview];
                View_reg = regView_L;
                [self.view addSubview:View_reg];
                  [self.tableView setScrollEnabled:NO];
            }
        }
        }

        [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
       
    }
    // }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;

    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}


#pragma marc - Export List and items

NSString *xmlStr;
-(void)ExList
{
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    
    // Create XML and pass to iCloud
    xmlStr=@"<List>";
    NSString *select_query;
    const char *select_stmt;
    sqlite3_stmt *compiled_stmt;
    sqlite3 *dbFamilyPacking;
    if (sqlite3_open([[mAppDelegate getDBListPath] UTF8String], &dbFamilyPacking) == SQLITE_OK)
    {
        select_query = [NSString stringWithFormat:@"SELECT * FROM list"];
        select_stmt = [select_query UTF8String];
        if(sqlite3_prepare_v2(dbFamilyPacking, select_stmt, -1, &compiled_stmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiled_stmt) == SQLITE_ROW)
            {
                NSString *addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,0)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<Data><pk>%@</pk>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,1)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<name>%@</name>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,2)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<sex>%@</sex>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,3)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<age>%@</age>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,4)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<smallIcon>%@</smallIcon>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,5)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<photo>%@</photo>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,6)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<lastUpdate>%@</lastUpdate>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,7)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<totalCount>%@</totalCount>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,8)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<checkedCount>%@</checkedCount>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,9)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<predefineId>%@</predefineId>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,10)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<predefineForAge>%@</predefineForAge>",addr];
                
                
                xmlStr =[xmlStr stringByAppendingString:@"</Data>"];
            }
            
            xmlStr =[xmlStr stringByAppendingString:@"</List>"];
            
            sqlite3_finalize(compiled_stmt);
        }
        else
        {
            NSLog(@"Error while creating detail view statement. '%s'", sqlite3_errmsg(dbFamilyPacking));
        }
        
        NSLog(@"Generated XML : %@",xmlStr);
    }
    
    [self ExItem];
}

-(void)ExItem
{
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    
    // Create XML and pass to iCloud
    xmlStr =[xmlStr stringByAppendingString:@"<Item>"];
    NSString *select_query;
    const char *select_stmt;
    sqlite3_stmt *compiled_stmt;
    sqlite3 *dbFamilyPacking;
    if (sqlite3_open([[mAppDelegate getDBListPath] UTF8String], &dbFamilyPacking) == SQLITE_OK)
    {
        select_query = [NSString stringWithFormat:@"SELECT * FROM item"];
        select_stmt = [select_query UTF8String];
        if(sqlite3_prepare_v2(dbFamilyPacking, select_stmt, -1, &compiled_stmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiled_stmt) == SQLITE_ROW)
            {
                NSString *addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,0)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<Data><pk>%@</pk>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,1)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<name>%@</name>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,2)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<qty>%@</qty>",addr];
                
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,3)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<tip>%@</tip>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,4)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<selected>%@</selected>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,5)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<custom>%@</custom>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,6)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<checked>%@</checked>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,7)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<listId>%@</listId>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,8)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<predefineId>%@</predefineId>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,9)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<predefineForAge>%@</predefineForAge>",addr];
                
                
                xmlStr =[xmlStr stringByAppendingString:@"</Data>"];
            }
            
            xmlStr =[xmlStr stringByAppendingString:@"</Item>"];
            
            sqlite3_finalize(compiled_stmt);
        }
    }
    [self ExBriefcase];
}

-(void)ExBriefcase
{
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    
    // Create XML and pass to iCloud
    xmlStr =[xmlStr stringByAppendingString:@"<Briefcase>"];
    NSString *select_query;
    const char *select_stmt;
    sqlite3_stmt *compiled_stmt;
    sqlite3 *dbFamilyPacking;
    if (sqlite3_open([[mAppDelegate getDBListPath] UTF8String], &dbFamilyPacking) == SQLITE_OK)
    {
        select_query = [NSString stringWithFormat:@"SELECT * FROM briefcase"];
        select_stmt = [select_query UTF8String];
        if(sqlite3_prepare_v2(dbFamilyPacking, select_stmt, -1, &compiled_stmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiled_stmt) == SQLITE_ROW)
            {
                NSString *addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,0)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<Data><pk>%@</pk>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,1)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<title>%@</title>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,2)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<passport>%@</passport>",addr];
                
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,3)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<license>%@</license>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,4)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<ssn>%@</ssn>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,5)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<id_card>%@</id_card>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,6)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<airline_name>%@</airline_name>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,7)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<flight>%@</flight>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,8)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<frequent_flyer>%@</frequent_flyer>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,9)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<car_name>%@</car_name>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,10)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<license_plate>%@</license_plate>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,11)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<hotel_name>%@</hotel_name>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,12)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<hotel_phone>%@</hotel_phone>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,13)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<hotel_address>%@</hotel_address>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,14)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<embassy_name>%@</embassy_name>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,15)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<embassy_address>%@</embassy_address>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,16)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<embassy_emergency>%@</embassy_emergency>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,17)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<other>%@</other>",addr];
                
                
                xmlStr =[xmlStr stringByAppendingString:@"</Data>"];
            }
            
            xmlStr =[xmlStr stringByAppendingString:@"</Briefcase>"];
            
            sqlite3_finalize(compiled_stmt);
        }
    }
    NSLog(@"MXML : %@",xmlStr);
    ExportParser *exportData=[[ExportParser alloc] init];
    [exportData initWithDBName:@"1" withXMLString:xmlStr];
    [exportData release];
}

#pragma marc - Import List and items

NSString *xmlFileList,*xmlFileItem,*xmlFileBriefcase;
-(void)ImList
{
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
	
	NSString *urlString = @"http://babypackandgo.com/packngohelp/ws/xmlexport.php";
	
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
	[request setURL:[NSURL URLWithString:urlString]];
	[request setHTTPMethod:@"POST"];
	
	
	NSString *boundary = [NSString stringWithString:@"---------------------------14737809831466499882746641449"];
	NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
	[request addValue:contentType forHTTPHeaderField: @"Content-Type"];

	
	NSMutableData *postBody = [NSMutableData data];
	
	[postBody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
	[postBody appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"userId\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
	[postBody appendData:[[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"]] dataUsingEncoding:NSUTF8StringEncoding]];
	
	[postBody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
	[postBody appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"dbname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
	[postBody appendData:[[NSString stringWithFormat:@"1"] dataUsingEncoding:NSUTF8StringEncoding]];
	
	// setting the body of the post to the reqeust
	[request setHTTPBody:postBody];
	
	// now lets make the connection to the web
	
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
	NSMutableString *returnString = [[NSMutableString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
	
	NSMutableString *stringModified=[returnString stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
	NSData *newReturnData=[stringModified dataUsingEncoding:NSUTF8StringEncoding];

   NSString *xmlFileBoth = [NSString stringWithFormat:@"%@",stringModified];
    
    xmlFileList = [[xmlFileBoth componentsSeparatedByString:@"</List>"] objectAtIndex:0];
    xmlFileItem = [[[[xmlFileBoth componentsSeparatedByString:@"</List>"] objectAtIndex:1] componentsSeparatedByString:@"<Briefcase>"] objectAtIndex:0];
    xmlFileBriefcase = [[[[xmlFileBoth componentsSeparatedByString:@"</List>"] objectAtIndex:1] componentsSeparatedByString:@"</Item>"] objectAtIndex:1];
    
    
    // Code for delete all existing records
    NSString *select_query;
    sqlite3 *database;
    if(sqlite3_open([[mAppDelegate getDBListPath] UTF8String], &database) == SQLITE_OK)
    {
        select_query = [NSString stringWithFormat:@"delete from list"];
        const char *sqlStatementShirts = [select_query UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatementShirts, -1, &compiledStatement, NULL) != SQLITE_OK)
        {
            NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
        else
        {
            sqlite3_step(compiledStatement);
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
    NSArray *arr=[xmlFileList componentsSeparatedByString:@"<Data>"];
    if ([arr count]>1)
    {
        for(int i=1;i<[arr count];i++)
        {
            NSString *str=[arr objectAtIndex:i];
            NSArray *arr1=[str componentsSeparatedByString:@"<pk>"];
            NSString *data=[arr1 objectAtIndex:1];
            NSRange ranfrom=[data rangeOfString:@"</pk>"];
            NSString *val11=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<name>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</name>"];
            NSString *val1=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<sex>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</sex>"];
            NSString *val2=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<age>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</age>"];
            NSString *val3=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<smallIcon>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</smallIcon>"];
            NSString *val4=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<photo>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</photo>"];
            NSString *val5=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<lastUpdate>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</lastUpdate>"];
            NSString *val6=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<totalCount>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</totalCount>"];
            NSString *val7=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<checkedCount>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</checkedCount>"];
            NSString *val8=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<predefineId>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</predefineId>"];
            NSString *val9=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<predefineForAge>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</predefineForAge>"];
            NSString *val10=[data substringToIndex:ranfrom.location];
            
            sqlite3 *database;
            if(sqlite3_open([[mAppDelegate getDBListPath] UTF8String], &database) == SQLITE_OK)
            {
                NSString *select_query = [NSString stringWithFormat:@"INSERT INTO list (pk,name,sex,age,smallIcon,photo,lastUpdate,totalCount,checkedCount,predefineId,predefineForAge) VALUES(\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",val11,val1,val2,val3,val4,val5,val6,val7,val8,val9,val10];
                const char *sqlStatementShirts = [select_query UTF8String];
                sqlite3_stmt *compiledStatement;
                if(sqlite3_prepare_v2(database, sqlStatementShirts, -1, &compiledStatement, NULL) != SQLITE_OK)
                {
                    NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                }
                else
                {
                    sqlite3_step(compiledStatement);
                }
                sqlite3_finalize(compiledStatement);
            }
            sqlite3_close(database);
        }
    }
    [self ImItem];
}

-(void)ImItem
{
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    
    // Code for delete all existing records
    NSString *select_query;
    sqlite3 *database;
    if(sqlite3_open([[mAppDelegate getDBListPath] UTF8String], &database) == SQLITE_OK)
    {
        select_query = [NSString stringWithFormat:@"delete from item"];
        const char *sqlStatementShirts = [select_query UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatementShirts, -1, &compiledStatement, NULL) != SQLITE_OK)
        {
            NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
        else
        {
            sqlite3_step(compiledStatement);
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
    // Code for parse all the records of iCloud and store in database
    
    NSArray *arr=[xmlFileItem componentsSeparatedByString:@"<Data>"];
    if ([arr count]>1)
    {
        for(int i=1;i<[arr count];i++)
        {
            NSString *str=[arr objectAtIndex:i];
            NSArray *arr1=[str componentsSeparatedByString:@"<pk>"];
            NSString *data=[arr1 objectAtIndex:1];
            NSRange ranfrom=[data rangeOfString:@"</pk>"];
            NSString *val11=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<name>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</name>"];
            NSString *val1=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<qty>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</qty>"];
            NSString *val2=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<tip>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</tip>"];
            NSString *val3=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<selected>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</selected>"];
            NSString *val4=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<custom>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</custom>"];
            NSString *val5=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<checked>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</checked>"];
            NSString *val6=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<listId>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</listId>"];
            NSString *val7=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<predefineId>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</predefineId>"];
            NSString *val8=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<predefineForAge>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</predefineForAge>"];
            NSString *val9=[data substringToIndex:ranfrom.location];
            
            sqlite3 *database;
            if(sqlite3_open([[mAppDelegate getDBListPath] UTF8String], &database) == SQLITE_OK)
            {
                select_query = [NSString stringWithFormat:@"INSERT INTO item (pk,name,qty,tip,selected,custom,checked,listId,predefineId,predefineForAge) VALUES(\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",val11,val1,val2,val3,val4,val5,val6,val7,val8,val9];
                const char *sqlStatementShirts = [select_query UTF8String];
                sqlite3_stmt *compiledStatement;
                if(sqlite3_prepare_v2(database, sqlStatementShirts, -1, &compiledStatement, NULL) != SQLITE_OK)
                {
                    NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                }
                else
                {
                    sqlite3_step(compiledStatement);
                }
                sqlite3_finalize(compiledStatement);
            }
            sqlite3_close(database);
        }
    }
    
    [self ImBriefcase];
}


-(void)ImBriefcase
{
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    
    // Code for delete all existing records
    NSString *select_query;
    sqlite3 *database;
    if(sqlite3_open([[mAppDelegate getDBListPath] UTF8String], &database) == SQLITE_OK)
    {
        select_query = [NSString stringWithFormat:@"delete from briefcase"];
        const char *sqlStatementShirts = [select_query UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatementShirts, -1, &compiledStatement, NULL) != SQLITE_OK)
        {
            NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
        else
        {
            sqlite3_step(compiledStatement);
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
    // Code for parse all the records of iCloud and store in database
    
    NSArray *arr=[xmlFileBriefcase componentsSeparatedByString:@"<Data>"];
    if ([arr count]>1)
    {
        for(int i=1;i<[arr count];i++)
        {
            NSString *str=[arr objectAtIndex:i];
            NSArray *arr1=[str componentsSeparatedByString:@"<pk>"];
            NSString *data=[arr1 objectAtIndex:1];
            NSRange ranfrom=[data rangeOfString:@"</pk>"];
            NSString *val18=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<title>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</title>"];
            NSString *val1=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<passport>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</passport>"];
            NSString *val2=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<license>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</license>"];
            NSString *val3=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<ssn>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</ssn>"];
            NSString *val4=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<id_card>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</id_card>"];
            NSString *val5=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<airline_name>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</airline_name>"];
            NSString *val6=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<flight>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</flight>"];
            NSString *val7=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<frequent_flyer>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</frequent_flyer>"];
            NSString *val8=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<car_name>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</car_name>"];
            NSString *val9=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<license_plate>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</license_plate>"];
            NSString *val10=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<hotel_name>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</hotel_name>"];
            NSString *val11=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<hotel_phone>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</hotel_phone>"];
            NSString *val12=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<hotel_address>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</hotel_address>"];
            NSString *val13=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<embassy_name>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</embassy_name>"];
            NSString *val14=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<embassy_address>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</embassy_address>"];
            NSString *val15=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<embassy_emergency>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</embassy_emergency>"];
            NSString *val16=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<other>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</other>"];
            NSString *val17=[data substringToIndex:ranfrom.location];
            
            sqlite3 *database;
            if(sqlite3_open([[mAppDelegate getDBListPath] UTF8String], &database) == SQLITE_OK)
            {
                select_query = [NSString stringWithFormat:@"INSERT INTO briefcase (pk,title,passport,license,ssn,id_card,airline_name,flight,frequent_flyer,car_name,license_plate,hotel_name,hotel_phone,hotel_address,embassy_name,embassy_address,embassy_emergency,other) VALUES(\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",val18,val1,val2,val3,val4,val5,val6,val7,val8,val9,val10,val11,val12,val13,val14,val15,val16,val17];
                const char *sqlStatementShirts = [select_query UTF8String];
                sqlite3_stmt *compiledStatement;
                if(sqlite3_prepare_v2(database, sqlStatementShirts, -1, &compiledStatement, NULL) != SQLITE_OK)
                {
                    NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                }
                else
                {
                    sqlite3_step(compiledStatement);
                }
                sqlite3_finalize(compiledStatement);
            }
            sqlite3_close(database);
        }
    }
    
}

@end
//
//  SelectItemViewController.m
//  BabyPacking
//
//  Created by Gary He on 5/20/09.
//  Copyright 2009 test. All rights reserved.
//

#import "SelectItemViewController.h"
#import "AddItemViewController.h"
#import "PackingList.h"
#import "Constant.h"
#import "DetailItemViewController.h"
#import "CSTableViewCell.h"
#import "RootViewController.h"
#import "BabyPackingAppDelegate.h"
#import "CategoriseListViewController.h"
#import "Items.h"
#import "DataManager.h"
#import "RestoreItemViewController.h"

@interface SelectItemViewController(Private)

- (BOOL) checkSelectedCount;
- (void) saveSelectedItemsToDB;
- (void) showSearchBar;
- (void) hideSearchBar;
- (PackingItem*) itemAtIndexPath:(NSIndexPath*)indexPath;
- (NSInteger)indexOfItem:(PackingItem *)item fromArray:(NSArray *)array;

@end


@implementation SelectItemViewController
@synthesize homeController;

@synthesize filteredItemArray;
@synthesize predefinedItems,babyPredefinedItems;
@synthesize currentList;
@synthesize isFromCategory,category,currentCategory,isSearchFromCategory,isBabyPacking;
@synthesize editList;
@synthesize categoryListInAppData;
@synthesize isEdit;
@synthesize player1;

- (void)dealloc {
	[myTableView release];
	[bgImageView release];
	[doneButton release];
	[addButton release];
	[selectAllButton release];
	[clearAllButton release];
	[searchButton release];
	[babyPredefinedItems release];
	[mySearchBar release];
	[predefinedItems release];
	[homeController release];
	[category release];
	[filteredItemArray release];
	[categoryListInAppData release];
	[currentCategory release];
	
	[checkImg release];
	[uncheckImg release];
	
	[currentList release];
	
	[player1 release];
	
	//[editView release];
	[delBtn release];
	[sortBtn release];
	[restoreBtn release];
	
	if (item5 != nil)
	{
		[item5 release];
		item5 = nil;
	}
	
    [super dealloc];
}



- (id)initWithNibName:(NSString *)nibName bundle:(NSBundle *)nibBundle {
	if (self = [super initWithNibName:nibName bundle:nibBundle]) {
        
        layoutManager = [[LayoutManager alloc] initWithPortraitView:self.view andLandscapeNIB:@"SelectItemViewController_L"];
		appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
		if (appDelegate.soundOn) {
			NSString *soundFilePath =
			[[NSBundle mainBundle] pathForResource: CHECK_AUDIO_NAME
											ofType: CHECK_AUDIO_EXT];
			
			NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
			
			AVAudioPlayer *newPlayer =
			[[AVAudioPlayer alloc] initWithContentsOfURL: fileURL
												   error: nil];
			[fileURL release];
			newPlayer.volume = DEF_VOLUME;
			self.player1 = newPlayer;
			[newPlayer release];
		}
	}
	return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
	appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
    
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor whiteColor];
    
	NSString *customFontName = [appDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];
	}
	else {
		label.font=[UIFont fontWithName:customFontName size:fontSize];
	}
	
	[label sizeToFit];
	
    //	label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
        label.textColor=[UIColor whiteColor];
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label.textColor = theColour1;
	}
	// and finally set the colour of your label
	
	self.navigationItem.titleView = label;
	label.text = TITLE_SELECT_ITEMS;
	[label sizeToFit];
	
	mySearchBar.frame = CGRectZero;
	mySearchBar.alpha = 0.0;
	if (TARGET_VERSION == VERSION_BABY_PACKING)
		myTableView.frame = CGRectMake(0, 0.0, myTableView.frame.size.width, myTableView.frame.size.height + 44.0);//CGRectMake(0, 0, myTableView.frame.size.width, myTableView.frame.size.height + 44);
	else
	{
		myTableView.frame = CGRectMake(0, 65.0, myTableView.frame.size.width, myTableView.frame.size.height);
		editView.frame = CGRectMake(0.0,myTableView.frame.size.height,768.0,44.0);
        //        [editView setFrame:CGRectMake(0,SCREEN_HEIGHT-150,SCREEN_WIDTH, editView.frame.size.height)];
        //		[self.view addSubview:editView];
        //SCREEN_HEIGHT-editView.frame.size.height
	}
    
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour21;
	// read the data back from the user defaults
	NSData *data211= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
	// check whether you got anything
	if(data211 == nil) {
		// use this to set the colour the first time your app runs
		myTableView.backgroundColor = [UIColor clearColor];
	} else {
		// this recreates the colour you saved
		theColour21 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data211];
		myTableView.backgroundColor = theColour21;
	}
	// and finally set the colour of your label
	
	if (currentList) {
        
		if (appDelegate.colorBgOn) {
			bgImageView.alpha = 1.0;
			if(currentList.sex == BabySexBoy) {
				if (TARGET_VERSION == VERSION_BABY_PACKING)
					bgImageView.image = [UIImage imageNamed:BLUE_BEAR];
				else
					bgImageView.image = [UIImage imageNamed:BACKGROUND_BLUE];
			} else if(currentList.sex == BabySexGirl) {
				if (TARGET_VERSION == VERSION_BABY_PACKING)
					bgImageView.image = [UIImage imageNamed:PINK_BEAR];
				else
					bgImageView.image = [UIImage imageNamed:BACKGROUND_PINK];
			} else {
				if (TARGET_VERSION == VERSION_BABY_PACKING)
					bgImageView.image = [UIImage imageNamed:GRAY_BEAR];
				else
					bgImageView.image = [UIImage imageNamed:BACKGROUND_GRAY];
			}
		}
		
		//		BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
		//	NSMutableArray *itemArray = [NSMutableArray arrayWithCapacity:0];
		if (TARGET_VERSION != VERSION_BABY_PACKING){
			isFromCategory = YES;
		}
        //		else{
        //			self.babyPredefinedItems = [NSMutableArray arrayWithCapacity:0];
        //			if (currentList.age == Baby) {
        //				babyPredefinedItems = appDelegate.babyPredefinedItems;
        //			} else if(currentList.age == Toddler){ // Toddler
        //				babyPredefinedItems = appDelegate.toddlerPredefinedItems;
        //			}
        //		}
		
	}
	//self.itemArray = [NSMutableArray arrayWithCapacity:16];
	self.filteredItemArray = [NSMutableArray arrayWithCapacity:16];
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
	
	if (!isSearchFromCategory)
	{
		label.text = @"Select List Items";
		[label sizeToFit];
		self.navigationItem.rightBarButtonItem = addButton;
		self.navigationItem.leftBarButtonItem = doneButton;
	}
	else
	{
		label.text = @"Search Items";
		[label sizeToFit];
		self.navigationItem.leftBarButtonItem = doneButton;
		searchButton.enabled = NO;
		[self showSearchBar];
	}
	
    
	
	checkImg = [[UIImage imageNamed:CHECK_IMAGE] retain];
	uncheckImg = [[UIImage imageNamed:UNCHECK_IMAGE] retain];
    
	
    for (PackingItem *item in predefinedItems){
        for(PackingItem *item2 in currentList.itemArray)
            
            if ([item.name isEqualToString:item2.name] ){
                item.selected = YES;
                break;
            }
        
			else
				item.selected = NO;
    }
	
	[self reorderListByAsc];
	[myTableView reloadData];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
}

- (IBAction) buttonAction:(id) sender {
	if (sender == addButton)
    {
		
		AddItemViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[AddItemViewController alloc] initWithNibName:@"AddItemViewController" bundle:nil];
		else
			controller = [[AddItemViewController alloc] initWithNibName:@"AddItemViewController1" bundle:nil];
		
		//[[AddItemViewController alloc] initWithNibName:@"AddItemViewController" bundle:nil];
		
		controller.currentList = self.currentList;
		if (!TARGET_VERSION == VERSION_BABY_PACKING){
			NSMutableArray *itemInApp = [self.categoryListInAppData objectForKey:K_LIST_ITEMS];
			controller.itemsInApp = itemInApp;
			controller.category = self.category;
			controller.currentCategory = self.currentCategory;
            //			controller.itemArray = predefinedItems;
		}
		else{
			controller.itemsInApp = babyPredefinedItems;
			controller.category = [NSString stringWithFormat:@"BabyItem"];
		}
		controller.itemArray = predefinedItems;
        
		UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
		
		[self.navigationController presentModalViewController:navController  animated:YES];
		[controller release];
		[navController release];
		
	}
    else if (sender == doneButton)
    {
        // sync with iCloud
        if(appDelegate.iCloudShow)
        {
            [self btnUpload];
        }
        
		if(!isFromCategory)
        {
			BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
			NSMutableArray * coutemsList = [PackingList findAllWithoutPhoto];
			
            if (isEdit) {
                [self.navigationController popViewControllerAnimated:YES];
            }
            else{
                
                if([coutemsList count] == 25)
                {
                    NSString *meg = [NSString stringWithFormat:@"You have created %d custom packing lists. How do you like this app so far? If you are happy, please consider writing a (nice) review at the App Store. Also, feel free to send us an email for any feature request or issues.",[coutemsList count]];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations" message:meg
                                                                   delegate:appDelegate cancelButtonTitle:@"Close popup" otherButtonTitles: @"Write a review",@"Send an email",nil];
                    [alert show];
                    [alert release];
                }
                
                
                
                // save selected items
                BOOL hasOne = [self checkSelectedCount];
                if (hasOne) {
                    [self saveSelectedItemsToDB];
                    
                    
                    [self.navigationController popToViewController:homeController animated:YES];
                    
                    // remove from memory
                    currentList.itemArray = nil;
                } else {
                    BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
                    [appDelegate showAlert:M_SELECT_ONE_TITLE message:M_SELECT_ONE];
                }
            }
		}
        else
        {
			[self saveSelectedItemsToDB];
			[self.navigationController popViewControllerAnimated:YES];
			// remove from memory
			// TODO: 这里不能赋空。
			//currentList.itemArray = nil;
		}
	}
    else if (sender == selectAllButton)
    {
        //		if(!isFromCategory){
        //			for (PackingItem *item in currentList.itemArray) {
        //				[item setSelected:YES];
        //			}
        //			[myTableView reloadData];
        //		}else{
        for (PackingItem *item in predefinedItems ){
            [item setSelected:YES];
        }
        [myTableView reloadData];
        //		}
        
		
	}
    else if (sender == clearAllButton)
    {
        //		if(!isFromCategory){
        //			for (PackingItem *item in currentList.itemArray) {
        //				[item setSelected:NO];
        //			}
        //			[myTableView reloadData];
        //		}else{
        for (PackingItem *item in predefinedItems ){
            [item setSelected:NO];
        }
        [myTableView reloadData];
        //		}
		
	}
    else if (sender == searchButton)
    {
		if (search) {
			[self hideSearchBar];
			search = NO;
			[filteredItemArray removeAllObjects];
		} else {
			[self showSearchBar];
			search = YES;
		}
	}
	else if (sender == delBtn)
	{
		if (delBtn.selected)
		{
			delBtn.selected = NO;
			[self setEditing:NO animated:YES];
		}
		else
		{
			delBtn.selected = YES;
			[self setEditing:YES animated:YES];
		}
	}
	else if (sender == sortBtn)
	{
		if (sortBtn.selected)
		{
			sortBtn.selected = NO;
			[self reorderListByAsc];
			[myTableView reloadData];
		}
		else
		{
			sortBtn.selected = YES;
			[self reorderListByDesc];
			[myTableView reloadData];
		}
	}
	else if (sender == restoreBtn)
	{
		BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
		if (!TARGET_VERSION == VERSION_BABY_PACKING){
			[appDelegate.dataManager getRestoreItemByCategory:currentCategory.pk];
		}
		/*else{
         [appDelegate.dataManager getRestoreItemByCategory:[NSString stringWithFormat:@"BabyItem"]];
         }*/
		
		RestoreItemViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[RestoreItemViewController alloc] initWithNibName:@"RestoreItemViewController" bundle:nil];
		else
			controller = [[RestoreItemViewController alloc] initWithNibName:@"RestoreItemViewController1" bundle:nil];
		
		//[[RestoreItemViewController alloc] initWithNibName:@"RestoreItemViewController" bundle:nil];
		
		//UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
		
		[self presentModalViewController:controller  animated:YES];
		[controller release];
		//[navController release];
	}
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animate {
	[super setEditing:editing animated:animate];
	myTableView.editing = editing;
	[myTableView reloadData];
}

-(void)reorderListByDesc{
	
	NSMutableArray *items = predefinedItems;
	
	for(int m = 0 ; m < [items count]; m++){
		for(int n = 0 ; n < [items count]-m-1; n++){
			PackingItem *item1 = [items objectAtIndex:n];
			PackingItem *item2 = [items objectAtIndex:n+1];
			NSString *name1 = item1.name;
			NSString *name2 = item2.name;
			if([name1 compare: name2] == NSOrderedAscending){
				PackingItem *tempItem = [item1 retain];
				[predefinedItems replaceObjectAtIndex:n withObject:item2];
				[predefinedItems replaceObjectAtIndex:n+1 withObject:tempItem];
				
			}
			
		}
	}
}

-(void)reorderListByAsc{
	
	NSMutableArray *items = predefinedItems;
	
	for(int m = 0 ; m < [items count]; m++){
		for(int n = 0 ; n < [items count]-m-1; n++){
			PackingItem *item1 = [items objectAtIndex:n];
			PackingItem *item2 = [items objectAtIndex:n+1];
			NSString *name1 = item1.name;
			NSString *name2 = item2.name;
			if([name1 compare: name2] == NSOrderedDescending){
				PackingItem *tempItem = [item1 retain];
				[predefinedItems replaceObjectAtIndex:n withObject:item2];
				[predefinedItems replaceObjectAtIndex:n+1 withObject:tempItem];
			}
		}
	}
}

- (BOOL) checkSelectedCount {
	for (PackingItem *item in predefinedItems) {
		if (item.selected) {
			return YES;
		}
	}
	return NO;
}

/*
 1. save selected items and delete unselected items
 2. Set new item array to current list
 3. update the checked count and total count
 4. Save the data to db
 
 */
- (void) saveSelectedItemsToDB {
	NSMutableArray *newItemArray = [NSMutableArray arrayWithCapacity:0];
    //	if (!isFromCategory){
    //		for (PackingItem *item in currentList.itemArray) {
    //			if (item.selected) {
    //				item.listId	 = currentList.pk;
    //				[item saveData];
    //				if(![newItemArray containsObject:item])
    //				[newItemArray addObject:item];
    //			} else {
    //				[item deleteData];
    //			}
    //		}
    //
    //	} else {
    //	[currentList loadItemsIfNeed];
    // TODO: 这里的逻辑有问题，currentList应该考虑保存2级层次进去
    // TODO: 目前的做法，暂时用1级列表代替，但是不考虑有重复项的情况
	
    [currentList.itemArray removeAllObjects];
    [currentList loadItemsIfNeed];
    [newItemArray addObjectsFromArray:currentList.itemArray];
    
    for(PackingItem *item in predefinedItems){
        
        NSInteger pos = [self indexOfItem:item fromArray:newItemArray];
        if (item.selected) {
            /*if ([self.category length] > 0)
             item.custom = self.category;*/
            item.listId	 = currentList.pk;
            if(pos == NSNotFound) {
                [item saveData];
                [newItemArray addObject:item];
            }
        } else if (pos != NSNotFound) {
            [newItemArray removeObjectAtIndex:pos];
        }
    }
    //	}
	//rNSLog(@"PackingItem total count = %d", [[PackingItem findAll] count]);
    
	currentList.itemArray = newItemArray;
	[currentList resetItems];
	[currentList updateCheckStatus];
	[currentList updateDataWithoutIconAndPhoto];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [layoutManager translateToPortraitForView:self.view withAnimation:NO];
        //[editView setFrame:CGRectMake(0,SCREEN_HEIGHT-150,SCREEN_WIDTH, editView.frame.size.height)];
        
    }
    else
    {
        [layoutManager translateToLandscapeForView:self.view withAnimation:NO];
        //[editView setFrame:CGRectMake(0,SCREEN_WIDTH-150,SCREEN_HEIGHT, editView.frame.size.height)];
    }
    
	if (!isSearchFromCategory && !isBabyPacking)
	{
		[predefinedItems removeAllObjects];
		//	[self loadPredefinedItems];
		
		BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
		[appDelegate.dataManager getItemByCategoty:currentCategory.pk];
		int i = 0;
		for (int j = 0; j < appDelegate.itemsFromDB.count; j++)
		{
			Items *itemDB = [appDelegate.itemsFromDB objectAtIndex:j];
			i++;
			PackingItem *item = [[PackingItem alloc] init];
			item.pk = itemDB.pk;
			item.name = itemDB.name;
			item.tip = itemDB.tip;
			item.qty = itemDB.qty;
			item.custom = itemDB.custom;
			//item.category = itemDB.custom;
			//item.predefineId = list.predefineId + i;
			item.predefineForAge = currentList.age;
			
			BOOL flag = NO;
			for (int x = 0; x < self.predefinedItems.count; x++)
			{
				PackingItem *it1 = [self.predefinedItems objectAtIndex:x];
				if ([it1.name isEqualToString:item.name])
					flag = YES;
			}
			
			if (!flag)
				[self.predefinedItems addObject:item];
			
			[item release];
		}
	}
	
	[currentList loadItemsIfNeed];
	for (PackingItem *item in predefinedItems){
		for(PackingItem *item2 in currentList.itemArray)
			
			if ([item.name isEqualToString:item2.name] ){
				item.selected = YES;
				break;
			}
		
			else
				item.selected = NO;
	}
	
	[self reorderListByAsc];
	[myTableView reloadData];
}

/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (PackingItem*) itemAtIndexPath:(NSIndexPath*)indexPath {
	PackingItem *item = nil;
	if (search) {
        if(indexPath.row < [filteredItemArray count])
            item = [filteredItemArray objectAtIndex:indexPath.row];
	} else {
        //		if(!isFromCategory){
        //			item = [currentList.itemArray objectAtIndex:indexPath.row];
        //		}else {
        if(indexPath.row < [predefinedItems count])
			item = [predefinedItems objectAtIndex:indexPath.row];
    }
    //	}
	return item;
}

- (NSInteger) indexOfItem:(PackingItem *)item fromArray:(NSArray *)array {
    
	for (int i=0; i<[array count]; i++) {
		PackingItem *temp = [array objectAtIndex:i];
		if ([temp.name isEqualToString:item.name]) {
			return i;
		}
	}
	
	return NSNotFound;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (search) {
		if (filteredItemArray) {
			NSLog(@"row count= %d", [filteredItemArray count]);
			return [filteredItemArray count];
		}
	} else {
        //		if (predefinedItems ==nil && currentList) {
        //			NSLog(@"row count= %d", [currentList.itemArray count]);
        //			return [currentList.itemArray count];
        //		}else{
        NSLog(@"row count= %d", [predefinedItems count]);
        return [predefinedItems count];
        //		}
	}
	return 0;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"CSTableViewCell";
    
    CSTableViewCell *cell = (CSTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
		cell = [[[CSTableViewCell alloc] initWithCSStyle:CSTableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
        
		cell.detailTextLabel.textColor = [UIColor darkGrayColor];
		cell.detailTextLabel.textAlignment = UITextAlignmentLeft;
        //  cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure.png"]];
        // cell.accessoryView.userInteractionEnabled = YES;
    }
    
    NSLog(@"indexPath  %d",indexPath.row);
    PackingItem *item = [self itemAtIndexPath:indexPath];
	//NSLog(@"item   predefineId = %d", item.predefineId);
	NSString *customFontName = [appDelegate getCustomFontName];
	
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
		cell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];
	}
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
		cell.textLabel.textColor = DEF_COLOR;
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		cell.textLabel.textColor = theColour1;
	}
	// and finally set the colour of your label
	
	cell.textLabel.text = item.name;
	if ([item hasTip])
        //cell.detailTextLabel.text = [NSString stringWithFormat:@"*x%@",item.qty];
        
		cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",item.qty];
	else if( item.qty && [item.qty length] > 0)
        //cell.detailTextLabel.text = [NSString stringWithFormat:@"*x%@",item.qty];
        
		cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", item.qty];
	else
		cell.detailTextLabel.text = nil;
	//cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.accessoryType = UITableViewCellAccessoryDetailButton;
	if (item.selected) {
		cell.imageView.image = checkImg;
	} else {
		cell.imageView.image = uncheckImg;
	}
    
    cell.imageView.frame = CGRectMake(8, 12, 18, 19);
	
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
    UIColor *theColour111;
    // read the data back from the user defaults
    NSData *data1111= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
    // check whether you got anything
    if(data1111 == nil) {
        // use this to set the colour the first time your app runs
    } else {
        // this recreates the colour you saved
        theColour111 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data1111];
        cell.backgroundColor = theColour111;
    }
    // and finally set the colour of your label
    
    
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
    UIColor *theColour21;
    // read the data back from the user defaults
    NSData *data211= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
    // check whether you got anything
    if(data211 == nil) {
        // use this to set the colour the first time your app runs
    } else {
        // this recreates the colour you saved
        theColour21 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data211];
        cell.textLabel.textColor = theColour21;
    }
    // and finally set the colour of your label
    BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
    NSString *customFontName1 = [appDelegate getCustomFontName];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
        cell.textLabel.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];
    }
    else{
        cell.textLabel.font=[UIFont fontWithName:customFontName1 size:15];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
	if (appDelegate.soundOn) {
		[player1 play];
	}
    
	
	PackingItem *item = [self itemAtIndexPath:indexPath];
	
	[item reverseSelectStatus];
	[self saveSelectedItemsToDB];
	[tableView reloadData];
	
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    // So all the items can to be modified~~
	//PackingItem *item = [self itemAtIndexPath:indexPath];
	//if ([item isPredefine]) { // not allow to modify predefined items
    //		DetailItemViewController *controller = [[DetailItemViewController alloc] initWithNibName:@"DetailItemViewController" bundle:nil];
    //		controller.packingItem = item;
    //		[self.navigationController pushViewController:controller animated:YES];
    //		[controller release];
    //	} else {
	AddItemViewController *controller = nil;
	
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		controller = [[AddItemViewController alloc] initWithNibName:@"AddItemViewController" bundle:nil];
	else
		controller = [[AddItemViewController alloc] initWithNibName:@"AddItemViewController1" bundle:nil];
	
	//[[AddItemViewController alloc] initWithNibName:@"AddItemViewController" bundle:nil];
	controller.editingItem = [self itemAtIndexPath:indexPath];
	//NSLog(@"editing item -- %@",controller.editingItem.name);
	controller.currentList = currentList;
	controller.indexth = indexPath.row;
	if (TARGET_VERSION == VERSION_BABY_PACKING){
		controller.itemsInApp = babyPredefinedItems;
	}
	else{
		NSMutableArray *itemInApp = [self.categoryListInAppData objectForKey:K_LIST_ITEMS];
		controller.itemsInApp = itemInApp;
	}
    
    controller.itemArray = predefinedItems;
	[self.navigationController pushViewController:controller  animated:YES];
	[controller release];
}


// Override to support conditional editing of the table view.
/*- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 PackingItem *item = [self itemAtIndexPath:indexPath];
 if ([item.custom isEqualToString:@"FromCustom"])
 return YES;
 else
 return NO;
 }*/

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		
		index = indexPath.row;
		
		if (item5 != nil)
		{
			[item5 release];
			item5 = nil;
		}
		
		item5 = [self itemAtIndexPath:indexPath];
		[item5 retain];
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"If you delete this item then it will also be deleted from your custom packing list(s) where it exist. Do you want to delete?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO",nil];
		[alert show];
		[alert release];
	}
	
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
	{
		// Delete the row from the data source
		
		if ([item5.custom length] > 0) {
			[item5 deleteByName];
			NSMutableArray *itemsList = nil;
			if (isFromCategory == NO)
			{
				itemsList = babyPredefinedItems;
				[itemsList removeObjectAtIndex:index];
				[predefinedItems removeObjectAtIndex:index];
			}
			else
			{
				itemsList = [categoryListInAppData objectForKey:K_LIST_ITEMS];
				BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate *)[[UIApplication sharedApplication] delegate];
				Items *itemDB = [[Items alloc] init];
				itemDB.pk = item5.pk;
				itemDB.name = item5.name;
				itemDB.tip = item5.tip;
				itemDB.qty = item5.qty;
				itemDB.checked = 0;
				itemDB.deleted = 1;
				if (TARGET_VERSION == VERSION_BABY_PACKING)
					itemDB.custom = [NSString stringWithFormat:@"BabyItem"];
				else
					itemDB.custom = self.category;
				
				[appDelegate.dataManager updateItem:itemDB];
				[predefinedItems removeObjectAtIndex:index];
			}
			
			[myTableView reloadData];
			/*if ([[[UIDevice currentDevice].systemVersion substringToIndex:2] isEqualToString:@"2."])
             [myTableView reloadData];*/
		}
		/*if (![[[UIDevice currentDevice].systemVersion substringToIndex:2] isEqualToString: @"2."])
         [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];*/
	}
}


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText
{
	//NSLog(@"filterContentForSearchText = %@", searchText);
	/*
	 Update the filtered array based on the search text and scope.
	 */
	
	[self.filteredItemArray removeAllObjects]; // First clear the filtered array.
	
	/*
	 Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
	 */
    //	if(predefinedItems == nil){
    //		for (PackingItem *item in currentList.itemArray)
    //		{
    //
    //			//		NSComparisonResult result = [item.name compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length])];
    //			NSRange result = [item.name rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
    //			if (result.location != NSNotFound)
    //			{
    //				[self.filteredItemArray addObject:item];
    //			}
    //
    //		}
    //	}else{
    for (PackingItem *item in predefinedItems)
    {
        
        //		NSComparisonResult result = [item.name compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length])];
        NSRange result = [item.name rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
        if (result.location != NSNotFound)
            [self.filteredItemArray addObject:item];
    }
    //}
	
	//NSLog(@"filterContentForSearchText >> filteredItemArray count = %d", [filteredItemArray count]);
}


#pragma mark -

#pragma mark UISearchBarDelegate Delegate Methods
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	//NSLog(@"textDidChange");
	if (isSearchFromCategory)
		search = YES;
	[self filterContentForSearchText:searchText];
	[myTableView reloadData];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
	//NSLog(@"searchBarTextDidEndEditing");
	[myTableView reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
	//NSLog(@"searchBarCancelButtonClicked");
	
	if (isSearchFromCategory) {
		search = NO;
		[self.navigationController popViewControllerAnimated:YES];
		//return;
	}
	search = NO;
	[self hideSearchBar];
    
	[searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	//NSLog(@"searchBarSearchButtonClicked");
	[searchBar resignFirstResponder];
	[self filterContentForSearchText:searchBar.text];
	[myTableView reloadData];
}

- (void) showSearchBar {
    mySearchBar.frame = CGRectMake(0, 0, myTableView.frame.size.width, 44);
    if (IOS_NEWER_THAN(7))
    {
        mySearchBar.frame = CGRectMake(0, 65, myTableView.frame.size.width, 44);
    }
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationRepeatCount:0];
	
	mySearchBar.alpha = 1.0;
	//editView.alpha = 0.0;
	myTableView.frame = CGRectMake(0, 65, myTableView.frame.size.width, myTableView.frame.size.height - 65);
	[mySearchBar becomeFirstResponder];
	
	[UIView commitAnimations];
}

- (void) hideSearchBar {
	mySearchBar.frame = CGRectZero;
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationRepeatCount:0];
	
	mySearchBar.alpha = 0.0;
	myTableView.frame = CGRectMake(0, 65, myTableView.frame.size.width, myTableView.frame.size.height + 65);
	//editView.alpha = 1.0;
	
	[UIView commitAnimations];
}

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{            return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    UIWindow *window = [[[UIApplication sharedApplication] windows]objectAtIndex:0];
    
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        
        [layoutManager translateToPortraitForView:self.view withAnimation:YES];
        //        [editView setFrame:CGRectMake(0,SCREEN_HEIGHT-150,SCREEN_WIDTH, editView.frame.size.height)];
        //         NSLog(@"%f,editView%@",SCREEN_HEIGHT,editView.description);
        //        NSLog(@"Window Portrait Height = %f ",window.frame.size.width);
        
    }
    else
    {
        [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
        //       [editView setFrame:CGRectMake(0,SCREEN_HEIGHT-150,SCREEN_WIDTH, editView.frame.size.height)];
        //       NSLog(@"%f,editView%@",SCREEN_HEIGHT,editView.description);
        //		 [self.view addSubview:editView];
        //       NSLog(@"Window Landscape Height = %f ",window.frame.size.width);
        
    }
    // }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}



#pragma mark - upload in iCloud

-(void)btnUpload
{
    
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    
    // Create XML and pass to iCloud
    NSString *xmlStr=@"<Item>";
    NSString *select_query;
    const char *select_stmt;
    sqlite3_stmt *compiled_stmt;
    sqlite3 *dbFamilyPacking;
    if (sqlite3_open([[mAppDelegate getDBListPath] UTF8String], &dbFamilyPacking) == SQLITE_OK)
    {
        select_query = [NSString stringWithFormat:@"SELECT * FROM item"];
        select_stmt = [select_query UTF8String];
        if(sqlite3_prepare_v2(dbFamilyPacking, select_stmt, -1, &compiled_stmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiled_stmt) == SQLITE_ROW)
            {
                NSString *addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,0)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<Data><pk>%@</pk>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,1)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<name>%@</name>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,2)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<qty>%@</qty>",addr];
                
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,3)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<tip>%@</tip>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,4)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<selected>%@</selected>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,5)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<custom>%@</custom>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,6)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<checked>%@</checked>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,7)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<listId>%@</listId>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,8)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<predefineId>%@</predefineId>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,9)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<predefineForAge>%@</predefineForAge>",addr];
                
                
                xmlStr =[xmlStr stringByAppendingString:@"</Data>"];
            }
            
            xmlStr =[xmlStr stringByAppendingString:@"</Item>"];
            
            sqlite3_finalize(compiled_stmt);
            
            appDelegate.notesItem = xmlStr;
            // Notify the previouse view to save the changes locally
            [[NSNotificationCenter defaultCenter] postNotificationName:@"New Note List Item" object:self userInfo:[NSDictionary dictionaryWithObject:xmlStr forKey:@"NoteItem"]];
        }
    }
}

@end


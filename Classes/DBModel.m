//
//  DBModel.m
//  BabyPacking
//
//  Created by Gary He on 5/31/09.
//  Copyright 2009 test. All rights reserved.
//

#import "DBModel.h"

// Static variables for compiled SQL queries. This implementation choice is to be able to share a one time
// compilation of each query across all instances of the class. Each time a query is used, variables may be bound
// to it, it will be "stepped", and then reset for the next usage. When the application begins to terminate,
// a class method will be invoked to "finalize" (delete) the compiled queries - this must happen before the database
// can be closed.


@implementation DBModel
@synthesize pk;
@synthesize dirty;
@synthesize hydrated;

// Finalize (delete) all of the SQLite compiled queries.
+ (void)finalizeStatements {
	
}

- (id)initWithPK:(NSInteger)primaryKey {
	if (self = [super init]) {
		if (conn == nil) {
			conn = [DBConnection sharedConnection];
		}
		hydrated = NO;
		dirty = YES;
	}
	return self;
}

- (id) init {
	if (self = [super init]) {
		if (conn == nil) {
			conn = [DBConnection sharedConnection];
		}
		hydrated = NO;
		dirty = YES;
	}
	
	return self;
}

- (void)dealloc {
    [super dealloc];
}

- (void)setPK:(NSInteger)aValue {
    if (pk == aValue) return;
    dirty = YES;
    pk = aValue;
}

- (void)insertData {
	
}
- (void)updateData {
	
}
- (void)deleteData {
	
}

- (void)saveData {
	if (pk == INVALID_PK) {
		[self insertData];
	} else {
		[self updateData];
	}
}

+ (NSMutableArray*) findAll {
	return [NSMutableArray arrayWithCapacity:0];
}

@end

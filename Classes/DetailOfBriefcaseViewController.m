//
//  DetailOfBriefcaseViewContronller.m
//  BabyPacking
//
//  Created by eve on 9/7/09.
//  Copyright 2009 test. All rights reserved.
//

#import "DetailOfBriefcaseViewController.h"
#import "BabyPackingAppDelegate.h"
#import "Constant.h"

#define TAG_TEXTFIELD_BASE			-40
#define COUNT_BRIEFCASE_FIELDS		17

@interface DetailOfBriefcaseViewController(Private)
- (UITableViewCell *)getTopCell;
- (UITextField *)getTextFieldFromCell:(UITableViewCell *)cell;
@end

@implementation DetailOfBriefcaseViewController

@synthesize briefcaseItem;
//@synthesize textFieldArray;
@synthesize textArray;
//@synthesize labelArray;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
    	[super viewDidLoad];
    
	appDelegate=(BabyPackingAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	currentTextField = nil;
	
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor whiteColor];
    

    
	NSString *customFontName = [appDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];		
	}
	else {
		label.font=[UIFont fontWithName:customFontName size:fontSize];		
	}
	
	[label sizeToFit];
	
	
	//label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
            label.textColor=[UIColor whiteColor];
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label.textColor = theColour1;
	}
	// and finally set the colour of your label
	
	self.navigationItem.titleView = label;	
	
	if (briefcaseItem){
		label.text = @"Briefcase Details";
		[label sizeToFit];
		isNewItem = NO;
	}else{
		label.text = @"Add New Briefcase";
		[label sizeToFit];
		isNewItem = YES;
	}
	
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 30, 30);
    [btn setImage:[UIImage imageNamed:@"Save.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    saveButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = saveButton;
    
    
//	self.navigationItem.rightBarButtonItem = saveButton;
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(0, 0, 30, 30);
    [btn2 setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cancelButton = [[UIBarButtonItem alloc] initWithCustomView:btn2];
    self.navigationItem.leftBarButtonItem = cancelButton;
    
//	self.navigationItem.leftBarButtonItem = cancelButton;
	
	//self.textFieldArray = [NSMutableArray arrayWithCapacity:16];
	//for (int i = 0;i<17;i++){
	//self.textFieldArray = [NSMutableArray arrayWithCapacity:0];
	self.textArray = [NSMutableArray arrayWithCapacity:0];
	for (int i = 0;i<COUNT_BRIEFCASE_FIELDS;i++){
		
		NSString* text = [NSString stringWithString:@""];
		if (briefcaseItem) {
			if (i ==0) {
				text = briefcaseItem.title;
			} else if (i == 1) {
				text = briefcaseItem.passport;
			} else if (i == 2){
				text = briefcaseItem.license;
			}else if (i == 3) {
				text = briefcaseItem.ssn;
			} else if (i == 4){
				text = briefcaseItem.id_card;
			}else if (i == 5) {
				text = briefcaseItem.airline_name;
			} else if (i == 6){
				text = briefcaseItem.flight;
			}else if (i == 7) {
				text = briefcaseItem.frequent_flyer;
			} else if (i == 8){
				text = briefcaseItem.car_name;
			}else if (i == 9) {
				text = briefcaseItem.license_plate;
			} else if (i == 10){
				text = briefcaseItem.hotel_name;
			}else if (i == 11) {
				text = briefcaseItem.hotel_phone;
			} else if (i == 12){
				text = briefcaseItem.hotel_address;
			}else if (i == 13) {
				text = briefcaseItem.embassy_name;
			} else if (i == 14){
				text = briefcaseItem.embassy_address;
			}else if (i == 15) {
				text = briefcaseItem.embassy_emergency;
			} else if (i == 16) {
				text = briefcaseItem.other;
			}
		}
		[textArray addObject:text];
	}
		
		/*
		 UITextField * textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 6, 300, 25)];
		textField.clearsOnBeginEditing = NO;
		[textField setDelegate:self];
		if (i == 16)
			textField.returnKeyType = UIReturnKeyDone;
		else
			textField.returnKeyType = UIReturnKeyNext;
		[textField addTarget:self action:@selector(textFieldDone) forControlEvents:UIControlEventEditingDidEndOnExit];
		if (briefcaseItem) {
			if (i ==0) {
				textField.text = briefcaseItem.title;
			} else if (i == 1) {
				textField.text = briefcaseItem.passport;
			} else if (i == 2){
				textField.text = briefcaseItem.license;
			}else if (i == 3) {
				textField.text = briefcaseItem.ssn;
			} else if (i == 4){
				textField.text = briefcaseItem.id_card;
			}else if (i == 5) {
				textField.text = briefcaseItem.airline_name;
			} else if (i == 6){
				textField.text = briefcaseItem.flight;
			}else if (i == 7) {
				textField.text = briefcaseItem.frequent_flyer;
			} else if (i == 8){
				textField.text = briefcaseItem.car_name;
			}else if (i == 9) {
				textField.text = briefcaseItem.license_plate;
			} else if (i == 10){
				textField.text = briefcaseItem.hotel_name;
			}else if (i == 11) {
				textField.text = briefcaseItem.hotel_phone;
			} else if (i == 12){
				textField.text = briefcaseItem.hotel_address;
			}else if (i == 13) {
				textField.text = briefcaseItem.embassy_name;
			} else if (i == 14){
				textField.text = briefcaseItem.embassy_address;
			}else if (i == 15) {
				textField.text = briefcaseItem.embassy_emergency;
			} else if (i == 16) {
				textField.text = briefcaseItem.other;
			}
		}
//		else
//			textField.text = [NSString stringWithFormat:@"text%d",i];
		textField.tag = TAG_TEXTFIELD_BASE + i;
		[textFieldArray addObject:textField];
		[textField release];
	}
	UITextField *textField = [textFieldArray objectAtIndex:0];
	[textField becomeFirstResponder];
		 */

	mTableView.tableFooterView = footerTableView;
	UITableViewCell* topCell = [self getTopCell];
	if (!topCell) return;
	UITextField* textField = [self getTextFieldFromCell:topCell];
	if (!textField) return;
	[textField becomeFirstResponder];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
	[mTableView release];
	[saveButton release];
	[cancelButton release];
	[footerTableView release]; 
	[briefcaseItem release];
//	[labelArray release];
	//[textFieldArray release];
	[textArray release];
}

#pragma mark Private methods implementation

- (UITableViewCell *)getTopCell {
	
	UITableViewCell* topCell = nil;
	NSInteger min = 0;
	
	NSArray* cells = [mTableView visibleCells];
	for (UITableViewCell* cell in cells) {
		if (!topCell) {
			topCell = cell;
			min = cell.tag;
			continue;
		}
		
		NSInteger tag = cell.tag;
		
		if (tag < min) {
			min = tag;
			topCell = cell;
		}
	}
	
	return topCell;
}

- (UITextField *)getTextFieldFromCell:(UITableViewCell *)cell {
	
	NSArray* views = cell.contentView.subviews;
	UITextField* textField = nil;
	for (UIView* view in views) {
		if ([[view class] isSubclassOfClass:[UITextField class]]) {
			textField = (id)view;
			break;
		}
	}
	
	return textField;
}

- (void) buttonAction:(id) sender {
	if (sender == cancelButton.customView){
		[self.navigationController popViewControllerAnimated:YES];
	}else if (sender == saveButton.customView){
		
		[currentTextField resignFirstResponder];
		NSString *name = [textArray objectAtIndex:0];//[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
		if (![name length] > 0) {
			BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
			[appDelegate showAlert:M_INPUT_NAME_TITLE message:M_INPUT_NAME];
			return;
		}
		
		if (!briefcaseItem) {
			isNewItem = YES;
			BriefcaseItem *item = [[BriefcaseItem alloc] init];
			self.briefcaseItem = item;
			[item release];
		} else {
			isNewItem = NO;
		}
		
		//briefcaseItem.pk =  .pk;
		for (int i = 0;i<[textArray count];i++){
			//UITextField * textField = [textFieldArray objectAtIndex:i];
			NSString* text = [textArray objectAtIndex:i];
			if (i ==0) {
				briefcaseItem.title = text;
			} else if (i == 1) {
				briefcaseItem.passport = text;
			} else if (i == 2){
				briefcaseItem.license = text;
			}else if (i == 3) {
				briefcaseItem.ssn = text;
			} else if (i == 4){
				briefcaseItem.id_card = text;
			}else if (i == 5) {
				briefcaseItem.airline_name = text;
			} else if (i == 6){
				briefcaseItem.flight = text;
			}else if (i == 7) {
				briefcaseItem.frequent_flyer = text;
			} else if (i == 8){
				briefcaseItem.car_name = text;
			}else if (i == 9) {
				briefcaseItem.license_plate = text;
			} else if (i == 10){
				briefcaseItem.hotel_name = text;
			}else if (i == 11) {
				briefcaseItem.hotel_phone = text;
			} else if (i == 12){
				briefcaseItem.hotel_address = text;
			}else if (i == 13) {
				briefcaseItem.embassy_name = text;
			} else if (i == 14){
				briefcaseItem.embassy_address = text;
			}else if (i == 15) {
				briefcaseItem.embassy_emergency = text;
			} else if (i == 16) {
				briefcaseItem.other = text;
			}
		}    
			
		if (isNewItem) {
			[briefcaseItem insertData];
			[self.navigationController popViewControllerAnimated:YES];
		} else {
			[briefcaseItem updateData];
			[self.navigationController popViewControllerAnimated:YES];
		}
	}
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return [textArray count];
}
//
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	NSString *title = nil;
	if (section == 0) {
		title = TITLE_BRIEFCASE_NAME;
	} else if (section == 1){
		title = TITLE_BRIEFCASE_PASSPORT;
	} else if (section == 2){
		title = TITLE_BRIEFCASE_LICENSE;
	} else if (section == 3){
		title = TITLE_BRIEFCASE_SOCIAL_SECURITY;
	} else if (section == 4){
		title = TITLE_BRIEFCASE_ID;
	} else if (section == 5){
		title = TITLE_BRIEFCASE_AIRLINE_NAME;
	} else if (section == 6){
		title = TITLE_BRIEFCASE_FLIGHT;
	} else if (section == 7){
		title = TITLE_BRIEFCASE_FREQUENT_FLYER;
	} else if (section == 8){
		title = TITLE_BRIEFCASE_CAR;
	} else if (section == 9){
		title = TITLE_BRIEFCASE_PLATE;
	} else if (section == 10){
		title = TITLE_BRIEFCASE_HOTEL_NAME;
	} else if (section == 11){
		title = TITLE_BRIEFCASE_HOTEL_PHONE;
	} else if (section == 12){
		title = TITLE_BRIEFCASE_HOTEL_ADDRESS;
	} else if (section == 13){
		title = TITLE_BRIEFCASE_EMBASSY_NAME;
	} else if (section == 14){
		title = TITLE_BRIEFCASE_EMBASSY_ADDRESS;
	} else if (section == 15){
		title = TITLE_BRIEFCASE_EMERGENCY;
	} else if (section == 16){
		title = TITLE_BRIEFCASE_OTHER;
	} else 
		title = @"";

	return title;
}
// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 1;
}

/*- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)] autorelease];

    [headerView setBackgroundColor:[UIColor lightGrayColor]];
    return headerView;
}*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 45;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// TODO: 这里需要修改实现，频繁的addSubview和removeFromSuperview容易出问题
	static NSString *CellIdentifier = @"Briefcase cell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	UITextField *textField = nil;
	if (cell == nil) {
		
		cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		textField = [[UITextField alloc] initWithFrame:CGRectMake(10, 6, 300, 25)];
		textField.clearsOnBeginEditing = NO;
		[textField setDelegate:self];
		[textField addTarget:self action:@selector(textFieldDidEnter:) forControlEvents:UIControlEventEditingDidBegin];
		[textField addTarget:self action:@selector(textFieldDidEnd:) forControlEvents:UIControlEventEditingDidEnd];
        textField.backgroundColor = [UIColor whiteColor];
        
        if (indexPath.section == 0) {
            textField.placeholder = [NSString stringWithFormat:@"Enter %@...",TITLE_BRIEFCASE_NAME];
        } else if (indexPath.section == 1){
             textField.placeholder = [NSString stringWithFormat:@"Enter %@...",TITLE_BRIEFCASE_PASSPORT];
        } else if (indexPath.section == 2){
             textField.placeholder = [NSString stringWithFormat:@"Enter %@...",TITLE_BRIEFCASE_LICENSE];
        } else if (indexPath.section == 3){
             textField.placeholder = [NSString stringWithFormat:@"Enter %@...",TITLE_BRIEFCASE_SOCIAL_SECURITY];
        } else if (indexPath.section == 4){
             textField.placeholder = [NSString stringWithFormat:@"Enter %@...",TITLE_BRIEFCASE_ID];
        } else if (indexPath.section == 5){
             textField.placeholder = [NSString stringWithFormat:@"Enter %@...",TITLE_BRIEFCASE_AIRLINE_NAME];
        } else if (indexPath.section == 6){
             textField.placeholder = [NSString stringWithFormat:@"Enter %@...",TITLE_BRIEFCASE_FLIGHT];
        } else if (indexPath.section == 7){
             textField.placeholder = [NSString stringWithFormat:@"Enter %@...",TITLE_BRIEFCASE_FREQUENT_FLYER];
        } else if (indexPath.section == 8){
             textField.placeholder = [NSString stringWithFormat:@"Enter %@...",TITLE_BRIEFCASE_CAR];
        } else if (indexPath.section == 9){
             textField.placeholder = [NSString stringWithFormat:@"Enter %@...",TITLE_BRIEFCASE_PLATE];
        } else if (indexPath.section == 10){
             textField.placeholder = [NSString stringWithFormat:@"Enter %@...",TITLE_BRIEFCASE_HOTEL_NAME];
        } else if (indexPath.section == 11){
             textField.placeholder = [NSString stringWithFormat:@"Enter %@...",TITLE_BRIEFCASE_HOTEL_PHONE];
        } else if (indexPath.section == 12){
             textField.placeholder = [NSString stringWithFormat:@"Enter %@...",TITLE_BRIEFCASE_HOTEL_ADDRESS];
        } else if (indexPath.section == 13){
             textField.placeholder = [NSString stringWithFormat:@"Enter %@...",TITLE_BRIEFCASE_EMBASSY_NAME];
        } else if (indexPath.section == 14){
             textField.placeholder = [NSString stringWithFormat:@"Enter %@...",TITLE_BRIEFCASE_EMBASSY_ADDRESS];
        } else if (indexPath.section == 15){
             textField.placeholder = [NSString stringWithFormat:@"Enter %@...",TITLE_BRIEFCASE_EMERGENCY];
        } else if (indexPath.section == 16){
             textField.placeholder = [NSString stringWithFormat:@"Enter %@...",TITLE_BRIEFCASE_OTHER];
        } else 
            textField.placeholder = @"";
        
		[cell.contentView addSubview:textField];
      //  cell.contentView.backgroundColor= [UIColor grayColor];
       cell.contentView.backgroundColor=[UIColor colorWithRed:240 green:241 blue:243 alpha:0.40];
		[textField release];
		
	}else {
		
		NSArray* views = cell.contentView.subviews;
		for (UIView* view in views) {
			if ([[view class] isSubclassOfClass:[UITextField class]]) {
				textField = (id)view;
				break;
			}
		}
		/*
		//NSLog(@"tag = %d", cell.tag);
		NSInteger index = cell.tag - TAG_TEXTFIELD_BASE;
		UITextField * textField = [textFieldArray objectAtIndex:index];
		// Remove if the textField is really on the cell
		if ([cell.contentView.subviews indexOfObject:textField] != NSNotFound) {
			[textField removeFromSuperview]; 
		}
		 */
	}
    
	if (!textField) NSLog(@"TextField = nil!");
	textField.text = [textArray objectAtIndex:indexPath.section];
	if (indexPath.section == 16)
		textField.returnKeyType = UIReturnKeyDone;
	else
		textField.returnKeyType = UIReturnKeyNext;
	textField.tag = indexPath.section;
	cell.tag = indexPath.section;
	
//	if ([textField canBecomeFirstResponder])
//		[textField becomeFirstResponder];
	/*
	UITextField * textField = [textFieldArray objectAtIndex:indexPath.section];
	[cell.contentView addSubview:textField];
	cell.tag = TAG_TEXTFIELD_BASE + indexPath.section;
	 */
		
	return cell;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	[currentTextField resignFirstResponder];
	currentTextField = nil;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	
	UITableViewCell* topCell = [self getTopCell];
	if (!topCell) return;
	NSInteger section = topCell.tag;
	NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:section];
	[mTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
	
	UITextField* textField = [self getTextFieldFromCell:topCell];
	if (!textField) return;
	[textField becomeFirstResponder];
}

//- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
//	
//	UITableViewCell* topCell = [self getTopCell];
//	if (!topCell) return;
//	UITextField* textField = [self getTextFieldFromCell:topCell];
//	if (!textField) return;
//	[textField becomeFirstResponder];
//}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	
//	for (int i = 0; i<16; i++){
//		UITextField *text = [textFieldArray objectAtIndex:i];
//		UITextField *nextText =[textFieldArray objectAtIndex:i+1];
//		if(textField == text){
//			[textField resignFirstResponder];
//			[nextText becomeFirstResponder];
//		}
//	}
	NSInteger section = textField.tag;
	if (section < 0 || section > [textArray count]-1) return NO;
	
	NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:section];
	[textField resignFirstResponder];
	[mTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
	

//	NSString* text = [textArray objectAtIndex:section];
//	text = textField.text;
	
	UITableViewCell* cell = [mTableView cellForRowAtIndexPath:indexPath];
	indexPath = [NSIndexPath indexPathForRow:0 inSection:section+1];
	cell = [mTableView cellForRowAtIndexPath:indexPath];
	UITextField* nextTextField = [self getTextFieldFromCell:cell];
	if (!nextTextField) return NO;
	[nextTextField becomeFirstResponder];
	
	return YES;
}

- (IBAction)textFieldDidEnter:(id)sender {
	currentTextField = sender;
}

- (IBAction)textFieldDidEnd:(id)sender {
	
	UITextField* textField = sender;
	
	NSInteger tag = textField.tag;
	NSLog(textField.text);
	[textArray replaceObjectAtIndex:tag withObject:textField.text];
}

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
       // [layoutManager translateToPortraitForView:self.view withAnimation:YES];
    }
    else
    {
       // [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
    }
    // }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}


@end

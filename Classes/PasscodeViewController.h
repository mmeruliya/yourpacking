//
//  PasscodeViewController.h
//  BabyPacking
//
//  Created by eve on 9/9/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LayoutManager.h"

// TODO: 这个protocol用不到的话，应该删掉；或者应该重新思考自己的做法是否正确
//@protocol PasscodeViewControllerDelegate
//
//- (void)PasscodeViewDidSave;
//
//@end

//@class ExpenseCoreAppDelegate;

@interface PasscodeViewController : UIViewController<UIActionSheetDelegate,UITextFieldDelegate>{
	id mDelegate;
	NSString *passcode_typed;
	NSString *passcode;
	
    LayoutManager *layoutManager;
	IBOutlet UIBarButtonItem *button_cancel;
	IBOutlet UIBarButtonItem *button_save;
	IBOutlet UIButton *reset;
	IBOutlet UITextField *code_seg_one;
	IBOutlet UITextField *code_seg_two;
	IBOutlet UITextField *code_seg_three;
	IBOutlet UITextField *code_seg_four;
	IBOutlet UITextField *passcode_text;
	IBOutlet UILabel *lable_passcode;
	IBOutlet UIImageView *background_image;
	IBOutlet UITextField *textfield_none;
	
	BOOL isSaveClicked;
}

@property(nonatomic,assign) id/*<PasscodeViewControllerDelegate>*/ delegate;
@property(nonatomic,retain) NSString *passcode;

- (IBAction) valueDidChange:(id)sender;
- (IBAction) buttonSaveDidClick:(id)sender;
- (IBAction) buttonCancelDidClick:(id)sender;
-(IBAction) button_action:(id)sender;
-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex;
@end

//
//  CheckListViewController.h
//  BabyPacking
//
//  Created by Gary He on 5/21/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSHeaderView.h"
#import "ProgressBar.h"
#import "PackingList.h"
#import <AVFoundation/AVFoundation.h>
#import "LayoutManager.h"
#import "syncTask.h"

@class CSTableViewCell;
@class BabyPackingAppDelegate;

@interface CheckListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, AVAudioPlayerDelegate> {
	
	BabyPackingAppDelegate *appDelegate;
	LayoutManager *layoutManager;
    
	IBOutlet UIImageView *bgImageView;
	IBOutlet UIImageView *bgBorderView;
    
	IBOutlet UIButton *Donebtn;
    IBOutlet UIButton *Editbtn;
    
	IBOutlet UITableView *myTableView;
	IBOutlet UIBarButtonItem *doneButton;
	IBOutlet UIBarButtonItem *addButton;
	IBOutlet UIBarButtonItem *emailButton;
	IBOutlet UIBarButtonItem *resetButton;
	IBOutlet UIBarButtonItem *filterButton;
	
	IBOutlet UIBarButtonItem *reorderButton;
	IBOutlet UIBarButtonItem *reorderGroupButton;
	IBOutlet UIBarButtonItem *descButton;
	IBOutlet UIBarButtonItem *ascButton;
	IBOutlet UIBarButtonItem *editButton;
	IBOutlet UIBarButtonItem *copyButton;
	
	IBOutlet UIToolbar *editToolBar;
	
    IBOutlet UILabel *label;
    
	IBOutlet UILabel *statusLabel;
	IBOutlet ProgressBar *progressBar;
	
	IBOutlet UIImageView *progressImageView;
	RootViewController *homeController;
	UIImage *checkImg;
	UIImage *uncheckImg;
	
	UIImage *inImg;
	UIImage *outImg;
	
	UIImage *addImg;
	CSTableViewCell *movedCell;
	//UIImage *uncheckImg;
	
	
	NSMutableArray *uncheckedItemArray;
	NSMutableArray *sections;
	NSMutableArray *customListArray;
    NSMutableArray *predefinedItems;
    NSMutableArray *babyPredefinedItems;
    NSString *titleName;
    
	
	PackingList *currentList;
	BOOL showUncheckedItem;
	BOOL canEmail;
	BOOL editList; 
	BOOL edit;
	BOOL move;
	BOOL moveGroup;
	
	AVAudioPlayer *player1;
	AVAudioPlayer *player2;	
	
	UIViewController *rootController;
    NSString *controllerTitle;
   }

@property (strong) NSMetadataQuery *metadataQuery;
@property (strong) syncTask * doc;
@property (strong) NSMetadataQuery *query;

@property(nonatomic, retain) NSMutableArray *predefinedItems;
@property(nonatomic, retain) NSMutableArray *babyPredefinedItems;

@property(nonatomic, retain) NSMutableArray *uncheckedItemArray;
@property(nonatomic, retain) NSMutableArray *customListArray;
@property(nonatomic, retain) PackingList *currentList;
@property(nonatomic, retain) RootViewController *homeController;
@property(nonatomic, retain) NSMutableArray *sections;
@property(nonatomic) BOOL canEmail;
@property(nonatomic) BOOL editList;
@property(nonatomic) BOOL move;
@property(nonatomic) BOOL moveGroup;
@property(nonatomic) BOOL edit;
@property(nonatomic, retain) AVAudioPlayer *player1;
@property(nonatomic, retain) AVAudioPlayer *player2;
@property(nonatomic, retain) NSString *titleName;
@property (nonatomic, retain) UIViewController *rootController;
@property (nonatomic, retain) NSString *controllerTitle;

- (IBAction) buttonAction:(id)sender;
-(BOOL) groupItems;
-(void) reorderListByDesc;
-(void)reorderListByAsc;
- (void)loadNewArray;

- (void)saveWhenChecked;
- (id)initWithNibName:(NSString *)nibName bundle:(NSBundle *)nibBundle withObj:(PackingList *)listObj;

@end

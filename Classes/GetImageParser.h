//
//  GetImageParser.h
//  packngo
//
//  Created by Mehul Bhuva on 07/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLParserBase.h"

@interface GetImageParser : NSObject <NSXMLParserDelegate>
{
	BOOL mSuccessful;
	
	NSMutableDictionary* mData;
	NSMutableString* mCurrentString;
	NSMutableDictionary* mCurItem;
	NSMutableArray *mCurItemArray;
	NSMutableArray *userDetails;
	BOOL storingCharacters;
	id mDelegate;
	
	NSData *image_response_data;
}
@property (nonatomic, retain) NSData *image_response_data;
@property (nonatomic, retain) NSMutableString* mCurrentString;
@property (nonatomic, assign) BOOL successful;
@property (nonatomic, retain) NSMutableDictionary* data;
@property (nonatomic, retain) NSMutableDictionary* mCurItem;
@property (nonatomic, retain) NSMutableArray *mCurItemArray;
@property (nonatomic, retain) NSMutableArray *userDetails;
- (NSData *)initWithData:(NSString *)listid withUserId:(NSString *)userid;
//- (void)initFromStr:(NSString *)str;
- (void)displayInfo;
@end
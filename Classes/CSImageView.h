//
//  CSImageView.h
//  BabyPacking
//
//  Created by Gary He on 6/6/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CSImageView : UIImageView {
	id target;
	SEL action;
}

@property(nonatomic, assign) id target;
@property(nonatomic, assign) SEL action;

@end

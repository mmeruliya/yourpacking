//
//  SelectItemViewController.h
//  BabyPacking
//
//  Created by Gary He on 5/20/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "PackingList.h"
#import "LayoutManager.h"
#import "syncTask.h"

@class RootViewController;
@class PackingItem;
@class BabyPackingAppDelegate;

@interface SelectItemViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UIAlertViewDelegate> {
	
	BabyPackingAppDelegate *appDelegate;
	LayoutManager *layoutManager;
	IBOutlet UITableView *myTableView;
	
	IBOutlet UIImageView *bgImageView;
	
	IBOutlet UIBarButtonItem *doneButton;
	IBOutlet UIBarButtonItem *addButton;
	IBOutlet UIBarButtonItem *selectAllButton;
	IBOutlet UIBarButtonItem *clearAllButton;
	IBOutlet UIBarButtonItem *searchButton;
	
	IBOutlet UISearchBar *mySearchBar;
	BOOL search;
	BOOL isFromCategory;
	NSString *category;
	
	RootViewController *homeController;
	
	NSMutableArray *filteredItemArray;
	NSMutableArray *predefinedItems;
	NSMutableArray *babyPredefinedItems;
	NSMutableDictionary *categoryListInAppData;
	
	UIImage *checkImg;
	UIImage *uncheckImg;
	
	PackingList *currentList;
	BOOL editList;
	
	AVAudioPlayer *player1;
	
	IBOutlet UIView *editView;
	IBOutlet UIButton *delBtn;
	IBOutlet UIButton *sortBtn;
	IBOutlet UIButton *restoreBtn;
	
	PackingList *currentCategory;
	
	int index;
	PackingItem *item5;
	
	BOOL isSearchFromCategory;
	BOOL isBabyPacking;
    
    BOOL isEdit;
}
@property (strong) NSMetadataQuery *metadataQuery;
@property (strong) syncTask * doc;
@property (strong) NSMetadataQuery *query;

@property(nonatomic, retain) RootViewController *homeController;
@property(nonatomic, retain) NSMutableArray *filteredItemArray;
@property(nonatomic, retain) NSMutableArray *predefinedItems;
@property(nonatomic, retain) NSMutableArray *babyPredefinedItems;
@property(nonatomic, retain) NSMutableDictionary *categoryListInAppData;
@property(nonatomic, retain) PackingList *currentList;
@property(nonatomic, retain) NSString *category;
@property(nonatomic) BOOL editList,isFromCategory,isSearchFromCategory,isBabyPacking;
@property(nonatomic) BOOL isEdit;
@property (nonatomic, retain) PackingList *currentCategory;

@property(nonatomic, retain) AVAudioPlayer *player1;

- (IBAction) buttonAction:(id) sender;

-(void) reorderListByDesc;
-(void)reorderListByAsc;

@end

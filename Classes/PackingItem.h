//
//  PackingItem.h
//  BabyPacking
//
//  Created by Gary He on 5/20/09.
//  Copyright 2009 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBModel.h"
#import "BabyPacking_Prefix.pch"
#import "syncTask.h"

@interface PackingItem : DBModel {
	NSString *name;
	NSString *tip;
	NSString *qty;
	NSString *custom;
	BOOL selected;
	BOOL checked;
	NSInteger listId;
	NSInteger predefineId;
	NSInteger predefineForAge;
}

@property (strong) NSMetadataQuery *metadataQuery;
@property (strong) syncTask * doc;
@property (strong) NSMetadataQuery *query;

@property(nonatomic, copy) NSString *name;
@property(nonatomic, copy) NSString *tip;
@property(nonatomic, copy) NSString *qty;
@property(nonatomic, copy)NSString *custom;
@property(nonatomic) BOOL selected;
@property(nonatomic) BOOL checked;
@property(nonatomic) NSInteger listId;
@property(nonatomic) NSInteger predefineId;
@property(nonatomic) NSInteger predefineForAge;

- (BOOL) isPredefine;
- (BOOL) isChecked;
//- (void) checkItem;
//- (void) uncheckItem;
- (void) reverseCheckStatus;
- (BOOL) hasTip;
- (void) reverseSelectStatus;
+ (NSMutableArray*) findByListId:(NSInteger) listId;
+ (void) deleteByListId:(NSInteger) listId;
- (void) deleteByName;
- (void)deleteAllData;
- (void) deleteByCategory:(NSString *)str;
- (void) replaceCategoryName:(NSString *)str oldCategoryName:(NSString *)oldStr;
- (void) replaceItemName:(NSString *)str;
- (BOOL)isEqualToItem:(PackingItem *)item;
+ (void)upgradeTableFromDatabase:(sqlite3 *)db ToDatabase:(sqlite3 *)newdb;

@end

//
//  SettingsViewController.h
//  BabyPacking
//
//  Created by Gary He on 6/8/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextPickerViewController.h"
#import "PasscodeViewController.h"
#import "DetailedSettingsViewController.h"
#import "TTSwitch.h"
#import "TTFadeSwitch.h"
#import "LayoutManager.h"

@class BabyPackingAppDelegate;

@interface SettingsViewController : UITableViewController<TextPickerDelegate, DetailedSettingsViewDelegate,UITextFieldDelegate/*,PasscodeViewControllerDelegate*/> {

    
	BabyPackingAppDelegate *mAppDelegate;
    LayoutManager *layoutManager;
	IBOutlet UIBarButtonItem *cancelButton;
	IBOutlet UIBarButtonItem *doneButton;
//	UISwitch *soundSwidth;
//	UISwitch *soundProgressSwitch;
//	UISwitch *colorBGSwidth;
//	UISwitch *photoBGSwidth;
//	UISwitch *addItemByGroupSwidth;
//	UISwitch *readyMadeListShowSwidth;
//	UISwitch *welcomePageShowSwidth;
	UILabel	*fontTypeLabel;
	UILabel	*taskSoundLabel;
	UILabel	*sound1;
    UILabel	*sound2;
    
    TTFadeSwitch *soundSwidth;
    TTFadeSwitch *soundProgressSwitch;
	TTFadeSwitch *colorBGSwidth;
	TTFadeSwitch *photoBGSwidth;
	TTFadeSwitch *addItemByGroupSwidth;
	TTFadeSwitch *readyMadeListShowSwidth;
	TTFadeSwitch *welcomePageShowSwidth;
    TTFadeSwitch *iCloudSwidth;
	
	NSMutableString *babyXMLString;
	NSMutableString *packingXMLString;
	
	IBOutlet UIView *loginView;
    IBOutlet UIView *loginView_PDL;
    IBOutlet UIView *loginView_L;
	IBOutlet UIView *loginView1;
	IBOutlet UIView *regView;
    IBOutlet UIView *regView_PDL;
    IBOutlet UIView *regView_L;
	IBOutlet UIView *regView1;
	IBOutlet UIView *iphoneLogout_ImportView;
	IBOutlet UIView *iphoneLogout_ExportView;
    IBOutlet UIView *iphoneLogout_ImportView_L;
	IBOutlet UIView *iphoneLogout_ExportView_L;
	IBOutlet UIView *ipadLogout_ExportView;
	IBOutlet UIView *ipadLogout_ImportView;
    IBOutlet UIView *ipadLogout_ExportView_L;
	IBOutlet UIView *ipadLogout_ImportView_L;
	
    
	IBOutlet UIView *selectColorView;
	IBOutlet UIView *selectTextSizeView;
	
	IBOutlet UIPickerView *textSizePicker;
	
	IBOutlet UITextField *txtUsernameLogin;
	IBOutlet UITextField *txtPasswordLogin;
    
    IBOutlet UITextField *txtUsernameLogin_PDL;
	IBOutlet UITextField *txtPasswordLogin_PDL;

	IBOutlet UITextField *txtUsernameLogin1;
	IBOutlet UITextField *txtPasswordLogin1;
    
    IBOutlet UITextField *txtUsernameLogin_L;
	IBOutlet UITextField *txtPasswordLogin_L;
	
	IBOutlet UITextField *txtUsernameReg;
	IBOutlet UITextField *txtPasswordReg;
    
    IBOutlet UITextField *txtUsernameReg_PDL;
	IBOutlet UITextField *txtPasswordReg_PDL;

	IBOutlet UITextField *txtUsernameReg1;
	IBOutlet UITextField *txtPasswordReg1;
    
    IBOutlet UITextField *txtUsernameReg_L;
	IBOutlet UITextField *txtPasswordReg_L;
	
	IBOutlet UITextField *txtEmailReg;
    IBOutlet UITextField *txtEmailReg_PDL;
	IBOutlet UITextField *txtEmailReg1;
    IBOutlet UITextField *txtEmailReg_L;
	
	IBOutlet UILabel *lblTextSize;

	IBOutlet UIButton *importButton;
	IBOutlet UIButton *exportButton;	
	
	IBOutlet UIButton *btnSelectColorForTopBg;
	
	int colorSelectIndex;
	
	BOOL import;
	
	int count;
    
    UIButton *btnImport;
    UIButton *btnExport;
}
@property (nonatomic,retain) IBOutlet UIButton *importButton;
@property (nonatomic,retain) IBOutlet UIButton *exportButton;
@property (nonatomic,retain) IBOutlet UIButton *btnSelectColorForTopBg;

@property (nonatomic,retain) UIView *loginView;
@property (nonatomic,retain) UIView *loginView_PDL;
@property (nonatomic,retain) UIView *loginView_L;
@property (nonatomic,retain) UIView *loginView1;
@property (nonatomic,retain) UIView *regView;
@property (nonatomic,retain) UIView *regView_PDL;
@property (nonatomic,retain) UIView *regView_L;
@property (nonatomic,retain) UIView *regView1;
@property (nonatomic,retain) IBOutlet UIView *iphoneLogout_ImportView;
@property (nonatomic,retain) IBOutlet UIView *iphoneLogout_ExportView;
@property (nonatomic,retain) IBOutlet UIView *iphoneLogout_ImportView_L;
@property (nonatomic,retain) IBOutlet UIView *iphoneLogout_ExportView_L;
@property (nonatomic,retain) IBOutlet UIView *ipadLogout_ExportView;
@property (nonatomic,retain) IBOutlet UIView *ipadLogout_ImportView;
@property (nonatomic,retain) IBOutlet UIView *ipadLogout_ExportView_L;
@property (nonatomic,retain) IBOutlet UIView *ipadLogout_ImportView_L;


@property (nonatomic,retain) IBOutlet UITextField *txtUsernameLogin;
@property (nonatomic,retain) IBOutlet UITextField *txtPasswordLogin;

@property (nonatomic,retain) IBOutlet UITextField *txtUsernameLogin_PDL;
@property (nonatomic,retain) IBOutlet UITextField *txtPasswordLogin_PDL;

@property (nonatomic,retain) IBOutlet UITextField *txtUsernameLogin1;
@property (nonatomic,retain) IBOutlet UITextField *txtPasswordLogin1;

@property (nonatomic,retain) IBOutlet UITextField *txtUsernameLogin_L;
@property (nonatomic,retain) IBOutlet UITextField *txtPasswordLogin_L;

@property (nonatomic,retain) IBOutlet UITextField *txtUsernameReg;
@property (nonatomic,retain) IBOutlet UITextField *txtPasswordReg;

@property (nonatomic,retain) IBOutlet UITextField *txtUsernameReg_PDL;
@property (nonatomic,retain) IBOutlet UITextField *txtPasswordReg_PDL;

@property (nonatomic,retain) IBOutlet UITextField *txtUsernameReg1;
@property (nonatomic,retain) IBOutlet UITextField *txtPasswordReg1;

@property (nonatomic,retain) IBOutlet UITextField *txtUsernameReg_L;
@property (nonatomic,retain) IBOutlet UITextField *txtPasswordReg_L;

@property (nonatomic,retain) IBOutlet IBOutlet UITextField *txtEmailReg;
@property (nonatomic,retain) IBOutlet IBOutlet UITextField *txtEmailReg_PDL;
@property (nonatomic,retain) IBOutlet IBOutlet UITextField *txtEmailReg1;
@property (nonatomic,retain) IBOutlet IBOutlet UITextField *txtEmailReg_L;

@property (nonatomic,retain) NSMutableString *babyXMLString;
@property (nonatomic,retain) NSMutableString *packingXMLString;


- (IBAction) buttonAction:(id) sender;
- (IBAction) importDB:(id) sender;
- (IBAction) exportDB:(id) sender;
- (IBAction) getRegisteredClicked:(id) sender;
- (IBAction) loginClicked:(id) sender;
- (IBAction) submitClicked:(id) sender;
- (IBAction) cancelClicked:(id) sender;
- (BOOL) validateEmail: (NSString *) candidate;
- (void)startSync;
- (void)stopSync;
- (IBAction) logoutClicked:(id)sender;
- (IBAction) import:(id)sender;
- (IBAction) export:(id)sender;

- (IBAction) selectColor:(id)sender;
- (IBAction) colorButtonPressed:(id)sender;

- (IBAction) onMinusClicked:(id)sender;
- (IBAction) onPlusClicked:(id)sender;

- (IBAction)cancelImportExport:(id)sender;
//- (NSString *)base64Encoding;

+ (NSString *) defaultTitleForSection:(NSString *)section;
+ (NSString *) defaultValueForSection:(NSString *)section;
+ (NSString *) titleForValue:(NSString *)value forSection:(NSString *)section;
+ (NSString *) valueForTitle:(NSString *)title forSection:(NSString *)section;

@end

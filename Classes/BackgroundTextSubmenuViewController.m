//
//  BackgroundTextSubmenuViewController.m
//  BabyPacking
//
//  Created by Gary He on 6/8/09.
//  Copyright 2009 test. All rights reserved.
//

#import "Constant.h"
#import "BackgroundTextSubmenuViewController.h"
#import "BabyPackingAppDelegate.h"
#import "CustomizeCellViewController.h"
#import "CustomizeTopBarViewController.h"


@implementation BackgroundTextSubmenuViewController


- (void)dealloc {
    [super dealloc];
}

/*
 - (id)initWithStyle:(UITableViewStyle)style {
 // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 if (self = [super initWithStyle:style]) {
 }
 return self;
 }
 */

- (void)viewDidLoad {
	
    [super viewDidLoad];
    
    mAppDelegate = (id)[UIApplication sharedApplication].delegate;
    
    //Custom Title
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
   	
    if (IOS_NEWER_THAN(7))
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor blackColor];
    
	NSString *customFontName = [mAppDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
    //	label.font = [UIFont fontWithName:customFontName size:fontSize];
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];
	}
	else {
		label.font=[UIFont fontWithName:customFontName size:fontSize];
	}
	
	//label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;
    label.text = @"Background/Text Color";
	[label sizeToFit];
    
    
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
        label.textColor=[UIColor whiteColor];
        
        if (IOS_NEWER_THAN(7))
        {
//            self.navigationItem.backBarButtonItem.tintColor=[UIColor whiteColor];
//            self.navigationItem.leftBarButtonItem.tintColor=[UIColor whiteColor];
//            self.navigationItem.rightBarButtonItem.tintColor=[UIColor whiteColor];
            
        }
        
    } else
    {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label.textColor = theColour1;
        
        if (IOS_NEWER_THAN(7))
        {
//            self.navigationItem.backBarButtonItem.tintColor=theColour1;
//            self.navigationItem.leftBarButtonItem.tintColor=theColour1;
//            self.navigationItem.rightBarButtonItem.tintColor=theColour1;
            
        }
        
	}
	// and finally set the colour of your label
	
	
    
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]==nil) {
        
    }
    else {
        
    }
    
	
	NSLog(@"========%@",[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]);
	
	
	self.navigationItem.titleView = label;
	
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour21;
	// read the data back from the user defaults
	NSData *data211= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_BAR_COLOR];
	// check whether you got anything
	if(data211 == nil) {
        self.navigationController.navigationBar.tintColor=[UIColor blackColor];
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour21 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data211];
        self.navigationController.navigationBar.tintColor=theColour21;
	}
    
	// and finally set the colour of your label
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour212;
	// read the data back from the user defaults
	NSData *data212= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
	// check whether you got anything
	if(data212 == nil) {
		// use this to set the colour the first time your app runs
        myTableView.backgroundColor=[UIColor whiteColor];
	} else {
		// this recreates the colour you saved
		theColour212 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data212];
        myTableView.backgroundColor=theColour212;
	}
	// and finally set the colour of your label
    
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
}


- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
	NSString *customFontName = [mAppDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
    //	label.font = [UIFont fontWithName:customFontName size:fontSize];
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
	}
	
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
        if (IOS_NEWER_THAN(7))
        {
            self.navigationItem.backBarButtonItem.tintColor=[UIColor whiteColor];
            self.navigationItem.leftBarButtonItem.tintColor=[UIColor whiteColor];
            self.navigationItem.rightBarButtonItem.tintColor=[UIColor whiteColor];
        }
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
        if (IOS_NEWER_THAN(7))
        {
//            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
//            btn.frame = CGRectMake(0, 0, 30, 30);
//            [btn setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
//            [btn addTarget:self action:@selector(resetCriteria:) forControlEvents:UIControlEventTouchUpInside];
//            
//            createBtItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
//            
//            self.navigationItem.leftBarButtonItem = createBtItem;
            
           // self.navigationItem.backBarButtonItem.tintColor=theColour1;
//            self.navigationItem.leftBarButtonItem.tintColor=theColour1;
//            self.navigationItem.rightBarButtonItem.tintColor=theColour1;
        }
	}
	// and finally set the colour of your label
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 30, 30);
    [btn setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(resetCriteria:) forControlEvents:UIControlEventTouchUpInside];
    
    createBtItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    self.navigationItem.leftBarButtonItem = createBtItem;
    
    
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour21;
	// read the data back from the user defaults
	NSData *data211= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
	// check whether you got anything
	if(data211 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour21 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data211];
	}
	// and finally set the colour of your label
    
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour31;
	// read the data back from the user defaults
	NSData *data311= [[NSUserDefaults standardUserDefaults] dataForKey: K_SET_HEADER_TEXT_COLOR];
	// check whether you got anything
	if(data311 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour31 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data311];
	}
    
	// and finally set the colour of your label
    
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour41;
	// read the data back from the user defaults
	NSData *data411= [[NSUserDefaults standardUserDefaults] dataForKey: K_SET_HEADER_COLOR];
	// check whether you got anything
	if(data411 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour41 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data411];
	}
	// and finally set the colour of your label
    
    
}

/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

-(IBAction)resetCriteria:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma UItableviewcontroller Delegate Methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        CustomizeTopBarViewController *controller = nil;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            controller = [[CustomizeTopBarViewController alloc] initWithNibName:@"CustomizeTopBarViewController_ipad" bundle:nil];
        else
            controller = [[CustomizeTopBarViewController alloc] initWithNibName:@"CustomizeTopBarViewController" bundle:nil];
        
        [self.navigationController pushViewController:controller animated:YES];
        [controller release];
        
    }
    else if (indexPath.row == 1) {
        CustomizeCellViewController *controller = nil;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            controller = [[CustomizeCellViewController alloc] initWithNibName:@"CustomizeCellViewController_ipad" bundle:nil];
        else
            controller = [[CustomizeCellViewController alloc] initWithNibName:@"CustomizeCellViewController1" bundle:nil];
        
        //[[PasscodeViewController alloc] initWithNibName:@"PasscodeViewController" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
        [controller release];
        
    }
}


#pragma UITableViewController Datasource Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
		if (indexPath.row == 0) {
            cell.textLabel.text = @"Customize Top Bar";
        }
        else if (indexPath.row == 1) {
            cell.textLabel.text = @"Customize Table Cell";
        }
    }
    
    
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour212;
	// read the data back from the user defaults
	NSData *data212= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
	// check whether you got anything
	if(data212 == nil) {
		// use this to set the colour the first time your app runs
        cell.textLabel.textColor=[UIColor blackColor];
	} else {
		// this recreates the colour you saved
		theColour212 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data212];
        cell.textLabel.textColor=theColour212;
	}
	// and finally set the colour of your label
    
	NSString *customFontName1 = [mAppDelegate getCustomFontName];
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
		cell.textLabel.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];
	}
    else{
		cell.textLabel.font=[UIFont fontWithName:customFontName1 size:15];
    }
    
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
    UIColor *theColour1;
    // read the data back from the user defaults
    NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
    // check whether you got anything
    if(data11 == nil) {
        // use this to set the colour the first time your app runs
    } else {
        // this recreates the colour you saved
        theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
        cell.backgroundColor = theColour1;
    }
    // and finally set the colour of your label
    
    
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
    UIColor *theColour21;
    // read the data back from the user defaults
    NSData *data211= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
    // check whether you got anything
    if(data211 == nil) {
        // use this to set the colour the first time your app runs
    } else {
        // this recreates the colour you saved
        theColour21 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data211];
        cell.textLabel.textColor = theColour21;
    }

    
    return cell;
}

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        // [layoutManager translateToPortraitForView:self.view withAnimation:YES];
    }
    else
    {
        // [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
    }
    // }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}



@end
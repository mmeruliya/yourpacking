//
//  List.h
//  BabyPacking
//
//  Created by Jay Lee on 09/04/09.
//  Copyright 2009 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBModel.h"

//typedef enum {
//	StatusList,
//	CustomList,
//	ReadyMadeList,
//	BriefcaseList,
//} ListType;
//
//typedef enum {
//	BabySexNone,
//	BabySexBoy,
//	BabySexGirl,
//} BabySex;
//
//typedef enum {
//	Baby,
//	Toddler,
//	Infant_baby,
//	Child,
//	Adult,
//	Pet,
//	ShoppingList,
//	Other
//} BabyAge;
//
////typedef enum  {
////	Infant_baby,
////	Child,
////	Adult,
////	Pet,
////	ShoppingList,
////} FamilyType;
//
@interface BriefcaseItem : DBModel {
	NSString *title;
	NSString *passport;
	NSString *license;
	NSString *ssn;
	NSString *id_card;
	NSString *airline_name;
	NSString *flight;
	NSString *frequent_flyer;
	NSString *car_name;
	NSString *license_plate;
	NSString *hotel_name;
	NSString *hotel_phone;
	NSString *hotel_address;
	NSString *embassy_name;
	NSString *embassy_address;
	NSString *embassy_emergency;
	NSString *other;
	NSInteger predefineId;
	NSMutableArray *briefcase;
}

@property(nonatomic, copy) NSString* title;
@property(nonatomic, copy) NSString* passport;
@property(nonatomic, copy) NSString* license;
@property(nonatomic, copy) NSString* ssn;
@property(nonatomic, copy) NSString* id_card;
@property(nonatomic, copy) NSString* airline_name;
@property(nonatomic, copy) NSString* flight;
@property(nonatomic, copy) NSString* frequent_flyer;
@property(nonatomic, copy) NSString* car_name;
@property(nonatomic, copy) NSString* license_plate;
@property(nonatomic, copy) NSString* hotel_name;
@property(nonatomic, copy) NSString* hotel_phone;
@property(nonatomic, copy) NSString* hotel_address;
@property(nonatomic, copy) NSString* embassy_name;
@property(nonatomic, copy) NSString* embassy_address;
@property(nonatomic, copy) NSString* embassy_emergency;
@property(nonatomic, copy) NSString* other;
@property(nonatomic) NSInteger predefineId;
@property(nonatomic, retain) NSMutableArray *briefcase;

+ (NSMutableArray*) findAll;

- (BOOL) isPredefine;
- (BOOL) isEqualToItem:(BriefcaseItem *)item;
- (void)deleteAllData;
+ (void)upgradeTableFromDatabase:(sqlite3 *)db ToDatabase:(sqlite3 *)newdb;

@end

//
//  LayoutManager.m
//  ExamExcellence
//
//  Created by dipali on 4/3/12.
//  Copyright (c) 2012 Cogneti. All rights reserved.
//



#import "LayoutManager.h"

#pragma mark - Definition of layout manager item

@class LayoutManagerItem;

@interface LayoutManagerItem : NSObject
{
    NSInteger tag;
    CGRect portraitRect;
    CGRect landscapeRect;
}
@property (nonatomic) NSInteger tag;
@property (nonatomic) CGRect portraitRect;
@property (nonatomic) CGRect landscapeRect;
- (id)   initWithTag:(NSInteger)t andPortraitRect:(CGRect)pRec
    andLandscapeRect:(CGRect)lRec;
+ (LayoutManagerItem *)itemWithTag:(NSInteger)t
                    portraitCGRect:(CGRect)pRec
                   landscapeCGRect:(CGRect)lRec;
@end



@implementation LayoutManagerItem

@synthesize tag, portraitRect, landscapeRect;

- (id)   initWithTag:(NSInteger)t
     andPortraitRect:(CGRect)pRec
    andLandscapeRect:(CGRect)lRec
{
    self = [super init];

    if (self)
    {
        tag = t;
        portraitRect = pRec;
        landscapeRect = lRec;
    }

    return self;
}

+ (LayoutManagerItem *)itemWithTag:(NSInteger)t
                    portraitCGRect:(CGRect)pRec
                   landscapeCGRect:(CGRect)lRec
{
    return [[self alloc] initWithTag:t andPortraitRect:pRec andLandscapeRect:lRec];
}

@end



#pragma mark - Implementation of LayoutManager



@implementation LayoutManager



@synthesize items;



// Standard initialiser
- (id)init
{
    self = [super init];

    if (self)
    {
        // Initialise the storage dictionary
        items = [[NSMutableDictionary alloc] init];
    }

    return self;
}

- (id)initWithSubViewPortraitView:(UIView *)pView
                  andLandscapeNIB:(UIView *)lView
{
    self = [self init];

    if (self)
    {
        // Create a view controller to read the landscape subviews

        [self getAllSubViews:pView andLandscapeView:lView];
        // Ditch the landscape view
        lView = nil;
    }

    return self;
}

// Initialise with current portrait view and landscape NIB
- (id)initWithPortraitView:(UIView *)pView
           andLandscapeNIB:(NSString *)lNib
{
    self = [self init];

    if (self)
    {
        // Create a view controller to read the landscape subviews
        UIViewController *lView = [[UIViewController alloc] initWithNibName:lNib bundle:nil];

        [self getAllSubViews:pView andLandscapeView:lView.view];
        
        // Ditch the landscape view
        lView = nil;
    }

    return self;
}

- (void)getAllSubViews:(UIView *)pView
      andLandscapeView:(UIView *)lview
{
    NSArray *lSubviews = [lview subviews];

    // get the portrait subviews
    NSArray *pSubviews = [pView subviews];

    // Enumerate the portrait view
    for (int sv = 0; sv < [pSubviews count]; sv++)
    {
        // get the view object (portrait)
        UIView *pObject = (id)[pSubviews objectAtIndex : sv];

        if (pObject.tag > 0)
        {
            // check the landscape object
            for (int lv = 0; lv < [lSubviews count]; lv++)
            {
                UIView *lObject = (id)[lSubviews objectAtIndex : lv];

                //--------------
                if ([[pObject subviews] count] > 0)
                {
                    [self getAllSubViews:pObject andLandscapeView:lObject];
                }

                //-----------------
                if (lObject.tag == pObject.tag)
                {
                    CGRect lFrm = lObject.frame;
                    CGRect pFrm = pObject.frame;

                    [self addItemWithTag:pObject.tag andPortraitRect:pFrm andLandscapeRect:lFrm];
                }
            }
        }
    }
}

// Add a new view item to the list
- (void)addItemWithTag:(NSInteger)tag
       andPortraitRect:(CGRect)pRect
      andLandscapeRect:(CGRect)lRect
{
    LayoutManagerItem *item = [LayoutManagerItem itemWithTag:tag portraitCGRect:pRect landscapeCGRect:lRect];

    [items addEntriesFromDictionary:[NSDictionary dictionaryWithObject:item forKey:[NSNumber numberWithInt:tag]]];
}

// Returns landscape rect for a given tag
- (CGRect)getLandscapeRectForTag:(NSInteger)tag
{
    return [[items objectForKey:[NSNumber numberWithInt:tag]] landscapeRect];
}

// Returns portrait rect for a given tag
- (CGRect)getPortraitRectForTag:(NSInteger)tag
{
    return [[items objectForKey:[NSNumber numberWithInt:tag]] portraitRect];
}

// Automatically change all the object frames to the landscape stored frames
- (void)translateToLandscapeForView:(UIView *)vw
                      withAnimation:(BOOL)animated
{
    NSArray *subviews = [vw subviews];

    if (animated)
    {
        [UIView beginAnimations:@"animtolandscape" context:nil];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationDelegate:self];
    }

    [self getAllSubViewsWhileTranslatingToLandscape:subviews];

    if (animated)
    {
        [UIView commitAnimations];
    }
}

- (void)getAllSubViewsWhileTranslatingToLandscape:(NSArray *)subviews
{
    for (int sv = 0; sv < [subviews count]; sv++)
    {
        UIView *cObject = (id)[subviews objectAtIndex : sv];

        if (cObject.tag > 0)
        {
            //-------
            if ([[cObject subviews] count] > 0)
            {
                NSArray *pSubviews1 = [cObject subviews];
                [self getAllSubViewsWhileTranslatingToLandscape:pSubviews1];
            }

            //--------
            CGRect newFrame = [self getLandscapeRectForTag:cObject.tag];
            [cObject setFrame:newFrame];
        }
    }
}

// Automatically change all the object frames to the portrait stored frames
- (void)translateToPortraitForView:(UIView *)vw
                     withAnimation:(BOOL)animated
{
    NSArray *subviews = [vw subviews];

    if (animated)
    {
        [UIView beginAnimations:@"animtoportrait" context:nil];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationDelegate:self];
    }

    [self getAllSubViewsWhileTranslatingToPotrait:subviews];

    if (animated)
    {
        [UIView commitAnimations];
    }
}

- (void)getAllSubViewsWhileTranslatingToPotrait:(NSArray *)subviews
{
    for (int sv = 0; sv < [subviews count]; sv++)
    {
        UIView *cObject = (id)[subviews objectAtIndex : sv];

        if (cObject.tag > 0)
        {
            //-------
            if ([[cObject subviews] count] > 0)
            {
                NSArray *pSubviews1 = [cObject subviews];
                [self getAllSubViewsWhileTranslatingToPotrait:pSubviews1];
            }

            //--------
            CGRect newFrame = [self getPortraitRectForTag:cObject.tag];
            [cObject setFrame:newFrame];
        }
    }
}

@end

//
//  BriefcaseViewController.m
//  BabyPacking
//
//  Created by eve on 9/4/09.
//  Copyright 2009 test. All rights reserved.
//

#import "BriefcaseViewController.h"
#import "BabyPackingAppDelegate.h"

#import "DetailOfBriefcaseViewController.h"
#import "Constant.h"

@interface BriefcaseViewController(Private)

- (void) loadBriefcaseList;
- (BriefcaseItem*) itemAtIndexPath:(NSIndexPath*)indexPath;
@end

@implementation BriefcaseViewController

@synthesize briefcaseList,row,deleteRow,lbl1,lbl2,lbl3,lbl4,lbl5,lbl6,lbl7,lbl8,lbl9,lbl10,lbl11,lbl12,lbl13,lbl14,lbl15,lbl16;;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
 // Custom initialization
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];
	BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
	
	UILabel *label1 = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	label1.backgroundColor = [UIColor clearColor];

    
	NSString *customFontName = [appDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
    
    
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label1.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];
	}
	else {
		label1.font=[UIFont fontWithName:customFontName size:fontSize];
	}

	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
        label1.textColor=[UIColor whiteColor];
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label1.textColor = theColour1;
	}
    
//	label1.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label1.textAlignment = UITextAlignmentCenter;
	self.navigationItem.titleView = label1;	
	label1.text = @"Briefcase";
	[label1 sizeToFit];
    
    


    
    
    
	
	UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 30, 30);
    [btn setImage:[UIImage imageNamed:@"AddTop.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    addButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = addButton;
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(0, 0, 30, 30);
    [btn2 setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn2];
    self.navigationItem.leftBarButtonItem = doneButton;
    
	[self loadBriefcaseList];
	self.row = -10;

	
	if (appDelegate.colorBgOn ) 	
		bgImageView.alpha = 1.0;
	else 
		bgImageView.alpha = 0;

	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
    
    
    
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self loadBriefcaseList];
  	[mTableView reloadData];
}
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */
- (void)loadBriefcaseList {
	
	self.briefcaseList = [BriefcaseItem findAll];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
	[super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
		
	
}


- (void)dealloc {
	[super dealloc];
	[mTableView release];
	[addButton release];
	[editButton release];
	[doneButton release];
	[briefcaseList release];
}



- (void) buttonAction:(id) sender {
	if (sender == doneButton.customView){
		[self.navigationController popViewControllerAnimated:YES];
	}else if (sender == addButton.customView){
		if(row >= 0){
			
			NSArray *indexPaths = [NSArray arrayWithObjects:
							   [NSIndexPath indexPathForRow:row+1 inSection:0],nil];
			
			self.row = -10;
			[mTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
			
		}
		
		DetailOfBriefcaseViewController  *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[DetailOfBriefcaseViewController alloc] initWithNibName:@"DetailOfBriefcaseViewController" bundle:nil];
		else
			controller = [[DetailOfBriefcaseViewController alloc] initWithNibName:@"DetailOfBriefcaseViewController1" bundle:nil];
		
		//[[DetailOfBriefcaseViewController  alloc] initWithNibName:@"DetailOfBriefcaseViewController" bundle:nil];
		[self.navigationController pushViewController:controller animated:YES];
		[controller release];			
	}
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (BriefcaseItem*) itemAtIndexPath:(NSIndexPath*)indexPath {
	BriefcaseItem *item = nil;
	item = [briefcaseList objectAtIndex:indexPath.row];
	return item;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

	NSInteger count = [self.briefcaseList count];
	if (row >= 0) count++;
	return count;

}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *CellIdentifier = @"Cell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		
	}
	if(indexPath.row != row+1){
		if ([briefcaseList count]){
			
			cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
			// TODO: NSArray里面有doesContain这个方法吗？
			//if ([cell.subviews doesContain:label] )
			if ([cell.subviews indexOfObject:dataView] != NSNotFound)
				[dataView removeFromSuperview];
           // [dataView removeFromSuperview];
			if(row<0 || indexPath.row <=row){
				BriefcaseItem *item = [briefcaseList objectAtIndex:indexPath.row];
				cell.text = item.title;
			}else{
				BriefcaseItem *item = [briefcaseList objectAtIndex:indexPath.row-1];
				cell.text = item.title;
			} 
			 
			
		}
		
	}
				
// Set up the cell...
	
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
    UIColor *theColour1;
    // read the data back from the user defaults
    NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
    // check whether you got anything
    if(data11 == nil) {
        // use this to set the colour the first time your app runs
    } else {
        // this recreates the colour you saved
        theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
        cell.backgroundColor = theColour1;
    }
    // and finally set the colour of your label
    
    
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
    UIColor *theColour21;
    // read the data back from the user defaults
    NSData *data211= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
    // check whether you got anything
    if(data211 == nil) {
        // use this to set the colour the first time your app runs
    } else {
        // this recreates the colour you saved
        theColour21 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data211];
        cell.textLabel.textColor = theColour21;
    }
    // and finally set the colour of your label
    BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
    NSString *customFontName1 = [appDelegate getCustomFontName];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
        cell.textLabel.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];
    }
    else{
        cell.textLabel.font=[UIFont fontWithName:customFontName1 size:15];
    }
    
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if(row >= 0){
		if(indexPath.row == row+1)return;
		else{
			NSArray *indexPaths = [NSArray arrayWithObjects:
							   [NSIndexPath indexPathForRow:row+1 inSection:0],nil];
			
			self.row = -10;
			[tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
			
			
		}
			}else{
		self.row = indexPath.row;
		NSArray *indexPaths = [NSArray arrayWithObjects:
						   [NSIndexPath indexPathForRow:indexPath.row+1 inSection:0],nil];
		
		[tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		NSIndexPath* newIndexPath = [NSIndexPath indexPathForRow:indexPath.row+1 inSection:0];
		UITableViewCell *cell = [tableView cellForRowAtIndexPath:newIndexPath];
		BriefcaseItem *item = [self itemAtIndexPath:indexPath];
		NSString *msg = [NSString stringWithFormat:@"  Passport # %@ \n   Driver's License # %@ \n   Social Security # %@ \n   ID Card # %@ \n   Airline: %@ \n   Flight # %@ \n   Frequent Flyer # %@ \n   Car Name: %@ \n   License Plate # %@ \n   Hotel Name: %@ \n   Hotel Phone # %@ \n   Hotel Address: %@ \n   Embassy # %@ \n   Embassy Address: %@ \n   Emergency # %@ \n   Other: %@",item.passport,item.license,item.ssn,item.id_card,item.airline_name,item.flight,item.frequent_flyer,item.car_name,item.license_plate,item.hotel_name,item.hotel_phone,item.hotel_address,item.embassy_name,item.embassy_address,item.embassy_emergency,item.other];
		label.text = msg;
                lbl1.text = item.passport;
                lbl2.text = item.license;
                lbl3.text = item.ssn;
                lbl4.text = item.id_card;
                lbl5.text = item.airline_name;
                lbl6.text = item.flight;
                lbl7.text = item.frequent_flyer;
                lbl8.text = item.car_name;
                lbl9.text = item.license_plate;
                lbl10.text = item.hotel_name;
                lbl11.text = item.hotel_phone;
                lbl12.text = item.hotel_address;
                lbl13.text = item.embassy_name;
                lbl14.text = item.embassy_address;
                lbl15.text = item.embassy_emergency;
                lbl16.text = item.other;
                
		label.adjustsFontSizeToFitWidth = YES;
		[cell addSubview:dataView];
		cell.text = nil;
		cell.accessoryType = UITableViewCellAccessoryNone;
		
	}
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
	if(row >= 0){
		
		NSArray *indexPaths = [NSArray arrayWithObjects:
						   [NSIndexPath indexPathForRow:row+1 inSection:0],nil];
		
		self.row = -10;
		[tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		
	}
		
	DetailOfBriefcaseViewController  *controller = nil;
	
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		controller = [[DetailOfBriefcaseViewController alloc] initWithNibName:@"DetailOfBriefcaseViewController" bundle:nil];
	else
		controller = [[DetailOfBriefcaseViewController alloc] initWithNibName:@"DetailOfBriefcaseViewController1" bundle:nil];
	
	//[[DetailOfBriefcaseViewController  alloc] initWithNibName:@"DetailOfBriefcaseViewController" bundle:nil];
	controller.briefcaseItem = [self itemAtIndexPath:indexPath];
	[self.navigationController pushViewController:controller animated:YES];
	[controller release];
	
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if(indexPath.row-1 == row)
		return dataView.frame.size.height;
	else		
		return 44;
}
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
	// Return NO if you do not want the specified item to be editable.
	if(indexPath.row-1 == row)
		return NO;
	else	
	return YES;
	
}

//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
//	return UITableViewCellEditingStyleNone;
//}

//- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
//	return NO;
//}




// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		// Delete the row from the data source.
		if(row >= 0){
			
			NSArray *indexPaths = [NSArray arrayWithObjects:
							   [NSIndexPath indexPathForRow:row+1 inSection:0],nil];
			
			self.row = -10;
			[mTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationRight];
			
		}
		
		BriefcaseItem *item = [self itemAtIndexPath:indexPath];
		NSString * msg = [[NSString alloc] initWithFormat:@"Do you want to permanently delete %@'s briefcase details?",item.title]; 
		UIActionSheet *alert = [[UIActionSheet alloc] initWithTitle:msg
												delegate:self 
									 cancelButtonTitle:@"Cancel"
								    destructiveButtonTitle:@"YES"
									    otherButtonTitles: nil];
		[alert showInView:self.view];	
		[alert release];
		[msg release];
		
		self.deleteRow = indexPath.row;
	
	}	
	
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
	if (buttonIndex == [actionSheet destructiveButtonIndex]){
		//return YES;
		BriefcaseItem *item = [briefcaseList objectAtIndex:self.deleteRow];
		[item deleteData];
		[briefcaseList removeObjectAtIndex:self.deleteRow];
		[mTableView reloadData];	
		

	}
	
}

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{            return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
       // [layoutManager translateToPortraitForView:self.view withAnimation:YES];
    }
    else
    {
        //[layoutManager translateToLandscapeForView:self.view withAnimation:YES];
    }
    // }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}



@end


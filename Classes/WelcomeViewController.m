//
//  welcomeViewController.m
//  BabyPacking
//
//  Created by eve on 12/15/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "WelcomeViewController.h"
#import "RootViewController.h"
#import "HelpViewController.h"

@implementation WelcomeViewController

@synthesize helpController;
@synthesize homeController;
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        layoutManager = [[LayoutManager alloc] initWithPortraitView:self.view andLandscapeNIB:@"WelcomeViewController_L"];
          }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [layoutManager translateToPortraitForView:self.view withAnimation:NO];
    }
    else
    {
        [layoutManager translateToLandscapeForView:self.view withAnimation:NO];
    }
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[homeController release];
	[helpController release];
    
    txtView.text=NSLocalizedString(@"WelcomeText", @"WelcomeText");
    [forHelpButton setTitle:NSLocalizedString(@"HelpButtonLabelText", @"HelpButtonLabelText") forState:UIControlStateNormal];
    [forListButton setTitle:NSLocalizedString(@"HelpButtonLabelText", @"HelpButtonLabelText") forState:UIControlStateNormal];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
    
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

- (IBAction) buttonAction:(id)sender{
	if(sender == forHelpButton){
		HelpViewController *controller = [[HelpViewController alloc] initWithNibName:@"HelpViewController" bundle:nil];
		
		CGFloat y = self.view.frame.size.height - controller.view.frame.size.height;
		CGRect frame = controller.view.frame;
		frame.origin.y = y;
		controller.view.frame = frame;
		//controller.homeContoller = self;
		self.helpController = controller;
		[self.view addSubview:helpController.view];
		[controller release];
	}

}

- (void) closeHelpPage {
	self.helpController = nil;
//	myTableView.userInteractionEnabled = YES;
//	self.navigationController.navigationBar.userInteractionEnabled = YES;
}

- (void) valueChanged:(id) sender {
	NSLog(@"valueChanged");
}

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{            return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
   // return (UIInterfaceOrientationMaskAll);
    return YES;
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        [layoutManager translateToPortraitForView:self.view withAnimation:YES];
    }
    else
    {
        [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
    }
    // }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}


@end

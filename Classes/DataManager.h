//
//  DataManager.h
//  RWS
//
//  Created by Hitesh on 8/31/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h> 

@class BabyPackingAppDelegate;
@class Items;
@class Categories;

// This is the main data manager class that handles all database transactions for sessions and parties.
@interface DataManager : NSObject 
{
	BabyPackingAppDelegate   	*appDelegate;
	sqlite3				        *database;
}

@property (nonatomic,readonly) sqlite3 *database;

- (void) createDatabaseIfNotExists;
- (void) getItemByCategoty:(int)catId;
- (void) addItem:(Items *)item;
- (void) updateItem:(Items *)item;
- (void) getCategoriesByList:(int)listid;
- (void) addCategory:(Categories *)cat;
- (void) updateCategory:(Categories *)cat;
- (void) deleteCategory;
- (void) deleteItems;
- (void) getRestoreItemByCategory:(int)catId;
- (void) getRestoreCategory;
- (void) getAllItemsByCategory:(int)catId;
- (void) getAllData;
//New
- (void) updateItemsOldCategory:(NSString *)oldCat withNewCategory:(NSString *)newCat;
@end

//
//  SearchResultParser.m
//  BabyPacking
//
//  Created by Jay Lee on 11/19/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import "SearchResultParser.h"

@implementation SearchResultParser

@synthesize successful = mSuccessful;
@synthesize data = mData;

- (void)dealloc
{
	[mCurItem release];
	[mData release];
	[super dealloc];
}

- (void)beforeParsing
{
	mSuccessful = NO;
	self.data = [NSMutableDictionary dictionaryWithCapacity:0];
	[mData setObject:[NSMutableArray arrayWithCapacity:0] forKey:@"Values"];
}

- (void)parser:(NSXMLParser*)parser didStartElement:(NSString*)elementName namespaceURI:(NSString*)namespaceURI qualifiedName:(NSString*)qualifiedName attributes:(NSDictionary*)attributes
{
	[super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributes];
	
	if ([self xmlPathEndsWith:@"task", nil] ||
		[self xmlPathEndsWith:@"rem_record", nil]) {
		mCurItem = [[NSMutableDictionary alloc] initWithCapacity:0];
	}
}

- (void)didEndElement:(NSString*)elementName namespaceURI:(NSString*)namespaceURI qualifiedName:(NSString*)qualifiedName
{
	if ([self xmlPathEndsWith:@"errorcode", nil]) {
		self.successful = [[self trimmedString] intValue]>=0?YES:NO;
	} else if ([self xmlPathEndsWith:@"id", nil]) {
		[mCurItem setObject:[self trimmedString] forKey:@"id"];
	} else if ([self xmlPathEndsWith:@"deviceToken", nil]) {
		[mCurItem setObject:[self trimmedString] forKey:@"deviceToken"];
	} else if ([self xmlPathEndsWith:@"task_id", nil]) {
		[mCurItem setObject:[self trimmedString] forKey:@"task_id"];
	} else if ([self xmlPathEndsWith:@"title", nil]) {
		[mCurItem setObject:[self trimmedString] forKey:@"title"];
	} else if ([self xmlPathEndsWith:@"text", nil]) {
		[mCurItem setObject:[self trimmedString] forKey:@"text"];
	} else if ([self xmlPathEndsWith:@"category", nil]) {
		[mCurItem setObject:[self trimmedString] forKey:@"category"];
	} else if ([self xmlPathEndsWith:@"sdate", nil]) {
		[mCurItem setObject:[self trimmedString] forKey:@"sdate"];
	} else if ([self xmlPathEndsWith:@"edate", nil]) {
		[mCurItem setObject:[self trimmedString] forKey:@"edate"];
	} else if ([self xmlPathEndsWith:@"message", nil]) {
		[mCurItem setObject:[self trimmedString] forKey:@"message"];
	} else if ([self xmlPathEndsWith:@"sound", nil]) {
		[mCurItem setObject:[self trimmedString] forKey:@"sound"];
	} else if ([self xmlPathEndsWith:@"time", nil]) {
		[mCurItem setObject:[self trimmedString] forKey:@"time"];
	} else if ([self xmlPathEndsWith:@"repeating", nil]) {
		[mCurItem setObject:[self trimmedString] forKey:@"repeating"];
	} else if ([self xmlPathEndsWith:@"times", nil]) {
		[mCurItem setObject:[self trimmedString] forKey:@"times"];
	} else if ([self xmlPathEndsWith:@"task", nil] ||
		[self xmlPathEndsWith:@"rem_record", nil]) {
		NSMutableArray* values = [mData objectForKey:@"Values"];
		[values addObject:mCurItem];
		[mCurItem release];
		mCurItem = nil;
	}
}

@end

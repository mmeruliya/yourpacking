//
//  PasscodeViewController.m
//  BabyPacking
//
//  Created by eve on 9/9/09.
//  Copyright 2009 test. All rights reserved.
//

#import "PasscodeViewController.h"
#import "BabyPackingAppDelegate.h"
#import "BriefcaseItem.h"
#import "BriefcaseViewController.h"

@implementation PasscodeViewController

@synthesize passcode;
@synthesize delegate = mDelegate;

- (void)dealloc {
	[passcode_typed release];
	[passcode release];
	[super dealloc];
}

/*
 // The designated initializer. Override to perform setup that is required before the view is loaded.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
 // Custom initialization
 }
 return self;
 }
 */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        layoutManager = [[LayoutManager alloc] initWithPortraitView:self.view andLandscapeNIB:@"PasscodeViewController_L"];
        //self = [super initWithNibName:@"RecalliPhoneView" bundle:nibBundleOrNil];
        
    }
    return self;
}


- (void)viewDidLoad {
	lable_passcode.text = NSLocalizedString(@"Passcode:",nil);
	[button_save setTitle:NSLocalizedString(@"Save",nil)];
	[button_cancel setTitle:NSLocalizedString(@"Cancel",nil)];
//	self.navigationItem.rightBarButtonItem = button_save;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 30, 30);
    btn.tag=1;
    [btn setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(buttonCancelDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
   
    
//	self.navigationItem.leftBarButtonItem = button_cancel;
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(0, 0, 30, 30);
    btn2.tag=1;
    [btn2 setImage:[UIImage imageNamed:@"Done.png"] forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(buttonSaveDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn2];

	code_seg_one.font = [UIFont fontWithName:@"Arial-BoldMT" size:56];
	code_seg_two.font = [UIFont fontWithName:@"Arial-BoldMT" size:56];
	code_seg_three.font = [UIFont fontWithName:@"Arial-BoldMT" size:56];
	code_seg_four.font = [UIFont fontWithName:@"Arial-BoldMT" size:56];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [layoutManager translateToPortraitForView:self.view withAnimation:NO];
    }
    else
    {
        [layoutManager translateToLandscapeForView:self.view withAnimation:NO];
    }

	
	background_image.frame = CGRectMake(0, 0, 320.0, 480.0);
	isSaveClicked = NO;
	[self.view bringSubviewToFront:code_seg_one];
	[self.view bringSubviewToFront:code_seg_two];
	[self.view bringSubviewToFront:code_seg_three];
	[self.view bringSubviewToFront:code_seg_four];
	[self.view bringSubviewToFront:passcode_text];
	[self.view bringSubviewToFront:lable_passcode];
	BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
	passcode_text.text = appDelegate.password ;
	if (appDelegate.password){
		code_seg_one.text = @"*";
		code_seg_two.text = @"*";
		code_seg_three.text = @"*";
		code_seg_four.text = @"*";
	}
	if (!appDelegate.password){
		[textfield_none becomeFirstResponder];
		textfield_none.text = self.passcode;
		reset.enabled= NO;
		//[self valueDidChange:textfield_none];
		code_seg_one.text = @"";
		code_seg_two.text = @"";
		code_seg_three.text = @"";
		code_seg_four.text = @"";
		
		passcode_text.text = @"";
		
	}
}

//- (void)viewWillDisappear:(BOOL)animated{
//	[super viewWillDisappear:YES];
//	NSLog(@"VIEW DISAPPEAR");
////	[code_seg_one resignFirstResponder];
////	[code_seg_two resignFirstResponder];
////	[code_seg_three resignFirstResponder];
////	[code_seg_four resignFirstResponder];
////	[passcode_text resignFirstResponder];
//	[textfield_none becomeFirstResponder];
//	[textfield_none resignFirstResponder];
//}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
	return NO;
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	//[self valueDidChange:textfield_none];
	//UITextField *textFieldController = sender;
	//NSString *text = textFieldController.text;
	switch ([textField.text length]+1) {
		case 0:
			code_seg_one.text = @"";
			code_seg_two.text = @"";
			code_seg_three.text = @"";
			code_seg_four.text = @"";
			
			passcode_text.text = @"";
			self.passcode = passcode_text.text;
			break;
			
		case 1:
			code_seg_one.text = @"*";
			code_seg_two.text = @"";
			code_seg_three.text = @"";
			code_seg_four.text = @"";
			
			passcode_text.text = @"";
			self.passcode = passcode_text.text;
			break;
		case 2:
			code_seg_one.text = @"*";
			code_seg_two.text = @"*";
			code_seg_three.text = @"";
			code_seg_four.text = @"";
			
			passcode_text.text = @"";
			self.passcode = passcode_text.text;
			break;
		case 3:
			code_seg_one.text = @"*";
			code_seg_two.text = @"*";
			code_seg_three.text = @"*";
			code_seg_four.text = @"";
			
			passcode_text.text = @"";
			self.passcode = passcode_text.text;
			break;
		case 4:
			code_seg_one.text = @"*";
			code_seg_two.text = @"*";
			code_seg_three.text = @"*";
			code_seg_four.text = @"*";
			passcode_text.text = [NSString stringWithFormat:@"%@%@",textField.text,string];//[textFieldController.text substringToIndex:4];
			self.passcode = passcode_text.text;
			break;
			
		default:
			break;
	}
	
	if ([textField.text length] > 4) 
	{
		//passcode_text.text = [textFieldController.text substringToIndex:4];
		//self.passcode = passcode_text.text;
		textField.text = [textField.text substringToIndex:4];
	}
	
	return YES;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
	if (!isSaveClicked)
		[textField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
	// Release anything that's not essential, such as cached data
}

- (IBAction)valueDidChange:(id)sender {
	
	UITextField *textFieldController = sender;
	NSString *text = textFieldController.text;
	switch ([text length]+1) {
		case 0:
			code_seg_one.text = @"";
			code_seg_two.text = @"";
			code_seg_three.text = @"";
			code_seg_four.text = @"";
			
			passcode_text.text = @"";
			self.passcode = passcode_text.text;
			break;
			
		case 1:
			code_seg_one.text = @"*";
			code_seg_two.text = @"";
			code_seg_three.text = @"";
			code_seg_four.text = @"";
			
			passcode_text.text = @"";
			self.passcode = passcode_text.text;
			break;
		case 2:
			code_seg_one.text = @"*";
			code_seg_two.text = @"*";
			code_seg_three.text = @"";
			code_seg_four.text = @"";
			
			passcode_text.text = @"";
			self.passcode = passcode_text.text;
			break;
		case 3:
			code_seg_one.text = @"*";
			code_seg_two.text = @"*";
			code_seg_three.text = @"*";
			code_seg_four.text = @"";
			
			passcode_text.text = @"";
			self.passcode = passcode_text.text;
			break;
		case 4:
			code_seg_one.text = @"*";
			code_seg_two.text = @"*";
			code_seg_three.text = @"*";
			code_seg_four.text = @"*";
			//passcode_text.text = [textFieldController.text substringToIndex:4];
			//self.passcode = passcode_text.text;
			break;
			
		default:
			break;
	}
	
	if ([textFieldController.text length] > 4) 
	{
		passcode_text.text = [textFieldController.text substringToIndex:4];
		self.passcode = passcode_text.text;
		textFieldController.text = [textFieldController.text substringToIndex:4];
	}
}

//- (IBAction) valueDidSwitch:(id)sender {
//	UISwitch *switcher = sender;
//	self.isPasscodeOn = switcher.on;
//}

- (IBAction) buttonSaveDidClick:(id)sender {
	
	isSaveClicked = YES;
	[code_seg_one resignFirstResponder];
	[code_seg_two resignFirstResponder];
	[code_seg_three resignFirstResponder];
	[code_seg_four resignFirstResponder];
	[passcode_text resignFirstResponder];
	[textfield_none resignFirstResponder];
	if ([@"" isEqualToString:passcode_text.text]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error:",nil) 
											   message:NSLocalizedString(@"Please enter passcode!",nil) 
											  delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
		[alert show];
		[alert release];
		return;
	}
	
	BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
	appDelegate.password = passcode_text.text ;
	NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
//	[userSettings setObject:appDelegate.password forkey: @"password"];
	[userSettings setObject:appDelegate.password forKey:@"password"];
	[userSettings synchronize];
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction) buttonCancelDidClick:(id)sender {
//	[code_seg_one resignFirstResponder];
//	[code_seg_two resignFirstResponder];
//	[code_seg_three resignFirstResponder];
//	[code_seg_four resignFirstResponder];
//	[passcode_text resignFirstResponder];
	isSaveClicked=YES;
	[textfield_none resignFirstResponder];	

	[self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidDisappear:(BOOL)animated{
	NSLog(@"VIEW DISAPPEAR");
	[textfield_none resignFirstResponder];
	[super viewDidDisappear:YES];
}

- (IBAction) button_action:(id)sender {
	if(sender == reset){

		UIActionSheet *alert = [[UIActionSheet alloc] initWithTitle:@"If you reset the password,the briefcase list will be deleted at the same time!"
												 delegate:self 
										  cancelButtonTitle:@"Cancel"
									  destructiveButtonTitle:@"YES,I Agree"
										  otherButtonTitles: nil];
		[alert showInView:self.view];	
		[alert release];
		
	}
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
	if (buttonIndex == [actionSheet destructiveButtonIndex]){
		reset.enabled = NO;
		[textfield_none becomeFirstResponder];
		BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
		appDelegate.password = nil;
		NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
		[userSettings setObject:appDelegate.password forKey:@"password"];
		[userSettings synchronize];
		self.passcode = nil;
		passcode_text.text = nil;
		code_seg_one.text = @"";
		code_seg_two.text = @"";
		code_seg_three.text = @"";
		code_seg_four.text = @"";
		NSMutableArray *briefcaseItemArray = [BriefcaseItem findAll];
		for(BriefcaseItem *item in briefcaseItemArray){
			[item deleteData];
		} 
	}
}
#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
         [layoutManager translateToPortraitForView:self.view withAnimation:YES];
    }
    else
    {
        [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
    }
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}


//- (void)viewDidUnload{
//	[super viewDidUnload];
//	NSLog(@"VIEW UNLOAD");
//	[textfield_none resignFirstResponder];
//
//}


@end
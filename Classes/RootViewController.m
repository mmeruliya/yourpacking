//
//  RootViewController.m
//  BabyPacking
//
//  Created by Gary He on 5/18/09.
//  Copyright test 2009. All rights reserved.
//
#import "BabyPackingAppDelegate.h"
#import "RootViewController.h"
#import "Constant.h"
#import "AddListViewController.h"
#import "CheckListViewController.h"
#import "CSTableViewCell.h"
#import "InfoViewController.h"
#import "BriefcaseViewController.h"
#import "PackingList.h"
#import "PackingItem.h"
#import "SettingsViewController.h"
#import "ReadyMadeListForDeluxe.h"
#import "TaskAlertViewController.h"

#define MAX_RETRY_COUNT				3

#define ALERT_VIEW_WIDTH			280
#define ALERT_VIEW_HEIGHT			200
#define ALERT_VIEW_TOP				60
#define PASSCODE_LABEL_TOP			70
#define PASSCODE_LABEL_LEFT			20
#define PASSCODE_LABEL_HEIGHT		20
#define PASSCODE_TEXTFIELD_TOP		100
#define PASSCODE_TEXTFIELD_LEFT		20
#define PASSCODE_TEXTFIELD_HEIGHT	30
#define CODE_SEGMENT_OFFSET			50
#define CODE_SEGMENT_SIZE			40
#define CODE_SEGMENT_SPACE			6
enum {
	TABBAR_HELP = -5,
	TABBAR_TODO,
	TABBAR_BRIEFCASE,
	TABBAR_INFO,
	TABBAR_SETTINGS
};

NSString *strCode;

@interface RootViewController(Private)

- (void) loadCustomList;
- (void) updateStatusList;
- (void) tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item;
- (void) applicationFinishedLaunching;
//- (void) createCustomListIfNeed;
//- (void) needReloadCustomList;
//- (void) removeCustomListIfNeed;
//- (void) removeStatusListIfNeed; 
@end


@implementation RootViewController
@synthesize landscapeVw,portraitVw;
@synthesize customDict;
@synthesize readyMadeDict;
@synthesize readyMadeListForDeluxeArray;
@synthesize statusDict;
@synthesize allListArray;
@synthesize isAddCustomList;
@synthesize helpController,picker;
@synthesize welcomeController;
@synthesize brifcaseLoginView;
@synthesize txt_one,txt_two,txt_four,txt_three;

bool isShowingLandscapeView;

- (id)initWithCoder:(NSCoder *)decoder {
	if (self = [super initWithCoder:decoder]) {
		//NSLog(@"RootViewController >> initWithCoder");
		self.allListArray = [NSMutableArray arrayWithCapacity:2];
		
		self.customDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSMutableArray arrayWithCapacity:128], K_LIST, [NSNumber numberWithInt:CustomList], K_TYPE, nil];
		self.statusDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSMutableArray arrayWithCapacity:128], K_LIST, [NSNumber numberWithInt:StatusList], K_TYPE, nil];
	}
	return self;
}

- (void)dealloc
{
	[myTableView release];
    [createBtItem release];
	[settingsBtItem release];
	[infoBtItem release];
	[helpBtItem release];
	[mBlankView release];
	[customDict release];
	[readyMadeDict release];
	[statusDict release];
	[allListArray release];
	
	[pinkBearIcon release];
	[blueBearIcon release];
	[templateIcon release];
	[boyIcon release];
	[girlIcon release];
	[manIcon release];
	[womanIcon release];
	[petIcon release];
	[ShoppingIcon release];
	[otherIcon release];
	
	[helpController release];
	[mTaskAlertController release];
	
    [myTableViewLand release];
    [mTabBarLand release];
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        layoutManager = [[LayoutManager alloc] initWithPortraitView:self.view andLandscapeNIB:@"RootViewController_L"];
        //self = [super initWithNibName:@"RecalliPhoneView" bundle:nibBundleOrNil];
        
    }
    return self;
}


- (void)viewDidUnload {
    [mTabBarLand release];
    mTabBarLand = nil;
    [myTableViewLand release];
    myTableViewLand = nil;
	// Release anything that can be recreated in viewDidLoad or on demand.
	// e.g. self.myOutlet = nil;
	// Save the state of the search UI so that it can be restored if the view is re-created.
}



//#pragma mark Rotation
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    //self.view=UIInterfaceOrientationIsPortrait(interfaceOrientation);
//    return YES;
//}
////-(NSUInteger)supportedInterfaceOrientations{
////    return UIInterfaceOrientationMaskPortrait; // etc
////}
//
//#pragma mark orientation change methods
//
//- (void)orientationChanged:(NSNotification *)notification
//{
//    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
//    if (UIDeviceOrientationIsLandscape(deviceOrientation) &&
//        !isShowingLandscapeView)
//    {
//        self.view=landscapeVw;
//        isShowingLandscapeView = YES;
//    }
//    else if (UIDeviceOrientationIsPortrait(deviceOrientation) &&
//             isShowingLandscapeView)
//    {
//        self.view=portraitVw;
//        isShowingLandscapeView = NO;
//    }
//}


- (void)viewDidLoad {
    
    
    //  Observer to catch changes from iCloud
    NSUbiquitousKeyValueStore *store = [NSUbiquitousKeyValueStore defaultStore];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(storeDidChange:)
                                                 name:NSUbiquitousKeyValueStoreDidChangeExternallyNotification
                                               object:store];
    
    [[NSUbiquitousKeyValueStore defaultStore] synchronize];
    
    // Observer to catch the local changes
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didAddNewNote:)
                                                 name:@"New Note List Item"
                                               object:nil];

    // notification for changing in orientation change
//  [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:@"UIDeviceOrientationDidChangeNotification" object:nil];
//[self orientationChanged:nil];
    
       	// Add 'Back' navigation button
	appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
	
	UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release];
	
//Custom Title	
    
    
	label = [[UILabel alloc] initWithFrame:CGRectZero] ;
	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor whiteColor];
    labelLand = [[UILabel alloc] initWithFrame:CGRectZero] ;
	labelLand.backgroundColor = [UIColor clearColor];
    labelLand.textColor=[UIColor whiteColor];
    
	NSString *customFontName = [appDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
//	label.font = [UIFont fontWithName:customFontName size:fontSize];
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];
        labelLand.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];
	}
	else {
		label.font=[UIFont fontWithName:customFontName size:fontSize];
        labelLand.font=[UIFont fontWithName:customFontName size:fontSize];
	}
	
	//label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;
    
 //   labelLand.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	labelLand.textAlignment = UITextAlignmentCenter;

	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
        label.textColor=[UIColor whiteColor];
        labelLand.textColor=[UIColor whiteColor];
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label.textColor = theColour1;
        labelLand.textColor = theColour1;
	}
	// and finally set the colour of your label

	
	self.navigationItem.titleView = label;
    self.navigationItem.titleView = labelLand;
	
    
    UIColor *theColour11;
	// read the data back from the user defaults
	NSData *data111= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data111 == nil) {
		// use this to set the colour the first time your app runs
        label.textColor=[UIColor whiteColor];
	} else {
		// this recreates the colour you saved
		theColour11 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data111];
		label.textColor = theColour11;
	}
	// and finally set the colour of your label
	
	self.navigationItem.titleView = label;
	label.text = TITLE_CATEGORY_LIST;
	[label sizeToFit];
	
    
//	self.navigationController.navigationBar.tintColor=[UIColor blackColor];
     [super viewDidLoad];
	if (TARGET_VERSION == VERSION_BABY_PACKING){
		label.text = TITLE_APP_NAME_BABY;
        label.text = NSLocalizedString(TITLE_APP_NAME_BABY, @"APP_NAME_BABY");
        labelLand.text = TITLE_APP_NAME_BABY;
        labelLand.text = NSLocalizedString(TITLE_APP_NAME_BABY, @"APP_NAME_BABY");
    }
	else if (TARGET_VERSION == VERSION_FAMILY_PACKING){
		label.text = TITLE_APP_NAME_FAMILY;
        label.text = NSLocalizedString(TITLE_APP_NAME_FAMILY, @"APP_NAME_FAMILY");
        labelLand.text = TITLE_APP_NAME_FAMILY;
        labelLand.text = NSLocalizedString(TITLE_APP_NAME_FAMILY, @"APP_NAME_FAMILY");
    }
	else if (TARGET_VERSION == VERSION_YOU_PACKING){
		label.text = TITLE_APP_NAME_YOU;
        label.text = NSLocalizedString(TITLE_APP_NAME_YOU, @"APP_NAME_YOU");
        labelLand.text = TITLE_APP_NAME_FAMILY;
        labelLand.text = NSLocalizedString(TITLE_APP_NAME_FAMILY, @"APP_NAME_FAMILY");
    }
	else{
		label.text = TITLE_APP_NAME_UNKNOWN;
        label.text = NSLocalizedString(TITLE_APP_NAME_UNKNOWN, @"APP_NAME_UNKNOWN");
        labelLand.text = TITLE_APP_NAME_FAMILY;
        labelLand.text = NSLocalizedString(TITLE_APP_NAME_FAMILY, @"APP_NAME_FAMILY");
    }

	[label sizeToFit];
    [labelLand sizeToFit];
	
	CGRect headerFrame = CGRectMake(0, 0, 320, 26);

	statusHeaderView = [[CSHeaderView alloc] initWithFrame:headerFrame];
	statusHeaderView.backgroundColor = COLOR_STATUS_HEADER_BG;
	statusHeaderView.textLabel.text = TITLE_STATUS_HEADER;
    statusHeaderView.textLabel.text = NSLocalizedString(TITLE_STATUS_HEADER, @"STATUS_HEADER_TEXT");

	
	customHeaderView = [[CSHeaderView alloc] initWithFrame:headerFrame];
	customHeaderView.backgroundColor = COLOR_CUSTOM_HEADER_BG;
	customHeaderView.textLabel.text = TITLE_CUSTOM_HEADER;
    customHeaderView.textLabel.text = NSLocalizedString(TITLE_CUSTOM_HEADER, @"CUSTOM_HEADER_TEXT");
    
	readyMadeHeaderView = [[CSHeaderView alloc] initWithFrame:headerFrame];
	readyMadeHeaderView.backgroundColor = COLOR_READY_MADE_HEADER_BG;
	readyMadeHeaderView.textLabel.text = TITLE_READY_MAKE_HEADER;
    readyMadeHeaderView.textLabel.text = NSLocalizedString(TITLE_READY_MAKE_HEADER, @"READYMADE_HEADER_TEXT");
    
    UIButton *Editbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    Editbtn.frame = CGRectMake(0, 0, 30, 30);
    Editbtn.tag=1;
    [Editbtn setImage:[UIImage imageNamed:@"Edit.png"] forState:UIControlStateNormal];
   // [Editbtn setBackgroundColor:[UIColor blackColor]];
    [Editbtn addTarget:self action:@selector(EditButton:) forControlEvents:UIControlEventTouchUpInside];
    
    editButtonItem=[[UIBarButtonItem alloc] initWithCustomView:Editbtn];
    
    self.navigationItem.leftBarButtonItem = editButtonItem;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 30, 30);
    [btn setImage:[UIImage imageNamed:@"AddTop.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    createBtItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
	self.navigationItem.rightBarButtonItem = createBtItem;
    
	pinkBearIcon = [[UIImage imageNamed:PINK_BEAR_ICON] retain];
	blueBearIcon = [[UIImage imageNamed:BLUE_BEAR_ICON] retain];
	templateIcon = [[UIImage imageNamed:TEMPLATE_ICON] retain];
	boyIcon = [[UIImage imageNamed:BOY_ICON] retain];
	girlIcon = [[UIImage imageNamed:GIRL_ICON] retain];
	manIcon = [[UIImage imageNamed:MAN_ICON] retain];
	womanIcon = [[UIImage imageNamed:WOMAN_ICON] retain];
	petIcon = [[UIImage imageNamed:PET_ICON] retain];
	ShoppingIcon = [[UIImage imageNamed:SHOPPING_ICON] retain];
	otherIcon = [[UIImage imageNamed:OTHER_ICON] retain];

	//mBlankView.fram;

	//[self.view addSubview:mBlankView];
	mBlankView.hidden = YES;
	
//	WelcomeViewController *controller = [[WelcomeViewController alloc] initWithNibName:@"WelcomeViewController" bundle:nil];
//	self.welcomeController = controller;
//	[self.view addSubview:welcomeController.view];
//	[self.navigationItem.leftBarButtonItem.];
//	[controller release];
	
	if (TARGET_VERSION != VERSION_FAMILY_PACKING)
    {
		UITabBarItem *item = [mTabBar.items objectAtIndex:2];
		item.enabled = NO;
		item.image = nil;
		item.title = nil;
       
        UITabBarItem *itemLand = [mTabBarLand.items objectAtIndex:2];
		itemLand.enabled = NO;
		itemLand.image = nil;
		itemLand.title = nil;

	}
	
	[self loadCustomList];
	[self updateStatusList];	
    
   if(IOS_NEWER_THAN(7))
    {
      
    }
    else
    {
       // mTabBar.backgroundColor=[UIColor colorWithRed:64/256 green:64/256 blue:64/256 alpha:1];
        mTabBar.backgroundImage=[UIImage imageNamed:@"normbot.png"];
        
//        [mTabBar insertSubview:[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"HelpTab.png"]] autorelease] atIndex:1];

//        mTabBar.selectedImageTintColor=[UIColor colorWithRed:67/256 green:130/256 blue:219/256 alpha:1];
        
    }
    //    mTabBar.selectedItem.ti
    //For Localization
    
//    lblRoottext.text = NSLocalizedString(@"RootText", @"RootText");
//    [btnAddNewList setTitle:NSLocalizedString(@"AddNewListText", @"AddNewListText") forState:UIControlStateNormal];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
}


//- (void)createCustomListIfNeed {
//	if (!customDict) {
//		self.customDict = [NSDictionary dictionaryWithObjectsAndKeys:[NSMutableArray arrayWithCapacity:16], K_LIST, [NSNumber numberWithInt:CustomList], K_TYPE, nil];
//		if (statusDict) {
//			[allListArray insertObject:customDict atIndex:1];
//		} else {
//			[allListArray insertObject:customDict atIndex:0];
//		}
//	}
//}
//
//- (void)needReloadCustomList {
//	NSMutableArray *customListArray = [PackingList findAll];
//	if (customDict) {
//		NSMutableArray *tempArray = [customDict objectForKey:K_LIST];
//		tempArray = [NSMutableArray arrayWithArray:customListArray];
//	} else {
//		self.customDict = [NSDictionary dictionaryWithObjectsAndKeys:customListArray, K_LIST, [NSNumber numberWithInt:CustomList], K_TYPE, nil];
//		if (statusDict) {
//			[allListArray insertObject:customDict atIndex:1];
//		} else {
//			[allListArray insertObject:customDict atIndex:0];
//		}
//	}
//}

// load custom list when home display for the first time
- (void)loadCustomList {
//	NSMutableArray *customListArray = [PackingList findAllWithoutPhoto];
//	//NSLog(@"loadCustomList count = %d", [customListArray count]);
//	if ([customListArray count] > 0) {
//		self.customDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:customListArray, K_LIST, [NSNumber numberWithInt:CustomList], K_TYPE, nil];
//		[allListArray insertObject:customDict atIndex:0];
//	}
	
	NSMutableArray *customListArray = [PackingList findAllWithoutPhoto];
	//NSLog(@"loadCustomList count = %d", [customListArray count]);

	[customDict setObject:customListArray forKey:K_LIST];

}
//
//- (void)removeCustomListIfNeed {
//	if (customDict) {
//		NSMutableArray *customListArray = [customDict objectForKey:K_LIST];
//		if ([customListArray count] == 0) {
//			[allListArray removeObject:customDict];
//			self.customDict = nil;
//		}
//	}
//}
//
//
//
//- (void)removeStatusListIfNeed {
//	if (statusDict) {
//		NSMutableArray *statusListArray = [statusDict objectForKey:K_LIST];
//		if ([statusListArray count] == 0) {
//			[allListArray removeObject:statusDict];
//			self.statusDict = nil;
//		}
//	}
//}

- (void) reloadData {
	[self updateStatusList];
	
	[allListArray removeAllObjects];
	NSMutableArray *tempListArray = nil;
	
	tempListArray = [statusDict objectForKey:K_LIST];
	if ([tempListArray count] > 0) {
		[allListArray addObject:statusDict];
	}
	
//	tempListArray = [customDict objectForKey:K_LIST];
//	if ([tempListArray count] > 0) {
//		[allListArray addObject:customDict];
//	}
	
	[allListArray addObject:readyMadeDict];
	
	[myTableView reloadData];
    [myTableViewLand reloadData];

}

- (void)updateStatusList {
//	NSMutableArray *statusListArray = nil;
//	if (statusDict) {
//		statusListArray = [statusDict objectForKey:K_LIST];
//		[statusListArray removeAllObjects];
//	} else {
//		statusListArray = [NSMutableArray arrayWithCapacity:16];
//	}
//	
//	NSMutableArray *tempListArray = nil;
//	if (customDict) {
//		tempListArray = [customDict objectForKey:K_LIST];
//		for(PackingList *list in tempListArray) {
//			if ([list inUse]) {
//				[statusListArray addObject:list];
//			}
//		}
//	}
//
//	if (readyMadeDict) {
//		tempListArray = [readyMadeDict objectForKey:K_LIST];
//		for(PackingList *list in tempListArray) {
//			if ([list inUse]) {
//				[statusListArray addObject:list];
//			}
//		}
//	}
//	
//	if ([statusListArray count] > 0) {
//		if (!statusDict) {
//			self.statusDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:statusListArray, K_LIST, [NSNumber numberWithInt:StatusList], K_TYPE, nil];
//			[allListArray insertObject:statusDict atIndex:0];
//		} 
//	} else {
//		[allListArray removeObject:statusDict];
//		self.statusDict = nil;
//	}
	
	NSMutableArray *statusListArray = [statusDict objectForKey:K_LIST];
	
	[statusListArray removeAllObjects];
	
	NSMutableArray *tempListArray = nil;

	// search custom list
	tempListArray = [customDict objectForKey:K_LIST];
	for(PackingList *list in tempListArray) {
	//	if ([list inUse]) {
			[statusListArray addObject:list];
	//	}
	}
	// search ready-made list
	tempListArray = [readyMadeDict objectForKey:K_LIST];
	for(PackingList *list in tempListArray) {
		if ([list inUse]) {
			[statusListArray addObject:list];
		}
	}
	for(NSMutableArray *array in readyMadeListForDeluxeArray){
		for(PackingList *list in array){
			if([list inUse])
				[statusListArray addObject:list];
		}
	}
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
//    {
//        [layoutManager translateToPortraitForView:self.view withAnimation:NO];
//    }
//    else
//    {
//        [layoutManager translateToLandscapeForView:self.view withAnimation:NO];
//    }
    
    [self.tabBarItem setImageInsets: UIEdgeInsetsMake(40, 0, -40, 0)];
	
	NSString *customFontName =[[NSString alloc] initWithFormat:@"%@",[appDelegate getCustomFontName]] ;
    NSLog(@"custome font>>>%@",customFontName);
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
//	label.font = [UIFont fontWithName:customFontName size:fontSize];
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil)
    {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];
        labelLand.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];	
	}
	else
    {
		label.font=[UIFont fontWithName:customFontName size:fontSize];
        labelLand.font=[UIFont fontWithName:customFontName size:fontSize];
	}
	
	[label sizeToFit];
    [labelLand sizeToFit];

	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour;
	// read the data back from the user defaults
	NSData *data1= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_BAR_COLOR];
	// check whether you got anything
	if(data1 == nil)
    {
        if (IOS_NEWER_THAN(7))
        {
    // kirti        self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
            self.navigationController.navigationBar.backgroundColor=[UIColor blackColor];
//            self.navigationItem.backBarButtonItem.tintColor=[UIColor whiteColor];
//            self.navigationItem.leftBarButtonItem.tintColor=[UIColor whiteColor];
//            self.navigationItem.rightBarButtonItem.tintColor=[UIColor whiteColor];
        }
        else
        {
            self.navigationController.navigationBar.tintColor = [UIColor blackColor];
        }
        
		// use this to set the colour the first time your app runs
	} else
    {
		// this recreates the colour you saved
		theColour = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data1];
        
		if (IOS_NEWER_THAN(7))
        {
       self.navigationController.navigationBar.barTintColor = theColour;
        }
        else
        {
            self.navigationController.navigationBar.tintColor = theColour;
        }
	}
	// and finally set the colour of your label

	
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil)
    {
		// use this to set the colour the first time your app runs
        if (IOS_NEWER_THAN(7))
        {
            label.textColor=[UIColor whiteColor];
            labelLand.textColor=[UIColor whiteColor];
            
            
            self.navigationItem.backBarButtonItem.tintColor=[UIColor whiteColor];;
            self.navigationItem.leftBarButtonItem.tintColor=[UIColor whiteColor];;
            self.navigationItem.rightBarButtonItem.tintColor=[UIColor whiteColor];;

        }
        else
        {
            label.textColor=[UIColor whiteColor];
            labelLand.textColor=[UIColor whiteColor];

        }
       
	}
    else
    {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
        
        if (IOS_NEWER_THAN(7)) {
            self.navigationItem.backBarButtonItem.tintColor=theColour1;
            self.navigationItem.leftBarButtonItem.tintColor=theColour1;
            self.navigationItem.rightBarButtonItem.tintColor=theColour1;
        }
        
		label.textColor = theColour1;
	}
	// and finally set the colour of your label
	
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour2;
	// read the data back from the user defaults
	NSData *data12= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
	// check whether you got anything
	if(data12 == nil) {
		// use this to set the colour the first time your app runs
        myTableView.backgroundColor = [UIColor whiteColor];
          myTableViewLand.backgroundColor = [UIColor whiteColor];
        
	} else {
		// this recreates the colour you saved
		theColour2 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data12];
		myTableView.backgroundColor = theColour2;
        myTableViewLand.backgroundColor = theColour2;

	}
	// and finally set the colour of your label
	
	
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour22;
	// read the data back from the user defaults
	NSData *data22= [[NSUserDefaults standardUserDefaults] dataForKey: K_SET_HEADER_COLOR];
	// check whether you got anything
	if(data22 == nil) {
		// use this to set the colour the first time your app runs
		statusHeaderView.backgroundColor = [UIColor purpleColor];        
        customHeaderView.backgroundColor = [UIColor purpleColor];        
        readyMadeHeaderView.backgroundColor = [UIColor purpleColor];        
	} else {
		// this recreates the colour you saved
		theColour22 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data22];
		statusHeaderView.backgroundColor = theColour22;
        customHeaderView.backgroundColor = theColour22;
        readyMadeHeaderView.backgroundColor = theColour22;
	}
	// and finally set the colour of your label
	
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour33;
	// read the data back from the user defaults
	NSData *data33= [[NSUserDefaults standardUserDefaults] dataForKey: K_SET_HEADER_TEXT_COLOR];
	// check whether you got anything
	if(data33 == nil) {
		// use this to set the colour the first time your app runs
		statusHeaderView.textLabel.textColor = [UIColor whiteColor];        
        customHeaderView.textLabel.textColor = [UIColor whiteColor];        
        readyMadeHeaderView.textLabel.textColor = [UIColor whiteColor];        
	} else {
		// this recreates the colour you saved
		theColour33 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data33];
		statusHeaderView.textLabel.textColor = theColour33;
        customHeaderView.textLabel.textColor = theColour33;
        readyMadeHeaderView.textLabel.textColor = theColour33;
	}
	// and finally set the colour of your label
    
   	
	[self loadCustomList];
	mBlankView.hidden = YES;
	[mTabBar setSelectedItem: nil];
    [mTabBarLand setSelectedItem: nil];
    
  //  myTableView.backgroundColor = [UIColor clearColor];
  //  myTableViewLand.backgroundColor = [UIColor clearColor];

	[self reloadData];
}


//- (void)viewDidAppear:(BOOL)animated {
//    [super viewDidAppear:animated];
//}


- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	
}


- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
	[self setEditing:NO animated:NO];
}

/*
 // Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
 */

- (void)startPinCode
{
    txt_one.text = @"";
    txt_two.text = @"";
    txt_three.text = @"";
    txt_four.text = @"";
    [txt_one becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
		
    tabBar.selectedImageTintColor=[UIColor blackColor];
    
	if (item.tag == TABBAR_INFO)
    {
		InfoViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[InfoViewController alloc] initWithNibName:@"InfoViewController" bundle:nil];
		else
			controller = [[InfoViewController alloc] initWithNibName:@"InfoViewController1" bundle:nil];
		
		[self.navigationController pushViewController:controller animated:YES];
		[controller release];
	} else if (item.tag == TABBAR_SETTINGS) {
		SettingsViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
		else
			controller = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController1" bundle:nil];
		
		[self.navigationController pushViewController:controller animated:YES];
		[controller release];
	} else if (item.tag == TABBAR_TODO) {	
		/*
		[self.navigationController presentModalViewController:mTaskAlertController animated:YES];
		 */
#if !TARGET_IPHONE_SIMULATOR
		NSString* systemVersion = [UIDevice currentDevice].systemVersion;
		if ([[systemVersion substringToIndex:2] isEqualToString:@"2."]) {
			// Current device version is lower than 3.0
			UIAlertView* alert = [[UIAlertView alloc] initWithTitle:M_ERROR 
															message:M_ERROR_LOW_DEVICE_VERSION 
														   delegate:self 
												  cancelButtonTitle:I_OK
												  otherButtonTitles:nil];
			[alert show];
			[alert release];
			[mTabBar setSelectedItem:nil];
            [mTabBarLand setSelectedItem:nil];

			return;
		}
#endif
		
		TaskAlertViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[TaskAlertViewController alloc] initWithNibName:@"TaskAlertViewController" bundle:nil];
		else
			controller = [[TaskAlertViewController alloc] initWithNibName:@"TaskAlertViewController1" bundle:nil];
		
		[self.navigationController pushViewController:controller animated:YES];
		[controller release];
		
	} else if (item.tag == TABBAR_HELP) {
		//myTableView.userInteractionEnabled = NO;
        //myTableViewLand.userInteractionEnabled = NO;
		//self.navigationController.navigationBar.userInteractionEnabled = NO;
		
		HelpViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[HelpViewController alloc] initWithNibName:@"HelpViewController" bundle:nil];
		else
			controller = [[HelpViewController alloc] initWithNibName:@"HelpViewController1" bundle:nil];
		
		/*CGFloat y = self.view.frame.size.height - controller.view.frame.size.height;
		CGRect frame = controller.view.frame;
		frame.origin.y = y;
		controller.view.frame = frame;
		controller.homeContoller = self;
		self.helpController = controller;
		[self.view addSubview:helpController.view];
		[controller release];
		[mTabBar setSelectedItem:nil];*/
		[self.navigationController pushViewController:controller animated:YES];
		[controller release];
		
	}else if (item.tag == TABBAR_BRIEFCASE){
		
		appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
		
		if (!appDelegate.firstTime){
			
			UIAlertView *mAlertView = [[UIAlertView alloc] initWithTitle:@"Do you want to set-up a password to protect your personal information?" message: nil
													  delegate:self cancelButtonTitle:@"YES" otherButtonTitles: @"NO",nil];
			[mAlertView setTag:1];
			[mAlertView show];	
			[mAlertView release];
			appDelegate.firstTime = YES;
			NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
			[userSettings setBool:appDelegate.firstTime forKey:@"firstTime"];
			[userSettings synchronize];
			
		}else{
			if (appDelegate.password){
                
                if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
                {
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                    {
                        [brifcaseLoginView setFrame:CGRectMake(225,200, 300, 100)];
                    }
                    else
                    {
                        [brifcaseLoginView setFrame:CGRectMake(10,100, 300, 100)];
                    }
                }
                else
                {
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                    {
                        [brifcaseLoginView setFrame:CGRectMake(350,50, 320, 100)];
                    }
                    else
                    {
                         [brifcaseLoginView setFrame:CGRectMake(85,01, 320, 100)];
                    }
                }
                [self.navigationController.navigationBar setUserInteractionEnabled:NO];
                 [myTableView setUserInteractionEnabled:NO];
                [self.view addSubview:brifcaseLoginView];
				[self.view bringSubviewToFront:brifcaseLoginView];
                [code_seg_four becomeFirstResponder];
                
                
//				mPasscodeAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Login Briefcase",@"LOGIN_BRIEFCASE")
//													   message:NSLocalizedString(@"Please enter passcode!\n\n\n\n",@"ENTER_PASSCODE") delegate:self 
//											   cancelButtonTitle:NSLocalizedString(@"Login",@"LOGIN")	otherButtonTitles:nil];
//				//				mPasscodeAlertView.delegate = self;
//				[mPasscodeAlertView setTag:2];
//				[mPasscodeAlertView show];
//				[mPasscodeAlertView release];
                
                
                
			}else{
				BriefcaseViewController  *controller = nil;
				
				if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
					controller = [[BriefcaseViewController alloc] initWithNibName:@"BriefcaseViewController" bundle:nil];
				else
					controller = [[BriefcaseViewController alloc] initWithNibName:@"BriefcaseViewController1" bundle:nil];
				
				[self.navigationController pushViewController:controller animated:YES];
				[controller release];
			}
			
		}
	}
	
}
- (IBAction) buttonAction:(id) sender
{
	if (sender == createBtItem || sender != settingsBtItem ) {
				
		AddListViewController *controller = nil;
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[AddListViewController alloc] initWithNibName:@"AddListViewController" bundle:nil];
		else
			controller = [[AddListViewController alloc] initWithNibName:@"AddListViewController1" bundle:nil];
		
		controller.homeController = self;
		controller.customListArray = [customDict objectForKey:K_LIST];
		[self.navigationController pushViewController:controller animated:YES];
		[controller release];
	}
}

-(IBAction)EditButton:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    
    if ([sender tag]==1)
    {
        myTableView.editing = YES;
        myTableViewLand.editing = YES;
        [myTableView reloadData];
        [myTableViewLand reloadData];
        btn.tag=2;
        
        [btn setImage:[UIImage imageNamed:@"Done.png"] forState:UIControlStateNormal];
    }
    else
    {
        myTableView.editing = NO;
        myTableViewLand.editing = NO;
        [myTableView reloadData];
        [myTableViewLand reloadData];
        btn.tag=1;

        [btn setImage:[UIImage imageNamed:@"Edit.png"] forState:UIControlStateNormal];
    }
}

- (void) closeHelpPage {
	self.helpController = nil;
	myTableView.userInteractionEnabled = YES;
    myTableViewLand.userInteractionEnabled = YES;
	self.navigationController.navigationBar.userInteractionEnabled = YES;
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animate {
	[super setEditing:editing animated:animate];
	myTableView.editing = editing;
    myTableViewLand.editing = editing;
	[myTableView reloadData];
    [myTableViewLand reloadData];
}

- (int) hasInUseInReadyMadeList{
	int i = 0;
	ListType listType;
	NSArray *listArray;
	for(NSDictionary *dict in allListArray){
		listType = [[dict objectForKey:K_TYPE] intValue];
		listArray = [dict objectForKey:K_LIST];
		if(listType == ReadyMadeList)
			break;
	}

	if (TARGET_VERSION == VERSION_YOU_PACKING){
		for(PackingList *list in [readyMadeListForDeluxeArray objectAtIndex:2]){
			if ([list inUse]) 
				i++;
		}
		return i;
	}
	else if (TARGET_VERSION == VERSION_FAMILY_PACKING){
		for(NSMutableArray *array in readyMadeListForDeluxeArray){
			int j = 0;
			for(PackingList *list in array){
				if([list inUse])
					j++;
			}
			if(j>0)
				i++;
		}
		return i;
	}
	else {
		for(PackingList *list in listArray){
			if ([list inUse]) 
				i++;
		}
		return i;
	}
	return 0;
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
	if(appDelegate.readyMadeListShow || [self hasInUseInReadyMadeList])
		return [allListArray count];
	else{
		if ([allListArray count] == 1){
			mBlankView.hidden = NO;
			return 1;
		}
		else
			return [allListArray count] - 1;
	}
		
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

	NSDictionary *dict = [allListArray objectAtIndex:section];
	ListType listType = [[dict objectForKey:K_TYPE] intValue];
	if (listType == StatusList) {
		return statusHeaderView;
	} 
	else if (listType == CustomList) {
		return customHeaderView;
	}
	else if (listType == ReadyMadeList) {
		BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
		if(!appDelegate.readyMadeListShow && [allListArray count] == 1 )
			return nil;
		else
			return readyMadeHeaderView;
	} else {
		return nil;
	}
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	NSDictionary *dict = [allListArray objectAtIndex:section];
	ListType listType = [[dict objectForKey:K_TYPE] intValue];
	
	BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate; 
	if (listType == ReadyMadeList && appDelegate.readyMadeListShow){
		if (TARGET_VERSION == VERSION_YOU_PACKING)
			return 2;
		else if (TARGET_VERSION == VERSION_FAMILY_PACKING)
			return 4;
		else
			return 5;
	}else if(listType == ReadyMadeList && !appDelegate.readyMadeListShow){
		return [self hasInUseInReadyMadeList];
	}
		
	else {
		    return [[dict objectForKey:K_LIST] count];
	}


}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//	NSDictionary *dict = [allListArray objectAtIndex:section];
//	return [dict objectForKey:@"Title"];
//}
//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
//	return [NSArray arrayWithObjects:@"S", @"C", @"T",nil];
//}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSDictionary *dict = [allListArray objectAtIndex:indexPath.section];
	ListType listType = [[dict objectForKey:K_TYPE] intValue];
	NSArray *listArray = [dict objectForKey:K_LIST];
	UITableViewCell *cell;
	if (listType == StatusList) {
		static NSString *CellIdentifier = @"StatusCell";
		
		cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[CSTableViewCell alloc] initWithCSStyle:CSTableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];

			cell.hidesAccessoryWhenEditing =NO;
			((CSTableViewCell*)cell).imageView.contentMode = UIViewContentModeScaleAspectFill;
		}

		[cell setNeedsDisplay];
		CSTableViewCell *csCell = (CSTableViewCell*) cell;
		// Configure the cell.
		PackingList *list = [listArray objectAtIndex:indexPath.row];

		if ([list hasIcon]) {
			csCell.image = list.icon;
		} else if(TARGET_VERSION == VERSION_BABY_PACKING){
		
			if (list.sex == BabySexBoy) {
				csCell.image = blueBearIcon;
			} else if (list.sex == BabySexGirl) {
				csCell.image = pinkBearIcon;
			} else {
				csCell.image = otherIcon;
			}
			
		}else if (TARGET_VERSION == VERSION_FAMILY_PACKING){
			if(list.sex == BabySexBoy && list.age == Adult)
				csCell.image = manIcon;
			else if (list.sex == BabySexGirl && list.age == Adult)
				csCell.image = womanIcon;
			else if (list.sex == BabySexBoy && list.age == Child)
				csCell.image = boyIcon;
			else if (list.sex == BabySexGirl && list.age == Child)
				csCell.image = girlIcon;
			else if (list.sex == BabySexBoy && list.age == Infant_baby)
				csCell.image = blueBearIcon;
			else if (list.sex == BabySexGirl && list.age == Infant_baby)
				csCell.image = pinkBearIcon;
			else if (list.age == Pet)
				csCell.image = petIcon;
			else if (list.age == ShoppingList)
				csCell.image = ShoppingIcon;
			else if (list.age == Other)
				csCell.image = otherIcon;
			else
				csCell.image =otherIcon;
		}else if (TARGET_VERSION == VERSION_YOU_PACKING){
			if(list.sex == BabySexBoy)
				csCell.image = manIcon;
			else if(list.sex == BabySexGirl)
				csCell.image = womanIcon;
			else
				csCell.image = otherIcon;
		}
		
		NSString *customFontName = [appDelegate getCustomFontName];
		
		if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
			csCell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];		
		}
		
		if (list.sex == BabySexBoy) {
			csCell.textLabel.textColor = BOY_COLOR;
		} else if(list.sex == BabySexGirl) {
			csCell.textLabel.textColor = GIRL_COLOR;
		} else {			
			//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
			UIColor *theColour1;
			// read the data back from the user defaults
			NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
			// check whether you got anything
			if(data11 == nil) {
				// use this to set the colour the first time your app runs
				csCell.textLabel.textColor = DEF_COLOR;
			} else {
				// this recreates the colour you saved
				theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
				csCell.textLabel.textColor = theColour1;
			}
			// and finally set the colour of your label
		}
		
		
#ifdef __IPHONE_3_0
		
		if (myTableView.editing || myTableViewLand.editing) {
			if ([list editable]) {
				csCell.editingAccessoryType = UITableViewCellAccessoryDetailDisclosureButton;
			} else {
				csCell.editingAccessoryType = UITableViewCellAccessoryNone;
			}
			
		} else {
			if ([list isComplete]) {
				csCell.editingAccessoryType = UITableViewCellAccessoryCheckmark;
			} else {
				csCell.editingAccessoryType = UITableViewCellAccessoryNone;
			}
		}
#else
		if (myTableView.editing || myTableViewLand.editing) {
			if ([list editable]) {
				csCell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
			} else {
				csCell.accessoryType = UITableViewCellAccessoryNone;
			}
			
		} else {
			if ([list isComplete]) {
				
				csCell.accessoryType = UITableViewCellAccessoryCheckmark;
			} else {
				csCell.accessoryType = UITableViewCellAccessoryNone;
			}
		}
#endif
		if (myTableView.editing || myTableViewLand.editing) {
			if ([list editable]) {
				csCell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
			} else {
				csCell.accessoryType = UITableViewCellAccessoryNone;
			}
			
		} else {
			if ([list isComplete]) {
				
				csCell.accessoryType = UITableViewCellAccessoryCheckmark;
			} else {
				csCell.accessoryType = UITableViewCellAccessoryNone;
			}
		}
		
		
		if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
			csCell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];		
		}
		//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
		UIColor *theColour1;
		// read the data back from the user defaults
		NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
		// check whether you got anything
		if(data11 == nil) {
			// use this to set the colour the first time your app runs
			csCell.textLabel.textColor = DEF_COLOR;
		} else {
			// this recreates the colour you saved
			theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
			csCell.textLabel.textColor = theColour1;
		}
		// and finally set the colour of your label
		
		csCell.textLabel.text = list.name;
		csCell.detailTextLabel.text = [list getAbstract];
		if(list.checkedCount > 0)
			csCell.detailTextLabel.textColor = IN_USE_COLOR;
		else
			csCell.detailTextLabel.textColor = [UIColor darkGrayColor];
    } 
	else if (listType == CustomList) {
		static NSString *CellIdentifier = @"CustomCell";
		//UILabel *inUseLabel;
		cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[CSTableViewCell alloc] initWithCSStyle:CSTableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
			((CSTableViewCell*)cell).detailTextLabel.textColor = RED_COLOR;
			((CSTableViewCell*)cell).imageView.contentMode = UIViewContentModeScaleAspectFill;
			UIImageView *inUseView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 35, 22)];
			inUseView.image = [UIImage imageNamed:IS_USE_ICON];
			((CSTableViewCell*)cell).csAccessoryView = inUseView;
			[inUseView release];
		}
		CSTableViewCell *csCell = (CSTableViewCell*) cell;
		// Configure the cell.
		PackingList *list = [listArray objectAtIndex:indexPath.row];

		if ([list hasIcon]) {
			csCell.image = list.icon;
		} else if(TARGET_VERSION == VERSION_BABY_PACKING){
			
			if (list.sex == BabySexBoy) {
				csCell.image = blueBearIcon;
			} else if (list.sex == BabySexGirl) {
				csCell.image = pinkBearIcon;
			} else {
				csCell.image = otherIcon;
			}
			
		}else if (TARGET_VERSION == VERSION_FAMILY_PACKING){
			if(list.sex == BabySexBoy && list.age == Adult)
				csCell.image = manIcon;
			else if (list.sex == BabySexGirl && list.age == Adult)
				csCell.image = womanIcon;
			else if (list.sex == BabySexBoy && list.age == Child)
				csCell.image = boyIcon;
			else if (list.sex == BabySexGirl && list.age == Child)
				csCell.image = girlIcon;
			else if (list.sex == BabySexBoy && list.age == Infant_baby)
				csCell.image = blueBearIcon;
			else if (list.sex == BabySexGirl && list.age == Infant_baby)
				csCell.image = pinkBearIcon;
			else if (list.age == Pet)
				csCell.image = petIcon;
			else if (list.age == ShoppingList)
				csCell.image = ShoppingIcon;
			else if (list.age == Other)
				csCell.image = otherIcon;
		}else if (TARGET_VERSION == VERSION_YOU_PACKING){
			if(list.sex == BabySexBoy)
				csCell.image = manIcon;
			else
				csCell.image = womanIcon;
		}
		NSString *customFontName = [appDelegate getCustomFontName];
		
		if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
			csCell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];		
		}
		
		if (list.sex == BabySexBoy) {
			csCell.textLabel.textColor = BOY_COLOR;
		} else if(list.sex == BabySexGirl) {
			csCell.textLabel.textColor = GIRL_COLOR;
		} else {
			//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
			UIColor *theColour1;
			// read the data back from the user defaults
			NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
			// check whether you got anything
			if(data11 == nil) {
				// use this to set the colour the first time your app runs
				csCell.textLabel.textColor = DEF_COLOR;
			} else {
				// this recreates the colour you saved
				theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
				csCell.textLabel.textColor = theColour1;
			}
			// and finally set the colour of your label
			
		}	
		
		csCell.textLabel.text = list.name;
#ifdef __IPHONE_3_0
		if (myTableView.editing || myTableViewLand.editing) {
			[csCell hideAccessoryView];
			csCell.editingAccessoryType = UITableViewCellAccessoryDetailDisclosureButton;
		} else {
			csCell.editingAccessoryType = UITableViewCellAccessoryNone;
			if ([list inUse]) {
				[csCell showAccessoryView];
			} else {
				[csCell hideAccessoryView];
			}
		}
		
#else
		if (myTableView.editing ||  myTableViewLand.editing) {
			[csCell hideAccessoryView];
			csCell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
		} else {
			csCell.accessoryType = UITableViewCellAccessoryNone;
			if ([list inUse]) {
				[csCell showAccessoryView];
			} else {
				[csCell hideAccessoryView];
			}
		}
#endif
    } //end if (listType == CustomList)
	
	else if (listType == ReadyMadeList) {
		BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
		if (TARGET_VERSION == VERSION_BABY_PACKING){
			
			static NSString *CellIdentifier = @"TemplateCell";
			
			cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
			if (cell == nil) {
				cell = [[[CSTableViewCell alloc] initWithCSStyle:CSTableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
				((CSTableViewCell*)cell).detailTextLabel.textColor = RED_COLOR;
				//((CSTableViewCell*)cell).imageView.contentMode = UIViewContentModeScaleAspectFill;
				UIImageView *inUseView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 35, 22)];
				inUseView.image = [UIImage imageNamed:IS_USE_ICON];
				((CSTableViewCell*)cell).csAccessoryView = inUseView;
				[inUseView release];
			}
			CSTableViewCell *csCell = (CSTableViewCell*) cell;
			// Configure the cell.
			if(appDelegate.readyMadeListShow){
				PackingList *list = [listArray objectAtIndex:indexPath.row];
				NSString *customFontName = [appDelegate getCustomFontName];
				
				if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
					csCell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];		
				}
				//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
				UIColor *theColour1;
				// read the data back from the user defaults
				NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
				// check whether you got anything
				if(data11 == nil) {
					// use this to set the colour the first time your app runs
					csCell.textLabel.textColor = DEF_COLOR;
				} else {
					// this recreates the colour you saved
					theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
					csCell.textLabel.textColor = theColour1;
				}
				// and finally set the colour of your label
				
				csCell.textLabel.text = list.name;
				csCell.image = otherIcon;
				if ([list inUse])
					[csCell showAccessoryView];
				else
					[csCell hideAccessoryView];
			}else{
				int i = -1;
				for(PackingList *list in listArray){
					if([list inUse]){
						i++;
						if (indexPath.row == i){
							NSString *customFontName = [appDelegate getCustomFontName];
							
							if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
								csCell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];		
							}
							//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
							UIColor *theColour1;
							// read the data back from the user defaults
							NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
							// check whether you got anything
							if(data11 == nil) {
								// use this to set the colour the first time your app runs
								csCell.textLabel.textColor = DEF_COLOR;
							} else {
								// this recreates the colour you saved
								theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
								csCell.textLabel.textColor = theColour1;
							}
							// and finally set the colour of your label
							
							csCell.textLabel.text = list.name;
							csCell.image = otherIcon;
							[csCell showAccessoryView];
						}

					}
				}
			}

			

			
		} //end if (listType == ReadyMadeList)
		else if (TARGET_VERSION == VERSION_FAMILY_PACKING){
			static NSString *CellIdentifier = @"TemplateCell";
			
			cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
			if (cell == nil) {
				cell = [[[CSTableViewCell alloc] initWithCSStyle:CSTableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
				((CSTableViewCell*)cell).detailTextLabel.textColor = RED_COLOR;
				//((CSTableViewCell*)cell).imageView.contentMode = UIViewContentModeScaleAspectFill;
				UIImageView *inUseView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 35, 22)];
				inUseView.image = [UIImage imageNamed:IS_USE_ICON];
				((CSTableViewCell*)cell).csAccessoryView = inUseView;
				[inUseView release];
			}
			CSTableViewCell *csCell = (CSTableViewCell*) cell;
			// Configure the cell.
		//	PackingList *list = [listArray objectAtIndex:indexPath.row];
			if(appDelegate.readyMadeListShow){
				NSString *customFontName = [appDelegate getCustomFontName];
				
				if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
					csCell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];		
				}
				//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
				UIColor *theColour1;
				// read the data back from the user defaults
				NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
				// check whether you got anything
				if(data11 == nil) {
					// use this to set the colour the first time your app runs
					csCell.textLabel.textColor = DEF_COLOR;
				} else {
					// this recreates the colour you saved
					theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
					csCell.textLabel.textColor = theColour1;
				}
				// and finally set the colour of your label
				
				if(indexPath.row == 0){
					csCell.textLabel.text = @"Infant/Toddler";
                    csCell.textLabel.text = NSLocalizedString(@"Infant/Toddler", @"READYMADE_Infant_Toddler");
				}else if (indexPath.row == 1){
					csCell.textLabel.text = @"Child";
                    csCell.textLabel.text = NSLocalizedString(@"Child", @"READYMADE_Child");                    
				}else if (indexPath.row == 2){
					csCell.textLabel.text = @"Adult";
                    csCell.textLabel.text = NSLocalizedString(@"Adult", @"READYMADE_Adult");
				}else if (indexPath.row == 3){
					csCell.textLabel.text = @"Family";
                    csCell.textLabel.text = NSLocalizedString(@"Family", @"READYMADE_Family");
				}
				csCell.image = otherIcon;
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			}else{
			//	int i = -1;
				NSMutableArray *nameList = [[NSMutableArray alloc] initWithCapacity:0];
				for(int m = 0 ; m < 4; m++){
					NSString * text;
					if(m == 0){
						text = @"Infant/Toddler";
                        text = NSLocalizedString(@"Infant/Toddler", @"READYMADE_Infant_Toddler");
					}else if (m == 1){
						text = @"Child";
                        text = NSLocalizedString(@"Child", @"READYMADE_Child");
					}else if (m == 2){
						text = @"Adult";
                        text = NSLocalizedString(@"Adult", @"READYMADE_Adult");
					}else if (m == 3){
						text = @"Family";
                        text = NSLocalizedString(@"Family", @"READYMADE_Family");
					}
					NSMutableArray *array = [readyMadeListForDeluxeArray objectAtIndex:m];
					int j = 0;
					for(PackingList *list in array){
						if([list inUse])
							j++;
					}
					if(j > 0)
						[nameList addObject:text];

				}
				
				NSString *customFontName = [appDelegate getCustomFontName];
				
				if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
					csCell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];		
				}
				//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
				UIColor *theColour1;
				// read the data back from the user defaults
				NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
				// check whether you got anything
				if(data11 == nil) {
					// use this to set the colour the first time your app runs
					csCell.textLabel.textColor = DEF_COLOR;
				} else {
					// this recreates the colour you saved
					theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
					csCell.textLabel.textColor = theColour1;
				}
				// and finally set the colour of your label
				csCell.textLabel.text = [nameList objectAtIndex:indexPath.row];
				csCell.image = otherIcon;
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				[nameList release];
			}
		}
			
		else if (TARGET_VERSION == VERSION_YOU_PACKING){
			static NSString *CellIdentifier = @"TemplateCell";
			
			cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
			if (cell == nil) {
				cell = [[[CSTableViewCell alloc] initWithCSStyle:CSTableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
				((CSTableViewCell*)cell).detailTextLabel.textColor = RED_COLOR;
				//((CSTableViewCell*)cell).imageView.contentMode = UIViewContentModeScaleAspectFill;
				UIImageView *inUseView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 35, 22)];
				inUseView.image = [UIImage imageNamed:IS_USE_ICON];
				((CSTableViewCell*)cell).csAccessoryView = inUseView;
				[inUseView release];
			}
			CSTableViewCell *csCell = (CSTableViewCell*) cell;
			// Configure the cell.
			if(appDelegate.readyMadeListShow){
			PackingList *list = [[readyMadeListForDeluxeArray objectAtIndex:2] objectAtIndex:indexPath.row];
				NSString *customFontName = [appDelegate getCustomFontName];
				
				if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
					csCell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];		
				}
				//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
				UIColor *theColour1;
				// read the data back from the user defaults
				NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
				// check whether you got anything
				if(data11 == nil) {
					// use this to set the colour the first time your app runs
					csCell.textLabel.textColor = DEF_COLOR;
				} else {
					// this recreates the colour you saved
					theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
					csCell.textLabel.textColor = theColour1;
				}
				// and finally set the colour of your label
				
			csCell.textLabel.text = list.name;
			csCell.image = otherIcon;
			if ([list inUse])
				[csCell showAccessoryView];
			else
				[csCell hideAccessoryView];
			}else{
				int i = -1;
				for(PackingList *list in [readyMadeListForDeluxeArray objectAtIndex:2]){
					if([list inUse]){
						i++;
						if (indexPath.row == i){
							NSString *customFontName = [appDelegate getCustomFontName];
							
							if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
								csCell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];		
							}
							//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
							UIColor *theColour1;
							// read the data back from the user defaults
							NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
							// check whether you got anything
							if(data11 == nil) {
								// use this to set the colour the first time your app runs
								csCell.textLabel.textColor = DEF_COLOR;
							} else {
								// this recreates the colour you saved
								theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
								csCell.textLabel.textColor = theColour1;
							}
							// and finally set the colour of your label
							
							csCell.textLabel.text = list.name;
							csCell.image = otherIcon;
							[csCell showAccessoryView];
						}
						
					}
				}
			
			}
			
		}
	}

    
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
    UIColor *theColour1;
    // read the data back from the user defaults
    NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
    // check whether you got anything
    if(data11 == nil) {
        cell.backgroundColor = [UIColor whiteColor];
        // use this to set the colour the first time your app runs
    } else {
        // this recreates the colour you saved
        theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
        cell.backgroundColor = theColour1;
    }
    // and finally set the colour of your label
    
    
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
    UIColor *theColour21;
    // read the data back from the user defaults
    NSData *data211= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
    // check whether you got anything
    if(data211 == nil) {
        	cell.textLabel.textColor = DEF_COLOR;
        // use this to set the colour the first time your app runs
    } else {
        // this recreates the colour you saved
        theColour21 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data211];
        cell.textLabel.textColor = theColour21;
    }
    // and finally set the colour of your label
    BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
    NSString *customFontName1 = [appDelegate getCustomFontName];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
        cell.textLabel.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];
    }
    else{
        cell.textLabel.font=[UIFont fontWithName:customFontName1 size:15];
    }
    
    return cell;
}




// Override to support row selection in the table view.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	//Navigation logic may go here -- for example, create and push another view controller.
	BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
	NSDictionary *dict = [allListArray objectAtIndex:indexPath.section];
	ListType listType = [[dict objectForKey:K_TYPE] intValue];
	NSArray *listArray = [dict objectForKey:K_LIST];
	PackingList *list = [listArray objectAtIndex:indexPath.row];
	//NSLog(@"didSelectRowAtIndexPath list.checkCount = %d",list.checkedCount);
	if (listType == ReadyMadeList && (TARGET_VERSION == VERSION_FAMILY_PACKING)){
		ReadyMadeListForDeluxe *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[ReadyMadeListForDeluxe alloc] initWithNibName:@"ReadyMadeListForDeluxe" bundle:nil];
		else
			controller = [[ReadyMadeListForDeluxe alloc] initWithNibName:@"ReadyMadeListForDeluxe1" bundle:nil];
		
		controller.homeController = self;
		if(!appDelegate.readyMadeListShow){
			controller.readyMadeListArray = [self.readyMadeListForDeluxeArray objectAtIndex:indexPath.row];
			[self.navigationController pushViewController:controller animated:YES];
			[controller release];
		}else{
			CSTableViewCell *aCell = (CSTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];

			if ([aCell.textLabel.text isEqualToString:@"Infant/Toddler"]){
				controller.readyMadeListArray = [self.readyMadeListForDeluxeArray objectAtIndex:0];
              //  controller.titleName=NSLocalizedString(@"Infant/Toddler", @"READYMADE_Infant_Toddler");
                 controller.titleName=@"Infant/Toddler";
            }else if ([aCell.textLabel.text isEqualToString:@"Child"]){
				controller.readyMadeListArray = [self.readyMadeListForDeluxeArray objectAtIndex:1];
               // controller.titleName=NSLocalizedString(@"Child", @"READYMADE_Child");
                 controller.titleName=@"Child";
			}else if ([aCell.textLabel.text isEqualToString:@"Adult"]){
				controller.readyMadeListArray = [self.readyMadeListForDeluxeArray objectAtIndex:2];
               // controller.titleName=NSLocalizedString(@"Adult", @"READYMADE_Adult");
                 controller.titleName=@"Adult";
			}else if ([aCell.textLabel.text isEqualToString:@"Family"]){
				controller.readyMadeListArray = [self.readyMadeListForDeluxeArray objectAtIndex:3];
               // controller.titleName=NSLocalizedString(@"Family", @"READYMADE_Family");
                controller.titleName=@"Family";
            }
			[self.navigationController pushViewController:controller animated:YES];
			[controller release];
		}

	}
	else if (listType == ReadyMadeList && (TARGET_VERSION == VERSION_YOU_PACKING)){
		
		CheckListViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[CheckListViewController alloc] initWithNibName:@"CheckListViewController" bundle:nil];
		else
			controller = [[CheckListViewController alloc] initWithNibName:@"CheckListViewController1" bundle:nil];
		
		if(appDelegate.readyMadeListShow){	
			if(indexPath.row == 0){
				controller.title = @"Vacation Trip";
                controller.title = NSLocalizedString(@"Vacation Trip", @"Vacation_Trip");
            }
			else {
				controller.title = @"Business Trip";
                controller.title = NSLocalizedString(@"Business Trip", @"Business_Trip");
            }
			PackingList *list = [[readyMadeListForDeluxeArray objectAtIndex:2] objectAtIndex:indexPath.row];
			controller.currentList = list;
			if ([controller.currentList inUse])
				controller.canEmail=YES;
			else if(listType==StatusList){
				if (list.checkedCount != 0) {
					controller.canEmail=YES;
				} 
				controller.canEmail = NO;
				
			}
			else 
				controller.canEmail=NO;
			[self.navigationController pushViewController:controller animated:YES];
			[controller release];
		}else{
			CSTableViewCell *aCell = (CSTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
			if (aCell.textLabel.text == NSLocalizedString(@"Vacation Trip", @"Vacation_Trip")){
				controller.currentList = [[readyMadeListForDeluxeArray objectAtIndex:2] objectAtIndex:0];
				controller.title = @"Vacation Trip";
                controller.title = NSLocalizedString(@"Vacation Trip", @"Vacation_Trip");
			}else{
				controller.currentList = [[readyMadeListForDeluxeArray objectAtIndex:2] objectAtIndex:1];
				controller.title = @"Business Trip";
                controller.title = NSLocalizedString(@"Business Trip", @"Business_Trip");
			}
			if ([controller.currentList inUse])
				controller.canEmail=YES;
			else if(listType==StatusList){
				if (list.checkedCount != 0) {
					controller.canEmail=YES;
				} 
				controller.canEmail = NO;
				
			}
			else
				controller.canEmail=NO;
			[self.navigationController pushViewController:controller animated:YES];
			[controller release];
		}
	}
	else if(listType == ReadyMadeList && (TARGET_VERSION == VERSION_BABY_PACKING)){
		CheckListViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[CheckListViewController alloc] initWithNibName:@"CheckListViewController" bundle:nil];
		else
			controller = [[CheckListViewController alloc] initWithNibName:@"CheckListViewController1" bundle:nil];
		
		if(appDelegate.readyMadeListShow){
			controller.title = list.name;
			[list loadItemsIfNeed];
			controller.currentList = list;
			if ([controller.currentList inUse])
				controller.canEmail=YES;
			else if(listType==StatusList){
				if (list.checkedCount != 0) {
					controller.canEmail=YES;
				} 
				controller.canEmail = NO;
				
			}
			else 
				controller.canEmail=NO;
		}else{
			CSTableViewCell *aCell = (CSTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
			NSMutableArray *tempListArray = [readyMadeDict objectForKey:K_LIST];
			for(PackingList *list in tempListArray) {
				if (list.name == aCell.textLabel.text) {
					controller.title = list.name;
					[list loadItemsIfNeed];
					controller.currentList = list;
					if ([controller.currentList inUse])
						controller.canEmail=YES;
					else if(listType==StatusList){
						if (list.checkedCount != 0) {
							controller.canEmail=YES;
						} 
						controller.canEmail = NO;
						
					}
					else 
						controller.canEmail=NO;
				}
			}
		}
		controller.customListArray = [customDict objectForKey:K_LIST];
		[self.navigationController pushViewController:controller animated:YES];
		[controller release];
	}
	else{
		if(tableView.editing && [list editable]){
			AddListViewController *controller = nil;
			
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				controller = [[AddListViewController alloc] initWithNibName:@"AddListViewController" bundle:nil];
			else
				controller = [[AddListViewController alloc] initWithNibName:@"AddListViewController1" bundle:nil];
			
			controller.homeController = self;
			NSDictionary *dict = [allListArray objectAtIndex:indexPath.section];
			//ListType listType = [[dict objectForKey:K_TYPE] intValue];
			NSArray *listArray = [dict objectForKey:K_LIST];
			PackingList *list = [listArray objectAtIndex:indexPath.row];
			[list loadItemsIfNeed];
			controller.editingList = list;
        
            if ([list hasIcon]) {
                appDelegate.ImageName =appDelegate.ImageName;
                }
            else
                {
            if(list.sex == BabySexBoy && list.age == Adult)
				 appDelegate.ImageName = @"Woman.png";
			else if (list.sex == BabySexGirl && list.age == Adult)
				 appDelegate.ImageName = @"Man.png";
			else if (list.sex == BabySexBoy && list.age == Child)
				 appDelegate.ImageName = @"Boy.png";
			else if (list.sex == BabySexGirl && list.age == Child)
				 appDelegate.ImageName = @"Girl.png";
			else if (list.sex == BabySexBoy && list.age == Infant_baby)
				 appDelegate.ImageName = @"Man.png";
			else if (list.sex == BabySexGirl && list.age == Infant_baby)
				 appDelegate.ImageName = @"Woman.png";
			else if (list.age == Pet)
				 appDelegate.ImageName = @"Pet.png";
			else if (list.age == ShoppingList)
				 appDelegate.ImageName = @"Shopping.png";
			else if (list.age == Other)
				 appDelegate.ImageName = @"Other.png";
			else
				 appDelegate.ImageName = OTHER_ICON;
                }
           
			[self.navigationController pushViewController:controller animated:YES];
			[controller release];
		}
		else
        {
            [list loadItemsIfNeed];
			CheckListViewController *controller = nil;
			
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				controller = [[CheckListViewController alloc] initWithNibName:@"CheckListViewController" bundle:nil withObj:list];
			else
              
				controller = [[CheckListViewController alloc] initWithNibName:@"CheckListViewController1" bundle:nil withObj:list];
			
            controller.controllerTitle = [NSString stringWithFormat:@"%@",list.name];
            
			// will do nothing if this is a ready made list
			
			controller.currentList = list;
			if ([controller.currentList inUse])
				//controller.navigationItem.rightBarButtonItem.enabled=YES;
				controller.canEmail=YES;
			else if(listType==StatusList){
				if (list.checkedCount != 0) {
					controller.canEmail=YES;
				} 
				controller.canEmail = NO;
				
			}
			else 
				controller.canEmail=NO;
			//controller.navigationItem.rightBarButtonItem.enabled=NO;
			controller.customListArray = [customDict objectForKey:K_LIST];
			[self.navigationController pushViewController:controller animated:YES];
			[controller release];
		}
		
	}
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
	AddListViewController *controller = nil;
	
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		controller = [[AddListViewController alloc] initWithNibName:@"AddListViewController" bundle:nil];
	else
		controller = [[AddListViewController alloc] initWithNibName:@"AddListViewController1" bundle:nil];

	controller.homeController = self;
	NSDictionary *dict = [allListArray objectAtIndex:indexPath.section];
	//ListType listType = [[dict objectForKey:K_TYPE] intValue];
	NSArray *listArray = [dict objectForKey:K_LIST];
	PackingList *list = [listArray objectAtIndex:indexPath.row];
	[list loadItemsIfNeed];
	controller.editingList = list;
	[self.navigationController pushViewController:controller animated:YES];
	[controller release];
	
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    //return YES;
	NSDictionary *dict = [allListArray objectAtIndex:indexPath.section];
	ListType listType = [[dict objectForKey:K_TYPE] intValue];
	if (listType == ReadyMadeList) {
		return NO;
	} else {
		return YES;
	}
}

//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
//	return UITableViewCellEditingStyleNone;
//}

//- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
//	return NO;
//}




// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
		NSDictionary *dict = [allListArray objectAtIndex:indexPath.section];
		ListType listType = [[dict objectForKey:K_TYPE] intValue];
		NSMutableArray *listArray = [dict objectForKey:K_LIST];
		PackingList *list = [listArray objectAtIndex:indexPath.row];
		[list retain];
		[listArray removeObjectAtIndex:indexPath.row];
		[myTableView reloadData];
        [myTableViewLand reloadData];
		//[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
		
		if (listType == StatusList) {
			
			[list uncheckAllItems];
			[list updateData];
//			//will do nothing if the list is ready made list(because pk == INVALID_PK)
			[list updateItemsData];
//			
//			//don't need to reload status list here, just need check count
//			// modify it tomorrow
//			//[self removeStatusListIfNeed];
			// code above is used for version 1.3.2			
			
			[list deleteData];
			[self loadCustomList];
			[self updateStatusList];
			
			
		} else if (listType == CustomList) {
			// delete list in db
			[list deleteData];
			
			//for test
			NSLog(@"PackingItem total count = %d", [[PackingItem findAll] count]);
			
			//[self removeCustomListIfNeed];
			//for ui data
			//[self updateStatusList];
		}
		[list release];
		
		
		[self reloadData];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}



/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/
#pragma mark Alert view methods
- (void)willPresentAlertView:(UIAlertView *)alertView {
	if (alertView.tag == 2){
	
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			alertView.frame = CGRectMake((768-ALERT_VIEW_WIDTH)/2, (1004-ALERT_VIEW_HEIGHT)/2, ALERT_VIEW_WIDTH, ALERT_VIEW_HEIGHT);
		else
			alertView.frame = CGRectMake((320-ALERT_VIEW_WIDTH)/2, (350-ALERT_VIEW_HEIGHT)/2, ALERT_VIEW_WIDTH, ALERT_VIEW_HEIGHT);
		
		mLoginMessage.frame = CGRectMake(PASSCODE_LABEL_LEFT, PASSCODE_LABEL_TOP, ALERT_VIEW_WIDTH-2*PASSCODE_LABEL_LEFT, PASSCODE_LABEL_HEIGHT);
		mLoginMessage.font = [UIFont fontWithName:@"Arial" size:14];
		mLoginMessage.text = [NSString stringWithFormat:NSLocalizedString(@"%d more chances to try.",nil), MAX_RETRY_COUNT-mRetryCounter+1];
		[alertView addSubview:mLoginMessage];
		
		mPasscodeTextView.frame = CGRectMake(PASSCODE_TEXTFIELD_LEFT, PASSCODE_TEXTFIELD_TOP, ALERT_VIEW_WIDTH-2*PASSCODE_TEXTFIELD_LEFT, PASSCODE_TEXTFIELD_HEIGHT);
		mPasscodeTextView.keyboardAppearance = UIKeyboardAppearanceAlert;
		mPasscodeTextView.backgroundColor = [UIColor clearColor];
		mPasscodeTextView.alpha = 0.0;
		[alertView addSubview:mPasscodeTextView];
		
		CGFloat xPos = CODE_SEGMENT_OFFSET;
		code_seg_one.font = [UIFont fontWithName:@"Arial-BoldMT" size:36];
		code_seg_two.font = [UIFont fontWithName:@"Arial-BoldMT" size:36];
		code_seg_three.font = [UIFont fontWithName:@"Arial-BoldMT" size:36];
		code_seg_four.font = [UIFont fontWithName:@"Arial-BoldMT" size:36];
		code_seg_one.frame = CGRectMake(xPos, PASSCODE_TEXTFIELD_TOP, CODE_SEGMENT_SIZE, CODE_SEGMENT_SIZE);
		xPos += CODE_SEGMENT_SIZE + CODE_SEGMENT_SPACE;
		code_seg_two.frame = CGRectMake(xPos, PASSCODE_TEXTFIELD_TOP, CODE_SEGMENT_SIZE, CODE_SEGMENT_SIZE);
		xPos += CODE_SEGMENT_SIZE + CODE_SEGMENT_SPACE;
		code_seg_three.frame = CGRectMake(xPos, PASSCODE_TEXTFIELD_TOP, CODE_SEGMENT_SIZE, CODE_SEGMENT_SIZE);
		xPos += CODE_SEGMENT_SIZE + CODE_SEGMENT_SPACE;
		code_seg_four.frame = CGRectMake(xPos, PASSCODE_TEXTFIELD_TOP, CODE_SEGMENT_SIZE, CODE_SEGMENT_SIZE);
		[alertView addSubview:code_seg_one];
		[alertView addSubview:code_seg_two];
		[alertView addSubview:code_seg_three];
		[alertView addSubview:code_seg_four];
        
        
	}
}

- (void)didPresentAlertView:(UIAlertView *)alertView {
	[mPasscodeTextView becomeFirstResponder];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if(alertView.tag == 2){
		if (buttonIndex == 0) 
			ifLoginButtonClicked = YES;
		else
			ifLoginButtonClicked = NO;
	}
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {

	if (alertView.tag == 2){
		if (!ifLoginButtonClicked) return;
		
		ifLoginButtonClicked = NO;
		BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
		if (![strCode isEqualToString:appDelegate.password]) {
            
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry,Your password is wrong!" message: nil
												  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];	
			[alert release];

			// [self.navigationController popViewControllerAnimated:YES];
		} else {
			BriefcaseViewController  *controller = nil;
			
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				controller = [[BriefcaseViewController alloc] initWithNibName:@"BriefcaseViewController" bundle:nil];
			else
				controller = [[BriefcaseViewController alloc] initWithNibName:@"BriefcaseViewController1" bundle:nil];
			
			[self.navigationController pushViewController:controller animated:YES];
			[controller release];
		}

		code_seg_one.text = @"";
		code_seg_two.text = @"";
		code_seg_three.text = @"";
		code_seg_four.text = @"";
		
	}else if (alertView.tag == 1){
		if(buttonIndex == 0){
			PasscodeViewController *controller = nil;
			
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				controller = [[PasscodeViewController alloc] initWithNibName:@"PasscodeViewController" bundle:nil];
			else
				controller = [[PasscodeViewController alloc] initWithNibName:@"PasscodeViewController1" bundle:nil];
			
			[self.navigationController pushViewController:controller animated:YES];
			[controller release];
		}
		else if(buttonIndex == 1){
			BriefcaseViewController  *controller = nil;
			
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				controller = [[BriefcaseViewController alloc] initWithNibName:@"BriefcaseViewController" bundle:nil];
			else
				controller = [[BriefcaseViewController alloc] initWithNibName:@"BriefcaseViewController1" bundle:nil];
			
			[self.navigationController pushViewController:controller animated:YES];
			[controller release];
			
		}	
		
	}
}

-(IBAction) buttonLogin:(id)sender
{
    //BabyPackingAppDelegate *appDelegate1 = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
    NSLog(@"strCode : %@ %@",strCode,appDelegate.password);
    if (![strCode isEqualToString:appDelegate.password]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry,Your password is wrong!" message: nil
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [alert release];
        
        // [self.navigationController popViewControllerAnimated:YES];
    } else {
        BriefcaseViewController  *controller = nil;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            controller = [[BriefcaseViewController alloc] initWithNibName:@"BriefcaseViewController" bundle:nil];
        else
            controller = [[BriefcaseViewController alloc] initWithNibName:@"BriefcaseViewController1" bundle:nil];
        
        [self.navigationController pushViewController:controller animated:YES];
        [controller release];
    }
    txt_one.text = @"";
    txt_two.text = @"";
    txt_three.text = @"";
    txt_four.text = @"";
    [self.navigationController.navigationBar setUserInteractionEnabled:YES];
    [myTableView setUserInteractionEnabled:YES];
    [brifcaseLoginView removeFromSuperview];
    

}

- (void) applicationFinishedLaunching {
	
	[self performSelectorOnMainThread:@selector(updateRate) withObject:nil waitUntilDone:NO];
//	[self checkRecursiveTasks];
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateStyle:kCFDateFormatterMediumStyle];
//	self.titleDateString = [dateFormatter stringFromDate:[NSDate date]];
	[dateFormatter release];
	
	// UIImagePick
	picker = [UIImagePickerController new];	
#if TARGET_IPHONE_SIMULATOR
	picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
#else
	BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
	picker.sourceType = hasCamera?UIImagePickerControllerSourceTypeCamera:UIImagePickerControllerSourceTypePhotoLibrary;
#endif
	
}

- (IBAction)valueDidChange:(id)sender {
	
	UITextField *textFieldController = sender;
	NSString *text = textFieldController.text;
	switch ([text length]) {
		case 0:
			code_seg_one.text = @"";
			code_seg_two.text = @"";
			code_seg_three.text = @"";
			code_seg_four.text = @"";
			
			break;
			
		case 1:
			code_seg_one.text = @"*";
			code_seg_two.text = @"";
			code_seg_three.text = @"";
			code_seg_four.text = @"";
			
			break;
		case 2:
			code_seg_one.text = @"*";
			code_seg_two.text = @"*";
			code_seg_three.text = @"";
			code_seg_four.text = @"";
			
			break;
		case 3:
			code_seg_one.text = @"*";
			code_seg_two.text = @"*";
			code_seg_three.text = @"*";
			code_seg_four.text = @"";
			
			break;
		case 4:
			code_seg_one.text = @"*";
			code_seg_two.text = @"*";
			code_seg_three.text = @"*";
			code_seg_four.text = @"*";
			
			ifLoginButtonClicked = YES;
			[mPasscodeAlertView dismissWithClickedButtonIndex:0 animated:YES];
			break;
		default:
			break;
	}
	
	if ([textFieldController.text length] > 4) {
		textFieldController.text = [textFieldController.text substringToIndex:4];
	}
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	//[self valueDidChange:textfield_none];
	//UITextField *textFieldController = sender;
	//NSString *text = textFieldController.text;
//	switch ([textField.text length]+1) {
//		case 0:
//			code_seg_one.text = @"";
//			code_seg_two.text = @"";
//			code_seg_three.text = @"";
//			code_seg_four.text = @"";
//			
//			break;
//			
//		case 1:
//			code_seg_one.text = @"*";
//			code_seg_two.text = @"";
//			code_seg_three.text = @"";
//			code_seg_four.text = @"";
//			
//			break;
//		case 2:
//			code_seg_one.text = @"*";
//			code_seg_two.text = @"*";
//			code_seg_three.text = @"";
//			code_seg_four.text = @"";
//			
//			break;
//		case 3:
//			code_seg_one.text = @"*";
//			code_seg_two.text = @"*";
//			code_seg_three.text = @"*";
//			code_seg_four.text = @"";
//			
//			break;
//		case 4:
//			code_seg_one.text = @"*";
//			code_seg_two.text = @"*";
//			code_seg_three.text = @"*";
//			code_seg_four.text = @"*";
//			
//			ifLoginButtonClicked = YES;
//			[mPasscodeAlertView dismissWithClickedButtonIndex:0 animated:YES];
//			break;
//			
//		default:
//			break;
//	}
//	
//	if ([textField.text length] > 4) 
//	{
//		//passcode_text.text = [textFieldController.text substringToIndex:4];
//		//self.passcode = passcode_text.text;
//		textField.text = [textField.text substringToIndex:4];
//	}
    if ([textField tag] > 100) {
        
        [textField setText:string];
        
        NSInteger nextTag = textField.tag + 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        
        if (nextResponder)
        {            
            [nextResponder becomeFirstResponder];
        }
        
        else {
//            [txt_one resignFirstResponder];
//            [txt_two resignFirstResponder];
//            [txt_three resignFirstResponder];
            [txt_four resignFirstResponder];
           strCode = [[NSString stringWithFormat:@"%@%@%@%@", txt_one.text, txt_two.text, txt_three.text, txt_four.text]retain ];
            NSLog(@"this is strCode %@",strCode);
        }
        return NO;
    }
    return YES;
	return YES;
    
}



//- (void) textFieldDidEndEditing:(UITextField *)textField
//{
//	if (!ifLoginButtonClicked)
//		[textField becomeFirstResponder];
//}

//- (BOOL) textFieldShouldReturn:(UITextField *)textField
//{
//	return NO;
//}

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
//    UIInterfaceOrientation interfaceOrientation = [UIApplication sharedApplication].statusBarOrientation;
//    [self.topViewController shouldAutorotate];
        return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
   // return (UIInterfaceOrientationMaskAll);
       return YES;
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        [layoutManager translateToPortraitForView:self.view withAnimation:YES];
       // [brifcaseLoginView removeFromSuperview];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [brifcaseLoginView setFrame:CGRectMake(225,200, 300, 100)];
        }
        else
        {
            [brifcaseLoginView setFrame:CGRectMake(10,100, 300, 100)];
        }
      //  [self.view addSubview:brifcaseLoginView];
        
	}
    else
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [brifcaseLoginView setFrame:CGRectMake(350,50, 320, 100)];
        }
        else
        {
            [brifcaseLoginView setFrame:CGRectMake(85,01, 320, 100)];
        }
        [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
    }
     }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
        
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}

#pragma mark - functions for iCloud

- (NSString *)notesList
{
    if (appDelegate.notesList) {
        return appDelegate.notesList;
    }
    
    appDelegate.notesList = [[[NSUbiquitousKeyValueStore defaultStore] stringForKey:@"AVAILABLE_NOTES_List"] mutableCopy];
    if (!appDelegate.notesList) appDelegate.notesList = @"";
    
    return appDelegate.notesList;
}

- (NSString *)notesItem
{
    if (appDelegate.notesItem) {
        return appDelegate.notesItem;
    }
    
    appDelegate.notesItem = [[[NSUbiquitousKeyValueStore defaultStore] stringForKey:@"AVAILABLE_NOTES_Item"] mutableCopy];
    if (!appDelegate.notesItem) appDelegate.notesItem = @"";
    
    return appDelegate.notesItem;
}

#pragma mark - Observer New Note

- (void)didAddNewNote:(NSNotification *)notification
{
    // Update data on the iCloud
    [[NSUbiquitousKeyValueStore defaultStore] setString:appDelegate.notesList forKey:@"AVAILABLE_NOTES_List"];
    [[NSUbiquitousKeyValueStore defaultStore] setString:appDelegate.notesItem forKey:@"AVAILABLE_NOTES_Item"];
}

#pragma mark - Observer

- (void)storeDidChange:(NSNotification *)notification
{
    NSLog(@"Changed");

    // Retrieve the changes from iCloud
    appDelegate.notesList = [[[NSUbiquitousKeyValueStore defaultStore] stringForKey:@"AVAILABLE_NOTES_List"] mutableCopy];
       appDelegate.notesItem = [[[NSUbiquitousKeyValueStore defaultStore] stringForKey:@"AVAILABLE_NOTES_Item"] mutableCopy];

    [self downloadLists:appDelegate.notesList];
    [self downloadItems:appDelegate.notesItem];

}

-(void)downloadLists:(NSString *)xmlFile
{
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    
    // Code for delete all existing records
    NSString *select_query;
    sqlite3 *database;
    if(sqlite3_open([[mAppDelegate getDBListPath] UTF8String], &database) == SQLITE_OK)
    {
        select_query = [NSString stringWithFormat:@"delete from list"];
        const char *sqlStatementShirts = [select_query UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatementShirts, -1, &compiledStatement, NULL) != SQLITE_OK)
        {
            NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
        else
        {
            sqlite3_step(compiledStatement);
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
    NSArray *arr=[xmlFile componentsSeparatedByString:@"<Data>"];
    if ([arr count]>1)
    {
        for(int i=1;i<[arr count];i++)
        {
            NSString *str=[arr objectAtIndex:i];
            NSArray *arr1=[str componentsSeparatedByString:@"<pk>"];
            NSString *data=[arr1 objectAtIndex:1];
            NSRange ranfrom=[data rangeOfString:@"</pk>"];
            NSString *val11=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<name>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</name>"];
            NSString *val1=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<sex>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</sex>"];
            NSString *val2=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<age>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</age>"];
            NSString *val3=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<smallIcon>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</smallIcon>"];
            NSString *val4=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<photo>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</photo>"];
            NSString *val5=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<lastUpdate>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</lastUpdate>"];
            NSString *val6=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<totalCount>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</totalCount>"];
            NSString *val7=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<checkedCount>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</checkedCount>"];
            NSString *val8=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<predefineId>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</predefineId>"];
            NSString *val9=[data substringToIndex:ranfrom.location];
            
            arr1=[str componentsSeparatedByString:@"<predefineForAge>"];
            data=[arr1 objectAtIndex:1];
            ranfrom=[data rangeOfString:@"</predefineForAge>"];
            NSString *val10=[data substringToIndex:ranfrom.location];
            
            sqlite3 *database;
            if(sqlite3_open([[appDelegate getDBListPath] UTF8String], &database) == SQLITE_OK)
            {
               NSString *select_query = [NSString stringWithFormat:@"INSERT INTO list (pk,name,sex,age,smallIcon,photo,lastUpdate,totalCount,checkedCount,predefineId,predefineForAge) VALUES(\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",val11,val1,val2,val3,val4,val5,val6,val7,val8,val9,val10];
                const char *sqlStatementShirts = [select_query UTF8String];
                sqlite3_stmt *compiledStatement;
                if(sqlite3_prepare_v2(database, sqlStatementShirts, -1, &compiledStatement, NULL) != SQLITE_OK)
                {
                    NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                }
                else
                {
                    sqlite3_step(compiledStatement);
                }
                sqlite3_finalize(compiledStatement);
            }
            sqlite3_close(database);
        }
    }
}

-(void)downloadItems:(NSString *)xmlFile
{
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    
    // Code for delete all existing records
    NSString *select_query;
    sqlite3 *database;
    if(sqlite3_open([[mAppDelegate getDBListPath] UTF8String], &database) == SQLITE_OK)
    {
        select_query = [NSString stringWithFormat:@"delete from item"];
        const char *sqlStatementShirts = [select_query UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatementShirts, -1, &compiledStatement, NULL) != SQLITE_OK)
        {
            NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
        else
        {
            sqlite3_step(compiledStatement);
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
    // Code for parse all the records of iCloud and store in database

        NSArray *arr=[xmlFile componentsSeparatedByString:@"<Data>"];
        if ([arr count]>1)
        {
            for(int i=1;i<[arr count];i++)
            {
                NSString *str=[arr objectAtIndex:i];
                NSArray *arr1=[str componentsSeparatedByString:@"<pk>"];
                NSString *data=[arr1 objectAtIndex:1];
                NSRange ranfrom=[data rangeOfString:@"</pk>"];
                NSString *val11=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<name>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</name>"];
                NSString *val1=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<qty>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</qty>"];
                NSString *val2=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<tip>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</tip>"];
                NSString *val3=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<selected>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</selected>"];
                NSString *val4=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<custom>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</custom>"];
                NSString *val5=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<checked>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</checked>"];
                NSString *val6=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<listId>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</listId>"];
                NSString *val7=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<predefineId>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</predefineId>"];
                NSString *val8=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<predefineForAge>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</predefineForAge>"];
                NSString *val9=[data substringToIndex:ranfrom.location];
                
                sqlite3 *database;
                if(sqlite3_open([[mAppDelegate getDBListPath] UTF8String], &database) == SQLITE_OK)
                {
                    select_query = [NSString stringWithFormat:@"INSERT INTO item (pk,name,qty,tip,selected,custom,checked,listId,predefineId,predefineForAge) VALUES(\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",val11,val1,val2,val3,val4,val5,val6,val7,val8,val9];
                    const char *sqlStatementShirts = [select_query UTF8String];
                    sqlite3_stmt *compiledStatement;
                    if(sqlite3_prepare_v2(database, sqlStatementShirts, -1, &compiledStatement, NULL) != SQLITE_OK)
                    {
                        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                    }
                    else
                    {
                        sqlite3_step(compiledStatement);
                    }
                    sqlite3_finalize(compiledStatement);
                }
                sqlite3_close(database);
            }
        }
	[self loadCustomList];
    [self reloadData];
}

@end


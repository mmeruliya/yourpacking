//
//  CreateItemViewController.h
//  BabyPacking
//
//  Created by Gary He on 6/6/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PackingList.h"
#import "CSImageView.h"
#import "LayoutManager.h"

@class RootViewController;
@class BabyPackingAppDelegate;
@interface AddListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,  UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UIActionSheetDelegate>{

	BabyPackingAppDelegate *appDelegate;
    LayoutManager *layoutManager;
	IBOutlet UITableView *myTableView;
	IBOutlet UIView *tableHeaderView;
	
	IBOutlet UIView *boyGirlView;
	IBOutlet UIView *babyToddlerView;
	IBOutlet UIView *familyItemView;
	IBOutlet UIView *youItemView;
	
	IBOutlet CSImageView *photoView;
	IBOutlet UITextField *nameField;
	IBOutlet UILabel *addPhotoLabel;
	IBOutlet UILabel *boyGirlLabel;	
	
	IBOutlet UISegmentedControl *boyControl;
	IBOutlet UISegmentedControl *girlControl;
	IBOutlet UISegmentedControl *babyControl;
	IBOutlet UISegmentedControl *toddlerControl;
	IBOutlet UISegmentedControl *infant_babyControl;
	IBOutlet UISegmentedControl *childControl;
	IBOutlet UISegmentedControl *adultControl;
	IBOutlet UISegmentedControl *adultInFamilyControl;
	IBOutlet UISegmentedControl *petControl;
	IBOutlet UISegmentedControl *shoppingListControl;
	IBOutlet UISegmentedControl *otherControl;
	IBOutlet UISegmentedControl *otherInFamilyControl;

	IBOutlet UIBarButtonItem *cancelButton;
	IBOutlet UIBarButtonItem *saveButton;
	
	RootViewController *homeController;
	PackingList *editingList;
	NSMutableArray *customListArray;
	NSMutableArray *predefinedItems;
	NSMutableArray *babyPredefinedItems;
	
	BOOL isFirstTime;
	
	UIPopoverController *popoverController;
    
}

@property (nonatomic,retain) NSString *imageName;

@property(nonatomic, retain) RootViewController *homeController;
@property(nonatomic, retain) PackingList *editingList;
@property(nonatomic, retain) NSMutableArray *customListArray;
@property(nonatomic, retain) NSMutableArray *predefinedItems;
@property(nonatomic, retain) NSMutableArray *babyPredefinedItems;

- (IBAction) buttonAction:(id)sender;
- (IBAction) valueChanged:(id)sender;

@end

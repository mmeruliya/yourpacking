//
//  Categories.m
//  BabyPacking
//
//  Created by Hitesh on 12/15/10.
//  Copyright 2010 cmc limited. All rights reserved.
//

#import "Categories.h"

static sqlite3_stmt *statement = nil;
static sqlite3_stmt *add_statement = nil;
static sqlite3_stmt *delete_statement = nil;

@implementation Categories

@synthesize pk,name,deleted,type;

- (id) initWithPrimaryKey:(NSInteger)primaryKey database:(sqlite3 *)db
{
	if (self == [super init])
	{
		pk = primaryKey;
		database = db;
		
		if(statement == nil) 
		{
			const char *sql = "Select name, deleted, type from Categories Where pk = ?";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK)
				NSAssert1(0, @"Error while creating detail view statement. '%s'", sqlite3_errmsg(database));
		}
		
		sqlite3_bind_int(statement, 1, pk);
		
		if(SQLITE_DONE != sqlite3_step(statement)) 
		{	
			if (sqlite3_column_text(statement, 0) != nil)
				self.name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
			else 
				self.name = nil;

			self.deleted = sqlite3_column_int(statement, 1);
			self.type = sqlite3_column_int(statement, 2);
		}
		else
			NSAssert1(0, @"Error while getting the progress record. '%s'", sqlite3_errmsg(database));
		
		//Reset the detail statement.
		sqlite3_reset(statement);
	}
	
	return self;	
}

- (void) addCategory:(sqlite3 *)db
{
	database = db;
	if(add_statement == nil) 
	{
		const char *sql = "insert into Categories(name,deleted,type) values(?,?,?)";
		if(sqlite3_prepare_v2(database, sql, -1, &add_statement, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating insert statement. '%s'", sqlite3_errmsg(database));
	}
	
	sqlite3_bind_text(add_statement, 1, [self.name UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_int(add_statement, 2, deleted);
	sqlite3_bind_int(add_statement, 3, type);
	
	if(SQLITE_DONE != sqlite3_step(add_statement))
		NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
	else
		//SQLite provides a method to get the last primary key inserted by using sqlite3_last_insert_rowid
		pk = sqlite3_last_insert_rowid(database);
	
	//Reset the add statement.
	sqlite3_reset(add_statement);	
}

- (void) updateCategory:(sqlite3 *)db
{
	database = db;
	if(delete_statement == nil) 
	{
		const char *sql = "update Categories set name = ?, deleted = ? where pk = ?";
		if(sqlite3_prepare_v2(database, sql, -1, &delete_statement, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating insert statement. '%s'", sqlite3_errmsg(database));
	}
	
	sqlite3_bind_text(delete_statement, 1, [name UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_int(delete_statement, 2, deleted);
	sqlite3_bind_int(delete_statement, 3, pk);
	
	if (SQLITE_DONE != sqlite3_step(delete_statement))
		NSAssert1(0,@"Error while deleting data with %s",sqlite3_errmsg(database));
	
	//Reset the add statement.
	sqlite3_reset(delete_statement);
}

- (void) deleteCategory:(sqlite3 *)db
{
	database = db;
	if(delete_statement == nil) 
	{
		const char *sql = "delete from Categories";
		if(sqlite3_prepare_v2(database, sql, -1, &delete_statement, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating insert statement. '%s'", sqlite3_errmsg(database));
	}
		
	if (SQLITE_DONE != sqlite3_step(delete_statement))
		NSAssert1(0,@"Error while deleting data with %s",sqlite3_errmsg(database));
	
	//Reset the add statement.
	sqlite3_reset(delete_statement);
}

- (void) dealloc
{
	[name release];
	[super dealloc];
}

@end

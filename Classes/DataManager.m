//
//  DataManager.m
//  RWS
//
//  Created by Ravi on 8/31/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "DataManager.h"
#import "BabyPackingAppDelegate.h"
#import "Items.h"
#import "Categories.h"

//#define DATABASE_NAME @"YourAarti.sqlite"

#define DATABASE_NAME @"Packing.sqlite"

static sqlite3_stmt *statement = nil;

@implementation DataManager

@synthesize database;

// init method to intialiaze this class
- (id) init
{
	self = [super init];
	if (self != nil) 
	{
		appDelegate = (BabyPackingAppDelegate*)[[UIApplication sharedApplication] delegate];
		[self createDatabaseIfNotExists];
	}
	return self;
}

// Creates a writable copy of the bundled default database in the application Documents directory.
// Check if exists or not.. if not exists then create new copy else we are done.
- (void) createDatabaseIfNotExists 
{
    // First, test for existence.
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:DATABASE_NAME];
    success = [fileManager fileExistsAtPath:writableDBPath];
    if (success) 
	{
		// Open the database. The database was prepared outside the application.
		if (sqlite3_open([writableDBPath UTF8String], &database) != SQLITE_OK) 
		{
			NSLog(@"failed to open database.....");
		}
		
		return;
	}
    
	// The writable database does not exist, so copy the default to the appropriate location.
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DATABASE_NAME];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
	NSLog(@"hitesh New Database installed");
    if (!success)
	{
        NSLog(@"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
	
	// The database is stored in the application bundle. 
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
   // NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:DATABASE_NAME];
	
	// Open the database. The database was prepared outside the application.
	if (sqlite3_open([path UTF8String], &database) != SQLITE_OK) 
	{
		NSLog(@"failed to open database.....");
	}
}

// Open the database connection and retrieve minimal information for all objects.
// fills this fetched information into  containers that are in preserved in application
// delegate class.
- (void) getItemByCategoty:(int)catId
{
	if(appDelegate.itemsFromDB != nil)
	{
		[appDelegate.itemsFromDB removeAllObjects];
		[appDelegate.itemsFromDB release];
		appDelegate.itemsFromDB = nil;
	}
	//NSLog(@"Cat id %d",catId);
	appDelegate.itemsFromDB = [[NSMutableArray alloc] init];
	
	/*The database is stored in the application bundle. 
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:DATABASE_NAME];*/

	// Get the primary key for all books.
	const char *sql = "select pk from Items where categoryId = ? and deleted = 0";
	// Open the database. The database was prepared outside the application.
	//if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
	
		//sqlite3_stmt *statement;
        
		// Preparing a statement compiles the SQL query into a byte-code program in the SQLite library.
		// The third parameter is either the length of the SQL string or -1 to read up to the first null terminator.        
		if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) 
		{
			sqlite3_bind_int(statement, 1, catId);
			//sqlite3_bind_text(statement, 1, [cat UTF8String], -1, SQLITE_TRANSIENT);
			
			while (sqlite3_step(statement) == SQLITE_ROW) 
			{
				// The second parameter indicates the column index into the result set.
				int primaryKey = sqlite3_column_int(statement, 0);
               
				Items *obj = [[[Items alloc] initWithPrimaryKey:primaryKey database:database] autorelease];
				[appDelegate.itemsFromDB addObject:obj];
			}
		}
		else
		{
			NSLog(@"error is %s",sqlite3_errmsg(database));
		}
		NSLog(@"%d",[appDelegate.itemsFromDB count]);
		sqlite3_finalize(statement);
		statement = nil;
	}
	/*else 
	{
		// Even though the open failed, call close to properly clean up resources.
		sqlite3_close(database);
		NSLog(@"Failed to open database with message '%s'.", sqlite3_errmsg(database));
	}*/
}

- (void) addItem:(Items *)item;
{
	[item retain];
	[appDelegate.itemsFromDB addObject:item];
	[item addItem:database];
}

- (void) updateItem:(Items *)item;
{
	[item retain];
	//pDelegate.shoesRecords addObject:shoe];
	[item updateItem:database];
}

- (void) updateItemsOldCategory:(NSString *)oldCat withNewCategory:(NSString *)newCat
{
    Items *item=[[Items alloc] init];
	[item updateItemsOldCategory:oldCat withNewCategory:newCat dbase:database];
    [item release];
}


- (void) deleteItems{
	Items *item=[[Items alloc] init];
	[item deleteItems:database];
	[item release];
}

- (void) getCategoriesByList:(int)listid
{
	if(appDelegate.categoryFromDB != nil)
	{
		[appDelegate.categoryFromDB removeAllObjects];
		[appDelegate.categoryFromDB release];
		appDelegate.categoryFromDB = nil;
	}
	
	appDelegate.categoryFromDB = [[NSMutableArray alloc] init];
	
	/*The database is stored in the application bundle. 
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:DATABASE_NAME];*/
	
	// Open the database. The database was prepared outside the application.
	//if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
		// Get the primary key for all books.
		const char *sql = "select pk from Categories where deleted = 0";
		//sqlite3_stmt *statement;
        
		// Preparing a statement compiles the SQL query into a byte-code program in the SQLite library.
		// The third parameter is either the length of the SQL string or -1 to read up to the first null terminator.        
		if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) 
		{
			//sqlite3_bind_int(statement, 1, listid);
			while (sqlite3_step(statement) == SQLITE_ROW) 
			{
				// The second parameter indicates the column index into the result set.
				int primaryKey = sqlite3_column_int(statement, 0);
				
				Categories *obj = [[[Categories alloc] initWithPrimaryKey:primaryKey database:database] autorelease];
				[appDelegate.categoryFromDB addObject:obj];
			}
		}
		else
		{
			NSLog(@"error is %s",sqlite3_errmsg(database));
		}
		sqlite3_finalize(statement);
		statement = nil;
	}
	/*else 
	{
		// Even though the open failed, call close to properly clean up resources.
		sqlite3_close(database);
		NSLog(@"Failed to open database with message '%s'.", sqlite3_errmsg(database));
	}*/
}

- (void) addCategory:(Categories *)cat
{
	[cat retain];
	[appDelegate.categoryFromDB addObject:cat];
	[cat addCategory:database];
}

- (void) updateCategory:(Categories *)cat
{
	[cat retain];
	//pDelegate.shoesRecords addObject:cat];
	[cat updateCategory:database];
}

- (void) deleteCategory{
	Categories *cat=[[Categories alloc] init];
	[cat deleteCategory:database];
	[cat release];
}

- (void) getRestoreItemByCategory:(int)catId
{
	if(appDelegate.restoreItems != nil)
	{
		[appDelegate.restoreItems removeAllObjects];
		[appDelegate.restoreItems release];
		appDelegate.restoreItems = nil;
	}
	
	appDelegate.restoreItems = [[NSMutableArray alloc] init];
	
	/*The database is stored in the application bundle. 
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:DATABASE_NAME];*/
	
	// Open the database. The database was prepared outside the application.
	//if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
		// Get the primary key for all books.
		const char *sql = "select pk from Items where categoryId = ? and deleted = 1";
		//sqlite3_stmt *statement;
        
		// Preparing a statement compiles the SQL query into a byte-code program in the SQLite library.
		// The third parameter is either the length of the SQL string or -1 to read up to the first null terminator.        
		if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) 
		{
			//sqlite3_bind_int(statement, 2, checked);
			//sqlite3_bind_text(statement, 1, [cat UTF8String], -1, SQLITE_TRANSIENT);
			sqlite3_bind_int(statement, 1, catId);
			
			while (sqlite3_step(statement) == SQLITE_ROW) 
			{
				// The second parameter indicates the column index into the result set.
				int primaryKey = sqlite3_column_int(statement, 0);
				
				Items *obj = [[[Items alloc] initWithPrimaryKey:primaryKey database:database] autorelease];
				[appDelegate.restoreItems addObject:obj];
			}
		}
		else
		{
			NSLog(@"error is %s",sqlite3_errmsg(database));
		}
		sqlite3_finalize(statement);
		statement = nil;
	}
	/*else 
	{
		// Even though the open failed, call close to properly clean up resources.
		sqlite3_close(database);
		NSLog(@"Failed to open database with message '%s'.", sqlite3_errmsg(database));
	}*/
}

- (void) getRestoreCategory
{
	if(appDelegate.restoreCategories != nil)
	{
		[appDelegate.restoreCategories removeAllObjects];
		[appDelegate.restoreCategories release];
		appDelegate.restoreCategories = nil;
	}
	
	appDelegate.restoreCategories = [[NSMutableArray alloc] init];
	
	/*The database is stored in the application bundle. 
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:DATABASE_NAME];*/
	
	// Open the database. The database was prepared outside the application.
	//if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
		// Get the primary key for all books.
		const char *sql = "select pk from Categories where deleted = 1";
		//sqlite3_stmt *statement;
        
		// Preparing a statement compiles the SQL query into a byte-code program in the SQLite library.
		// The third parameter is either the length of the SQL string or -1 to read up to the first null terminator.        
		if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) 
		{
			//sqlite3_bind_int(statement, 1, listid);
			while (sqlite3_step(statement) == SQLITE_ROW) 
			{
				// The second parameter indicates the column index into the result set.
				int primaryKey = sqlite3_column_int(statement, 0);
				
				Categories *obj = [[[Categories alloc] initWithPrimaryKey:primaryKey database:database] autorelease];
				[appDelegate.restoreCategories addObject:obj];
			}
		}
		else
		{
			NSLog(@"error is %s",sqlite3_errmsg(database));
		}
		sqlite3_finalize(statement);
		statement = nil;
	}
	/*else 
	{
		// Even though the open failed, call close to properly clean up resources.
		sqlite3_close(database);
		NSLog(@"Failed to open database with message '%s'.", sqlite3_errmsg(database));
	}*/
}

- (void) getAllItemsByCategory:(int)catId
{
	if(appDelegate.itemsFromDB != nil)
	{
		[appDelegate.itemsFromDB removeAllObjects];
		[appDelegate.itemsFromDB release];
		appDelegate.itemsFromDB = nil;
	}
	//NSLog(@"Cat id %d",catId);
	appDelegate.itemsFromDB = [[NSMutableArray alloc] init];
	/*The database is stored in the application bundle. 
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:DATABASE_NAME];*/
	
	// Open the database. The database was prepared outside the application.
	//if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
		// Get the primary key for all books.
		const char *sql = "select pk from Items where categoryId = ? and deleted = 1";
		//sqlite3_stmt *statement;
        
		// Preparing a statement compiles the SQL query into a byte-code program in the SQLite library.
		// The third parameter is either the length of the SQL string or -1 to read up to the first null terminator.        
		if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) 
		{
			//sqlite3_bind_int(statement, 2, checked);
			//sqlite3_bind_text(statement, 1, [cat UTF8String], -1, SQLITE_TRANSIENT);
			sqlite3_bind_int(statement, 1, catId);
			
			while (sqlite3_step(statement) == SQLITE_ROW) 
			{
				// The second parameter indicates the column index into the result set.
				int primaryKey = sqlite3_column_int(statement, 0);
				
				Items *obj = [[[Items alloc] initWithPrimaryKey:primaryKey database:database] autorelease];
				[appDelegate.itemsFromDB addObject:obj];
			}
		}
		else
		{
			NSLog(@"error is %s",sqlite3_errmsg(database));
		}
		sqlite3_finalize(statement);
		statement = nil;
	}
	/*else 
	{
		// Even though the open failed, call close to properly clean up resources.
		sqlite3_close(database);
		NSLog(@"Failed to open database with message '%s'.", sqlite3_errmsg(database));
	}*/
		
}

- (void) getAllData{

	if(appDelegate.allItemsList != nil)
	{
		[appDelegate.allItemsList removeAllObjects];
		[appDelegate.allItemsList release];
		appDelegate.allItemsList = nil;
	}
	//NSLog(@"Cat id %d",catId);
	appDelegate.allItemsList = [[NSMutableArray alloc] init];
	/*The database is stored in the application bundle. 
	 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	 NSString *documentsDirectory = [paths objectAtIndex:0];
	 NSString *path = [documentsDirectory stringByAppendingPathComponent:DATABASE_NAME];*/
	
	// Open the database. The database was prepared outside the application.
	//if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
		// Get the primary key for all books.
		const char *sql = "select pk from Items";
		//sqlite3_stmt *statement;
        
		// Preparing a statement compiles the SQL query into a byte-code program in the SQLite library.
		// The third parameter is either the length of the SQL string or -1 to read up to the first null terminator.        
		if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) 
		{
			//sqlite3_bind_int(statement, 2, checked);
			//sqlite3_bind_text(statement, 1, [cat UTF8String], -1, SQLITE_TRANSIENT);
		//	sqlite3_bind_int(statement, 1, catId);
			
			while (sqlite3_step(statement) == SQLITE_ROW) 
			{
				// The second parameter indicates the column index into the result set.
				int primaryKey = sqlite3_column_int(statement, 0);
				
				Items *obj = [[[Items alloc] initWithPrimaryKey:primaryKey database:database] autorelease];
				[appDelegate.allItemsList addObject:obj];
			}
		}
		else
		{
			NSLog(@"error is %s",sqlite3_errmsg(database));
		}
		sqlite3_finalize(statement);
		statement = nil;
	}
	/*else 
	 {
	 // Even though the open failed, call close to properly clean up resources.
	 sqlite3_close(database);
	 NSLog(@"Failed to open database with message '%s'.", sqlite3_errmsg(database));
	 }*/
	
	if(appDelegate.allCategoriesList != nil)
	{
		[appDelegate.allCategoriesList removeAllObjects];
		[appDelegate.allCategoriesList release];
		appDelegate.allCategoriesList = nil;
	}
	
	appDelegate.allCategoriesList = [[NSMutableArray alloc] init];
	
	/*The database is stored in the application bundle. 
	 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	 NSString *documentsDirectory = [paths objectAtIndex:0];
	 NSString *path = [documentsDirectory stringByAppendingPathComponent:DATABASE_NAME];*/
	
	// Open the database. The database was prepared outside the application.
	//if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
		// Get the primary key for all books.
		const char *sql = "select pk from Categories";
		//sqlite3_stmt *statement;
        
		// Preparing a statement compiles the SQL query into a byte-code program in the SQLite library.
		// The third parameter is either the length of the SQL string or -1 to read up to the first null terminator.        
		if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) 
		{
			//sqlite3_bind_int(statement, 1, listid);
			while (sqlite3_step(statement) == SQLITE_ROW) 
			{
				// The second parameter indicates the column index into the result set.
				int primaryKey = sqlite3_column_int(statement, 0);
				
				Categories *obj = [[[Categories alloc] initWithPrimaryKey:primaryKey database:database] autorelease];
				[appDelegate.allCategoriesList addObject:obj];
			}
		}
		else
		{
			NSLog(@"error is %s",sqlite3_errmsg(database));
		}
		sqlite3_finalize(statement);
		statement = nil;
	}
	/*else 
	 {
	 // Even though the open failed, call close to properly clean up resources.
	 sqlite3_close(database);
	 NSLog(@"Failed to open database with message '%s'.", sqlite3_errmsg(database));
	 }*/
	
}

- (void) dealloc
{
	[super dealloc];
}

@end

//
//  SetImageParser.h
//  packngo
//
//  Created by Mehul Bhuva on 07/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLParserBase.h"

@interface SetImageParser : NSObject <NSXMLParserDelegate>
{
	BOOL mSuccessful;

	NSMutableDictionary* mData;
	NSMutableString* mCurrentString;
	NSMutableDictionary* mCurItem;
	NSMutableArray *mCurItemArray;
	NSMutableArray *userDetails;
	BOOL storingCharacters;
	id mDelegate;
}
@property (nonatomic, retain) NSMutableString* mCurrentString;
@property (nonatomic, assign) BOOL successful;
@property (nonatomic, retain) NSMutableDictionary* data;
@property (nonatomic, retain) NSMutableDictionary* mCurItem;
@property (nonatomic, retain) NSMutableArray *mCurItemArray;
@property (nonatomic, retain) NSMutableArray *userDetails;
- (void)initWithData:(NSString *)listid withUserId:(NSString *)userid withData:(NSData *)imageData;
//- (void)initFromStr:(NSString *)str;
- (void)displayInfo;
@end

//
//  LayoutManager.h
//  ExamExcellence
//
//  Created by dipali on 4/3/12.
//  Copyright (c) 2012 Cogneti. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LayoutManager : NSObject
{
    NSMutableDictionary *items;
}

@property (nonatomic, strong) NSMutableDictionary *items;

- (id)initWithSubViewPortraitView:(UIView *)pView
                  andLandscapeNIB:(UIView *)lView;
- (id)initWithPortraitView:(UIView *)pView
           andLandscapeNIB:(NSString *)lNib;
- (void)addItemWithTag:(NSInteger)tag
       andPortraitRect:(CGRect)pRect
      andLandscapeRect:(CGRect)lRect;
- (CGRect)getLandscapeRectForTag:(NSInteger)tag;
- (CGRect)getPortraitRectForTag:(NSInteger)tag;
- (void)translateToLandscapeForView:(UIView *)vw
                      withAnimation:(BOOL)animated;
- (void)translateToPortraitForView:(UIView *)vw
                     withAnimation:(BOOL)animated;

- (void)getAllSubViews:(UIView *)pView
      andLandscapeView:(UIView *)lview;
- (void)getAllSubViewsWhileTranslatingToLandscape:(NSArray *)subviews;
- (void)getAllSubViewsWhileTranslatingToPotrait:(NSArray *)subviews;
@end

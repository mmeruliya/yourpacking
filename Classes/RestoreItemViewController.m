    //
//  RestoreItemViewController.m
//  BabyPacking
//
//  Created by Hitesh on 12/17/10.
//  Copyright 2010 cmc limited. All rights reserved.
//

#import "RestoreItemViewController.h"
#import "BabyPackingAppDelegate.h"
#import "Items.h"
#import "DataManager.h"

@implementation RestoreItemViewController

@synthesize tableView1;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
	appDelegate = (BabyPackingAppDelegate *)[[UIApplication sharedApplication] delegate];
	index = 0;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
    [super viewDidLoad];
}

- (IBAction) onDoneclicked:(id)sender
{
	[self dismissModalViewControllerAnimated:YES];
}

- (void)dealloc 
{
	[tableView1 release];
    [super dealloc];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [appDelegate.restoreItems count];
}

/*- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	return 33.0;
}*/

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"CSTableViewCell";
    
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		cell.detailTextLabel.textColor = [UIColor darkGrayColor];
		cell.detailTextLabel.textAlignment = UITextAlignmentLeft;
    }
    Items *item = [appDelegate.restoreItems objectAtIndex:indexPath.row];
	//NSLog(@"item   predefineId = %d", item.predefineId);
	cell.textLabel.text = item.name;
	/*if ([item hasTip])
		cell.detailTextLabel.text = [NSString stringWithFormat:@"*x%@",item.qty];	
	else if( item.qty && [item.qty length] > 0)
		cell.detailTextLabel.text = [NSString stringWithFormat:@"x%@", item.qty];
	else
		cell.detailTextLabel.text = nil;	
	cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
	if (item.selected) {
		cell.image = checkImg;
	} else {
		cell.image = uncheckImg;
	}*/
	
	cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {	

	index = indexPath.row;
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Do you want to restore selected item?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO",nil];
	[alert show];
	[alert release];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
	{
		Items *item = [appDelegate.restoreItems objectAtIndex:index];
		item.deleted = 0;
		[appDelegate.dataManager updateItem:item];
		
		[appDelegate.restoreItems removeObjectAtIndex:index];
		
		[tableView1 reloadData];
	}
}

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{            return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
       // [layoutManager translateToPortraitForView:self.view withAnimation:YES];
    }
    else
    {
        //[layoutManager translateToLandscapeForView:self.view withAnimation:YES];
    }
    // }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}


@end

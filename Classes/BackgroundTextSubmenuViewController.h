//
//  BackgroundTextSubmenuViewController.h
//  BabyPacking
//
//  Created by Gary He on 6/8/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BabyPackingAppDelegate;

@interface BackgroundTextSubmenuViewController : UIViewController <UITableViewDelegate,UITableViewDataSource> {
	
	BabyPackingAppDelegate *mAppDelegate;
    
    IBOutlet UITableView *myTableView;
 
    IBOutlet UIBarButtonItem *createBtItem;
}


@end

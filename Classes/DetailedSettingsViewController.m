//
//  DetailedSettingsViewController.m
//  BabyPacking
//
//  Created by Jay Lee on 7/14/09.
//  Copyright 2009 IQ Engines All rights reserved.
//

#import "Constant.h"
#import "DetailedSettingsViewController.h"
#import "BabyPackingAppDelegate.h"
#import "SettingsViewController.h"

@interface DetailedSettingsViewController(Private)

- (void) playSound:(NSString *)sound;
@end

@implementation DetailedSettingsViewController

@synthesize delegate = mDelegate;
@synthesize data = mData;
@synthesize defaultValue = mDefaultValue;

- (void)dealloc {
	AudioServicesDisposeSystemSoundID(mSoundID);
	[mTableView release];
	[mDefaultValue release];
	[mOriginalValue release];
	[mData release];
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        
//        layoutManager = [[LayoutManager alloc] initWithPortraitView:self.view andLandscapeNIB:@"DetailedSettingsViewController_L"];
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [layoutManager translateToPortraitForView:self.view withAnimation:NO];
    }
    else
    {
        [layoutManager translateToLandscapeForView:self.view withAnimation:NO];
    }

}

- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
	mAppDelegate = (id)[UIApplication sharedApplication].delegate;
	mTitles = [mData objectForKey:@"Titles"];
	mValues = [mData objectForKey:@"Values"];
	if (!self.defaultValue) self.defaultValue = [mData objectForKey:@"DefaultValue"];
	mOriginalValue = [[NSString alloc] initWithString:mDefaultValue];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 30, 30);
    btn.tag=1;
    [btn setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(goBck:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor whiteColor];
    
	NSString *customFontName = [mAppDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];		
	}
	else {
		label.font=[UIFont fontWithName:customFontName size:fontSize];		
	}
	
	[label sizeToFit];
	
	
	//label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
    label.textColor=[UIColor whiteColor];        
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label.textColor = theColour1;
	}
	// and finally set the colour of your label
	
	self.navigationItem.titleView = label;
	label.text = [mData objectForKey:@"Title"];
	[label sizeToFit];
	
	mValueIsChanged = NO;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
}

- (IBAction)goBck:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	
	if ([K_SET_TASK_SOUND isEqualToString:[mData objectForKey:@"Key"]]) {
		if ([mDelegate respondsToSelector:@selector(onSelectedAlertSound:)]) {
			NSString* title = [SettingsViewController titleForValue:mDefaultValue forSection:K_SET_TASK_SOUND];
			title = title ? title : [SettingsViewController defaultTitleForSection:K_SET_TASK_SOUND];
			[mDelegate onSelectedAlertSound:title];
		}
	}
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)settingsButtonDidClick:(id)sender {
	
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Private member function implementation

- (void) playSound:(NSString *)sound {
	
	if (mSoundID != 0) {
		AudioServicesDisposeSystemSoundID(mSoundID);
		mSoundID = 0;
	}
	
	NSBundle *mainBundle = [NSBundle mainBundle];
	CFURLRef aFileURL = (CFURLRef)[NSURL fileURLWithPath:[mainBundle pathForResource:sound ofType:nil] isDirectory:NO];
	if (aFileURL != nil)  {
		AudioServicesCreateSystemSoundID(aFileURL, &mSoundID);
	}
	
	AudioServicesPlaySystemSound(mSoundID);
}

#pragma mark -
#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [mTitles count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Set up the cell...
	cell.text = [mTitles objectAtIndex:indexPath.row];
	if ([mDefaultValue isEqualToString:[mValues objectAtIndex:indexPath.row]]) {
		cell.textColor = [UIColor colorWithRed:0.22 green:0.329 blue:0.529 alpha:1.0];
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	} else {
		cell.textColor = [UIColor blackColor];
		cell.accessoryType = UITableViewCellAccessoryNone;
	}
	
	NSString *customFontName1 = [mAppDelegate getCustomFontName];
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
		cell.textLabel.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];			
	}
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		cell.backgroundColor = theColour1;
	}
	// and finally set the colour of your label	
	
	
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour21;
	// read the data back from the user defaults
	NSData *data211= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
	// check whether you got anything
	if(data211 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour21 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data211];
		cell.textLabel.textColor = theColour21;
	}
	// and finally set the colour of your label
	
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	// Restore the previous selection to normal state
	NSUInteger index = [mValues indexOfObject:mDefaultValue];
	if (index != NSNotFound) {
		// Restore original one
		UITableViewCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
//		cell.textColor = [UIColor blackColor];
		cell.accessoryType = UITableViewCellAccessoryNone;
	} else {
		// Restore all
		NSUInteger count = [tableView numberOfRowsInSection:0];
		for (int i=0; i<count; i++) {
			UITableViewCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
//			cell.textColor = [UIColor blackColor];
			cell.accessoryType = UITableViewCellAccessoryNone;
		}
	}
	
	// set currect selected
	self.defaultValue = [mValues objectAtIndex:indexPath.row];
	if ([K_SET_TASK_SOUND isEqualToString:[mData objectForKey:@"Key"]]) {
		[self playSound:mDefaultValue];
	}

	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//	cell.textColor = [UIColor colorWithRed:0.22 green:0.329 blue:0.529 alpha:1.0];
	cell.accessoryType = UITableViewCellAccessoryCheckmark;
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	mValueIsChanged = ![mDefaultValue isEqualToString:mOriginalValue];
}

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        // [layoutManager translateToPortraitForView:self.view withAnimation:YES];
    }
    else
    {
        // [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
    }
    // }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

@end


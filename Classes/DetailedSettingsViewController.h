//
//  DetailedSettingsViewController.h
//  BabyPacking
//
//  Created by Jay Lee on 7/14/09.
//  Copyright 2009 IQ Engines All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>
#import <UIKit/UIKit.h>
#import "LayoutManager.h"

@protocol DetailedSettingsViewDelegate<NSObject>
- (void) onSelectedAlertSound:(NSString*) text;
@end

@class BabyPackingAppDelegate;
@class SettingsViewController;

@interface DetailedSettingsViewController : UIViewController {
	
    LayoutManager *layoutManager;
	BabyPackingAppDelegate *mAppDelegate;
	id<DetailedSettingsViewDelegate> mDelegate;
	NSArray *mTitles;
	NSArray *mValues;
	NSString *mDefaultValue;
	NSString *mOriginalValue;
	BOOL mValueIsChanged;
	SystemSoundID mSoundID;
	
	NSMutableDictionary *mData;
	
	IBOutlet UITableView *mTableView;
}

@property(nonatomic, assign) id<DetailedSettingsViewDelegate> delegate;
@property(nonatomic, retain) NSMutableDictionary *data;
@property(nonatomic, copy) NSString *defaultValue;

@end

//
//  AddCategoryViewController.m
//  BabyPacking
//
//  Created by Hitesh on 12/17/10.
//  Copyright 2010 cmc limited. All rights reserved.
//

#import "AddCategoryViewController.h"
#import "PackingList.h"
#import "BabyPackingAppDelegate.h"
#import "DataManager.h"
#import "Categories.h"
#import "Constant.h"
#import "PackingItem.h"

@implementation AddCategoryViewController

@synthesize categoryListArray,currentList,editingCategory;
@synthesize strCategoryName;
#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad 
{
    [super viewDidLoad];

    appdelegate = (BabyPackingAppDelegate *)[[UIApplication sharedApplication] delegate];
	//	self.tableView.sectionFooterHeight = 0;
	//	self.tableView.sectionHeaderHeight = 0;
	
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
	self.navigationItem.rightBarButtonItem = saveButton;
	self.navigationItem.leftBarButtonItem = cancelButton;
	
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor whiteColor];
    
	NSString *customFontName = [appdelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];		
	}
	else {
		label.font=[UIFont fontWithName:customFontName size:fontSize];		
	}
	
	[label sizeToFit];
	
	
	//label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
            label.textColor=[UIColor whiteColor];
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label.textColor = theColour1;
	}
	// and finally set the colour of your label
	
	self.navigationItem.titleView = label;	

	if (editingCategory) {
		nameField.text = editingCategory.name;
		isNewCategory = NO;
		
		label.text = @"Edit Category";
		[label sizeToFit];
		
	} else {
		isNewCategory = YES;
		label.text = @"Add New Category";
		[label sizeToFit];
	}
	
	[nameField becomeFirstResponder];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
    
    UIColor *theColour;
	NSData *data1= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_BAR_COLOR];
	if(data1 == nil)
    {
        if (IOS_NEWER_THAN(7))
        {
            self.navigationController.navigationBar.backgroundColor=[UIColor blackColor];
        }
        else
        {
            self.navigationController.navigationBar.tintColor = [UIColor blackColor];
        }
        
		// use this to set the colour the first time your app runs
	} else
    {
		// this recreates the colour you saved
		theColour = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data1];
        
		if (IOS_NEWER_THAN(7))
        {
            self.navigationController.navigationBar.barTintColor = theColour;
        }
        else
        {
            self.navigationController.navigationBar.tintColor = theColour;
        }
	}
	// and finally set the colour of your label
    
	

	if(data11 == nil)
    {
		// use this to set the colour the first time your app runs
        if (IOS_NEWER_THAN(7))
        {
            self.navigationItem.backBarButtonItem.tintColor=[UIColor whiteColor];;
            self.navigationItem.leftBarButtonItem.tintColor=[UIColor whiteColor];;
            self.navigationItem.rightBarButtonItem.tintColor=[UIColor whiteColor];;
        }
	}
    else
    {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
        
        if (IOS_NEWER_THAN(7)) {
            self.navigationItem.backBarButtonItem.tintColor=theColour1;
            self.navigationItem.leftBarButtonItem.tintColor=theColour1;
            self.navigationItem.rightBarButtonItem.tintColor=theColour1;
        }
        
	}
    
	UIColor *theColour2;
	// read the data back from the user defaults
	NSData *data12= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
	// check whether you got anything
	if(data12 == nil) {
		// use this to set the colour the first time your app runs
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.backgroundColor = [UIColor whiteColor];
        
	} else {
		// this recreates the colour you saved
		theColour2 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data12];
		self.tableView.backgroundColor = theColour2;
        self.tableView.backgroundColor = theColour2;
        
	}
	// and finally set the colour of your label
}

- (IBAction) buttonAction:(id)sender
{
	if (sender == cancelButton)
	{
		if (isNewCategory) {
			[self dismissModalViewControllerAnimated:YES];
		} else {
			[self.navigationController popViewControllerAnimated:YES];
		}
	}
	else if (sender == saveButton)
	{
		NSString *name = [nameField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
		if (![name length] > 0) {
			BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
			[appDelegate showAlert:M_INPUT_NAME_TITLE message:M_INPUT_NAME];
			return;
		}
		
		if (editingCategory == nil) {
			isNewCategory = YES;
		} else {
			isNewCategory = NO;
		}
		
		if (isNewCategory == YES)
		{
            NSLog(@"IS NEW IS YESSSSSSSSS");
			for (PackingList *oldItem in self.categoryListArray){
				if ([oldItem.name isEqualToString:[NSString stringWithFormat:@"%@",nameField.text]]){
					BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
					[appDelegate showAlert:M_INPUT_NAME_TITLE message:M_RENAME_TOO];
					nameField.text = nil;
					return;
				}
			}
			
			Categories *category = [[Categories alloc] init];
			//NSLog(@"category -- %@",nameField.text);
			category.name = [NSString stringWithFormat:@"%@",nameField.text];
			category.deleted = 0;
			category.type = 1;
			[appdelegate.dataManager addCategory:category];
			[self dismissModalViewControllerAnimated:YES];
		}
		else 
		{
            NSLog(@"IS NEW IS NOOOOOOOOOOO");
			int i = 0;
			for (PackingList *oldItem in self.categoryListArray){
				if ([oldItem.name isEqualToString:[NSString stringWithFormat:@"%@",nameField.text]]){
					i++;
				}
			}
			
			if (i > 1)
			{
				BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
				[appDelegate showAlert:M_INPUT_NAME_TITLE message:M_RENAME_TOO];
				nameField.text = nil;
				return;
			}
			
			for (PackingItem *oldItem in currentList.itemArray){
				[oldItem replaceCategoryName:[NSString stringWithFormat:@"%@",nameField.text] oldCategoryName:strCategoryName];
			}
            //New
            [appdelegate.dataManager updateItemsOldCategory:strCategoryName withNewCategory:nameField.text];
			
			Categories *category = [[Categories alloc] init];
			//NSLog(@"category -- %@",nameField.text);
			category.pk = editingCategory.pk;
			category.name = [NSString stringWithFormat:@"%@",nameField.text];
			category.deleted = 0;
			[appdelegate.dataManager updateCategory:category];
			[self.navigationController popViewControllerAnimated:YES];
		}
	}
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	NSString *title = nil;
	
	title = [NSString stringWithFormat:@"Enter Category Name"];
	
	return title;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	return 33.0;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return categoryNameCell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark -
#pragma mark Memory management

- (void)dealloc 
{
	[saveButton release];
	[cancelButton release];
	[categoryNameCell release];
	[nameField release];
	[currentList release];
	[categoryListArray release];
	[editingCategory release];
	
    [super dealloc];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	
	[textField resignFirstResponder];
	
	return YES;
}

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{            return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
       // [layoutManager translateToPortraitForView:self.view withAnimation:YES];
    }
    else
    {
        //[layoutManager translateToLandscapeForView:self.view withAnimation:YES];
    }
    // }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}


@end


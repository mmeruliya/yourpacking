//
//  CategoryViewController.h
//  BabyPacking
//
//  Created by Jay Lee on 10/14/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BabyPackingAppDelegate;

@interface CategoryViewController : UIViewController {
	BabyPackingAppDelegate* mAppDelegate;
	NSArray* mCategoryList;
	NSString* mDefaultValue;
	
	IBOutlet UITableView* mTableView;
	IBOutlet UIImageView* mIconView;
	IBOutlet UILabel* mTitleLabel;
}

@property(nonatomic, copy) NSString* defaultValue;

+ (NSString *)titleForValue:(NSString *)aValue;
+ (NSString *)valueForTitle:(NSString *)aTitle;
+ (UIImage *)imageForTitle:(NSString *)aTitle;
+ (UIImage *)imageForValue:(NSString *)aValue;

@end

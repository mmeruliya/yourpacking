//
//  ProgressBar.h
//  BabyPacking
//
//  Created by Gary He on 5/26/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ProgressBar : UIView {
	CGFloat progress;
}

@property(nonatomic) CGFloat progress;

@end

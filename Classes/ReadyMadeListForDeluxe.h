//
//  ReadyMadeListForDeluxe.h
//  BabyPacking
//
//  Created by eve on 9/21/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

@class BabyPackingAppDelegate;

@interface ReadyMadeListForDeluxe : UIViewController<UITableViewDelegate, UITableViewDataSource>  {

    BabyPackingAppDelegate *appDelegate;
    
	IBOutlet UIImageView *bgImageView;
	//IBOutlet UIImageView *bgBorderView;
	
	IBOutlet UITableView *myTableView;
	UIImage *templateIcon;

	NSMutableArray *readyMadeListArray;

	
	RootViewController *homeController;
	UIImage *otherIcon;

	BOOL isFromCheckList;
    
    NSString *titleName;
    
    UILabel *label;
}

@property(nonatomic, retain) NSString *titleName;
@property(nonatomic, retain) RootViewController *homeController;
@property(nonatomic, retain) NSMutableArray   *readyMadeListArray; 

@property (assign, readwrite) BOOL isFromCheckList;

- (void) setFlag;

@end

//
//  ImportParser.m
//  BabyPacking
//
//  Created by Mehul Bhuva on 07/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ImportParser.h"
#import "BabyPackingAppDelegate.h"
#import "Categories.h"
#import "Items.h"
#import "PackingItem.h"
#import "PackingList.h"
#import "BriefcaseItem.h"
#import "RecordItem.h"
#import "TaskItem.h"
#import "DataManager.h"
#import "GetImageParser.h"

@implementation ImportParser

@synthesize successful = mSuccessful;
@synthesize data = mData;
@synthesize mCurItem,mCurItemArray,userDetails,mCurrentString,mCurItemDetail;

- (void)dealloc
{
	//[mArray release];
	[mDelegate release];
	[mCurItem release];
	[mCurItemDetail release];
//	[list_id_forImage release];
	[mData release];
	[mCurrentString release];
	[super dealloc];
}



- (void)beforeParsing
{
	mSuccessful = NO;
	self.data = [NSMutableDictionary dictionaryWithCapacity:0];
	[mData setObject:[NSMutableArray arrayWithCapacity:0] forKey:@"Values"];
}

- (void)initWithDBName:(NSString *)DBName{
	
	appDelegate=(BabyPackingAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	list_id_forImage=[[NSString alloc] init];
	
	storingCharacters=NO;
	mCurrentString=[[NSMutableString alloc] initWithString:@""];
	NSLog(@"initIMportPARSER WITH URL");
	mCurItem = [[NSMutableDictionary alloc] initWithCapacity:0];
	//	mCurItemArray = [[NSMutableArray alloc] initWithCapacity:0];
	//	self.userDetails=[[NSMutableArray alloc] initWithCapacity:0];
	//	NSLog(@"URL STRING=%@",str);
	
	
	//##############################################	
	
//	NSString *urlString = @"http://192.168.0.7/packngohelp/ws/xmlexport.php";
	//	NSString *urlString = @"http://www.madhukantpatel.com/packngohelp/ws/import.php";		
//	http://babypackandgo.com/packngohelp/ws
	NSString *urlString = @"http://babypackandgo.com/packngohelp/ws/xmlexport.php";	
	// setting up the request object now
	
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
	[request setURL:[NSURL URLWithString:urlString]];
	[request setHTTPMethod:@"POST"];
	
	
	NSString *boundary = [NSString stringWithString:@"---------------------------14737809831466499882746641449"];
	NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
	[request addValue:contentType forHTTPHeaderField: @"Content-Type"];
	/*
	 now lets create the body of the post
	 */
	
	NSLog(@"%@",DBName);
//	NSLog(@"%@",xmlString);	
	
	NSMutableData *postBody = [NSMutableData data];
	
	[postBody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
	[postBody appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"userId\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
	[postBody appendData:[[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"]] dataUsingEncoding:NSUTF8StringEncoding]]; 	
	
	[postBody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
	[postBody appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"dbname\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
	[postBody appendData:[[NSString stringWithFormat:@"%@",DBName] dataUsingEncoding:NSUTF8StringEncoding]];  
	
	// setting the body of the post to the reqeust
	[request setHTTPBody:postBody];
	
	// now lets make the connection to the web
	
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
	NSMutableString *returnString = [[NSMutableString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
	
	NSMutableString *stringModified=[returnString stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
	NSData *newReturnData=[stringModified dataUsingEncoding:NSUTF8StringEncoding];
	
	NSLog(@"DATA = %@",stringModified);
	NSXMLParser *parser=[[NSXMLParser alloc] initWithData:newReturnData];
	[parser setDelegate:self];
	[parser parse];
	[parser release];

}

- (void) displayInfo{
	NSLog(@"initDIsplay Info");
	
	
	//		NSLog(@"ID=%@",[mCurItem objectForKey:@"id"]);
	NSLog(@"NAME=%@",[mCurItem objectForKey:@"status"]);	
	NSLog(@"NAME=%@",[mCurItem objectForKey:@"name"]);
	NSLog(@"REACHOUT ID=%@",[mCurItem objectForKey:@"reachoutid"]);
	NSLog(@"GENDER=%@",[mCurItem objectForKey:@"gender"]);
	NSLog(@"AGE=%@",[mCurItem objectForKey:@"age"]);
	NSLog(@"IMAGE URL=%@",[mCurItem objectForKey:@"imageurl"]);
	//		NSLog(@"RATING=%@",[dict1 objectForKey:@"rating"]);
	NSLog(@"MSTATUS=%@",[mCurItem objectForKey:@"marritalstatus"]);
	NSLog(@"LATITUDE=%@",[mCurItem objectForKey:@"latitude"]);
	NSLog(@"LONGITUDE=%@",[mCurItem objectForKey:@"longitude"]);
	//		NSLog(@"DISTANCE=%@",[mCurItem objectForKey:@"distance"]);
	
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
//	NSLog(@"DID START");
//	NSLog(@"ELEMENT NAME = %@",elementName);
	if ([elementName isEqualToString:@"categoriesdetail"]) {
		//[appDelegate.dataManager deleteCategory];
		isCategoriesDetail=NO;
		isItemsDetail=NO;		
		isPackingItemDetail=NO;
		isPackingListDetail=NO;
		isBriefcaseItemDetail=NO;
		isRecordItemDetail=NO;
		isTaskItemDetail=NO;
	}
	else if ([elementName isEqualToString:@"itemsdetail"]) {
//		[appDelegate.dataManager deleteItems];
		isCategoriesDetail=NO;
		isItemsDetail=NO;		
		isItemsDetail=NO;				
		isPackingItemDetail=NO;
		isPackingListDetail=NO;
		isBriefcaseItemDetail=NO;
		isRecordItemDetail=NO;
		isTaskItemDetail=NO;
	}
	else if ([elementName isEqualToString:@"itemdetail"]) {
		PackingItem *pi=[[PackingItem alloc] init];		
		[pi deleteAllData];
		[pi release];
		isCategoriesDetail=NO;
		isItemsDetail=NO;		
		isPackingItemDetail=YES;
		isPackingListDetail=NO;
		isBriefcaseItemDetail=NO;
		isRecordItemDetail=NO;
		isTaskItemDetail=NO;		
	}
	else if ([elementName isEqualToString:@"listdetail"]) {
		PackingList *pl=[[PackingList alloc] init];
		[pl deleteAllData];
		[pl release];
		isCategoriesDetail=NO;
		isItemsDetail=NO;		
		isPackingItemDetail=NO;
		isPackingListDetail=YES;
		isBriefcaseItemDetail=NO;
		isRecordItemDetail=NO;
		isTaskItemDetail=NO;		
	}
	else if ([elementName isEqualToString:@"briefcasedetail"]) {
		BriefcaseItem *bci=[[BriefcaseItem alloc] init];
		[bci deleteAllData];
		[bci release];
		isCategoriesDetail=NO;
		isItemsDetail=NO;		
		isPackingItemDetail=NO;
		isPackingListDetail=NO;
		isBriefcaseItemDetail=YES;
		isRecordItemDetail=NO;
		isTaskItemDetail=NO;		
	}
	else if ([elementName isEqualToString:@"recorddetail"]) {
		RecordItem *ri=[[RecordItem alloc] init];
		[ri deleteAllData];
		[ri release];
		isCategoriesDetail=NO;
		isItemsDetail=NO;		
		isPackingItemDetail=NO;
		isPackingListDetail=NO;
		isBriefcaseItemDetail=NO;
		isRecordItemDetail=YES;
		isTaskItemDetail=NO;		
	}
	else if ([elementName isEqualToString:@"taskdetail"]) {
		TaskItem *ti=[[TaskItem alloc] init];
		[ti deleteAllData];
		[ti release];
		isCategoriesDetail=NO;
		isItemsDetail=NO;		
		isPackingItemDetail=NO;
		isPackingListDetail=NO;
		isBriefcaseItemDetail=NO;
		isRecordItemDetail=NO;
		isTaskItemDetail=YES;		
	}
	else if ([elementName isEqualToString:@"categories"])
	{
		mCurItem = [[NSMutableDictionary alloc] initWithCapacity:0];
		aCategories = [[Categories alloc] init];		
	}		
	else if ([elementName isEqualToString:@"Items"])
	{
		mCurItem = [[NSMutableDictionary alloc] initWithCapacity:0];
		aItems=[[Items alloc] init];
	}
	else if ([elementName isEqualToString:@"item"]) {
		mCurItem = [[NSMutableDictionary alloc] initWithCapacity:0];
		pItem=[[PackingItem alloc] init];
		pItem.dirty = YES;
	}
	else if ([elementName isEqualToString:@"list"])
	{
		mCurItem = [[NSMutableDictionary alloc] initWithCapacity:0];
		pList=[[PackingList alloc] init];
		pList.dirty = YES;
	}
	else if ([elementName isEqualToString:@"briefcase"])
	{
		mCurItem = [[NSMutableDictionary alloc] initWithCapacity:0];
		bItem=[[BriefcaseItem alloc] init];
		bItem.dirty = YES;
	}
	else if ([elementName isEqualToString:@"record"])
	{
		mCurItem = [[NSMutableDictionary alloc] initWithCapacity:0];
		rItem=[[RecordItem alloc] init];
		rItem.dirty = YES;
	}
	else if ([elementName isEqualToString:@"task"])
	{
		mCurItem = [[NSMutableDictionary alloc] initWithCapacity:0];
		tItem=[[TaskItem alloc] init];
		tItem.dirty = YES;
	}
	else
	{
		[mCurrentString setString:@""];
		storingCharacters = YES;
	}	
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
//	NSLog(@"DID END");
//	NSLog(@"ELEMENT NAME = %@",elementName);
	if ([elementName isEqualToString:@"categoriesdetail"]) {
		isCategoriesDetail=NO;
		isItemsDetail=NO;		
		isPackingItemDetail=NO;
		isPackingListDetail=NO;
		isBriefcaseItemDetail=NO;
		isRecordItemDetail=NO;
		isTaskItemDetail=NO;		
	}
	else if ([elementName isEqualToString:@"itemsdetail"]) {
		isCategoriesDetail=NO;
		isItemsDetail=NO;		
		isPackingItemDetail=NO;
		isPackingListDetail=NO;
		isBriefcaseItemDetail=NO;
		isRecordItemDetail=NO;
		isTaskItemDetail=NO;		
	}
	else if ([elementName isEqualToString:@"itemdetail"]) {
		isCategoriesDetail=NO;
		isItemsDetail=NO;		
		isPackingItemDetail=NO;
		isPackingListDetail=NO;
		isBriefcaseItemDetail=NO;
		isRecordItemDetail=NO;
		isTaskItemDetail=NO;				
	}
	else if ([elementName isEqualToString:@"listdetail"]) {
		isCategoriesDetail=NO;
		isItemsDetail=NO;		
		isPackingItemDetail=NO;
		isPackingListDetail=NO;
		isBriefcaseItemDetail=NO;
		isRecordItemDetail=NO;
		isTaskItemDetail=NO;				
	}
	else if ([elementName isEqualToString:@"briefcasedetail"]) {
		isCategoriesDetail=NO;
		isItemsDetail=NO;		
		isPackingItemDetail=NO;
		isPackingListDetail=NO;
		isBriefcaseItemDetail=NO;
		isRecordItemDetail=NO;
		isTaskItemDetail=NO;				
	}
	else if ([elementName isEqualToString:@"recorddetail"]) {
		isCategoriesDetail=NO;
		isItemsDetail=NO;		
		isPackingItemDetail=NO;
		isPackingListDetail=NO;
		isBriefcaseItemDetail=NO;
		isRecordItemDetail=NO;
		isTaskItemDetail=NO;				
	}
	else if ([elementName isEqualToString:@"taskdetail"]) {
		isCategoriesDetail=NO;
		isItemsDetail=NO;		
		isPackingItemDetail=NO;
		isPackingListDetail=NO;
		isBriefcaseItemDetail=NO;
		isRecordItemDetail=NO;
		isTaskItemDetail=NO;				
	}
	
	
	if (isCategoriesDetail) {
		if ([elementName isEqualToString:@"pk"]) {
//			aCategories.pk=[[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] intValue];
		}
		else if ([elementName isEqualToString:@"name"]) {
			aCategories.name=[NSString stringWithFormat:@"%@",mCurrentString];
			NSLog(@"NAME=%@",mCurrentString);
		}
		else if ([elementName isEqualToString:@"deleted"]) {
			aCategories.deleted=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
						NSLog(@"%d",aCategories.deleted);
		}
		else if ([elementName isEqualToString:@"type"]) {
			aCategories.type=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
						NSLog(@"%d",aCategories.type);
		}
		else if ([elementName isEqualToString:@"categories"]) {
			[appDelegate.dataManager addCategory:aCategories];
			[aCategories release];
			NSLog(@"CATEGORY ADDED SUCCESFULLY");
		}
	}
	else if (isItemsDetail) {
		if ([elementName isEqualToString:@"pk"]) {
			//aItems.pk=[[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] intValue];
		}
		else if ([elementName isEqualToString:@"checked"]) {
			aItems.checked=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
			NSLog(@"ITEM CHECKED = %d",aItems.checked);
		}
		else if ([elementName isEqualToString:@"name"]) {
			aItems.name=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"tip"]) {
			aItems.tip=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"custom"]) {
			aItems.custom=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"qty"]) {
			aItems.qty=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"deleted"]) {
			aItems.deleted=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
		}
		else if ([elementName isEqualToString:@"categoryId"]) {
			aItems.categoryId=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
		}
		else if ([elementName isEqualToString:@"Items"]) {
			[appDelegate.dataManager addItem:aItems];
			NSLog(@"ITEMS ADDED SUCCESSFULLY");
			[aItems release];
		}		
	}
	else if (isPackingItemDetail) {
		if ([elementName isEqualToString:@"pk"]) {
			//pItem.pk=[[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] intValue];
		}
		else if ([elementName isEqualToString:@"name"]) {
			pItem.name=[NSString stringWithFormat:@"%@",mCurrentString];
			NSLog(@"ITEM NAME = %@",pItem.name);
		}
		else if ([elementName isEqualToString:@"qty"]) {
			pItem.qty=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"tip"]) {
			pItem.tip=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"selected"]) {
			pItem.selected=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
		}
		else if ([elementName isEqualToString:@"custom"]) {
			pItem.custom=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"checked"]) {
			pItem.checked=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
		}		
		else if ([elementName isEqualToString:@"listId"]) {
			pItem.listId=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
		}		
		else if ([elementName isEqualToString:@"predefineId"]) {
			pItem.predefineId=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
		}		
		else if ([elementName isEqualToString:@"predefineForAge"]) {
			pItem.predefineForAge=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
		}
		else if ([elementName isEqualToString:@"item"]) {
			[pItem insertData];
			NSLog(@"PackingItem Succesfully inserted");
			[pItem release];
		}
	}
	else if (isPackingListDetail) {
			if ([elementName isEqualToString:@"pk"]) {
				//pList.pk=[[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] intValue];
				list_id_forImage=[NSString stringWithFormat:@"%@",mCurrentString];
			}
			else if ([elementName isEqualToString:@"name"]) {
				pList.name=[NSString stringWithFormat:@"%@",mCurrentString];
				NSLog(@"ITEM NAME = %@",pList.name);
			}
			else if ([elementName isEqualToString:@"sex"]) {
				pList.sex=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
			}
			else if ([elementName isEqualToString:@"age"]) {
				pList.age=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
			}
			else if ([elementName isEqualToString:@"smallIcon"]) {
				if ([mCurrentString isEqualToString:@"1"]) {

					GetImageParser *getImage=[[GetImageParser alloc] init];
					UIImage* image1 = [UIImage imageWithData:[getImage initWithData:[NSString stringWithFormat:@"%@",list_id_forImage] withUserId:[[NSUserDefaults standardUserDefaults] objectForKey:@"USERID"]]];
					if(image1 != nil)
					{
						NSLog(@"menu image success.......");
						pList.icon = image1;
						pList.photo=image1;
					}
					
					NSLog(@"%@",pList.icon);
					[getImage release];
				}
			}
//			else if ([elementName isEqualToString:@"photo"]) {
//				pList.photo=[NSString stringWithFormat:@"%@",mCurrentString];
//			}
			else if ([elementName isEqualToString:@"lastUpdate"]) {
				pList.lastUpdate=[[NSString stringWithFormat:@"%@",mCurrentString] floatValue];
			}		
			else if ([elementName isEqualToString:@"totalCount"]) {
				pList.totalCount=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
			}		
			else if ([elementName isEqualToString:@"checkedCount"]) {
				pList.checkedCount=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
			}		
			else if ([elementName isEqualToString:@"predefineId"]) {
				pList.predefineId=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
			}
			else if ([elementName isEqualToString:@"predefineForAge"]) {
				pList.predefineForAge=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
			}
			else if ([elementName isEqualToString:@"list"]) {
				[pList insertData];
				NSLog(@"PackingList Succesfully inserted");
				[pList release];
			}
		}
	else if (isBriefcaseItemDetail) {
		if ([elementName isEqualToString:@"pk"]) {
			//bItem.pk=[[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] intValue];
		}
		else if ([elementName isEqualToString:@"title"]) {
			bItem.title=[NSString stringWithFormat:@"%@",mCurrentString];
			NSLog(@"ITEM NAME = %@",bItem.title);
		}
		else if ([elementName isEqualToString:@"passport"]) {
			bItem.passport=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"license"]) {
			bItem.license=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"ssn"]) {
			bItem.ssn=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"id_card"]) {
			bItem.id_card=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"airline_name"]) {
			bItem.airline_name=[NSString stringWithFormat:@"%@",mCurrentString];
		}		
		else if ([elementName isEqualToString:@"flight"]) {
			bItem.flight=[NSString stringWithFormat:@"%@",mCurrentString];
		}		
		else if ([elementName isEqualToString:@"frequent_flyer"]) {
			bItem.frequent_flyer=[NSString stringWithFormat:@"%@",mCurrentString];
		}		
		else if ([elementName isEqualToString:@"car_name"]) {
			bItem.car_name=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"license_plate"]) {
			bItem.license_plate=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"hotel_name"]) {
			bItem.hotel_name=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"hotel_phone"]) {
			bItem.hotel_phone=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"hotel_address"]) {
			bItem.hotel_address=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"embassy_name"]) {
			bItem.embassy_name=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"embassy_address"]) {
			bItem.embassy_address=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"embassy_emergency"]) {
			bItem.embassy_emergency=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"other"]) {
			bItem.other=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"briefcase"]) {
			[bItem insertData];
			NSLog(@"BriefcaseItem Succesfully inserted");
			[bItem release];
		}
	}
	else if (isRecordItemDetail) {
		if ([elementName isEqualToString:@"id"]) {
			//rItem.pk=[[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] intValue];
		}
		else if ([elementName isEqualToString:@"remote_id"]) {
			rItem.remote_id=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
			NSLog(@"ITEM NAME = %d",rItem.remote_id);
		}
		else if ([elementName isEqualToString:@"tid"]) {
			rItem.task_id=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
		}
		else if ([elementName isEqualToString:@"title"]) {
			rItem.title=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"category"]) {
			rItem.category=[NSString stringWithFormat:@"%@",mCurrentString];
		}		
		else if ([elementName isEqualToString:@"repeat"]) {
			rItem.repeat=[NSString stringWithFormat:@"%@",mCurrentString];
		}		
		else if ([elementName isEqualToString:@"interval"]) {
			rItem.interval=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
		}		
		else if ([elementName isEqualToString:@"start_date"]) {
			NSDateFormatter *dFormat=[[NSDateFormatter alloc] init];
			rItem.start_date=[dFormat dateFromString:[NSString stringWithFormat:@"%@",mCurrentString]];
			[dFormat release];
		}
		else if ([elementName isEqualToString:@"end_date"]) {
			NSDateFormatter *dFormat1=[[NSDateFormatter alloc] init];
			rItem.end_date=[dFormat1 dateFromString:[NSString stringWithFormat:@"%@",mCurrentString]];
			[dFormat1 release];
		}
		else if ([elementName isEqualToString:@"text"]) {
			rItem.text=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"device_token"]) {
			rItem.device_token=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"create_date"]) {
			NSDateFormatter *dFormat2=[[NSDateFormatter alloc] init];
			rItem.create_date=[dFormat2 dateFromString:[NSString stringWithFormat:@"%@",mCurrentString]];
			[dFormat2 release];			
		}
		else if ([elementName isEqualToString:@"update_date"]) {
			NSDateFormatter *dFormat3=[[NSDateFormatter alloc] init];
			rItem.update_date=[dFormat3 dateFromString:[NSString stringWithFormat:@"%@",mCurrentString]];
			[dFormat3 release];						
		}
		else if ([elementName isEqualToString:@"record"]) {
			[rItem insertData];
			NSLog(@"RecordItem Succesfully inserted");
			[rItem release];
		}
	}
	else if (isTaskItemDetail) {
		if ([elementName isEqualToString:@"id"]) {
			//tItem.pk=[[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] intValue];
		}
		else if ([elementName isEqualToString:@"remote_id"]) {
			tItem.remote_id=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
			NSLog(@"TASK REMOTE ID = %d",tItem.remote_id);
		}
		else if ([elementName isEqualToString:@"title"]) {
			tItem.title=[NSString stringWithFormat:@"%@",mCurrentString];
			NSLog(@"TASK TITLE = %@",tItem.title);			
		}
		else if ([elementName isEqualToString:@"category"]) {
			tItem.category=[NSString stringWithFormat:@"%@",mCurrentString];
		}		
		else if ([elementName isEqualToString:@"repeat"]) {
			tItem.repeat=[NSString stringWithFormat:@"%@",mCurrentString];
		}		
		else if ([elementName isEqualToString:@"interval"]) {
			tItem.interval=[[NSString stringWithFormat:@"%@",mCurrentString] intValue];
		}		
		else if ([elementName isEqualToString:@"start_date"]) {
			NSDateFormatter *dFormat=[[NSDateFormatter alloc] init];
			rItem.start_date=[dFormat dateFromString:[NSString stringWithFormat:@"%@",mCurrentString]];
			[dFormat release];			
		}
		else if ([elementName isEqualToString:@"end_date"]) {
			NSDateFormatter *dFormat1=[[NSDateFormatter alloc] init];
			rItem.end_date=[dFormat1 dateFromString:[NSString stringWithFormat:@"%@",mCurrentString]];
			[dFormat1 release];			
		}
		else if ([elementName isEqualToString:@"text"]) {
			tItem.text=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"device_token"]) {
			tItem.device_token=[NSString stringWithFormat:@"%@",mCurrentString];
		}
		else if ([elementName isEqualToString:@"create_date"]) {
			NSDateFormatter *dFormat2=[[NSDateFormatter alloc] init];
			rItem.create_date=[dFormat2 dateFromString:[NSString stringWithFormat:@"%@",mCurrentString]];
			[dFormat2 release];						
		}
		else if ([elementName isEqualToString:@"update_date"]) {
			NSDateFormatter *dFormat3=[[NSDateFormatter alloc] init];
			rItem.update_date=[dFormat3 dateFromString:[NSString stringWithFormat:@"%@",mCurrentString]];
			[dFormat3 release];									
		}
		else if ([elementName isEqualToString:@"record"]) {
			[tItem insertData];
			NSLog(@"TaskItem Succesfully inserted");
		}
		else if ([elementName isEqualToString:@"task"]) {
			[tItem insertData];
			NSLog(@"TASK ITEM INSERTED SUCCESFULLY");
			[tItem release];
		}

	}
	storingCharacters=NO;
}

- (void)parser:(NSXMLParser*)parser foundCharacters:(NSString*)string
{
	if (storingCharacters) {
		[mCurrentString appendString:string];		
	}
	
}

- (void) parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
	NSLog(@"IMPORT PARSER Error : %@",[parseError localizedDescription]);
}


//- (void)parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock
//{
//    NSString *someString = [[NSString alloc] initWithData:CDATABlock encoding:NSUTF8StringEncoding];
//	[mCurItem setObject:someString forKey:@"message"];
//}

@end
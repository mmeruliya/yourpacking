//
//  SettingsViewController.m
//  BabyPacking
//
//  Created by Gary He on 6/8/09.
//  Copyright 2009 test. All rights reserved.
//

#import "Constant.h"
#import "CustomizeCellViewController.h"
#import "BabyPackingAppDelegate.h"

@implementation CustomizeCellViewController

- (void)dealloc {
	
	[lblTextSize release];
	[cancelButton release];
	[doneButton release];
    [super dealloc];
}

/*
 - (id)initWithStyle:(UITableViewStyle)style {
 // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 if (self = [super initWithStyle:style]) {
 }
 return self;
 }
 */

-(IBAction) selectColor:(id)sender {
    
    if ([sender tag]==1) {
		colorSelectIndex=1;
	}
	else if ([sender tag]==2) {
		colorSelectIndex=2;		
	}
    
    ColorPickerViewController *colorPickerViewController;
    
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){    
        
        colorPickerViewController = [[ColorPickerViewController alloc] initWithNibName:@"ColorPickerViewController_ipad" bundle:nil];
    }else{
        colorPickerViewController = [[ColorPickerViewController alloc] initWithNibName:@"ColorPickerViewController" bundle:nil];
    }
    
    colorPickerViewController.delegate = self;
#ifdef IPHONE_COLOR_PICKER_SAVE_DEFAULT
        colorPickerViewController.defaultsKey = @"SwatchColor";
#else
    // We re-use the current value set to the background of this demonstration view
//    colorPickerViewController.defaultsColor = colorSwatch.backgroundColor;
#endif
    [self presentModalViewController:colorPickerViewController animated:YES];
    [colorPickerViewController release];
}

- (void)colorPickerViewController:(ColorPickerViewController *)colorPicker didSelectColor:(UIColor *)color {
    
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:color];
    //    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:colorPicker.defaultsKey];
    
	if (colorSelectIndex==1) {

		// write the data into the user defaults
		[[NSUserDefaults standardUserDefaults] setObject: colorData forKey: K_CELL_BG_COLOR];
		
	}
	else {
		
		// write the data into the user defaults
		[[NSUserDefaults standardUserDefaults] setObject: colorData forKey: K_CELL_TEXT_COLOR];
		
	}
    
    [colorPicker dismissModalViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        layoutManager = [[LayoutManager alloc] initWithPortraitView:self.view andLandscapeNIB:@"CustomizeCellViewController_L"];
        //self = [super initWithNibName:@"RecalliPhoneView" bundle:nibBundleOrNil];
        
    }
    return self;
}



- (void)viewDidLoad {
	
    [super viewDidLoad];

    mAppDelegate = (id)[UIApplication sharedApplication].delegate;    
	
    if (IOS_NEWER_THAN(7))
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor blackColor];    
	NSString *customFontName = [mAppDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];		
	}
	[label sizeToFit];
	
	count=0;
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]==nil) {
        lblTextSize.text = @"14";
    }
    else {
        lblTextSize.text = [[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE];        
    }

	
	//label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
    label.textColor=[UIColor whiteColor];        
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label.textColor = theColour1;
	}
	// and finally set the colour of your label
	
	self.navigationItem.titleView = label;	
	label.text = @"Customize Table Cell";
	[label sizeToFit];
	
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 30, 30);
    [btn setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    btn.tag=1;
    cancelButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
  

//	self.navigationItem.rightBarButtonItem = doneButton;
	self.navigationItem.leftBarButtonItem = cancelButton;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
	
}


- (IBAction) buttonAction:(id) sender {
	if ([sender tag]== 1) {
		[self.navigationController popViewControllerAnimated:YES];
	} else if (sender == doneButton) {
		// save settings	
		
		NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
		[userSettings setObject:mAppDelegate.addItemsByGroup forKey:K_SET_ADDITEMSBYGROUP];
		[userSettings setObject:mAppDelegate.welcomePageShow forKey:K_SET_WELCOMEPAGESHOW];
		[userSettings setObject:mAppDelegate.fontType forKey:K_SET_FONT_TYPE];
		[userSettings synchronize];
		[self.navigationController popViewControllerAnimated:YES];
	}
}
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [layoutManager translateToPortraitForView:self.view withAnimation:NO];
    }
    else
    {
        [layoutManager translateToLandscapeForView:self.view withAnimation:NO];
    }

	NSString *customFontName = [mAppDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
    //	label.font = [UIFont fontWithName:customFontName size:fontSize];
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
		lblCellTextPreview.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];		
	}
	else {
		lblCellTextPreview.font=[UIFont fontWithName:customFontName size:fontSize];		
	}
	
	//lblCellTextPreview.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	lblCellTextPreview.textAlignment = UITextAlignmentCenter;
    
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		lblCellTextPreview.textColor = theColour1;
	}
	// and finally set the colour of your label
    
    
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour21;
	// read the data back from the user defaults
	NSData *data211= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
	// check whether you got anything
	if(data211 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour21 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data211];
		imgCellBGPreview.backgroundColor = theColour21;
	}
	// and finally set the colour of your label
    
}

/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (IBAction) onSettingResetClicked:(id)sender{
    NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
    [userSettings setObject:nil forKey:K_CELL_BG_COLOR];
    [userSettings setObject:nil forKey:K_CELL_TEXT_COLOR];
    [userSettings setObject:nil forKey:K_CELL_TEXT_SIZE];
    lblCellTextPreview.textColor=[UIColor clearColor];
    imgCellBGPreview.backgroundColor=[UIColor clearColor];
    lblTextSize.text=@"";
}

- (IBAction) onMinusClicked:(id)sender{
	NSLog(@"NUMBEAlkhf lhaslf a ===== %d",[lblTextSize.text intValue]);
	if ([lblTextSize.text intValue]-1>0) {
		int i=[lblTextSize.text intValue];
		i--;
		lblTextSize.text=[NSString stringWithFormat:@"%d",i];		
		[[NSUserDefaults standardUserDefaults] setObject:lblTextSize.text forKey:K_CELL_TEXT_SIZE];				
        
        NSString *customFontName = [mAppDelegate getCustomFontName];
		lblCellTextPreview.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];
        
	}
}

- (IBAction) onPlusClicked:(id)sender{
	if ([lblTextSize.text intValue]<30) {
		int i=[lblTextSize.text intValue];
		i++;
		lblTextSize.text=[NSString stringWithFormat:@"%d",i];			
		[[NSUserDefaults standardUserDefaults] setObject:lblTextSize.text forKey:K_CELL_TEXT_SIZE];		
	
        NSString *customFontName = [mAppDelegate getCustomFontName];
		lblCellTextPreview.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];
    }
}

- (IBAction) colorButtonPressed:(id)sender{
	if ([selectColorView superview]) {
		[selectColorView removeFromSuperview];
	}
	
	if (colorSelectIndex==1) {
		UIButton *btn=(UIButton *)sender;
		
		UIColor *selectedColor = btn.backgroundColor;
		
		NSData *data = [NSKeyedArchiver archivedDataWithRootObject: selectedColor];	
		
		// write the data into the user defaults
		[[NSUserDefaults standardUserDefaults] setObject: data forKey: K_CELL_BG_COLOR];
		
	}
	else {
		UIButton *btn=(UIButton *)sender;
		
		UIColor *selectedColor = btn.backgroundColor;
		
		NSData *data = [NSKeyedArchiver archivedDataWithRootObject: selectedColor];	
		
		// write the data into the user defaults
		[[NSUserDefaults standardUserDefaults] setObject: data forKey: K_CELL_TEXT_COLOR];
		
	}
	
	
}

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
         [layoutManager translateToPortraitForView:self.view withAnimation:YES];
    }
    else
    {
         [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
    }
    // }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}

//- (IBAction) selectColor:(id)sender{
//	if ([sender tag]==1) {
//		colorSelectIndex=1;
//	}
//	else {
//		colorSelectIndex=2;		
//	}
//	
//	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
//		[UIView beginAnimations:nil context:nil];
//		[selectColorView setFrame:CGRectMake(0, 768, 1024, 768)];
//		[self.view addSubview:selectColorView];
//		
//		[UIView setAnimationDuration:0.75f];
//		[selectColorView setFrame:CGRectMake(0, 0, 768, 1024)];
//		[UIView commitAnimations];	
//	}
//	else {
//		[UIView beginAnimations:nil context:nil];
//		[selectColorView setFrame:CGRectMake(0, 480, 320, 570)];
//		[self.view addSubview:selectColorView];
//		
//		
//		[UIView setAnimationDuration:0.75f];
//		[selectColorView setFrame:CGRectMake(0, 0, 320, 570)];
//		[UIView commitAnimations];	
//	}
//}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

@end
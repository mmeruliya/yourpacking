//
//  DetailOfBriefcaseViewContronller.h
//  BabyPacking
//
//  Created by eve on 9/7/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BriefcaseItem.h"

@class BabyPackingAppDelegate;

@interface DetailOfBriefcaseViewController : UIViewController <UITextFieldDelegate>{

	BabyPackingAppDelegate *appDelegate;
	IBOutlet UIBarButtonItem	*saveButton;
	IBOutlet UIBarButtonItem	*cancelButton;
	IBOutlet UITableView		*mTableView;
	IBOutlet UIView				*footerTableView;
	UITextField					*currentTextField;
	NSMutableArray				*textArray;
	BriefcaseItem				*briefcaseItem;
	BOOL isNewItem;
}
@property(nonatomic, retain) BriefcaseItem *briefcaseItem;
@property(nonatomic, retain) NSMutableArray *textArray;

- (void) buttonAction:(id) sender;

@end

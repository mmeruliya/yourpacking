//
//  BriefcaseViewController.h
//  BabyPacking
//
//  Created by eve on 9/4/09.
//  Copyright 2009 test. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "BriefcaseItem.h"

@interface BriefcaseViewController : UIViewController<UIActionSheetDelegate> {
	
	IBOutlet UIBarButtonItem *doneButton;
	IBOutlet UIBarButtonItem *editButton;
	IBOutlet UIBarButtonItem *addButton;
	IBOutlet UITableView     *mTableView;
	IBOutlet UIImageView     *bgImageView;
	IBOutlet UILabel         *label;
	NSMutableArray           *briefcaseList;
	NSInteger row;
	NSInteger deleteRow;
    IBOutlet UIView *dataView;

	}
@property(nonatomic, retain) NSMutableArray *briefcaseList;
@property NSInteger row,deleteRow;
@property(nonatomic, retain) IBOutlet UILabel *lbl1,*lbl2,*lbl3,*lbl4,*lbl5,*lbl6,*lbl7,*lbl8,*lbl9,*lbl10,*lbl11,*lbl12,*lbl13,*lbl14,*lbl15,*lbl16;

- (void) buttonAction:(id) sender;


@end

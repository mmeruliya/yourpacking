//
//  RequestHandler.m
//  BabyPacking
//
//  Created by Jay Lee on 10/13/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import "RequestHandler.h"
#import "ResponseHandler.h"

#define NETWORK_TIMEOUT 30

static NSString* PostBoundary = @"0xKhTmLbOuNdArY";

NSInteger SortKeyByName(id item1, id item2, void *context)
{
	NSString* string1 = item1;
	NSString* string2 = item2;
	
    return [string1 compare:string2 options:NSCaseInsensitiveSearch | NSLiteralSearch];
}

@interface RequestHandler (Private)

- (NSData*)generateFormData:(NSDictionary*)variables;
- (void)closeConnection;
- (void)reportError;

@end

@implementation RequestHandler

@synthesize delegate = mDelegate;
@synthesize url = mRequestURL;
@synthesize requestArguments = mArguments;
@synthesize additionalArguments = mAdditionalArguments;
@synthesize method = mRequestMethod;
@synthesize timeout = mTimeOut;
@synthesize connection = mConnection;
@synthesize connectionData = mConnectionData;
@synthesize userData = mUserData;

- (NSInteger) inSort:(id)item1 to:(id)item2
{
	NSString* string1 = item1;
	NSString* string2 = item2;
	
    return [string1 compare:string2 options:NSCaseInsensitiveSearch | NSLiteralSearch];
}

- (void)dealloc
{
	[self closeConnection];
	[mArguments release];
	[mAdditionalArguments release];
	[mRequestMethod release];
	[mRequestURL release];
	[mUserData release];
	[super dealloc];
}

- (id)init {
	if (self = [super init]) {
		mAppDelegate = (id)[UIApplication sharedApplication].delegate;
		mTimeOut = NETWORK_TIMEOUT;
	}
	
	return self;	
}

- (void)startRequest {
	
	// Prepare request.
	NSMutableDictionary *fields = [NSMutableDictionary dictionaryWithCapacity:0];
	
	if ([@"POST" isEqualToString:mRequestMethod]) {
		NSURL* url = [NSURL URLWithString:mRequestURL];
		NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url
															   cachePolicy:NSURLRequestReturnCacheDataElseLoad
														   timeoutInterval:mTimeOut];
		[request setHTTPMethod:@"POST"];
		NSString* contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", PostBoundary];
		[request addValue:contentType forHTTPHeaderField: @"Content-Type"];	
		
		[fields setValuesForKeysWithDictionary:mAdditionalArguments];
		[fields setValuesForKeysWithDictionary:mArguments];
		NSData* postData = [self generateFormData:fields];
		[request setHTTPBody:postData];
		self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
		
	} else if ([@"GET" isEqualToString:mRequestMethod]) {
		NSMutableDictionary* arguements = [NSMutableDictionary dictionaryWithDictionary:mArguments];
		NSArray *keys = [arguements allKeys];
		NSArray* sorted_keys = [keys sortedArrayUsingFunction:SortKeyByName context:nil];
		NSMutableString *query_string = [NSMutableString stringWithCapacity:0];
		for (NSString *key in sorted_keys) {
			NSString *val = [arguements objectForKey:key];
			[query_string appendFormat:@"%@=%@&", key, val];
		}
		
		NSString* stringURL = [NSString stringWithFormat:@"%@%@", mRequestURL, [query_string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
		NSURL* url = [NSURL URLWithString:stringURL];
		NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url
															   cachePolicy:NSURLRequestUseProtocolCachePolicy
														   timeoutInterval:mTimeOut];
		self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
	}
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

#pragma mark Private methods implementation

- (NSData*)generateFormData:(NSDictionary*)variables
{
	NSMutableData* result = [[NSMutableData new] autorelease];	
	for (NSString* key in variables) {
		id value = [variables valueForKey:key];
		[result appendData:[[NSString stringWithFormat:@"--%@\r\n", PostBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
		if ([value isKindOfClass:[NSString class]])
		{
			[result appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
			[result appendData:[[NSString stringWithFormat:@"%@",value] dataUsingEncoding:NSUTF8StringEncoding]];
		}
		else if ([value isKindOfClass:[NSData class]])
		{
			[result appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", key, @"photo.jpg"] dataUsingEncoding:NSUTF8StringEncoding]];
			[result appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
			[result appendData:value];
		}
		[result appendData:[[NSString stringWithString:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
	}
	[result appendData:[[NSString stringWithFormat:@"--%@--\r\n", PostBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
	
	return result;
}

- (void)closeConnection
{
	[mConnection cancel];
	[mConnectionData release];
	mConnectionData = nil;
	[mConnection release];
	mConnection = nil;
}

#pragma mark NSURLConnection delegate implementation

- (void)connection:(NSURLConnection*)connection didReceiveData:(NSData*)data
{
	[mConnectionData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection*)connection
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	NSData* xmlData = [[mConnectionData retain] autorelease];
	[self closeConnection];
	[mDelegate request:self responseCompleteWithData:xmlData];
}

- (void)connection:(NSURLConnection*)connection didFailWithError:(NSError*)error
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[mDelegate request:self responseFailedWithError:error];
	[self closeConnection];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	self.connectionData = [NSMutableData dataWithLength:0];
}

@end

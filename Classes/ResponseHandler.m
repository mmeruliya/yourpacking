//
//  SearchResultParser.m
//  BabyPacking
//
//  Created by Jay Lee on 10/13/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import "ResponseHandler.h"

@implementation ResponseHandler

@synthesize errorCode = mErrorCode;
@synthesize found = mFound;
@synthesize errorMessage = mErrorMessage;
@synthesize task_id = mTask_id;

- (void)dealloc
{
	[mErrorMessage release];
	[super dealloc];
}

- (void)beforeParsing
{
	self.found = NO;
	self.errorCode = 1;
}

- (void)didEndElement:(NSString*)elementName namespaceURI:(NSString*)namespaceURI qualifiedName:(NSString*)qualifiedName
{
	if ([self xmlPathEndsWith:@"Reminder", nil]) {
		self.found = YES;
	} else if ([self xmlPathEndsWith:@"Reminder", @"error", nil]) {
		self.errorCode = [[self trimmedString] intValue];
	} else if ([self xmlPathEndsWith:@"Reminder", @"message", nil]) {
		self.errorMessage = [self trimmedString];
	} else if ([self xmlPathEndsWith:@"Reminder", @"id", nil]) {
		self.task_id = [[self trimmedString] intValue];
	}
}

@end

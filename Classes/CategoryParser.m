//
//  CategoryParser.m
//  BabyPacking
//
//  Created by Hitesh Vaghasiya on 22/04/11.
//  Copyright 2011 Dev IT Solution pvt ltd. All rights reserved.
//

#import "CategoryParser.h"

@implementation CategoryParser

@synthesize successful = mSuccessful;
@synthesize data = mData;

- (void)dealloc
{
	[mCurItem release];
	[mData release];
	[super dealloc];
}

- (void)beforeParsing
{
	mSuccessful = NO;
	self.data = [NSMutableDictionary dictionaryWithCapacity:0];
	[mData setObject:[NSMutableArray arrayWithCapacity:0] forKey:@"Values"];
}

- (void)parser:(NSXMLParser*)parser didStartElement:(NSString*)elementName namespaceURI:(NSString*)namespaceURI qualifiedName:(NSString*)qualifiedName attributes:(NSDictionary*)attributes
{
	[super parser:parser didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributes];
	if ([elementName isEqualToString:[NSString stringWithFormat:@"category"]]) {
		mCurItem = [[NSMutableDictionary alloc] initWithCapacity:0];
	}
}

- (void)didEndElement:(NSString*)elementName namespaceURI:(NSString*)namespaceURI qualifiedName:(NSString*)qualifiedName
{
	if ([elementName isEqualToString:@"id"]) {
		[mCurItem setObject:[self trimmedString] forKey:@"id"];
	} else if ([elementName isEqualToString:@"parentId"]) {
		[mCurItem setObject:[self trimmedString] forKey:@"parentId"];
	} else if ([elementName isEqualToString:@"name"]) {
		[mCurItem setObject:[self trimmedString] forKey:@"name"];
	} else if ([elementName isEqualToString:@"shortdescription"]) {
		[mCurItem setObject:[self trimmedString] forKey:@"shortdescription"];
	} else if ([elementName isEqualToString:@"description"]) {
		[mCurItem setObject:[self trimmedString] forKey:@"description"];
	} else if ([elementName isEqualToString:[NSString stringWithFormat:@"category"]]) {
		NSMutableArray* values = [mData objectForKey:@"Values"];
		[values addObject:mCurItem];
		[mCurItem release];
		mCurItem = nil;
	}
}

@end

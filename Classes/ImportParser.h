//
//  ImportParser.h
//  BabyPacking
//
//  Created by Mehul Bhuva on 07/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <string.h>

#define HexCharToNybble(x) ((char)((x > '9') ? tolower(x) - 'a' + 10 : x - '0') & 0xF)


@class BabyPackingAppDelegate;
@class Categories;
@class Items;
@class PackingItem;
@class PackingList;
@class BriefcaseItem;
@class RecordItem;
@class TaskItem;

@interface ImportParser : NSObject <NSXMLParserDelegate>{
	BOOL mSuccessful;
	BabyPackingAppDelegate *appDelegate;
	NSMutableDictionary* mData;
	NSMutableString* mCurrentString;
	NSMutableDictionary* mCurItem;
	NSMutableDictionary* mCurItemDetail;
	NSMutableArray *mCurItemArray;
	NSMutableArray *userDetails;
	NSString *list_id_forImage;
	
	
	BOOL storingCharacters;
	id mDelegate;
	BOOL isItemsDetail;
	BOOL isCategoriesDetail;
	BOOL isPackingItemDetail;
	BOOL isPackingListDetail;
	BOOL isBriefcaseItemDetail;
	BOOL isRecordItemDetail;
	BOOL isTaskItemDetail;
	
	Categories *aCategories;
	Items *aItems;
	PackingItem *pItem;
	PackingList *pList;
	BriefcaseItem *bItem;
	RecordItem *rItem;
	TaskItem *tItem;
}
@property (nonatomic, retain) NSMutableString* mCurrentString;
@property (nonatomic, assign) BOOL successful;
@property (nonatomic, retain) NSMutableDictionary* data;
@property (nonatomic, retain) NSMutableDictionary* mCurItem;
@property (nonatomic, retain) NSMutableDictionary* mCurItemDetail;
@property (nonatomic, retain) NSMutableArray *mCurItemArray;
@property (nonatomic, retain) NSMutableArray *userDetails;
- (void)initWithDBName:(NSString *)DBName;
//- (void)initFromStr:(NSString *)str;
- (void)displayInfo;

@end

//
//  RootViewController.m
//  BabyPacking
//
//  Created by Jay Lee on 10/13/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import "Constant.h"
#import "TaskAlertViewController.h"
#import "AddViewController.h"
#import "CategoryViewController.h"
#import "BabyPackingAppDelegate.h"
#import "SearchResultParser.h"
#import "TaskItem.h"
#import "RecordItem.h"
#import "Utils.h"

UIView *blank_view;

typedef enum {
	TAG_TABBAR_UPCOMING = -3,
	TAG_TABBAR_PAST,
	TAG_TABBAR_HISTORY
} TabbarComponentTag;

typedef enum {
	TAG_CELL_ICON = -90,
	TAG_CELL_TITLE,
	TAG_CELL_BODY,
	TAG_CELL_DATETIME,
	TAG_CELL_REPEAT
} CellComponentTag;

typedef enum {
	STATE_UPCOMING = 0,
	STATE_PAST,
	STATE_RECENTS
} TabbarState;

@interface TaskAlertViewController(Private)

- (void)updateDataOnline;
- (void)processAlertTasks;
- (void)processAlertRecords;
- (void)reloadTableView;
- (void)prepareTasks;
- (void)prepareRecentRecords;
- (void)messageDidCome:(id)object;
@end

@implementation TaskAlertViewController

@synthesize lblTit,btnAddtask;

- (void)dealloc {
	[mDateFormatter release];
	[mBlankView release];
    [mBlankView_L release];
	[mAllTasksOnline release];
	[mAllRecordsOnline release];
	[mTabBar release];
	[mData release];
	[mTableView release];
	[mBtnCreate release];
    [super dealloc];
}

- (void)awakeFromNib {
	mUpdatingOnline = NO;
	mDateFormatter = [[NSDateFormatter alloc] init];
	[mDateFormatter setDateStyle:NSDateFormatterMediumStyle];
	[mDateFormatter setTimeStyle:kCFDateFormatterShortStyle];
	mData = [[NSMutableDictionary alloc] initWithCapacity:0];
	mAllTasksOnline = [[NSMutableArray alloc] initWithCapacity:0];
	mAllRecordsOnline = [[NSMutableArray alloc] initWithCapacity:0];
}
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        layoutManager = [[LayoutManager alloc] initWithPortraitView:self.view andLandscapeNIB:@"TaskAlertViewController_L"];
        [self awakeFromNib];
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //[self reloadTableView];
   

//    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
//    {
//        [layoutManager translateToPortraitForView:self.view withAnimation:NO];
//            [blank_view removeFromSuperview];
//            blank_view = mBlankView;
//            [self.view addSubview:blank_view];
//    }
//    else
//    {
//        [layoutManager translateToLandscapeForView:self.view withAnimation:NO];
//        [blank_view removeFromSuperview];
//        blank_view = mBlankView_L;
//        [self.view addSubview:blank_view];
//    }
    [blank_view removeFromSuperview];
    blank_view = mBlankView;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
        {
            blank_view.frame=CGRectMake(0, 0, 768, 975);
            mBlankView.frame=CGRectMake(0, 0, 768, 975);
        }
        else
        {
            blank_view.frame=CGRectMake(0, 0, 1100, 717);
            mBlankView.frame=CGRectMake(0, 0, 1100, 717);
        }

    }
    
    
    [self.view addSubview:blank_view];
    
    
    //Change By MS
    UITabBarItem *item = [mTabBar.items objectAtIndex:3];
	item.enabled = NO;
	item = [mTabBar.items objectAtIndex:0];
    
    mTabBar.backgroundImage=[UIImage imageNamed:@"normbot.png"];
    
    [self loadUpcomingData];
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    //  Observer to catch changes from iCloud
    NSUbiquitousKeyValueStore *store = [NSUbiquitousKeyValueStore defaultStore];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(storeDidChange:)
                                                 name:NSUbiquitousKeyValueStoreDidChangeExternallyNotification
                                               object:store];
    
    [[NSUbiquitousKeyValueStore defaultStore] synchronize];
    
    // Observer to catch the local changes
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didAddNewTask:)
                                                 name:@"New Note Task"
                                               object:nil];

    
    [super viewDidLoad];
	mAppDelegate = [UIApplication sharedApplication].delegate;
    localNotif = [[UILocalNotification alloc] init];
	// Add 'Back' navigation button
//	UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:nil action:nil];
//    self.navigationItem.backBarButtonItem = backButton;
//    [backButton release];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 30, 30);
    btn.tag=1;
    [btn setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(goBck:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
//    self.navigationItem.leftBarButtonItem = cancelButton;
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(0, 0, 30, 30);
    btn2.tag=1;
    [btn2 setImage:[UIImage imageNamed:@"AddTop.png"] forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(buttonCreateDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    mBtnCreate = [[UIBarButtonItem alloc] initWithCustomView:btn2];
    
	self.navigationItem.rightBarButtonItem = mBtnCreate;
	

    
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor whiteColor];
    
	NSString *customFontName = [mAppDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];		
	}
	else {
		label.font=[UIFont fontWithName:customFontName size:fontSize];		
	}
	
	[label sizeToFit];
	
	
//	label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
            label.textColor=[UIColor whiteColor];
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label.textColor = theColour1;
	}
	// and finally set the colour of your label
	
	self.navigationItem.titleView = label;	
	label.text = @"Upcoming Tasks";
	[label sizeToFit];
	
	
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour;
	// read the data back from the user defaults
	NSData *data1= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_BAR_COLOR];
	// check whether you got anything
	if(data1 == nil) {
		// use this to set the colour the first time your app runs
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
	} else {
		// this recreates the colour you saved
		theColour = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data1];
		self.navigationController.navigationBar.tintColor = theColour;
	}
	// and finally set the colour of your label
	
	
	UITabBarItem *item = [mTabBar.items objectAtIndex:3];
	item.enabled = NO;
	item = [mTabBar.items objectAtIndex:0];
	mTabBar.selectedItem = item;
	mTabBarState = STATE_UPCOMING;
    blank_view = mBlankView;
	blank_view.frame = mBlankView.frame;
	[self.view addSubview:blank_view];
	blank_view.hidden = YES;
	
	// Register message handler
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc addObserver:self selector:@selector(messageDidCome:) 
			   name:M_DATA_NEEDS_UPDATE
			 object:nil];
    
    //Change By MS
	//[self updateDataOnline];
    arrReminders = [[NSMutableArray alloc] init];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (IBAction)goBck:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// Remove message handler
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:M_DATA_NEEDS_UPDATE
												  object:nil];
}

#pragma mark -Change By MS

- (void)loadUpcomingData
{
    [arrReminders removeAllObjects];
    
    //Date Formatter
    NSDateFormatter    *entryDateFormat;
    
    //Now Date & Previous Date
    NSString                    *nowDate;
    NSNumber                *currentNowTimeStamp;
    
    entryDateFormat = [[NSDateFormatter alloc]init];
    [entryDateFormat setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    
    nowDate = [entryDateFormat stringFromDate:[NSDate date]];
    
    NSTimeInterval currentTimeStamp = [[NSDate date] timeIntervalSince1970];
    currentNowTimeStamp = [NSNumber numberWithDouble:currentTimeStamp];
    
    NSLog(@" Time :::: %@", currentNowTimeStamp);
    
    const char *dbpath = [[mAppDelegate getDBPath] UTF8String];
    
    if (sqlite3_open(dbpath, &dbFamilyPacking) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"Select * From tblReminder where StartDateTimeStamp > '%@'", currentNowTimeStamp];
        NSLog(@" ==> %@", querySQL);
        
        const char *sql = [querySQL UTF8String];
        
        sqlite3_stmt *searchStatement;
        
        if (sqlite3_prepare_v2(dbFamilyPacking, sql, -1, &searchStatement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(searchStatement) == SQLITE_ROW)
            {
                TaskItem* task = [[TaskItem alloc] init];
                
                // create task in database if not exists locally
                task.reminder_id = sqlite3_column_text(searchStatement, 0) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 0)] : @"";
                task.title = sqlite3_column_text(searchStatement, 1) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 1)] : @"";
                task.category = sqlite3_column_text(searchStatement, 4) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 4)] : @"";
                task.repeat = sqlite3_column_text(searchStatement, 5) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 5)] : @"";
                
                NSString *strInterval = sqlite3_column_text(searchStatement, 7) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 7)] : @"";
                task.interval = [strInterval intValue];
                
                NSString *strStartDate = sqlite3_column_text(searchStatement, 2) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 2)] : @"";
                task.start_date = [mDateFormatter dateFromString:strStartDate];
                
                NSString *strEndDate = sqlite3_column_text(searchStatement, 3) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 3)] : @"";
                task.end_date = [mDateFormatter dateFromString:strEndDate];
                
                task.text = sqlite3_column_text(searchStatement, 6) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 6)] : @"";
                
                [arrReminders addObject:task];
                [task release];
            }
        }
        sqlite3_finalize(searchStatement);
    }
    sqlite3_close(dbFamilyPacking);
    
    UITabBarItem* tabItem = [[mTabBar items] objectAtIndex:0];
	tabItem.badgeValue = [NSString stringWithFormat:@"%d", [arrReminders count]];
    
    [mTableView reloadData];
}

- (void)loadPastData
{
    [arrReminders removeAllObjects];
    
    //Date Formatter
    NSDateFormatter    *entryDateFormat;
    
    //Now Date & Previous Date
    NSString                    *nowDate;
    NSNumber                *currentNowTimeStamp;
    
    entryDateFormat = [[NSDateFormatter alloc]init];
    [entryDateFormat setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    
    nowDate = [entryDateFormat stringFromDate:[NSDate date]];
    
    NSTimeInterval currentTimeStamp = [[NSDate date] timeIntervalSince1970];
    currentNowTimeStamp = [NSNumber numberWithDouble:currentTimeStamp];
    
    NSLog(@" Time :::: %@", currentNowTimeStamp);
    
    const char *dbpath = [[mAppDelegate getDBPath] UTF8String];
    
    if (sqlite3_open(dbpath, &dbFamilyPacking) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"Select * From tblReminder where StartDateTimeStamp < '%@'", currentNowTimeStamp];
        NSLog(@" ==> %@", querySQL);
        
        const char *sql = [querySQL UTF8String];
        
        sqlite3_stmt *searchStatement;
        
        if (sqlite3_prepare_v2(dbFamilyPacking, sql, -1, &searchStatement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(searchStatement) == SQLITE_ROW)
            {
                TaskItem* task = [[TaskItem alloc] init];
                
                // create task in database if not exists locally
                task.reminder_id = sqlite3_column_text(searchStatement, 0) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 0)] : @"";
                task.title = sqlite3_column_text(searchStatement, 1) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 1)] : @"";
                task.category = sqlite3_column_text(searchStatement, 4) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 4)] : @"";
                task.repeat = sqlite3_column_text(searchStatement, 5) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 5)] : @"";
                
                NSString *strInterval = sqlite3_column_text(searchStatement, 7) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 7)] : @"";
                task.interval = [strInterval intValue];
                
                NSString *strStartDate = sqlite3_column_text(searchStatement, 2) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 2)] : @"";
                task.start_date = [mDateFormatter dateFromString:strStartDate];
                
                NSString *strEndDate = sqlite3_column_text(searchStatement, 3) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 3)] : @"";
                task.end_date = [mDateFormatter dateFromString:strEndDate];
                
                task.text = sqlite3_column_text(searchStatement, 6) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 6)] : @"";
                
                [arrReminders addObject:task];
                [task release];
            }
        }
        sqlite3_finalize(searchStatement);
    }
    sqlite3_close(dbFamilyPacking);
    
    UITabBarItem* tabItem = [[mTabBar items] objectAtIndex:1];
	tabItem.badgeValue = [NSString stringWithFormat:@"%d", [arrReminders count]];
    
    [mTableView reloadData];
}

- (IBAction)buttonAddNewTaskDidClick:(id)sender {
	AddViewController* controller = nil;
    
    mAppDelegate.globReminderId=@"";
  
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
     controller = [[AddViewController alloc] initWithNibName:@"AddViewController" bundle:nil];
     else
     controller = [[AddViewController alloc] initWithNibName:@"AddViewController1" bundle:nil];

	
	//[[AddViewController alloc] initWithNibName:@"AddViewController" bundle:nil];
	[self.navigationController pushViewController:controller animated:YES];
	[controller release];
}

- (IBAction)buttonCreateDidClick:(id)sender {
	AddViewController* controller = nil;
    
    mAppDelegate.globReminderId = @"";
	
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		controller = [[AddViewController alloc] initWithNibName:@"AddViewController" bundle:nil];
	else
		controller = [[AddViewController alloc] initWithNibName:@"AddViewController1" bundle:nil];
	
	//[[AddViewController alloc] initWithNibName:@"AddViewController" bundle:nil];
	[self.navigationController pushViewController:controller animated:YES];
	[controller release];
}

- (IBAction)buttonUpdateDidClick:(id)sender {
	[self updateDataOnline];
}

#pragma mark Private methods implementation

- (void)updateDataOnline {

	if (mUpdatingOnline) return;
	
//#if !TARGET_IPHONE_SIMULATOR
//	if ([@"Unknown" isEqualToString:mAppDelegate.deviceToken]) {
//		[self performSelector:@selector(updateDataOnline) withObject:nil afterDelay:3];
//		return;
//	}
//#endif
		
	[mAppDelegate startSynchronizing];
	mUpdatingOnline = YES;
	
	NSMutableDictionary* arguments = [NSMutableDictionary dictionaryWithCapacity:0];
	[arguments setObject:mAppDelegate.deviceToken forKey:@"remId"];
	RequestHandler *handler = [[RequestHandler alloc] init];
	handler.delegate = self;
	handler.method = @"GET";
	handler.url = API_LIST_TASK_URL;
	handler.requestArguments = arguments;
	handler.additionalArguments = nil;
	[handler startRequest];
	[handler release];
	
	NSDate* now = [NSDate date];
	arguments = [NSMutableDictionary dictionaryWithCapacity:0];
	[arguments setObject:mAppDelegate.deviceToken forKey:@"remId"];
	[arguments setObject:[[Utils getDateWithDays:-7 afterDate:now] description] forKey:@"sdate"];
	[arguments setObject:[now description] forKey:@"edate"];
	handler = [[RequestHandler alloc] init];
	handler.delegate = self;
	handler.method = @"POST";
	handler.url = API_SEARCH_RECORD_URL;
	handler.requestArguments = arguments;
	handler.additionalArguments = nil;
	[handler startRequest];
	[handler release];
}

- (void)processAlertTasks {
	for (NSDictionary* dict in mAllTasksOnline) {
		NSInteger remote_id = [[dict objectForKey:@"id"] intValue];
		TaskItem* task = [[TaskItem alloc] initWithRemoteId:remote_id];
		if (task.pk == INVALID_PK) {
			// create task in database if not exists locally
			task.remote_id = remote_id;
			task.title = [dict objectForKey:@"title"];
			task.category = [dict objectForKey:@"category"];
			task.repeat = [dict objectForKey:@"repeating"];
			task.interval = [[dict objectForKey:@"times"] intValue];
			if ([dict objectForKey:@"sdate"]) {
				task.start_date = [mDateFormatter dateFromString:[dict objectForKey:@"sdate"]];
			}
			if ([dict objectForKey:@"edate"]) {
				task.end_date = [mDateFormatter dateFromString:[dict objectForKey:@"edate"]];
			}
			task.text = [dict objectForKey:@"text"];
			task.device_token = [dict objectForKey:@"deviceToken"];
			[task saveData];
		}
		[task release];
	}
}

- (void)processAlertRecords {
	for (NSDictionary* dict in mAllRecordsOnline) {
		NSInteger remote_id = [[dict objectForKey:@"id"] intValue];
		RecordItem* record = [[RecordItem alloc] initWithRemoteId:remote_id];
		if (record.pk == INVALID_PK) {
			// create task in database if not exists locally
			record.remote_id = remote_id;
			record.task_id = [[dict objectForKey:@"task_id"] intValue];
			record.title = [dict objectForKey:@"title"];
			record.category = [dict objectForKey:@"category"];
			record.repeat = [dict objectForKey:@"repeating"];
			record.interval = [[dict objectForKey:@"times"] intValue];
			record.start_date = [mDateFormatter dateFromString:[dict objectForKey:@"sdate"]];
			record.end_date = [mDateFormatter dateFromString:[dict objectForKey:@"edate"]];
			record.text = [dict objectForKey:@"text"];
			record.device_token = [dict objectForKey:@"deviceToken"];
			[record saveData];
		}
		[record release];
	}
}

- (void)reloadTableView {
	
	// Reload data
	[self prepareTasks];
	[self prepareRecentRecords];
	NSArray* tabItems = [mTabBar items];
	UITabBarItem* tabItem = [tabItems objectAtIndex:0];
	NSArray* items = [mData objectForKey:@"upcoming"];
	tabItem.badgeValue = [NSString stringWithFormat:@"%d", [items count]];
	tabItem = [tabItems objectAtIndex:1];
	items = [mData objectForKey:@"past"];
	tabItem.badgeValue = [NSString stringWithFormat:@"%d", [items count]];
	
	[mTableView reloadData];
}

- (void)prepareTasks {
	
	NSMutableArray* upcomings = [NSMutableArray arrayWithCapacity:0];
	NSMutableArray* pasts = [NSMutableArray arrayWithCapacity:0];
	NSArray* items = [TaskItem findAll];
	for (TaskItem* item in items) {
		RecordItem* record = [RecordItem copyFromTaskItem:item];
		NSDate* now = [NSDate date];
		if ([item.end_date compare:now] != NSOrderedAscending) {
			// item.end_date is later than now
			if ([item.start_date compare:now] == NSOrderedAscending) {
				// task's start date is earlier than now
				if ([TYPE_REPEAT_NONE isEqualToString:item.repeat]) {
					[pasts addObject:record];
				} else {
					NSDate *new_start_date = [item getNextTaskTime];
					if ([new_start_date compare:item.end_date] != NSOrderedAscending) {
						// task's end date is earlier than next execution time
						// task is out-of-date
						[pasts addObject:record];
					} else {
						// task's end date is earlier than next execution time
						// task is out-of-date
						record.start_date = new_start_date;
						[upcomings addObject:record];
					}
				}
			} else {
				// task's start date is later than now
				// task is upcoming
				[upcomings addObject:record];
			}
		} else {
			// task's end date is earlier than now
			// task is out-of-date
			[pasts addObject:record];
		}
	}
	
	[mData setObject:upcomings forKey:@"upcoming"];
	[mData setObject:pasts forKey:@"past"];
	[UIApplication sharedApplication].applicationIconBadgeNumber = [upcomings count];
}

- (void)prepareRecentRecords {
	
	NSMutableArray* records = [NSMutableArray arrayWithCapacity:0];
	NSArray* items = [RecordItem findAll];
	[records addObjectsFromArray:items];
	
	[mData setObject:records forKey:@"recents"];
}

- (void)messageDidCome:(id)object {
	
	NSNotification* ntf = object;
	NSString* name = [ntf name];
	if ([M_DATA_NEEDS_UPDATE isEqualToString:name]) {
		[self updateDataOnline];
	}
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	
    //Change By dro
    if ([arrReminders count] == 0)
    {
        mBlankView.hidden = NO;
    }
    else
    {
        mBlankView.hidden = YES;
    }
    return [arrReminders count];
    
	NSArray* items = nil;
	if (mTabBarState == STATE_UPCOMING)
		items = [mData objectForKey:@"upcoming"];
	else if (mTabBarState == STATE_PAST)
		items = [mData objectForKey:@"past"];
	else if (mTabBarState == STATE_RECENTS)
		items = [mData objectForKey:@"recents"];
	if (!items || [items count] == 0) return 1;
	
    return [items count];
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
    //Change By MS
    return 1;
    
	NSArray* items = nil;
	if (mTabBarState == STATE_UPCOMING)
		items = [mData objectForKey:@"upcoming"];
	else if (mTabBarState == STATE_PAST)
		items = [mData objectForKey:@"past"];
	else if (mTabBarState == STATE_RECENTS)
		items = [mData objectForKey:@"recents"];
	
	// Show/Hide 'Add New Task' button and text
	mBlankView.hidden = YES;
	if (!items || [items count] <= 0) {
		if (mTabBarState == STATE_UPCOMING) {
			mBlankView.hidden = NO;
		} else {
			mBlankView.hidden = YES;
		}
		
		return 0;
	}
	
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	return mTplCell.frame.size.height;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"RootViewCell";
	UIImageView* iconView;
	UILabel* lbTitle;
	UILabel* lbBody;
	UILabel* lbDateTime;
	UILabel* lbRepeat;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
    
		iconView = [[UIImageView alloc] initWithFrame:mIconView.frame];
		iconView.tag = TAG_CELL_ICON;
		iconView.contentMode = mIconView.contentMode;
		[cell.contentView addSubview:iconView];
		[iconView release];
		lbTitle = [[UILabel alloc] initWithFrame:mTitleLabel.frame];
		lbTitle.tag = TAG_CELL_TITLE;
		lbTitle.backgroundColor = [UIColor clearColor];
//		lbTitle.font = mTitleLabel.font;
		
		NSString *customFontName = [mAppDelegate getCustomFontName];
		int fontSize = 18;
		if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
			fontSize -= 8;
		}
		lbTitle.font=[UIFont fontWithName:customFontName size:fontSize];		
		
		
		[cell.contentView addSubview:lbTitle];
		[lbTitle release];
		lbBody = [[UILabel alloc] initWithFrame:mBodyLabel.frame];
		lbBody.tag = TAG_CELL_BODY;
		lbBody.backgroundColor = [UIColor clearColor];
//		lbBody.font = mBodyLabel.font;
		lbBody.font=[UIFont fontWithName:customFontName size:fontSize];						
		[cell.contentView addSubview:lbBody];
		[lbBody release];
		lbDateTime = [[UILabel alloc] initWithFrame:mDateTimeLabel.frame];
		lbDateTime.tag = TAG_CELL_DATETIME;
		lbDateTime.backgroundColor = [UIColor clearColor];
//		lbDateTime.font = mDateTimeLabel.font;
		lbDateTime.font=[UIFont fontWithName:customFontName size:fontSize];				
		[cell.contentView addSubview:lbDateTime];
		[lbDateTime release];
		lbRepeat = [[UILabel alloc] initWithFrame:mRepeatLabel.frame];
		lbRepeat.tag = TAG_CELL_REPEAT;
		lbRepeat.backgroundColor = [UIColor clearColor];
//		lbRepeat.font = mRepeatLabel.font;
		lbRepeat.font=[UIFont fontWithName:customFontName size:fontSize];				
		[cell.contentView addSubview:lbRepeat];
		[lbRepeat release];
		
	} else {
		iconView = (id)[cell.contentView viewWithTag:TAG_CELL_ICON];
		lbTitle = (id)[cell.contentView viewWithTag:TAG_CELL_TITLE];
		lbBody = (id)[cell.contentView viewWithTag:TAG_CELL_BODY];
		lbDateTime = (id)[cell.contentView viewWithTag:TAG_CELL_DATETIME];
		lbRepeat = (id)[cell.contentView viewWithTag:TAG_CELL_REPEAT];
	}
	
    //Change By MS
    /*
	NSArray* items = nil;
	if (mTabBarState == STATE_UPCOMING) {
		items = [mData objectForKey:@"upcoming"];
		iconView.image = [UIImage imageNamed:@"tab_upcoming_tasks.png"];
	} else if (mTabBarState == STATE_PAST) {
		items = [mData objectForKey:@"past"];
		iconView.image = [UIImage imageNamed:@"tab_previous_tasks.png"];
	} else if (mTabBarState == STATE_RECENTS) {
		items = [mData objectForKey:@"recents"];
		iconView.image = [UIImage imageNamed:@"tab_recent_tasks.png"];
	}
	
	if (!items || [items count] <= 0) return cell;
	*/
    
	//RecordItem* record = [items objectAtIndex:indexPath.section];
    
    TaskItem *record = [arrReminders objectAtIndex:indexPath.section];
    
	/*
	if ([@"" isEqualToString:task.category]) {
		iconView.image = nil;
	} else {
		iconView.image = [UIImage imageNamed:[dict objectForKey:@"image"]];
	} 
	 */
	UIImage* image = [CategoryViewController imageForTitle:record.category];
	if (mTabBarState == STATE_UPCOMING) {
		if (!image) image = [UIImage imageNamed:@"tab_upcoming_tasks.png"];
	} else if (mTabBarState == STATE_PAST) {
		if (!image) image = [UIImage imageNamed:@"tab_previous_tasks.png"];
	} else if (mTabBarState == STATE_RECENTS) {
		if (!image) image = [UIImage imageNamed:@"tab_recent_tasks.png"];
	}
	NSString* text = [NSString stringWithFormat:@"Label: %@", record.category];
	NSString* time = [mDateFormatter stringFromDate:record.start_date];
	iconView.image = image;
	lbTitle.text = record.title;
    
    if ([record.category isEqualToString:@"Shop"])
    {
        iconView.image = [UIImage imageNamed:@"ShoppingForTask.png"];
    }
    else if ([record.category isEqualToString:@"Other"])
    {
        iconView.image = [UIImage imageNamed:@"OtherForTask.png"];
    }
    else
    {
        iconView.image = image;
    }

    
	lbBody.text = text;
	lbDateTime.text = time;
	lbRepeat.text = record.repeat;
	
	
	NSString *customFontName1 = [mAppDelegate getCustomFontName];
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
		cell.textLabel.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];			
		lbTitle.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];			
		lbBody.font = [UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];			
		lbDateTime.font = [UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];			
		lbRepeat.font = [UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];			
	}
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		cell.backgroundColor = theColour1;
	}
	// and finally set the colour of your label
    
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour21;
	// read the data back from the user defaults
	NSData *data211= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
	// check whether you got anything
	if(data211 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour21 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data211];
		cell.textLabel.textColor = theColour21;
		lbTitle.textColor = theColour21;
		lbBody.textColor = theColour21;
		lbDateTime.textColor = theColour21;
		lbRepeat.textColor = theColour21;
		
	}
	// and finally set the colour of your label
    
    [cell setSelectionStyle:UITableViewCellEditingStyleNone];
	
	return cell;
}


// Override to support row selection in the table view.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// Navigation logic may go here -- for example, create and push another view controller.
	AddViewController* controller = nil;
	
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		controller = [[AddViewController alloc] initWithNibName:@"AddViewController" bundle:nil];
	else
		controller = [[AddViewController alloc] initWithNibName:@"AddViewController1" bundle:nil];
	
	//[[AddViewController alloc] initWithNibName:@"AddViewController" bundle:nil];
	
     //Change By MS
    /*
	NSArray* items = nil;
	if (mTabBarState == STATE_UPCOMING)
		items = [mData objectForKey:@"upcoming"];
	else if (mTabBarState == STATE_PAST)
		items = [mData objectForKey:@"past"];
	else if (mTabBarState == STATE_RECENTS)
		items = [mData objectForKey:@"recents"];
	
	if (!items) {
		controller.remoteTaskID = 0;
	} else {
        
        RecordItem* record =  [items objectAtIndex:indexPath.section];
		controller.remoteTaskID = record.task_id;
		controller.currentRecord = record;
	}
    */
    
    TaskItem* task = [arrReminders objectAtIndex:indexPath.section];
    mAppDelegate.globReminderId = task.reminder_id;
	
	[self.navigationController pushViewController:controller animated:YES];
	[controller release];
}

- (void)tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    TaskItem* task = [arrReminders objectAtIndex:indexPath.row];
    
    const char *dbpath = [[mAppDelegate getDBPath] UTF8String];
    if (sqlite3_open(dbpath, &dbFamilyPacking) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"Delete From tblReminder where RemId = '%@'", task.reminder_id];
        NSLog(@" ==> %@", querySQL);
        
        const char *sql = [querySQL UTF8String];
        
        sqlite3_stmt *searchStatement;
        
        if (sqlite3_prepare_v2(dbFamilyPacking, sql, -1, &searchStatement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(searchStatement) == SQLITE_DONE)
            {
                NSLog(@"Deleted...");
            }
        }
        sqlite3_finalize(searchStatement);
    }
    sqlite3_close(dbFamilyPacking);
    
    [self btnUpload];
    
    UITabBarItem *item = [mTabBar.items objectAtIndex:3];
	item.enabled = NO;
	item = [mTabBar.items objectAtIndex:0];
    
    [self loadUpcomingData];
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source.
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
 }   
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor whiteColor];
    
	NSString *customFontName = [mAppDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];		
	}
	else {
		label.font=[UIFont fontWithName:customFontName size:fontSize];		
	}
	
	[label sizeToFit];
	
	
//	label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
            label.textColor=[UIColor whiteColor];
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label.textColor = theColour1;
	}
	// and finally set the colour of your label
	
	self.navigationItem.titleView = label;	
	
	if (item.tag == TAG_TABBAR_UPCOMING) {
		mTabBarState = STATE_UPCOMING;
		label.text = @"Upcoming Tasks";
		[label sizeToFit];
        
        //Change By MS
		//[self reloadTableView];
        [self loadUpcomingData];
        
	} else if (item.tag == TAG_TABBAR_PAST) {
		mTabBarState = STATE_PAST;
		label.text = @"Previous Tasks";
		[label sizeToFit];
        
        //Change By MS
		//[self reloadTableView];
        [self loadPastData];
        
	} else if (item.tag == TAG_TABBAR_HISTORY) {
		mTabBarState = STATE_RECENTS;
		label.text = @"One Week History";
		[label sizeToFit];
        
        //Change By MS
		//[self reloadTableView];
	}
}

#pragma mark -
#pragma mark <RequestHandlerDelegate> implementation

- (void)request:(RequestHandler *)request responseCompleteWithData:(NSData *)xmldata {
	
	NSLog(@">>>>>>>>>>>>>> responseCompleteWithData begin");
	if ([API_LIST_TASK_URL isEqualToString:request.url]) {
		
		SearchResultParser* handler = [[SearchResultParser alloc] initWithXMLData:xmldata];
		if (!handler || !handler.successful) {
			
			[mAppDelegate stopSynchronizing];
			mUpdatingOnline = NO;
			
			// Presend alert view
			UIAlertView* alert = [[UIAlertView alloc] initWithTitle:M_ERROR message: M_ERROR_NO_CONNECTION
														   delegate:self cancelButtonTitle:I_OK 
												  otherButtonTitles:nil];
			[alert show];
			[alert release];
			
			return;
		} else {
			
			[mAllTasksOnline removeAllObjects];
			[mAllTasksOnline addObjectsFromArray:[handler.data objectForKey:@"Values"]];
			[handler release];
			
			[self processAlertTasks];
			[self reloadTableView];
			[mAppDelegate stopSynchronizing];
			mUpdatingOnline = NO;
		}
	} else if ([API_SEARCH_RECORD_URL isEqualToString:request.url]) {
		SearchResultParser* handler = [[SearchResultParser alloc] initWithXMLData:xmldata];
		if (!handler || !handler.successful) {
			
			[mAppDelegate stopSynchronizing];
			mUpdatingOnline = NO;
			
			// Presend alert view
			UIAlertView* alert = [[UIAlertView alloc] initWithTitle:M_ERROR message: M_ERROR_NO_CONNECTION
														   delegate:self cancelButtonTitle:I_OK 
												  otherButtonTitles:nil];
			[alert show];
			[alert release];
			
			return;
		} else {
			
			[mAllRecordsOnline removeAllObjects];
			[mAllRecordsOnline addObjectsFromArray:[handler.data objectForKey:@"Values"]];
			[handler release];
			
			[self processAlertRecords];
			[self reloadTableView];
			[mAppDelegate stopSynchronizing];
			mUpdatingOnline = NO;
		}
	} else {
		
	}
	
	NSLog(@"responseCompleteWithData end <<<<<<<<<<<<<<<<");
}

- (void)request:(RequestHandler *)request responseFailedWithError:(NSError *)error {
	
	[mAppDelegate stopSynchronizing];
	mUpdatingOnline = NO;
	
	NSLog(@">>>>>>>>>>>>>> responseFailedWithError begin");
	if ([API_LIST_TASK_URL isEqualToString:request.url]) {		
		// Presend alert view
		UIAlertView* alert = [[UIAlertView alloc] initWithTitle:M_ERROR message: M_ERROR_NO_CONNECTION
													   delegate:self cancelButtonTitle:I_OK 
											  otherButtonTitles:nil];
		[alert show];
		[alert release];
	} else {
		
	}
	
	NSLog(@"responseFailedWithError end <<<<<<<<<<<<<<<<");
}

/*
#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
         [layoutManager translateToPortraitForView:self.view withAnimation:YES];
        
            if (blank_view ==mBlankView_L )
            {
               
                [blank_view removeFromSuperview];
                blank_view = mBlankView;
               [self.view addSubview:blank_view];
            }
        }
    
    else
    {
        [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
        
        if (blank_view ==mBlankView )
        {
            [blank_view removeFromSuperview];
            blank_view = mBlankView_L;
            [self.view addSubview:blank_view];
            
            
            
        }

    }
    // }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}*/

#pragma mark - upload in iCloud

-(void)btnUpload
{
    // Create XML and pass to iCloud
    NSString *xmlStr=@"<Family>";
    NSString *select_query;
    const char *select_stmt;
    sqlite3_stmt *compiled_stmt;
    sqlite3 *dbFamilyPacking;
    if (sqlite3_open([[mAppDelegate getDBPath] UTF8String], &dbFamilyPacking) == SQLITE_OK)
    {
        select_query = [NSString stringWithFormat:@"SELECT * FROM tblReminder"];
        select_stmt = [select_query UTF8String];
        if(sqlite3_prepare_v2(dbFamilyPacking, select_stmt, -1, &compiled_stmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiled_stmt) == SQLITE_ROW)
            {
                NSString *addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,0)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<Data><RemId>%@</RemId>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,1)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<RemName>%@</RemName>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,2)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<RemStartDate>%@</RemStartDate>",addr];
                
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,3)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<RemEndDate>%@</RemEndDate>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,4)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<RemCategory>%@</RemCategory>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,5)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<RemRepeat>%@</RemRepeat>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,6)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<RemNotes>%@</RemNotes>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,7)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<RemInterval>%@</RemInterval>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,8)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<RemMessage>%@</RemMessage>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,9)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<StartDateTimeStamp>%@</StartDateTimeStamp>",addr];
                
                
                xmlStr =[xmlStr stringByAppendingString:@"</Data>"];
            }
            
            xmlStr =[xmlStr stringByAppendingString:@"</Family>"];
            
            sqlite3_finalize(compiled_stmt);
            
            mAppDelegate.notesTask = xmlStr;
            // Notify the previouse view to save the changes locally
            [[NSNotificationCenter defaultCenter] postNotificationName:@"New Note Task" object:self userInfo:[NSDictionary dictionaryWithObject:xmlStr forKey:@"NoteTask"]];
        }
        else
        {
            NSLog(@"Error while creating detail view statement. '%s'", sqlite3_errmsg(dbFamilyPacking));
        }
        
        NSLog(@"Generated XML : %@",xmlStr);
    }
}


#pragma mark - functions for iCloud

- (NSString *)notesList
{
    if (mAppDelegate.notesTask) {
        return mAppDelegate.notesTask;
    }
    
    mAppDelegate.notesList = [[[NSUbiquitousKeyValueStore defaultStore] stringForKey:@"AVAILABLE_NOTES_Task"] mutableCopy];
    if (!mAppDelegate.notesTask) mAppDelegate.notesTask = @"";
    
    return mAppDelegate.notesTask;
}

#pragma mark - Observer New Note

- (void)didAddNewTask:(NSNotification *)notification
{
    // Update data on the iCloud
    [[NSUbiquitousKeyValueStore defaultStore] setString:mAppDelegate.notesTask forKey:@"AVAILABLE_NOTES_Task"];

}

#pragma mark - Observer

- (void)storeDidChange:(NSNotification *)notification
{
    NSLog(@"Changed");
    
    // Retrieve the changes from iCloud
    mAppDelegate.notesTask = [[[NSUbiquitousKeyValueStore defaultStore] stringForKey:@"AVAILABLE_NOTES_Task"] mutableCopy];

    
    [self downloadTasks:mAppDelegate.notesTask];
    
}

-(void)downloadTasks:(NSString *)xmlFile
{
    // Code for delete all existing records
    NSString *select_query;
    sqlite3 *database;
    if(sqlite3_open([[mAppDelegate getDBPath] UTF8String], &database) == SQLITE_OK)
    {
        select_query = [NSString stringWithFormat:@"delete from tblReminder"];
        const char *sqlStatementShirts = [select_query UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatementShirts, -1, &compiledStatement, NULL) != SQLITE_OK)
        {
            NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
        else
        {
            sqlite3_step(compiledStatement);
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
    //delete all existing reminders
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    

        NSArray *arr=[xmlFile componentsSeparatedByString:@"<Data>"];
        if ([arr count]>1)
        {
            for(int i=1;i<[arr count];i++)
            {
                NSString *str=[arr objectAtIndex:i];
                NSArray *arr1=[str componentsSeparatedByString:@"<RemId>"];
                NSString *data=[arr1 objectAtIndex:1];
                NSRange ranfrom=[data rangeOfString:@"</RemId>"];
                
                arr1=[str componentsSeparatedByString:@"<RemName>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</RemName>"];
                NSString *val1=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<RemStartDate>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</RemStartDate>"];
                NSString *val2=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<RemEndDate>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</RemEndDate>"];
                NSString *val3=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<RemCategory>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</RemCategory>"];
                NSString *val4=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<RemRepeat>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</RemRepeat>"];
                NSString *val5=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<RemNotes>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</RemNotes>"];
                NSString *val6=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<RemInterval>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</RemInterval>"];
                NSString *val7=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<RemMessage>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</RemMessage>"];
                NSString *val8=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<StartDateTimeStamp>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</StartDateTimeStamp>"];
                NSString *val9=[data substringToIndex:ranfrom.location];
                
                sqlite3 *database;
                if(sqlite3_open([[mAppDelegate getDBPath] UTF8String], &database) == SQLITE_OK)
                {
                    select_query = [NSString stringWithFormat:@"INSERT INTO tblReminder (RemName,RemStartDate,RemEndDate,RemCategory,RemRepeat,RemNotes,RemInterval,RemMessage,StartDateTimeStamp) VALUES(\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",val1,val2,val3,val4,val5,val6,val7,val8,val9];
                    const char *sqlStatementShirts = [select_query UTF8String];
                    sqlite3_stmt *compiledStatement;
                    if(sqlite3_prepare_v2(database, sqlStatementShirts, -1, &compiledStatement, NULL) != SQLITE_OK)
                    {
                        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                    }
                    else
                    {
                        sqlite3_step(compiledStatement);
                    }
                    sqlite3_finalize(compiledStatement);
                }
                sqlite3_close(database);
                
                
                
                NSDateFormatter *dFormat=[[NSDateFormatter alloc] init];
                NSDate *sdd=[dFormat dateFromString:[NSString stringWithFormat:@"%@",val2]];
                NSDate *edd=[dFormat dateFromString:[NSString stringWithFormat:@"%@",val3]];
                //set reminders
                NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
                // Break the date up into components
                NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
                                                               fromDate:sdd];
                NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit )
                                                               fromDate:sdd];
                
                // Set up the fire time
                NSDateComponents *dateComps = [[NSDateComponents alloc] init];
                [dateComps setDay:[dateComponents day]];
                [dateComps setMonth:[dateComponents month]];
                [dateComps setYear:[dateComponents year]];
                [dateComps setHour:[timeComponents hour]];
                // Notification will fire in one minute
                [dateComps setMinute:[timeComponents minute]];
                [dateComps setSecond:[timeComponents second]];
                NSDate *itemDate = [calendar dateFromComponents:dateComps];
                [dateComps release];
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
                NSString *docDirectory = [paths objectAtIndex:0];
                
                if (localNotif == nil)
                    return;
                
                localNotif.fireDate = itemDate;
                localNotif.timeZone = [NSTimeZone defaultTimeZone];
                localNotif.alertBody = [NSString stringWithFormat:@"%@ ", val1];
                // Set the action button
                localNotif.alertAction = @"View";
               // localNotif.soundName = K_SET_TASK_SOUND;
                NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
                localNotif.soundName =  [userSettings objectForKey:K_SET_TASK_SOUND];
                NSLog(@"Notification Date : %@", [userSettings objectForKey:K_SET_TASK_SOUND]);
                
                localNotif.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                // Schedule the notification
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
                
            }
        }
    
    [self loadUpcomingData];
    [self loadPastData];
}


#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{            return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
            toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        {
            

        }
        else
        {
    
        }
    }
    else
    {
        if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
            toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        {
            blank_view.frame=CGRectMake(0, 0, 768, 720);
            mBlankView.frame=CGRectMake(0, 0, 768, 720);
//            lblTit.frame=CGRectMake(lblTit.frame.origin.x-300, lblTit.frame.origin.y, lblTit.frame.size.width, lblTit.frame.size.height);
//             btnAddtask.frame=CGRectMake(btnAddtask.frame.origin.x-300, btnAddtask.frame.origin.y, btnAddtask.frame.size.width, btnAddtask.frame.size.height);

        }
        else
        {
             blank_view.frame=CGRectMake(0, 0, 1100, 975);
             mBlankView.frame=CGRectMake(0, 0, 1100, 975);
//            lblTit.frame=CGRectMake(lblTit.frame.origin.x+300, lblTit.frame.origin.y, lblTit.frame.size.width, lblTit.frame.size.height);
//            btnAddtask.frame=CGRectMake(btnAddtask.frame.origin.x+300, btnAddtask.frame.origin.y, btnAddtask.frame.size.width, btnAddtask.frame.size.height);
        }
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}


@end

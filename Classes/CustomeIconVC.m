//
//  CustomeIconVC.m
//  BabyPacking
//
//  Created by Kirti Avaiya on 05/09/13.
//
//

#import "CustomeIconVC.h"
#import "AddListViewController.h"
#import "BabyPackingAppDelegate.h"

@interface CustomeIconVC ()

@end

@implementation CustomeIconVC
@synthesize tblFill,imgArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    self.tblFill = @[@"baby_shower", @"baby_sitter_checklist", @"baby_travel", @"boating_trip", @"camping_trip", @"child_birthday", @"cleaning",@"diet",@"event_planner",@"family_travel",@"fishing_trip",@"grocery",@"grownup_birthday",@"holiday_party",@"home_buying",@"home_rental_walkthrough",@"hunting_trip",@"moving_to_do",@"newyear_party",@"party",@"personal_travel",@"pregnancy",@"rv",@"user_car_inspection",@"wedding_planning"];
//    self.imgArray = [NSArray arrayWithObjects:@"0.png", @"1.png", @"2.png",@"3.png", @"4.png", @"5.png",@"6.png", nil];
    imgArray = [[NSMutableArray alloc] init];
    [imgArray addObject:@"baby_shower.png"];
    [imgArray addObject:@"baby_sitter_checklist.png"];
    [imgArray addObject:@"baby_travel.png"];
    [imgArray addObject:@"boating_trip.png"];
    [imgArray addObject:@"camping_trip.png"];
    [imgArray addObject:@"child_birthday.png"];
    [imgArray addObject:@"cleaning.png"];
     [imgArray addObject:@"diet.png"];
     [imgArray addObject:@"event_planner.png"];
     [imgArray addObject:@"family_travel.png"];
     [imgArray addObject:@"fishing_trip.png"];
     [imgArray addObject:@"grocery.png"];
     [imgArray addObject:@"grownup_birthday.png"];
     [imgArray addObject:@"holiday_party.png"];
     [imgArray addObject:@"home_buying.png"];
     [imgArray addObject:@"home_rental_walkthrough.png"];
     [imgArray addObject:@"hunting_trip.png"];
     [imgArray addObject:@"moving_to_do.png"];
     [imgArray addObject:@"newyear_party.png"];
     [imgArray addObject:@"party.png"];
     [imgArray addObject:@"personal_travel.png"];
     [imgArray addObject:@"pregnancy.png"];
     [imgArray addObject:@"rv.png"];
     [imgArray addObject:@"user_car_inspection.png"];
     [imgArray addObject:@"wedding_planning.png"];


    // Do any additional setup after loading the view from its nib.
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 30, 30);
    btn.tag=1;
    [btn setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *doneButton=[[UIBarButtonItem alloc] initWithCustomView:btn];
	self.navigationItem.leftBarButtonItem = doneButton;
}

- (IBAction) buttonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	NSString *title = nil;
	
	title = [NSString stringWithFormat:@"Custom Icon "];
    
	return title;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
   // NSInteger numberOfRows = [self.tblFill count];
    return [self.tblFill count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	return 60;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell Identifier";
    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: CellIdentifier] autorelease];
    }
    cell.imageView.image=nil;
        NSString *fruit = [self.tblFill objectAtIndex:[indexPath row]];
        NSLog(@"Image Id %d.png",indexPath.row);
   
    NSLog(@"This is image array log %@",[self.imgArray objectAtIndex:indexPath.row]);
    cell.imageView.image = [UIImage imageNamed:[self.imgArray objectAtIndex:indexPath.row]];

    
      // cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d.png",indexPath.row]];
        [cell.textLabel setText:fruit];
	cell.textLabel.hidden = TRUE;
    // Fetch Fruit
   
    
    return cell;
  
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    AddListViewController *addControllerr = [[AddListViewController alloc]initWithNibName:@"AddListViewController1" bundle:nil];
//    addControllerr.imageName = [[NSString alloc]initWithString:[NSString stringWithFormat:@"%d.png",indexPath.row]];
    //CustomeIconVC *controller = [[CustomeIconVC alloc] initWithNibName:@"CustomeIconVC" bundle:nil];
  //  NSLog(@"This is image name %@ ",addControllerr.imageName);
    BabyPackingAppDelegate *app =(BabyPackingAppDelegate *) [[UIApplication sharedApplication] delegate];

    
    app.ImageName = [[NSString alloc]initWithString:[NSString stringWithFormat:@"%@",[imgArray objectAtIndex:indexPath.row]]];
 
    [self.navigationController popViewControllerAnimated:YES];

  //  [addControllerr release];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

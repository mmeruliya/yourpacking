//
//  GetImageParser.m
//  packngo
//
//  Created by Mehul Bhuva on 07/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GetImageParser.h"

@implementation GetImageParser

@synthesize successful = mSuccessful;
@synthesize data = mData;
@synthesize mCurItem,mCurItemArray,userDetails,mCurrentString,image_response_data;

- (void)dealloc
{
	//[mArray release];
	[mDelegate release];
	[image_response_data release];
	[mCurItem release];
	[mData release];
	[super dealloc];
}



- (void)beforeParsing
{
	mSuccessful = NO;
	self.data = [NSMutableDictionary dictionaryWithCapacity:0];
	[mData setObject:[NSMutableArray arrayWithCapacity:0] forKey:@"Values"];
}

- (NSData *)initWithData:(NSString *)listid withUserId:(NSString *)userid
{	
	
	image_response_data=[[NSData alloc] init];
	storingCharacters=NO;
	mCurrentString=[[NSMutableString alloc] initWithString:@""];
	NSLog(@"initGetImageParser WITH URL");
	mCurItem = [[NSMutableDictionary alloc] initWithCapacity:0];
	mCurItemArray = [[NSMutableArray alloc] initWithCapacity:0];
	self.userDetails=[[NSMutableArray alloc] initWithCapacity:0];
	//	NSLog(@"URL STRING=%@",str);
	
	
	//##############################################	
	//http://madhukantpatel.com/beacons/ws/user_registration2.php?reachoutid=%d&name=%@&age=%@&gender=Male&marritalstatus=UnMarried&latitude=12.23456&longitude=23.34556&image_url=%s",appDelegate.currentReachoutId,nameTxt.text,ageTxt.text,[data bytes]]	
//	NSString *urlString = @"http://192.168.0.26/packngohelp/ws/retrieve_images.php";

	NSString *urlString = @"http://babypackandgo.com/packngohelp/ws/retrieve_images.php";
	// setting up the request object now
	
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
	[request setURL:[NSURL URLWithString:urlString]];
	[request setHTTPMethod:@"POST"];
    
	/*
	 add some header info now
	 we always need a boundary when we post a file
	 also we need to set the content type
	 
	 You might want to generate a random boundary.. this is just the same 
	 as my output from wireshark on a valid html post
	 */
	NSString *boundary = [NSString stringWithString:@"---------------------------14737809831466499882746641449"];
	NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
	[request addValue:contentType forHTTPHeaderField: @"Content-Type"];
	
	/*
	 now lets create the body of the post
	 */
    NSLog(@"%@",listid);
    NSLog(@"%@",userid);
	NSMutableData *postBody = [NSMutableData data];
	
	[postBody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"userId\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[[NSString stringWithFormat:@"%@",userid] dataUsingEncoding:NSUTF8StringEncoding]]; 	
	
	[postBody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"listId\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[[NSString stringWithString:listid] dataUsingEncoding:NSUTF8StringEncoding]];  
	
	// setting the body of the post to the reqeust
	[request setHTTPBody:postBody];
	
	// now lets make the connection to the web
	
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
	NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
	NSLog(@"DATA = %@",returnString);
	//##############################################		
	
	
	
	
	
	
	
	
	
	//	NSURL *url=[[NSURL alloc] initWithString:str];
		NSXMLParser *parser = [[NSXMLParser alloc] initWithData:returnData];
	//	NSString *dstr=[[NSString alloc] initWithContentsOfURL:url];
	//	NSLog(@"Response == %@", dstr);
		[parser setDelegate:self];
		[parser parse];
		[parser release];
	//	[url release];
	
	/*if (![self parseXml]) {
	 return nil;
	 }*/	
	//	[self displayInfo];
	//	if (appDelegate.nearUserDetails!=nil) {
	//		[appDelegate.nearUserDetails removeAllObjects];
	//		appDelegate.nearUserDetails=nil;
	//		[appDelegate release];
	//		appDelegate.nearUserDetails=[[NSMutableArray alloc] init];		
	//	}
	//	appDelegate.nearUserDetails=self.userDetails;
	
	return image_response_data;
}

- (void) displayInfo{
	NSLog(@"initDIsplay Info");
	
	
	//		NSLog(@"ID=%@",[mCurItem objectForKey:@"id"]);
	NSLog(@"NAME=%@",[mCurItem objectForKey:@"status"]);	
	NSLog(@"NAME=%@",[mCurItem objectForKey:@"name"]);
	NSLog(@"REACHOUT ID=%@",[mCurItem objectForKey:@"reachoutid"]);
	NSLog(@"GENDER=%@",[mCurItem objectForKey:@"gender"]);
	NSLog(@"AGE=%@",[mCurItem objectForKey:@"age"]);
	NSLog(@"IMAGE URL=%@",[mCurItem objectForKey:@"imageurl"]);
	//		NSLog(@"RATING=%@",[dict1 objectForKey:@"rating"]);
	NSLog(@"MSTATUS=%@",[mCurItem objectForKey:@"marritalstatus"]);
	NSLog(@"LATITUDE=%@",[mCurItem objectForKey:@"latitude"]);
	NSLog(@"LONGITUDE=%@",[mCurItem objectForKey:@"longitude"]);
	//		NSLog(@"DISTANCE=%@",[mCurItem objectForKey:@"distance"]);
	
}

- (void)parser:(NSXMLParser*)parser didStartElement:(NSString*)elementName namespaceURI:(NSString*)namespaceURI qualifiedName:(NSString*)qualifiedName attributes:(NSDictionary*)attributes
{
	if ([elementName isEqualToString:@"response"]) {
		mCurItem = [[NSMutableDictionary alloc] initWithCapacity:0];
	}
	else if ([elementName isEqualToString:@"status"])
	{
		[mCurrentString setString:@""];
		storingCharacters = YES;		
	}		
//	else if ([elementName isEqualToString:@"reachoutid"])
//	{
//		[mCurrentString setString:@""];
//		storingCharacters = YES;
//	}
	else if ([elementName isEqualToString:@"imageurl"])
	{
		[mCurrentString setString:@""];
		storingCharacters = YES;
	}
//	else if ([elementName isEqualToString:@"name"])
//	{
//		[mCurrentString setString:@""];
//		storingCharacters = YES;
//	}
//	else if ([elementName isEqualToString:@"age"])
//	{
//		[mCurrentString setString:@""];
//		storingCharacters = YES;
//	}
//	else if ([elementName isEqualToString:@"gender"])
//	{
//		[mCurrentString setString:@""];
//		storingCharacters = YES;
//	}
//	else if ([elementName isEqualToString:@"marritalstatus"])
//	{
//		[mCurrentString setString:@""];
//		storingCharacters = YES;
//	}
//	else if ([elementName isEqualToString:@"latitude"])
//	{
//		[mCurrentString setString:@""];
//		storingCharacters = YES;
//	}
//	else if ([elementName isEqualToString:@"longitude"])
//	{
//		[mCurrentString setString:@""];
//		storingCharacters = YES;
//	}
	
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
	//	NSLog(@"DID END");
	if ([elementName isEqualToString:@"imageurl"])
	{
		NSString *urlString = [mCurrentString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
		NSData* imageData1 = [[NSData alloc]initWithContentsOfURL:[NSURL URLWithString:urlString]];
		image_response_data=imageData1;
	}
//	else if ([elementName isEqualToString:@"name"])
//	{
//		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"name"];
//	}
//	else if ([elementName isEqualToString:@"age"])
//	{
//		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"age"];
//	}
//	else if ([elementName isEqualToString:@"gender"])
//	{
//		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"gender"];
//	}
//	else if ([elementName isEqualToString:@"marritalstatus"])
//	{
//		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"marritalstatus"];
//	}
//	else if ([elementName isEqualToString:@"latitude"])
//	{
//		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"latitude"];
//	}
//	else if ([elementName isEqualToString:@"longitude"])
//	{
//		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"longitutde"];
//	}
//	else if ([elementName isEqualToString:@"status"])
//	{
//		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"status"];
//	}	
//	else if ([elementName isEqualToString:@"response"])
//	{
//		//self.userDetails=mCurItemArray;
//		//		[mCurItem removeAllObjects];
//	}
	
	storingCharacters=NO;
}

- (void)parser:(NSXMLParser*)parser foundCharacters:(NSString*)string
{
	if (storingCharacters) {
		[mCurrentString appendString:string];		
	}
	
}

- (void) parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
	NSLog(@"GetImageParser Error : %@",[parseError localizedDescription]);
}


//- (void)parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock
//{
//    NSString *someString = [[NSString alloc] initWithData:CDATABlock encoding:NSUTF8StringEncoding];
//	[mCurItem setObject:someString forKey:@"message"];
//}
@end
//
//  List.m
//  BabyPacking
//
//  Created by Gary He on 5/31/09.
//  Copyright 2009 test. All rights reserved.
//

#import "PackingList.h"
#import "PackingItem.h"
#import "BabyPackingAppDelegate.h"

static sqlite3_stmt *select_photo_statement = nil;
static sqlite3_stmt *select_all_without_photo_statement = nil;
static sqlite3_stmt *upgrade_check_table_statement = nil;
static sqlite3_stmt *upgrade_select_statement = nil;
static sqlite3_stmt *upgrade_insert_statement = nil;
static sqlite3_stmt *insert_statement = nil;
static sqlite3_stmt *init_statement = nil;
static sqlite3_stmt *delete_statement = nil;
static sqlite3_stmt *update_statement = nil;
static sqlite3_stmt *update_without_blob_statement = nil;
static sqlite3_stmt *hydrate_statement = nil;
static sqlite3_stmt *dehydrate_statement = nil;

@interface PackingList(Private)

- (void) deleteItemsFromDB;
- (void) loadItemsFromDB;

@end


@implementation PackingList
@synthesize name;
@synthesize sex;
@synthesize age;
@synthesize icon;
@synthesize photo;
@synthesize update;
@synthesize totalCount;
@synthesize checkedCount;
@synthesize predefineId;
@synthesize predefineForAge;
@synthesize itemArray;
@synthesize custom;
@synthesize lastUpdate;

- (id) init {
	if (self = [super init]) {
		self.name = @"";
		self.icon = nil;
		self.photo = nil;
		self.update = [NSDate date];
		self.itemArray = [NSMutableArray arrayWithCapacity:16];
		self.sex = BabySexNone;
		if (TARGET_VERSION == VERSION_BABY_PACKING)
			self.age = Baby;
		else if (TARGET_VERSION == VERSION_FAMILY_PACKING)
			self.age = Infant_baby;
		else if (TARGET_VERSION == VERSION_YOU_PACKING)
			self.age = Adult;
		self.totalCount = 0;
		self.checkedCount = 0;
		self.lastUpdate = 0.0;
		self.predefineId = 0;
		self.pk = INVALID_PK;
	}
	return self;
}

- (void)dealloc {
	[name release];
	[icon release];
	[update release];
	[itemArray release];
	[custom release];
    [super dealloc];
}

- (BOOL) editable {
	return (pk != INVALID_PK);
}

- (void) updateCheckStatus {
	// fix a bug here, must use self, before totalCount and checkedCount
	// so that the setXXX is called
	NSLog(@"ITEM ARRAY COUNT = %d",[itemArray count]);
	self.totalCount = [itemArray count];
	int tempCheckedCount = 0;
	for(PackingItem *item in itemArray) {
		if ([item isChecked]) {
			tempCheckedCount++;
		}
	}
	self.checkedCount = tempCheckedCount;
}


- (id)initWithPK:(NSInteger)primaryKey {
    
    
	NSLog(@"PackingList initWithPK pk = %d",primaryKey);
	if (self = [self init]) {
		pk = primaryKey;
		sqlite3 *database = conn.database;
		// Compile the query for retrieving book data. See insertNewBookIntoDatabase: for more detail.
        if (init_statement == nil) {
            // Note the '?' at the end of the query. This is a parameter which can be replaced by a bound variable.
            // This is a great way to optimize because frequently used queries can be compiled once, then with each
            // use new variable values can be bound to placeholders.
            const char *sql = "SELECT pk,name,sex,age,smallIcon,lastUpdate, totalCount, checkedCount, predefineId, predefineForAge FROM List WHERE pk=?";
            if (sqlite3_prepare_v2(database, sql, -1, &init_statement, NULL) != SQLITE_OK) {
                NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            }
        }
		// For this query, we bind the primary key to the first (and only) placeholder in the statement.
        // Note that the parameters are numbered from 1, not from 0.
        sqlite3_bind_int(init_statement, 1, pk);
		if (sqlite3_step(init_statement) == SQLITE_ROW) {
			int field = 1;
			char * val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.name = [NSString stringWithUTF8String:val];
			} else {
				self.name = @"";
			}
			
			self.sex = sqlite3_column_int(init_statement, field++);
			self.age = sqlite3_column_int(init_statement, field++);
			
			int length = sqlite3_column_bytes(init_statement, field);
			NSLog(@"read length = %d", length);
			void *bytes = (void* )sqlite3_column_blob(init_statement, field++);
			NSData *iconData = [NSData dataWithBytes:bytes length:length];
			self.icon = [UIImage imageWithData: iconData];
			
			self.update = [NSDate dateWithTimeIntervalSince1970: sqlite3_column_double(init_statement, field++)];
			self.lastUpdate = sqlite3_column_double(init_statement, field);
			self.totalCount = sqlite3_column_int(init_statement, field++);
			self.checkedCount = sqlite3_column_int(init_statement, field++);
			self.predefineId = sqlite3_column_int(init_statement, field++);
			self.predefineForAge = sqlite3_column_int(init_statement, field++);
		}
		// Reset the statement for future reuse.
        sqlite3_reset(init_statement);
        dirty = NO;
		hydrated = YES;
	}
	return self;
}
- (void)insertData {
	
	if (!dirty) return;
	NSLog(@"PackingList insertData pk = %d dirty = %d", pk, dirty);
	//if (pk == INVALID_PK) return;
    sqlite3 *database = conn.database;
    if (insert_statement == nil) {
        const char *sql = "INSERT INTO list (name,sex,age,smallIcon,photo,lastUpdate,totalCount,checkedCount,predefineId,predefineForAge) VALUES(?,?,?,?,?,?,?,?,?,?)";
        if (sqlite3_prepare_v2(database, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
	if (name == nil) self.name = @"";
	int field = 1;
    sqlite3_bind_text(insert_statement, field++, [name UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(insert_statement, field++, sex);
	sqlite3_bind_int(insert_statement, field++, age);
	if (icon == nil) {
		NSLog(@"NIL ICON");
		sqlite3_bind_zeroblob(insert_statement,field++,0);
	} else {
		NSLog(@"NOT NIL ICON");
		NSData *imageData = UIImagePNGRepresentation(icon);
		void *bytes = (void*)[imageData bytes];
		int length = [imageData length];
		NSLog(@"write icon length = %d", length);
		sqlite3_bind_blob(insert_statement, field++, bytes, length, SQLITE_TRANSIENT);
        
	}
	if (photo == nil) {
		sqlite3_bind_zeroblob(insert_statement,field++,0);
	} else {
		NSData *imageData = UIImagePNGRepresentation(photo);
		void *bytes = (void*)[imageData bytes];
		int length = [imageData length];
		NSLog(@"write photo length = %d", length);
		sqlite3_bind_blob(insert_statement, field++, bytes, length, SQLITE_TRANSIENT);
	}
    
	sqlite3_bind_double(insert_statement, field++, [update timeIntervalSince1970]);
	sqlite3_bind_int(insert_statement, field++, totalCount);
	sqlite3_bind_int(insert_statement, field++, checkedCount);
	sqlite3_bind_int(insert_statement, field++, predefineId);
	sqlite3_bind_int(insert_statement, field++, predefineForAge);
	int success = sqlite3_step(insert_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(insert_statement);
    if (success == SQLITE_ERROR) {
        NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
    } else {
        // SQLite provides a method which retrieves the value of the most recently auto-generated primary key sequence
        // in the database. To access this functionality, the table should have a column declared of type
        // "INTEGER PRIMARY KEY"
        self.pk = sqlite3_last_insert_rowid(database);
    }
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
    
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    if(mAppDelegate.iCloudShow)
    {
        [self btnUpload];
    }
}
- (void) updateDataWithoutIconAndPhoto {
	if (!dirty) return;
	if (pk == INVALID_PK) return;
	NSLog(@"PackingList updateDataWithoutIconAndPhoto pk = %d dirty = %d checkedCount = %d", pk, dirty, checkedCount);
    sqlite3 *database = conn.database;
    if (update_without_blob_statement == nil) {
        const char *sql = "UPDATE list SET name=?, sex=?, age=?, lastUpdate=?, totalCount=?, checkedCount=?, predefineId=?, predefineForAge=? WHERE pk=?";
        if (sqlite3_prepare_v2(database, sql, -1, &update_without_blob_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
	if (name == nil) self.name = @"";
	int field = 1;
    sqlite3_bind_text(update_without_blob_statement, field++, [name UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(update_without_blob_statement, field++, sex);
	sqlite3_bind_int(update_without_blob_statement, field++, age);
    
	sqlite3_bind_double(update_without_blob_statement, field++, [update timeIntervalSince1970]);
	sqlite3_bind_int(update_without_blob_statement, field++, totalCount);
	sqlite3_bind_int(update_without_blob_statement, field++, checkedCount);
	NSLog(@"PackingList updateDataWithoutIconAndPhoto >> totalCount = %d", totalCount);
	NSLog(@"PackingList updateDataWithoutIconAndPhoto >> checkedCount = %d", checkedCount);
	sqlite3_bind_int(update_without_blob_statement, field++, predefineId);
	sqlite3_bind_int(update_without_blob_statement, field++, predefineForAge);
	sqlite3_bind_int(update_without_blob_statement, field++, pk);
	
    int success = sqlite3_step(update_without_blob_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(update_without_blob_statement);
    if (success == SQLITE_ERROR) {
        NSAssert1(0, @"Error: failed to update to the database with message '%s'.", sqlite3_errmsg(database));
    }
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
    
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    if(mAppDelegate.iCloudShow)
    {
        [self btnUpload];
    }
}

- (void)updateData {
	if (!dirty) return;
	if (pk == INVALID_PK) return;
	NSLog(@"PackingList updateData pk = %d dirty = %d checkedCount = %d", pk, dirty, checkedCount);
    sqlite3 *database = conn.database;
    if (update_statement == nil) {
        const char *sql = "UPDATE list SET name=?, sex=?, age=?, smallIcon=?, photo=?, lastUpdate=?, totalCount=?, checkedCount=?, predefineId=?, predefineForAge=? WHERE pk=?";
        if (sqlite3_prepare_v2(database, sql, -1, &update_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
	if (name == nil) self.name = @"";
	int field = 1;
    sqlite3_bind_text(update_statement, field++, [name UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(update_statement, field++, sex);
	sqlite3_bind_int(update_statement, field++, age);
	if (icon == nil) {
		sqlite3_bind_zeroblob(update_statement,field++,0);
	} else {
		NSData *imageData = UIImagePNGRepresentation(icon);
		void *bytes = (void*)[imageData bytes];
		int length = [imageData length];
		NSLog(@"write icon length = %d", length);
		sqlite3_bind_blob(update_statement, field++, bytes, length, SQLITE_TRANSIENT);
	}
	if (photo == nil) {
		sqlite3_bind_zeroblob(update_statement,field++,0);
	} else {
		NSData *imageData = UIImagePNGRepresentation(photo);
		void *bytes = (void*)[imageData bytes];
		int length = [imageData length];
		NSLog(@"write photo length = %d", length);
		sqlite3_bind_blob(update_statement, field++, bytes, length, SQLITE_TRANSIENT);
	}
	sqlite3_bind_double(update_statement, field++, [update timeIntervalSince1970]);
	sqlite3_bind_int(update_statement, field++, totalCount);
	sqlite3_bind_int(update_statement, field++, checkedCount);
	sqlite3_bind_int(update_statement, field++, predefineId);
	sqlite3_bind_int(update_statement, field++, predefineForAge);
	sqlite3_bind_int(update_statement, field++, pk);
	
    int success = sqlite3_step(update_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(update_statement);
    if (success == SQLITE_ERROR) {
        NSAssert1(0, @"Error: failed to update to the database with message '%s'.", sqlite3_errmsg(database));
    }
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
    
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    if(mAppDelegate.iCloudShow)
    {
        [self btnUpload];
    }
    
}
- (void)deleteData {
	if (pk == INVALID_PK) return;
	NSLog(@"PackingList deleteData pk = %d", pk);
	//delete items in this list first
	[self deleteItemsFromDB];
	
	sqlite3 *database = conn.database;
    if (delete_statement == nil) {
        const char *sql = "DELETE FROM list WHERE pk=?";
        if (sqlite3_prepare_v2(database, sql, -1, &delete_statement, NULL) != SQLITE_OK) {
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
	sqlite3_bind_int(delete_statement, 1, pk);
    //int success = sqlite3_step(delete_statement);
	sqlite3_step(delete_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(delete_statement);
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
	delete_statement=nil;
    
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    if(mAppDelegate.iCloudShow)
    {
        [self btnUpload];
    }
}


- (void)deleteAllData {
	//	if (pk == INVALID_PK) return;
	//	NSLog(@"PackingItem deleteData pk = %d", pk);
	NSLog(@"DELETE ALL PACKINGLIST");
	sqlite3 *database = conn.database;
	if (delete_statement == nil) {
        const char *sql = "DELETE FROM list";
        if (sqlite3_prepare_v2(database, sql, -1, &delete_statement, NULL) != SQLITE_OK) {
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
	}
	//	sqlite3_bind_int(delete_statement, 1, pk);
    //int success = sqlite3_step(delete_statement);
	sqlite3_step(delete_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(delete_statement);
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
	delete_statement = nil;
    
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    if(mAppDelegate.iCloudShow)
    {
        [self btnUpload];
    }
}



-(PackingList*) copyCurrentList:(PackingList*)list{
	PackingList *copy = [[PackingList alloc] init];
	copy.name = [NSString stringWithFormat:@"%@ copy",list.name];
	copy.icon = list.icon;
	copy.photo = list.photo;
	copy.update = [NSDate date];
	copy.itemArray = list.itemArray;
	copy.sex = list.sex;
	copy.age = list.age;
	copy.totalCount = list.totalCount;
	copy.checkedCount = list.checkedCount ;
	copy.predefineId = 0;
	copy.pk = INVALID_PK;
	return [copy autorelease];
    
}

+ (NSMutableArray*) findAllWithoutPhoto {
    
    
	NSLog(@"PackingList findAllWithoutPhoto");
	NSMutableArray *all = [NSMutableArray arrayWithCapacity:32];
	sqlite3 *database = [DBConnection sharedConnection].database;
	
	// Compile the delete statement if needed.
	if (select_all_without_photo_statement == nil) {
		const char *sql = "SELECT pk,name,sex,age,smallIcon,lastUpdate,totalCount,checkedCount,predefineId, predefineForAge FROM list ORDER BY name ASC";
		if (sqlite3_prepare_v2(database, sql, -1, &select_all_without_photo_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			return all;
		}
	}
	
	// Execute the query.
	while (sqlite3_step(select_all_without_photo_statement) == SQLITE_ROW) {
		// The second parameter indicates the column index into the result set.
		int field = 0;
		PackingList *list = [[PackingList alloc] init];
		
		list.pk  = sqlite3_column_int(select_all_without_photo_statement, field++);
		list.name  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_without_photo_statement, field++)];
		list.sex = sqlite3_column_int(select_all_without_photo_statement, field++);
		list.age = sqlite3_column_int(select_all_without_photo_statement, field++);
		
		int length = sqlite3_column_bytes(select_all_without_photo_statement, field);
		void *bytes = (void* )sqlite3_column_blob(select_all_without_photo_statement, field++);
		NSData *iconData = [NSData dataWithBytes:bytes length:length];
		list.icon = [UIImage imageWithData: iconData];
		list.update = [NSDate dateWithTimeIntervalSince1970: sqlite3_column_double(select_all_without_photo_statement, field++)];
		list.lastUpdate = sqlite3_column_double(select_all_without_photo_statement, field);
		list.totalCount = sqlite3_column_int(select_all_without_photo_statement, field++);
		//NSLog(@"PackingList findAllWithoutPhoto >> totalCount = %d", list.totalCount);
		list.checkedCount = sqlite3_column_int(select_all_without_photo_statement, field++);
		//NSLog(@"PackingList findAllWithoutPhoto >> checkedCount = %d", list.checkedCount);
		list.predefineId = sqlite3_column_int(select_all_without_photo_statement, field++);
		list.predefineForAge = sqlite3_column_int(select_all_without_photo_statement, field++);
		list.dirty = NO;
		[all addObject:list];
		[list release];
	}
	// Reset the statement for future use.
	sqlite3_reset(select_all_without_photo_statement);
	
	return all;
}

+ (void)upgradeTableFromDatabase:(sqlite3 *)db ToDatabase:(sqlite3 *)newdb {
	
	// Check if table exists
	if (upgrade_check_table_statement == nil) {
		const char *sql = "SELECT COUNT(*) FROM sqlite_master where type='table' and name='list'";
		if (sqlite3_prepare_v2(db, sql, -1, &upgrade_check_table_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(db));
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(db));
		}
	}
	NSInteger count = 0;
	if (sqlite3_step(upgrade_check_table_statement) == SQLITE_ROW) {
		count = sqlite3_column_int(upgrade_check_table_statement, 0);
	}
	sqlite3_reset(upgrade_check_table_statement);
	sqlite3_finalize(upgrade_check_table_statement);
	upgrade_check_table_statement = nil;
	if (count <= 0) return;
	
	// Compile the delete statement if needed.
	if (upgrade_select_statement == nil) {
		const char *sql = "SELECT pk,name,sex,age,smallIcon,photo,lastUpdate, totalCount, checkedCount, predefineId, predefineForAge FROM List ORDER BY pk ASC";
		if (sqlite3_prepare_v2(db, sql, -1, &upgrade_select_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(db));
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(db));
		}
	}
	if (upgrade_insert_statement == nil) {
        const char *sql = "INSERT INTO list (name,sex,age,smallIcon,photo,lastUpdate,totalCount,checkedCount,predefineId,predefineForAge) VALUES(?,?,?,?,?,?,?,?,?,?)";
        if (sqlite3_prepare_v2(newdb, sql, -1, &upgrade_insert_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(newdb));
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(newdb));
        }
    }
	
	// Execute the query.
	NSInteger pk = -1;
	NSString* name;
	NSInteger sex;
	NSInteger age;
	UIImage* icon;
	UIImage* photo;
	NSDate* update;
	NSInteger totalCount;
	NSInteger checkedCount;
	NSInteger predefineId;
	NSInteger predefineForAge;
	while (sqlite3_step(upgrade_select_statement) == SQLITE_ROW) {
		// The second parameter indicates the column index into the result set.
		int field = 0;
		pk  = sqlite3_column_int(upgrade_select_statement, field++);
		char * val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			name = [NSString stringWithUTF8String:val];
		} else {
			name = @"";
		}
		
		sex = sqlite3_column_int(upgrade_select_statement, field++);
		age = sqlite3_column_int(upgrade_select_statement, field++);
		
		int length = sqlite3_column_bytes(upgrade_select_statement, field);
		void *bytes = (void* )sqlite3_column_blob(upgrade_select_statement, field++);
		NSData *iconData = [NSData dataWithBytes:bytes length:length];
		icon = [UIImage imageWithData: iconData];
		length = sqlite3_column_bytes(upgrade_select_statement, field);
		bytes = (void* )sqlite3_column_blob(upgrade_select_statement, field++);
		NSData *photoData = [NSData dataWithBytes:bytes length:length];
		photo = [UIImage imageWithData: photoData];
		
		update = [NSDate dateWithTimeIntervalSince1970: sqlite3_column_double(upgrade_select_statement, field++)];
		totalCount = sqlite3_column_int(upgrade_select_statement, field++);
		checkedCount = sqlite3_column_int(upgrade_select_statement, field++);
		predefineId = sqlite3_column_int(upgrade_select_statement, field++);
		predefineForAge = sqlite3_column_int(upgrade_select_statement, field++);
		
		int toField = 1;
		sqlite3_bind_text(upgrade_insert_statement, toField++, [name UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_int(upgrade_insert_statement, toField++, sex);
		sqlite3_bind_int(upgrade_insert_statement, toField++, age);
		if (icon == nil) {
			sqlite3_bind_zeroblob(upgrade_insert_statement,toField++,0);
		} else {
			NSData *imageData = UIImagePNGRepresentation(icon);
			void *bytes = (void*)[imageData bytes];
			int length = [imageData length];
			sqlite3_bind_blob(upgrade_insert_statement, toField++, bytes, length, SQLITE_TRANSIENT);
			
		}
		if (photo == nil) {
			sqlite3_bind_zeroblob(upgrade_insert_statement,toField++,0);
		} else {
			NSData *imageData = UIImagePNGRepresentation(photo);
			void *bytes = (void*)[imageData bytes];
			int length = [imageData length];
			NSLog(@"write photo length = %d", length);
			sqlite3_bind_blob(upgrade_insert_statement, toField++, bytes, length, SQLITE_TRANSIENT);
		}
		
		sqlite3_bind_double(upgrade_insert_statement, toField++, [update timeIntervalSince1970]);
		sqlite3_bind_int(upgrade_insert_statement, toField++, totalCount);
		sqlite3_bind_int(upgrade_insert_statement, toField++, checkedCount);
		sqlite3_bind_int(upgrade_insert_statement, toField++, predefineId);
		sqlite3_bind_int(upgrade_insert_statement, toField++, predefineForAge);
		sqlite3_step(upgrade_insert_statement);
		sqlite3_reset(upgrade_insert_statement);
	}
	// Reset the statement for future use.
	sqlite3_reset(upgrade_select_statement);
	sqlite3_finalize(upgrade_select_statement);
	upgrade_select_statement = nil;
	sqlite3_reset(upgrade_insert_statement);
	sqlite3_finalize(upgrade_insert_statement);
	upgrade_insert_statement = nil;
}

- (void) deleteItemsFromDB {
	
	// fix bug::itemArray maybe empty
    //	for(PackingItem *item in self.itemArray) {
    //		[item deleteData];
    //	}
	self.itemArray = nil;
	
	[PackingItem deleteByListId:pk];
}

- (NSMutableArray*) getItemArray {
	[self loadItemsIfNeed];
	return itemArray;
}

- (void) loadItemsIfNeed {
	// need more consider here
	if (!itemArray || [itemArray count] == 0) {
		[self loadItemsFromDB];
	}
}

- (void) loadItemsFromDB {
	if (pk == INVALID_PK) return;
	self.itemArray = [PackingItem findByListId:self.pk];
}

- (BOOL) isPredefine {
	return (predefineId != 0);
}

- (BOOL) inUse {
    //	for (PackingItem *item in itemArray) {
    //		if ([item isChecked]) {
    //			return YES;
    //		}
    //	}
	if (checkedCount != 0) {
		return YES;
	}
	return NO;
}
- (BOOL) hasIcon {
	if (icon) {
		return YES;
	} else {
		return NO;
	}
}

- (BOOL) isComplete {
	if (checkedCount == totalCount && totalCount != 0) {
		return YES;
	}
	return NO;
}

- (NSString*) getAbstract {
	NSString *abstract;
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateStyle:kCFDateFormatterShortStyle]; //03/21/09
	[dateFormatter setTimeStyle:kCFDateFormatterShortStyle];
	
	if (!update) {
		self.update = [NSDate date];
	}
	NSString *dateString = [dateFormatter stringFromDate: update];
	[dateFormatter release];
    
	int percent = 0;
	if (totalCount != 0) {
		percent = 100 * ((float)checkedCount/totalCount);
	}
	NSLog(@"Percentage = %d",percent);
	NSLog(@"ITEM ARRAY = %d",[itemArray count]);
	NSLog(@"Checked = %d",self.checkedCount);
	NSLog(@"Total = %d",self.totalCount);
	abstract = [NSString stringWithFormat:@"%d%%(%d / %d) - %@",percent, checkedCount, totalCount, dateString];
	return abstract;
}

- (void) uncheckAllItems {
	[self loadItemsIfNeed];
	for(PackingItem *item in itemArray) {
		[item setChecked:NO];
	}
	self.checkedCount = 0;
}

- (void) updateItemsData {
	if (pk == INVALID_PK) return;
	for(PackingItem *item in itemArray) {
		[item updateData];
		//NSLog(@"updateItemsData item.checked = %d", item.checked);
	}
}

- (void) resetItems {
	if (pk == INVALID_PK) return;
	[PackingItem deleteByListId:self.pk];
	for(PackingItem *item in itemArray) {
		item.dirty = YES;
		[item insertData];
		//NSLog(@"updateItemsData item.checked = %d", item.checked);
	}
}

- (void)setPrimaryKey:(NSInteger)aValue {
    if (pk == aValue) return;
    dirty = YES;
    pk = aValue;
}

- (void)setName:(NSString *)aString {
    if ((!name && !aString) || (name && aString && [name isEqualToString:aString])) return;
    dirty = YES;
    [name release];
    name = [aString copy];
}

- (void)setSex:(NSInteger)aValue {
	if (sex == aValue) return;
	dirty = YES;
	sex = aValue;
}

- (void)setAge:(NSInteger)aValue {
	if (age == aValue) return;
	dirty = YES;
	age = aValue;
}

- (void)setTotalCount:(NSInteger)aValue {
	//NSLog(@"setCheckedCount aValue = %d, totalCount = %d", aValue, totalCount );
	if (totalCount == aValue) return;
	dirty = YES;
	totalCount = aValue;
}

- (void)setCheckedCount:(NSInteger)aValue {
	//NSLog(@"setCheckedCount aValue = %d, checkedCount = %d", aValue, checkedCount );
	if (checkedCount == aValue) return;
	dirty = YES;
	checkedCount = aValue;
}
- (void) setPredefineId:(NSInteger) aId {
	if (predefineId != aId) {
		dirty = YES;
		predefineId = aId;
	}
}

- (void) setPredefineForAge:(NSInteger) newAge {
	if (predefineForAge != newAge) {
		dirty = YES;
		predefineForAge = newAge;
	}
}

- (void)setIcon:(UIImage*)aIcon {
	if (!icon && !aIcon || icon == aIcon) return;
	dirty = YES;
	[icon release];
	icon = [aIcon retain];
}

// Don't need
//- (void)setPhoto:(UIImage*)aPhoto {
//	if (!photo && !aPhoto || photo == aPhoto) return;
//	dirty = YES;
//	[photo release];
//	photo = [aIcon retain];
//}

// load big photo when necessary and set to nil after used
- (UIImage*)getPhoto {
	NSLog(@"load Photo...");
	if (!photo) {
		sqlite3 *database = conn.database;
        if (select_photo_statement == nil) {
            
            const char *sql = "SELECT photo FROM List WHERE pk=?";
            if (sqlite3_prepare_v2(database, sql, -1, &select_photo_statement, NULL) != SQLITE_OK) {
                NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            }
        }
        sqlite3_bind_int(select_photo_statement, 1, pk);
		if (sqlite3_step(select_photo_statement) == SQLITE_ROW) {
			int length = sqlite3_column_bytes(select_photo_statement, 0);
			NSLog(@"read photo length = %d", length);
			void *bytes = (void* )sqlite3_column_blob(select_photo_statement, 0);
			NSData *iconData = [NSData dataWithBytes:bytes length:length];
			self.photo = [UIImage imageWithData: iconData];
		}
        sqlite3_reset(select_photo_statement);
	}
	return photo;
}

+ (void)finalizeStatements {
	if (select_photo_statement) {
		sqlite3_finalize(select_photo_statement);
		select_photo_statement = nil;
	}
	
	if (select_all_without_photo_statement) {
		sqlite3_finalize(select_all_without_photo_statement);
		select_all_without_photo_statement = nil;
	}
    if (insert_statement) {
		sqlite3_finalize(insert_statement);
		insert_statement = nil;
	}
    if (init_statement) {
		sqlite3_finalize(init_statement);
		init_statement = nil;
	}
    if (delete_statement) {
		sqlite3_finalize(delete_statement);
		delete_statement = nil;
	}
	if (update_statement) {
		sqlite3_finalize(update_statement);
		update_statement = nil;
	}
	if (upgrade_check_table_statement) {
		sqlite3_finalize(upgrade_check_table_statement);
		upgrade_check_table_statement = nil;
	}
	if (upgrade_select_statement) {
		sqlite3_finalize(upgrade_select_statement);
		upgrade_select_statement = nil;
	}
	if (upgrade_insert_statement) {
		sqlite3_finalize(upgrade_insert_statement);
		upgrade_insert_statement = nil;
	}
	if (update_without_blob_statement) {
		sqlite3_finalize(update_without_blob_statement);
		update_without_blob_statement = nil;
	}
	if (hydrate_statement) {
		sqlite3_finalize(hydrate_statement);
		hydrate_statement = nil;
	}
    if (dehydrate_statement) {
		sqlite3_finalize(dehydrate_statement);
		dehydrate_statement = nil;
	}
}


-(void)btnUpload
{
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    
    // Create XML and pass to iCloud
    NSString *xmlStr=@"<List>";
    NSString *select_query;
    const char *select_stmt;
    sqlite3_stmt *compiled_stmt;
    sqlite3 *dbFamilyPacking;
    if (sqlite3_open([[mAppDelegate getDBListPath] UTF8String], &dbFamilyPacking) == SQLITE_OK)
    {
        select_query = [NSString stringWithFormat:@"SELECT * FROM list"];
        select_stmt = [select_query UTF8String];
        if(sqlite3_prepare_v2(dbFamilyPacking, select_stmt, -1, &compiled_stmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiled_stmt) == SQLITE_ROW)
            {
                NSString *addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,0)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<Data><pk>%@</pk>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,1)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<name>%@</name>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,2)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<sex>%@</sex>",addr];
                
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,3)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<age>%@</age>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,4)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<smallIcon>%@</smallIcon>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,5)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<photo>%@</photo>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,6)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<lastUpdate>%@</lastUpdate>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,7)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<totalCount>%@</totalCount>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,8)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<checkedCount>%@</checkedCount>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,9)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<predefineId>%@</predefineId>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,10)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<predefineForAge>%@</predefineForAge>",addr];
                
                
                xmlStr =[xmlStr stringByAppendingString:@"</Data>"];
            }
            
            xmlStr =[xmlStr stringByAppendingString:@"</List>"];
            
            sqlite3_finalize(compiled_stmt);
            
            BabyPackingAppDelegate* mAppDelegate;
            mAppDelegate = [UIApplication sharedApplication].delegate;
            mAppDelegate.notesList=xmlStr;
            
            // Notify the previouse view to save the changes locally
            [[NSNotificationCenter defaultCenter] postNotificationName:@"New Note List Item" object:self userInfo:[NSDictionary dictionaryWithObject:xmlStr forKey:@"NoteList"]];
            
        }
    }
}

@end

//
//  CategoryViewController.m
//  BabyPacking
//
//  Created by Jay Lee on 10/14/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import "CategoryViewController.h"
#import "BabyPackingAppDelegate.h"
#import "Constant.h"

typedef enum {
	TAG_CELL_ICON = -70,
	TAG_CELL_TITLE
} CellComponentTag;

@implementation CategoryViewController

@synthesize defaultValue = mDefaultValue;

- (void)dealloc {
	[mDefaultValue release];
	[mTableView release];
	[mIconView release];
	[mTitleLabel release];
    [super dealloc];
}

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	mAppDelegate = (BabyPackingAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSDictionary* root = [mAppDelegate.appDict objectForKey:@"APNS"];
	NSDictionary* category = [root objectForKey:@"Category"];
	mCategoryList = [category objectForKey:APNS_SUBITEM_ROOT];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 30, 30);
    btn.tag=1;
    [btn setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(goBck:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }    
}

- (IBAction)goBck:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

+ (NSString *)titleForValue:(NSString *)aValue {
	
	NSString* title = nil;
	
	BabyPackingAppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
	NSDictionary* root = [appDelegate.appDict objectForKey:@"APNS"];
	NSDictionary* category = [root objectForKey:@"Category"];
	NSArray* list = [category objectForKey:APNS_SUBITEM_ROOT];
	for (NSDictionary* dict in list) {
		NSString* value = [dict objectForKey:@"Value"];
		if ([value isEqualToString:aValue]) {
			title = [dict objectForKey:@"Title"];
			break;
		}
	}
	
	return title;
}

+ (NSString *)valueForTitle:(NSString *)aTitle {
	
	NSString* value = nil;
	
	BabyPackingAppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
	NSDictionary* root = [appDelegate.appDict objectForKey:@"APNS"];
	NSDictionary* category = [root objectForKey:@"Category"];
	NSArray* list = [category objectForKey:APNS_SUBITEM_ROOT];
	for (NSDictionary* dict in list) {
		NSString* title = [dict objectForKey:@"Title"];
		if ([title isEqualToString:aTitle]) {
			value = [dict objectForKey:@"Value"];
			break;
		}
	}
	
	return value;
}

+ (UIImage *)imageForTitle:(NSString *)aTitle {
	
	UIImage* image = nil;
	
	BabyPackingAppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
	NSDictionary* root = [appDelegate.appDict objectForKey:@"APNS"];
	NSDictionary* category = [root objectForKey:@"Category"];
	NSArray* list = [category objectForKey:APNS_SUBITEM_ROOT];
	for (NSDictionary* dict in list) {
		NSString* title = [dict objectForKey:@"Title"];
		if ([title isEqualToString:aTitle]) {
			image = [UIImage imageNamed:[dict objectForKey:@"Image"]];
			break;
		}
	}
	
	return image;
}

+ (UIImage *)imageForValue:(NSString *)aValue {

	UIImage* image = nil;
	
	BabyPackingAppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
	NSDictionary* root = [appDelegate.appDict objectForKey:@"APNS"];
	NSDictionary* category = [root objectForKey:@"Category"];
	NSArray* list = [category objectForKey:APNS_SUBITEM_ROOT];
	for (NSDictionary* dict in list) {
		NSString* value = [dict objectForKey:@"Value"];
		if ([value isEqualToString:aValue]) {
			image = [UIImage imageNamed:[dict objectForKey:@"Image"]];
			break;
		}
	}
	
	return image;
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [mCategoryList count];
}

/*
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
	
	NSMutableArray* indexes = [NSMutableArray arrayWithCapacity:0];
	for (NSDictionary* dict in mCategoryList) {
		NSString* title = [dict objectForKey:@"Title"];
		[indexes addObject:[title substringToIndex:1]];
	}
	
	return indexes;
}

// Show an alphabeta list at the side
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
	
	
	return index;
}
 */

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"CategoryViewCell";
	UIImageView* iconView;
	UILabel* lbTitle;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		iconView = [[UIImageView alloc] initWithFrame:mIconView.frame];
		iconView.tag = TAG_CELL_ICON;
		iconView.contentMode = mIconView.contentMode;
		[cell.contentView addSubview:iconView];
		[iconView release];
		lbTitle = [[UILabel alloc] initWithFrame:mTitleLabel.frame];
		lbTitle.tag = TAG_CELL_TITLE;
		lbTitle.backgroundColor = [UIColor clearColor];
		lbTitle.font = mTitleLabel.font;
		[cell.contentView addSubview:lbTitle];
		[lbTitle release];
	} else {
		iconView = (id)[cell.contentView viewWithTag:TAG_CELL_ICON];
		lbTitle = (id)[cell.contentView viewWithTag:TAG_CELL_TITLE];
	}
	
	NSDictionary* dict = [mCategoryList objectAtIndex:indexPath.row];
	lbTitle.text = [dict objectForKey:@"Title"];
   
    if ([[dict objectForKey:@"Title"] isEqualToString:@"Shop"])
    {
        iconView.image = [UIImage imageNamed:@"ShoppingForTask.png"];
    }
    else if ([[dict objectForKey:@"Title"] isEqualToString:@"Other"])
    {
        iconView.image = [UIImage imageNamed:@"OtherForTask.png"];
    }
    else
    {
        NSString* image = [dict objectForKey:@"Image"];
        iconView.image = [UIImage imageNamed:image];
    }

    
	
	NSString* value = [dict objectForKey:@"Title"];
	if ([mDefaultValue isEqualToString:value]) {
		lbTitle.textColor = [UIColor colorWithRed:0.22 green:0.329 blue:0.529 alpha:1.0];
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	} else {
		lbTitle.textColor = [UIColor blackColor];
		cell.accessoryType = UITableViewCellAccessoryNone;
	}
	
	NSString *customFontName1 = [mAppDelegate getCustomFontName];
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
		cell.textLabel.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];			
	}
    else{
		cell.textLabel.font=[UIFont fontWithName:customFontName1 size:15];			        
    }
 
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		cell.backgroundColor = theColour1;
	}
	// and finally set the colour of your label	
	
	
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour21;
	// read the data back from the user defaults
	NSData *data211= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
	// check whether you got anything
	if(data211 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour21 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data211];
		lbTitle.textColor = theColour21;
	}
	// and finally set the colour of your label
	
	
    return cell;
}

// Override to support row selection in the table view.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// Navigation logic may go here -- for example, create and push another view controller.
	NSDictionary* dict = [mCategoryList objectAtIndex:indexPath.row];
	NSString* value = [dict objectForKey:@"Title"];
	self.defaultValue = value;
	[mTableView reloadData];
}

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        // [layoutManager translateToPortraitForView:self.view withAnimation:YES];
    }
    else
    {
        // [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
    }
    // }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source.
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
 }   
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

@end

//
//  TaskItem.m
//  BabyPacking
//
//  Created by Jay Lee on 10/14/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import "Constant.h"
#import "TaskItem.h"

static sqlite3_stmt *select_by_category_statement = nil;
static sqlite3_stmt *select_all_statement = nil;
static sqlite3_stmt *insert_statement = nil;
static sqlite3_stmt *init_statement = nil;
static sqlite3_stmt *init_statement2 = nil;
static sqlite3_stmt *delete_statement = nil;
static sqlite3_stmt *update_statement = nil;
static sqlite3_stmt *upgrade_check_table_statement = nil;
static sqlite3_stmt *upgrade_select_statement = nil;
static sqlite3_stmt *upgrade_insert_statement = nil;
static sqlite3_stmt *hydrate_statement = nil;
static sqlite3_stmt *dehydrate_statement = nil;


@implementation TaskItem

@synthesize remote_id;
@synthesize title;
@synthesize category;
@synthesize repeat;
@synthesize interval;
@synthesize start_date;
@synthesize end_date;
@synthesize text;
@synthesize device_token;
@synthesize create_date;
@synthesize update_date;

//Change By MS
@synthesize reminder_id;

- (BOOL)isEqual:(id)anObject {
	//NSLog(@"isEqual >>>>>>>");
	if (anObject == self)
        return YES;
    if (!anObject || ![anObject isKindOfClass:[self class]])
        return NO;
    return [self isEqualToItem:anObject];
}

- (BOOL)isEqualToItem:(TaskItem *)item {

    if (self.pk == item.pk) {
		return YES;
	}
    return NO;
}

- (NSUInteger)hash {
    NSUInteger hash = 0;
    hash += [title hash];
    hash += [category hash];
	hash += [repeat hash];
	hash += interval;
	hash += [start_date hash];
	hash += [end_date hash];
	hash += [text hash];
	hash += [device_token hash];
	hash += [create_date hash];
	hash += [update_date hash];
	
    return hash;
}

- (id) init {
	if (self = [super init]) {
		title = @"";
		category = @"";
		repeat = TYPE_REPEAT_NONE;
		interval = 0;
		start_date = nil;
		text = @"";
		device_token = @"";
		create_date = nil;
		update_date = nil;
		pk = INVALID_PK;
	}
	return self;
}

- (void)dealloc {
	[title release];
	[category release];
	[repeat release];
	[start_date release];
	[end_date release];
	[text release];
	[device_token release];
	[create_date release];
	[update_date release];
    [super dealloc];
}

- (id)initWithPK:(NSInteger)primaryKey {
	NSLog(@"TaskItem initWithPK pk = %d ", primaryKey);
	if (self = [self init]) {
		self.pk = primaryKey;
		sqlite3 *database = conn.database;
		// Compile the query for retrieving book data. See insertNewBookIntoDatabase: for more detail.
        if (init_statement == nil) {
            // Note the '?' at the end of the query. This is a parameter which can be replaced by a bound variable.
            // This is a great way to optimize because frequently used queries can be compiled once, then with each
            // use new variable values can be bound to placeholders.
            const char *sql = "SELECT remote_id, title, category, repeat, interval, start_date, end_date, text, device_token, create_date, update_date FROM task WHERE id=?";
            if (sqlite3_prepare_v2(database, sql, -1, &init_statement, NULL) != SQLITE_OK) {
                NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));		
            }
        }
		// For this query, we bind the primary key to the first (and only) placeholder in the statement.
        // Note that the parameters are numbered from 1, not from 0.
        sqlite3_bind_int(init_statement, 1, pk);
		if (sqlite3_step(init_statement) == SQLITE_ROW) {
			int field = 0;
			self.remote_id = sqlite3_column_int(init_statement, field++);
			char * val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.title = [NSString stringWithUTF8String:val];
			} else {
				self.title = @"";
			}

			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.category = [NSString stringWithUTF8String:val];
			} else {
				self.category = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.repeat = [NSString stringWithUTF8String:val];
			}else {
				self.repeat = TYPE_REPEAT_NONE;
			}
			self.interval = sqlite3_column_int(init_statement, field++);
			double date = sqlite3_column_double(init_statement, field++);
			if (date != 0) {
				self.start_date = [NSDate dateWithTimeIntervalSince1970:date];
			}else {
				self.start_date = nil;
			}
			date = sqlite3_column_double(init_statement, field++);
			if (date != 0) {
				self.end_date = [NSDate dateWithTimeIntervalSince1970:date];
			}else {
				self.end_date = nil;
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.text = [NSString stringWithUTF8String:val];
			}else {
				self.text = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.device_token = [NSString stringWithUTF8String:val];
			}else {
				self.device_token = @"";
			}
			date = sqlite3_column_double(init_statement, field++);
			if (date != 0) {
				self.create_date = [NSDate dateWithTimeIntervalSince1970:date];
			}else {
				self.create_date = nil;
			}
			date = sqlite3_column_double(init_statement, field++);
			if (date != 0) {
				self.update_date = [NSDate dateWithTimeIntervalSince1970:date];
			}else {
				self.update_date = nil;
			}
		} 
		// Reset the statement for future reuse.
        sqlite3_reset(init_statement);
        dirty = NO;
		hydrated = YES;
	}
	return self;
}

- (id)initWithRemoteId:(NSInteger)remoteID {
	NSLog(@"TaskItem initWithRemoteId remote_id = %d ", remoteID);
	if (self = [self init]) {
		sqlite3 *database = conn.database;
		// Compile the query for retrieving book data. See insertNewBookIntoDatabase: for more detail.
        if (init_statement2 == nil) {
            // Note the '?' at the end of the query. This is a parameter which can be replaced by a bound variable.
            // This is a great way to optimize because frequently used queries can be compiled once, then with each
            // use new variable values can be bound to placeholders.
            const char *sql = "SELECT id, remote_id, title, category, repeat, interval, start_date, end_date, text, device_token, create_date, update_date FROM task WHERE remote_id=?";
            if (sqlite3_prepare_v2(database, sql, -1, &init_statement2, NULL) != SQLITE_OK) {
                NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));		
            }
        }
		// For this query, we bind the primary key to the first (and only) placeholder in the statement.
        // Note that the parameters are numbered from 1, not from 0.
        sqlite3_bind_int(init_statement2, 1, remoteID);
		if (sqlite3_step(init_statement2) == SQLITE_ROW) {
			int field = 0;
			self.pk = sqlite3_column_int(init_statement2, field++);
			self.remote_id = sqlite3_column_int(init_statement2, field++);
			char * val = (char *)sqlite3_column_text(init_statement2, field++);
			if (val != nil) {
				self.title = [NSString stringWithUTF8String:val];
			} else {
				self.title = @"";
			}
			
			val = (char *)sqlite3_column_text(init_statement2, field++);
			if (val != nil) {
				self.category = [NSString stringWithUTF8String:val];
			} else {
				self.category = @"";
			}
			val = (char *)sqlite3_column_text(init_statement2, field++);
			if (val != nil) {
				self.repeat = [NSString stringWithUTF8String:val];
			}else {
				self.repeat = TYPE_REPEAT_NONE;
			}
			self.interval = sqlite3_column_int(init_statement2, field++);
			double date = sqlite3_column_double(init_statement2, field++);
			if (date != 0) {
				self.start_date = [NSDate dateWithTimeIntervalSince1970:date];
			}else {
				self.start_date = nil;
			}
			date = sqlite3_column_double(init_statement2, field++);
			if (date != 0) {
				self.end_date = [NSDate dateWithTimeIntervalSince1970:date];
			}else {
				self.end_date = nil;
			}
			val = (char *)sqlite3_column_text(init_statement2, field++);
			if (val != nil) {
				self.text = [NSString stringWithUTF8String:val];
			}else {
				self.text = @"";
			}
			val = (char *)sqlite3_column_text(init_statement2, field++);
			if (val != nil) {
				self.device_token = [NSString stringWithUTF8String:val];
			}else {
				self.device_token = @"";
			}
			date = sqlite3_column_double(init_statement2, field++);
			if (date != 0) {
				self.create_date = [NSDate dateWithTimeIntervalSince1970:date];
			}else {
				self.create_date = nil;
			}
			date = sqlite3_column_double(init_statement2, field++);
			if (date != 0) {
				self.update_date = [NSDate dateWithTimeIntervalSince1970:date];
			}else {
				self.update_date = nil;
			}
		} 
		// Reset the statement for future reuse.
        sqlite3_reset(init_statement2);
        dirty = NO;
		hydrated = YES;
	}
	return self;
}

- (void)insertData {
	if (!dirty) return;
	NSLog(@"TaskItem insertData pk = %d dirty = %d", pk, dirty);
	//if (pk == INVALID_PK) return;
	
    sqlite3 *database = conn.database;
    if (insert_statement == nil) {
        static char *sql = "INSERT INTO task (remote_id, title, category, repeat, interval, start_date, end_date, text, device_token, create_date, update_date) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
        if (sqlite3_prepare_v2(database, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
	if (title == nil) self.title = @"";
	if (category == nil) self.category = @"";
	if (repeat == nil) self.repeat = @"";
	if (text == nil) self.text = @"";
	if (device_token == nil) self.device_token = @"";
	int field = 1;
	sqlite3_bind_int(insert_statement, field++, remote_id);
	sqlite3_bind_text(insert_statement, field++, [title UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [category UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [repeat UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_int(insert_statement, field++, interval);
	sqlite3_bind_double(insert_statement, field++, [start_date timeIntervalSince1970]);
	sqlite3_bind_double(insert_statement, field++, [end_date timeIntervalSince1970]);
	sqlite3_bind_text(insert_statement, field++, [text UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [device_token UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_double(insert_statement, field++, [[NSDate date] timeIntervalSince1970]);
	sqlite3_bind_double(insert_statement, field++, [[NSDate date] timeIntervalSince1970]);
	int success = sqlite3_step(insert_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(insert_statement);
    if (success == SQLITE_ERROR) {
        NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
    } else {
        // SQLite provides a method which retrieves the value of the most recently auto-generated primary key sequence
        // in the database. To access this functionality, the table should have a column declared of type 
        // "INTEGER PRIMARY KEY"
        self.pk = sqlite3_last_insert_rowid(database);
    }
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
}

- (void)updateData {
	if (!dirty) return;
	if (pk == INVALID_PK) return;
	NSLog(@"TaskItem updateData pk = %d dirty = %d", pk, dirty);
    sqlite3 *database = conn.database;
    if (update_statement == nil) {
        static char *sql = "UPDATE task SET remote_id=?, title=?, category=?, repeat=?, interval=?, start_date=?, end_date=?, text=?, device_token=?, update_date=? WHERE id=?";
        if (sqlite3_prepare_v2(database, sql, -1, &update_statement, NULL) != SQLITE_OK) {
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
	if (title == nil) self.title = @"";
	if (category == nil) self.category = @"";
	if (repeat == nil) self.repeat = @"";
	if (text == nil) self.text = @"";
	if (device_token == nil) self.device_token = @"";
	int field = 1;
	sqlite3_bind_int(update_statement, field++, remote_id);
	sqlite3_bind_text(update_statement, field++, [title UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [category UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [repeat UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_int(update_statement, field++, interval);
	sqlite3_bind_double(update_statement, field++, [start_date timeIntervalSince1970]);
	sqlite3_bind_double(update_statement, field++, [end_date timeIntervalSince1970]);
	sqlite3_bind_text(update_statement, field++, [text UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [device_token UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_double(update_statement, field++, [[NSDate date] timeIntervalSince1970]);
	sqlite3_bind_int(update_statement, field++, pk);
	
    int success = sqlite3_step(update_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(update_statement);
    if (success == SQLITE_ERROR) {
        NSAssert1(0, @"Error: failed to update to the database with message '%s'.", sqlite3_errmsg(database));
    }
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
}

- (void)deleteData {
	if (pk == INVALID_PK) return;
	NSLog(@"TaskItem deleteData pk = %d", pk);
	sqlite3 *database = conn.database;
    if (delete_statement == nil) {
        static char *sql = "DELETE FROM task WHERE id = ?";
        if (sqlite3_prepare_v2(database, sql, -1, &delete_statement, NULL) != SQLITE_OK) {
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
	sqlite3_bind_int(delete_statement, 1, pk);
    //int success = sqlite3_step(delete_statement);
	sqlite3_step(delete_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(delete_statement);
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
}

- (void)deleteAllData {
//	if (pk == INVALID_PK) return;
//	NSLog(@"TaskItem deleteData pk = %d", pk);
	sqlite3 *database = conn.database;
    if (delete_statement == nil) {
        static char *sql = "DELETE FROM task";
        if (sqlite3_prepare_v2(database, sql, -1, &delete_statement, NULL) != SQLITE_OK) {
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
//	sqlite3_bind_int(delete_statement, 1, pk);
    //int success = sqlite3_step(delete_statement);
	sqlite3_step(delete_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(delete_statement);
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
}


- (void) setRemote_id:(NSInteger) aValue {
	if (aValue != remote_id) {
		dirty = YES;
		remote_id = aValue;
	}
}

- (void)setTitle:(NSString *)aString {
    if ((!title && !aString) || (title && aString && [title isEqualToString:aString])) return;
    dirty = YES;
    [title release];
    title = [aString copy];
}

- (void)setCategory:(NSString *)aString {
    if ((!category && !aString) || (category && aString && [category isEqualToString:aString])) return;
    dirty = YES;
    [category release];
    category = [aString copy];
}

- (void)setRepeat:(NSString *)aString {
	if ((!repeat && !aString) || (repeat && aString && [repeat isEqualToString:aString])) return;
	dirty = YES;
	[repeat release];
	repeat = [aString copy];
}

- (void) setInterval:(NSInteger) aValue {
	if (aValue != interval) {
		dirty = YES;
		interval = aValue;
	}
}

- (void)setStart_date:(NSDate *)aDate {
	if ((!start_date && !aDate) || (start_date && aDate && [start_date isEqualToDate:aDate])) return;
	
	dirty = YES;
	[start_date release];
	start_date = [aDate copy];
}

- (void)setEnd_date:(NSDate *)aDate {
	if ((!end_date && !aDate) || (end_date && aDate && [end_date isEqualToDate:aDate])) return;
	
	dirty = YES;
	[end_date release];
	end_date = [aDate copy];
}

- (void)setText:(NSString *)aString {
	if ((!text && !aString) || (text && aString && [text isEqualToString:aString])) return;
	dirty = YES;
	[text release];
	text = [aString copy];
}

- (void)setDevice_token:(NSString *)aString {
	if ((!device_token && !aString) || (device_token && aString && [device_token isEqualToString:aString])) return;
	dirty = YES;
	[device_token release];
	device_token = [aString copy];
}

- (void)setCreate_date:(NSDate *)aDate {
	if ((!create_date && !aDate) || (create_date && aDate && [create_date isEqualToDate:aDate])) return;
	
	dirty = YES;
	[create_date release];
	create_date = [aDate copy];
}

- (void)setUpdate_date:(NSDate *)aDate {
	if ((!update_date && !aDate) || (update_date && aDate && [update_date isEqualToDate:aDate])) return;
	
	dirty = YES;
	[update_date release];
	update_date = [aDate copy];
}

// claculate task execution period by task repeat type
// return nil if task does not repeat
- (NSDateComponents *)calcOffsetDate {
	
	if ([TYPE_REPEAT_NONE isEqualToString:self.repeat]) return nil;
	
	NSDateComponents* offset = [[[NSDateComponents alloc] init] autorelease];
	[offset setSecond:0];
	if ([TYPE_REPEAT_HOURLY isEqualToString:self.repeat]) {
		[offset setHour:self.interval];
	} else if ([TYPE_REPEAT_DAILY isEqualToString:self.repeat]) {
		[offset setDay:self.interval];
	} else if ([TYPE_REPEAT_WEEKLY isEqualToString:self.repeat]) {
		[offset setWeek:self.interval];
	} else if ([TYPE_REPEAT_MONTHLY isEqualToString:self.repeat]) {
		[offset setMonth:self.interval];
	} else if ([TYPE_REPEAT_YEARLY isEqualToString:self.repeat]) {
		[offset setYear:self.interval];
	}
	
	return offset;
}

// get next task execution datetime
// if 'no repeat' task, then return its start_date
- (NSDate *)getNextTaskTime {
	
	NSDate* new_start_date = nil;

	// get task period
	// if 'no repeat', then component == nil;
	NSDateComponents* component = [self calcOffsetDate];
	if (component == nil) return self.start_date;
	
	// if start_date is later than now, return the start_date directly.
	NSDate* now = [NSDate date];
	if ([now compare:self.start_date] == NSOrderedAscending) return self.start_date;
	
	NSInteger hours = [component hour];
	NSTimeInterval duration_seconds = [now timeIntervalSinceDate:self.start_date];
	double duration_hours = duration_seconds/3600;
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	if (hours > duration_hours) {
		new_start_date = [gregorian dateByAddingComponents:component toDate:self.start_date options:0];
	} else {
		NSInteger count = lrint(ceil(duration_hours/hours));
		NSDateComponents* offsetToAdd = [[NSDateComponents alloc] init];
		[offsetToAdd setHour:count * hours];
		new_start_date = [gregorian dateByAddingComponents:offsetToAdd toDate:self.start_date options:0];
		[offsetToAdd release];
	}
	[gregorian release];
	
	return new_start_date;
}

+ (NSMutableArray*)findByCategory:(NSString *)category {
	NSLog(@"findBycategory = %@", category);
	
	NSMutableArray *items = [NSMutableArray arrayWithCapacity:0];
	sqlite3 *database = [DBConnection sharedConnection].database;
	
	// Compile the delete statement if needed.
	if (select_by_category_statement == nil) {
		const char *sql = "SELECT id, remote_id, title, category, repeat, interval, start_date, end_date, text, device_token, create_date, update_date FROM task WHERE category = ? ";
		if (sqlite3_prepare_v2(database, sql, -1, &select_by_category_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			return items;
		}
	}
	
	// Execute the query.
	sqlite3_bind_text(select_by_category_statement, 1, [category UTF8String], -1, SQLITE_TRANSIENT);
	while (sqlite3_step(select_by_category_statement) == SQLITE_ROW) {
		// The second parameter indicates the column index into the result set.
		TaskItem *item = [[TaskItem alloc] init];
		int field = 0;
		item.pk = sqlite3_column_int(select_by_category_statement, field++);
		item.remote_id = sqlite3_column_int(select_by_category_statement, field++);
		item.title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_by_category_statement, field++)];
		item.category = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_by_category_statement, field++)];
		item.repeat = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_by_category_statement, field++)];
		item.interval = sqlite3_column_int(select_by_category_statement, field++);
		item.start_date = [NSDate dateWithTimeIntervalSince1970:sqlite3_column_double(select_by_category_statement, field++)];
		item.end_date = [NSDate dateWithTimeIntervalSince1970:sqlite3_column_double(select_by_category_statement, field++)];
		item.text = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_by_category_statement, field++)];
		item.device_token = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_by_category_statement, field++)];
		item.create_date = [NSDate dateWithTimeIntervalSince1970:sqlite3_column_double(select_by_category_statement, field++)];
		item.update_date = [NSDate dateWithTimeIntervalSince1970:sqlite3_column_double(select_by_category_statement, field++)];
		//NSLog(@"findByListId item.checked = %d", item.checked);
		item.dirty = NO;
		[items addObject:item];
		[item release];
	}
	// Finalizing the statement.
	sqlite3_finalize(select_by_category_statement);
	select_by_category_statement = nil;
	
	return items;
}

+ (NSMutableArray*) findAll {
	//NSLog(@"TaskItem findAll");
	NSMutableArray *all = [NSMutableArray arrayWithCapacity:0];
	sqlite3 *database = [DBConnection sharedConnection].database;
	
	// Compile the delete statement if needed.
	if (select_all_statement == nil) {
		//const char *sql = "SELECT id, remote_id, title, category, repeat, interval, start_date, text, device_token, create_date, update_date FROM task WHERE repeat != 'none' or (repeat = 'none' AND start_date > strftime(\"%s\",'now','localtime'))  ORDER BY create_date ASC";
		const char *sql = "SELECT id, remote_id, title, category, repeat, interval, start_date, end_date, text, device_token, create_date, update_date FROM task ORDER BY create_date ASC";
		if (sqlite3_prepare_v2(database, sql, -1, &select_all_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			return all;
		}
	}
	
	// Execute the query.
	while (sqlite3_step(select_all_statement) == SQLITE_ROW) {
		// The second parameter indicates the column index into the result set.
		int field = 0;
		TaskItem *item = [[TaskItem alloc] init];
		
		item.pk = sqlite3_column_int(select_all_statement, field++);
		item.remote_id = sqlite3_column_int(select_all_statement, field++);
		item.title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		item.category = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		item.repeat = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		item.interval = sqlite3_column_int(select_all_statement, field++);
		item.start_date = [NSDate dateWithTimeIntervalSince1970:sqlite3_column_double(select_all_statement, field++)];
		item.end_date = [NSDate dateWithTimeIntervalSince1970:sqlite3_column_double(select_all_statement, field++)];
		item.text = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		item.device_token = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		item.create_date = [NSDate dateWithTimeIntervalSince1970:sqlite3_column_double(select_all_statement, field++)];
		item.update_date = [NSDate dateWithTimeIntervalSince1970:sqlite3_column_double(select_all_statement, field++)];
		item.dirty = NO;
		[all addObject:item];
		[item release];
	}
	// Finalizing the statement.
	sqlite3_finalize(select_all_statement);
	select_all_statement = nil;
	
	return all;
}

+ (void)upgradeTableFromDatabase:(sqlite3 *)db ToDatabase:(sqlite3 *)newdb {
	
	// Check if table exists
	if (upgrade_check_table_statement == nil) {
		const char *sql = "SELECT COUNT(*) FROM sqlite_master where type='table' and name='task'";
		if (sqlite3_prepare_v2(db, sql, -1, &upgrade_check_table_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(db));
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(db));
		}
	}
	NSInteger count = 0;
	if (sqlite3_step(upgrade_check_table_statement) == SQLITE_ROW) {
		count = sqlite3_column_int(upgrade_check_table_statement, 0);
	}
	sqlite3_reset(upgrade_check_table_statement);
	sqlite3_finalize(upgrade_check_table_statement);
	upgrade_check_table_statement = nil;
	if (count <= 0) return;
	
	// Compile the delete statement if needed.
	if (upgrade_select_statement == nil) {
		const char *sql = "SELECT id, remote_id, title, category, repeat, interval, start_date, end_date, text, device_token, create_date, update_date FROM task ORDER BY id ASC";
		if (sqlite3_prepare_v2(db, sql, -1, &upgrade_select_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(db));
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(db));
		}
	}
	if (upgrade_insert_statement == nil) {
        const char *sql = "INSERT INTO task (remote_id, title, category, repeat, interval, start_date, end_date, text, device_token, create_date, update_date) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        if (sqlite3_prepare_v2(newdb, sql, -1, &upgrade_insert_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(newdb));
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(newdb));
        }
    }
	
	// Execute the query.
	NSInteger pk = -1;
	NSInteger remote_id;
	NSString *title;
	NSString *category;
	NSString *repeat;
	NSInteger interval;
	NSDate* start_date;
	NSDate* end_date;
	NSString* text;
	NSString* device_token;
	NSDate* create_date;
	NSDate* update_date;
	while (sqlite3_step(upgrade_select_statement) == SQLITE_ROW) {
		// The second parameter indicates the column index into the result set.
		int field = 0;
		pk = sqlite3_column_int(upgrade_select_statement, field++);
		remote_id = sqlite3_column_int(upgrade_select_statement, field++);
		char * val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			title = [NSString stringWithUTF8String:val];
		} else {
			title = @"";
		}
		
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			category = [NSString stringWithUTF8String:val];
		} else {
			category = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			repeat = [NSString stringWithUTF8String:val];
		}else {
			repeat = TYPE_REPEAT_NONE;
		}
		interval = sqlite3_column_int(upgrade_select_statement, field++);
		double date = sqlite3_column_double(upgrade_select_statement, field++);
		if (date != 0) {
			start_date = [NSDate dateWithTimeIntervalSince1970:date];
		}else {
			start_date = nil;
		}
		date = sqlite3_column_double(upgrade_select_statement, field++);
		if (date != 0) {
			end_date = [NSDate dateWithTimeIntervalSince1970:date];
		}else {
			end_date = nil;
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			text = [NSString stringWithUTF8String:val];
		}else {
			text = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			device_token = [NSString stringWithUTF8String:val];
		}else {
			device_token = @"";
		}
		date = sqlite3_column_double(upgrade_select_statement, field++);
		if (date != 0) {
			create_date = [NSDate dateWithTimeIntervalSince1970:date];
		}else {
			create_date = nil;
		}
		date = sqlite3_column_double(upgrade_select_statement, field++);
		if (date != 0) {
			update_date = [NSDate dateWithTimeIntervalSince1970:date];
		}else {
			update_date = nil;
		}
		
		int toField = 1;
		sqlite3_bind_int(upgrade_insert_statement, toField++, remote_id);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [title UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [category UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [repeat UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_int(upgrade_insert_statement, toField++, interval);
		sqlite3_bind_double(upgrade_insert_statement, toField++, [start_date timeIntervalSince1970]);
		sqlite3_bind_double(upgrade_insert_statement, toField++, [end_date timeIntervalSince1970]);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [text UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [device_token UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_double(upgrade_insert_statement, toField++, [create_date timeIntervalSince1970]);
		sqlite3_bind_double(upgrade_insert_statement, toField++, [update_date timeIntervalSince1970]);
		sqlite3_step(upgrade_insert_statement);
		sqlite3_reset(upgrade_insert_statement);
	}
	// Reset the statement for future use.
	sqlite3_reset(upgrade_select_statement);
	sqlite3_finalize(upgrade_select_statement);
	upgrade_select_statement = nil;
	sqlite3_reset(upgrade_insert_statement);
	sqlite3_finalize(upgrade_insert_statement);
	upgrade_insert_statement = nil;	
}

+ (void)finalizeStatements {
	//NSLog(@"finalizeStatements = ");
    if (insert_statement) {
		sqlite3_finalize(insert_statement);
		insert_statement = nil;
	}
    if (init_statement) {
		sqlite3_finalize(init_statement);
		init_statement = nil;
	}
	if (init_statement2) {
		sqlite3_finalize(init_statement2);
		init_statement2 = nil;
	}
    if (delete_statement) {
		sqlite3_finalize(delete_statement);
		delete_statement = nil;
	}
	if (update_statement) {
		sqlite3_finalize(update_statement);
		update_statement = nil;
	}
	if (hydrate_statement) {
		sqlite3_finalize(hydrate_statement);
		hydrate_statement = nil;
	}
    if (dehydrate_statement) {
		sqlite3_finalize(dehydrate_statement);
		dehydrate_statement = nil;
	}
	if (upgrade_check_table_statement) {
		sqlite3_finalize(upgrade_check_table_statement);
		upgrade_check_table_statement = nil;
	}
	if (upgrade_select_statement) {
		sqlite3_finalize(upgrade_select_statement);
		upgrade_select_statement = nil;
	}
	if (upgrade_insert_statement) {
		sqlite3_finalize(upgrade_insert_statement);
		upgrade_insert_statement = nil;
	}
}
@end

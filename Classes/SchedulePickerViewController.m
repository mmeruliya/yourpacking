//
//  SchedulePickerViewController.m
//  BabyPacking
//
//  Created by Jay Lee on 11/18/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import "SchedulePickerViewController.h"

#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)
#define SCREEN_HEIGHT ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.height : [[UIScreen mainScreen] bounds].size.width) - 40 

BabyPackingAppDelegate *mAppDelegate;

@implementation SchedulePickerViewController

@synthesize startDate = mStartDate;
@synthesize endDate = mEndDate;
@synthesize mPickerView,mBtnDonePicker;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
      //  layoutManager = [[LayoutManager alloc] initWithPortraitView:self.view andLandscapeNIB:@"SchedulePickerViewController_L"];
        //self = [super initWithNibName:@"RecalliPhoneView" bundle:nibBundleOrNil];
        
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
	
    mAppDelegate = (BabyPackingAppDelegate *) [[UIApplication sharedApplication]delegate];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 30, 30);
    btn.tag=1;
    [btn setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(buttonCancelDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    mBtnCancel = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(0, 0, 30, 30);
    btn2.tag=1;
    [btn2 setImage:[UIImage imageNamed:@"Done.png"] forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(buttonDoneDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    mBtnDone = [[UIBarButtonItem alloc] initWithCustomView:btn2];
    
	self.navigationItem.leftBarButtonItem = mBtnCancel;
	self.navigationItem.rightBarButtonItem = mBtnDone;
    mBtnDonePicker.hidden = YES;
	
	// set date picker default style
	mDatePicker.timeZone = [NSTimeZone systemTimeZone];
	mDatePicker.locale = [NSLocale systemLocale];
	mDatePicker.calendar = [NSCalendar currentCalendar];
   // mDatePicker.hidden = YES;
	
	// init date formatter
	mDateFormatter = [[NSDateFormatter alloc] init];
	[mDateFormatter setDateStyle:NSDateFormatterMediumStyle];
	[mDateFormatter setTimeStyle:kCFDateFormatterShortStyle];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
}

- (void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
    
    [mTableView reloadData];
    
   /* if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [layoutManager translateToPortraitForView:self.view withAnimation:NO];
        
       
    }
    else
    {
        [layoutManager translateToLandscapeForView:self.view withAnimation:NO];
    }*/
    
    [mPickerView setFrame:CGRectMake(0,SCREEN_HEIGHT-mPickerView.frame.size.height-100,SCREEN_WIDTH, mPickerView.frame.size.height)];
	
	mStartDateSelected = YES;
	[mTempStartDate release];
	mTempStartDate = [self.startDate retain];
	[mTempEndDate release];
	mTempEndDate = [self.endDate retain];
	[mDatePicker setDate:mTempStartDate animated:NO];
	mStartTimeLabel.text = [mDateFormatter stringFromDate:mTempStartDate];
	mEndTimeLabel.text = [mDateFormatter stringFromDate:mTempEndDate];
	if ([mTempEndDate compare:mTempStartDate] == NSOrderedAscending) {
		mStartTimeLabel.textColor = [UIColor redColor];
		mEndTimeLabel.textColor = [UIColor redColor];
	} else {
		mStartTimeLabel.textColor = [UIColor colorWithRed:0.22 green:0.329 blue:0.529 alpha:1.0];
		mEndTimeLabel.textColor = [UIColor colorWithRed:0.22 green:0.329 blue:0.529 alpha:1.0];
	}
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
	[mTableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[mTempStartDate release];
	[mTempEndDate release];
	[mDateFormatter release];
	[mStartDate release];
	[mEndDate release];
    [super dealloc];
}

#pragma mark Button click methods

- (IBAction)buttonCancelDidClick:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)buttonDoneDidClick:(id)sender {
	self.startDate = mTempStartDate;
	self.endDate = mTempEndDate;
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)switchValueDidChange:(id)sender {
	
	UISwitch* switcher = sender;
	if (switcher.on) {
		mDatePicker.datePickerMode = UIDatePickerModeDate;
		[mDateFormatter setDateStyle:NSDateFormatterMediumStyle];
		[mDateFormatter setTimeStyle:kCFDateFormatterNoStyle];
	} else {
		mDatePicker.datePickerMode = UIDatePickerModeDateAndTime;
		[mDateFormatter setDateStyle:NSDateFormatterMediumStyle];
		[mDateFormatter setTimeStyle:kCFDateFormatterShortStyle];
	}
	mStartTimeLabel.text = [mDateFormatter stringFromDate:mTempStartDate];
	mEndTimeLabel.text = [mDateFormatter stringFromDate:mTempEndDate];
}

- (IBAction)pickerValueDidChange:(id)sender {
	
	UIDatePicker* picker = sender;
	
	if (mStartDateSelected) {
		[mTempStartDate release];
		mTempStartDate = [picker.date retain];
		mStartTimeLabel.text = [mDateFormatter stringFromDate:picker.date];
	} else {
		[mTempEndDate release];
		mTempEndDate = [picker.date retain];
		mEndTimeLabel.text = [mDateFormatter stringFromDate:picker.date];
	}
	
	if ([mTempEndDate compare:mTempStartDate] == NSOrderedAscending) {
		mStartTimeLabel.textColor = [UIColor redColor];
		mEndTimeLabel.textColor = [UIColor redColor];
	} else {
		mStartTimeLabel.textColor = [UIColor colorWithRed:0.22 green:0.329 blue:0.529 alpha:1.0];
		mEndTimeLabel.textColor = [UIColor colorWithRed:0.22 green:0.329 blue:0.529 alpha:1.0];
	}
}

-(IBAction)donePicker:(id)sender
{
    [mPickerView removeFromSuperview];
    mBtnDonePicker.hidden = YES;
}




#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (mAppDelegate.startDate == YES)
        return 1;
    else
        return 2;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
	
	UITableViewCell* cell = nil;
	if (indexPath.row == 0) {
        if (mAppDelegate.startDate == YES)
            cell = mStartsCell;
        else
            cell = mEndsCell;
	} else if (indexPath.row == 1) {
		cell = mSwitchCell;
	} else {
		cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
	}
	
    return cell;
}

// Override to support row selection in the table view.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// Navigation logic may go here -- for example, create and push another view controller.
	if (indexPath.row == 0) {
               
        [mPickerView setFrame:CGRectMake(0,SCREEN_HEIGHT-mPickerView.frame.size.height-100,SCREEN_WIDTH, mPickerView.frame.size.height)];
        mBtnDonePicker.hidden = NO;
        [self.view addSubview:mPickerView];
        if (mAppDelegate.startDate == YES)
        {
            mStartDateSelected = YES;
            if (mTempStartDate) mDatePicker.date = mTempStartDate;
        }
        else
        {
            mStartDateSelected = NO;
            if (mTempEndDate) mDatePicker.date = mTempEndDate;
        }
        
	} else if (indexPath.row == 1) {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
		if (mStartDateSelected) {
			mStartsCell.selected = YES;
			mEndsCell.selected = NO;
		} else {
			mStartsCell.selected = NO;
			mEndsCell.selected = YES;
		}
//        mBtnDonePicker.hidden = NO;
//        [mPickerView setFrame:CGRectMake(0,SCREEN_HEIGHT-mPickerView.frame.size.height,SCREEN_WIDTH, mPickerView.frame.size.height)];
//        [self.view addSubview:mPickerView];        //[self.view addSubview:mPickerView];
//		 mStartDateSelected = NO;
//		if (mTempEndDate) mDatePicker.date = mTempEndDate;
	} else if (indexPath.row == 2) {
		[tableView deselectRowAtIndexPath:indexPath animated:NO];
		if (mStartDateSelected) {
			mStartsCell.selected = YES;
			mEndsCell.selected = NO;
		} else {
			mStartsCell.selected = NO;
			mEndsCell.selected = YES;
		}
	}
}

/*
#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
	
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
         [layoutManager translateToPortraitForView:self.view withAnimation:YES];
    }
    else
    {
        [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
    }
    // }
    [mPickerView setFrame:CGRectMake(0,SCREEN_HEIGHT-mPickerView.frame.size.height-100,SCREEN_WIDTH, mPickerView.frame.size.height)];

}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}*/


@end

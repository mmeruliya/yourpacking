//
//  CheckListViewController.m
//  BabyPacking
//
//  Created by Gary He on 5/21/09.
//  Copyright 2009 test. All rights reserved.
//
#import "BabyPackingAppDelegate.h"
#import "CheckListViewController.h"
#import "AddItemViewController.h"
#import "Constant.h"
#import "PackingItem.h"
#import "CSTableViewCell.h"
#import "DetailItemViewController.h"
#import "ProgressBar.h"
#import "RootViewController.h"

@interface CheckListViewController(Private)

- (void) initPlayers;
- (void) sendEmail;
- (void) updateProgressBarAnimated:(BOOL) animated;
- (PackingItem*) itemAtIndexPath:(NSIndexPath*)indexPath;
@end


@implementation CheckListViewController

@synthesize uncheckedItemArray;
@synthesize homeController;
@synthesize currentList,customListArray;
@synthesize editList;
@synthesize canEmail;
@synthesize player1;
@synthesize player2;
@synthesize sections;
@synthesize move,edit,moveGroup;

- (id)initWithNibName:(NSString *)nibName bundle:(NSBundle *)nibBundle {
	if (self = [super initWithNibName:nibName bundle:nibBundle]) {
		BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
		if (appDelegate.soundOn) {
			[self initPlayers];
		}
		[[NSNotificationCenter defaultCenter] addObserver:self 
												 selector:@selector(fontChanged:) 
													 name:NOTIFY_FONT_CHANGED 
												   object:nil];
	}
	return self;
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:NOTIFY_FONT_CHANGED
												  object:nil];
	[sections release];
	[bgImageView release];
	[bgBorderView release];
	
	[myTableView release];
	[doneButton release];
	[emailButton release];
	[resetButton release];
	[filterButton release];
	[addButton release];
	[copyButton release];
	
	[editToolBar release];
	[editButton release];
	[ascButton release];
	[descButton release];
	[reorderButton release];
	
	[statusLabel release];
	[progressBar release];
	
	[checkImg release];
	[uncheckImg release];
	
	[inImg release];
	[outImg release];
	
	[addImg release];
	
	
	[uncheckedItemArray release];
	[currentList release];
	
	[player1 release];
	[player2 release];
    [super dealloc];
}

- (void) fontChanged:(NSNotification*)notification {
	BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
	NSString *customFontName = [appDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	statusLabel.font = [UIFont fontWithName:customFontName size:fontSize];
}


/*
 - (id)initWithStyle:(UITableViewStyle)style {
 // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 if (self = [super initWithStyle:style]) {
 }
 return self;
 }
 */


- (void)viewDidLoad {
    [super viewDidLoad];
	
	filterButton.title = TITLE_CLEAR_CHECK;
	bgImageView.alpha = BG_PHOTO_ALPHA;
	editToolBar.alpha = 0.0;
	self.sections = [[NSMutableArray alloc] initWithCapacity:0];
	if (currentList) {
		
		BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
		
		
		if (appDelegate.photoBgOn && [currentList hasIcon]) {
			
			bgImageView.image = currentList.photo;
			bgImageView.alpha = 0.5;
			bgBorderView.alpha = 1.0;
			if(currentList.sex == BabySexBoy) {
				if (TARGET_VERSION == VERSION_BABY_PACKING)
					bgBorderView.image = [UIImage imageNamed:BLUE_BORDER];
				else
					bgBorderView.image = [UIImage imageNamed:BACKGROUND_BORDER_BLUE];
			} else if (currentList.sex == BabySexGirl){
				if (TARGET_VERSION == VERSION_BABY_PACKING)
					bgBorderView.image = [UIImage imageNamed:PINK_BORDER];
				else 
					bgBorderView.image = [UIImage imageNamed:BACKGROUND_BORDER_PINK];
			}else{
				bgBorderView.image = [UIImage imageNamed:BACKGROUND_BORDER_GRAY];
			}
			
		} else if (appDelegate.colorBgOn) {
			bgImageView.alpha = 1.0;
			if(currentList.sex == BabySexBoy) {
				if (TARGET_VERSION == VERSION_BABY_PACKING)
					bgImageView.image = [UIImage imageNamed:BLUE_BEAR];
				else
					bgImageView.image = [UIImage imageNamed:BACKGROUND_BLUE];
			} else if(currentList.sex == BabySexGirl) {
				if (TARGET_VERSION == VERSION_BABY_PACKING)
					bgImageView.image = [UIImage imageNamed:PINK_BEAR];
				else
					bgImageView.image = [UIImage imageNamed:BACKGROUND_PINK];
			} else {
				if (TARGET_VERSION == VERSION_BABY_PACKING)
					bgImageView.image = [UIImage imageNamed:GRAY_BEAR];
				else
					bgImageView.image = [UIImage imageNamed:BACKGROUND_GRAY];
			}
		}
		
		// Split items into groups
		if([self groupItems]){
			reorderGroupButton.enabled = YES;
			for (PackingItem *item in currentList.itemArray) {
				NSString *from = item.custom;
				if (!from || [@"" isEqualToString:from]) {
					// the data may be from version older than 1.3.1
					// search the group from categories
					NSArray* categoryListInAppData = [appDelegate.appDict objectForKey:K_CATEGORY_LIST];
					BOOL found = NO;
					for (NSDictionary* category in categoryListInAppData) {
						NSArray* items = [category objectForKey:K_LIST_ITEMS];
						for (NSDictionary* each in items) {
							NSString* name = [each objectForKey:K_ITEM_NAME];
							if ([name isEqualToString:item.name]) {
								found = YES;
								item.custom = [category objectForKey:K_ITEM_NAME];
								break;
							}
						}
						
						if (found) break;
					}
					if (!found) {
						item.custom = @"FromCustom";
					}
				}
			}
		}
		else{
			reorderGroupButton.enabled = NO;
		}
	}
	
	//myTableView.backgroundColor = BOY_BG_COLOR;
	checkImg = [[UIImage imageNamed:CHECK_IMAGE] retain];
	uncheckImg = [[UIImage imageNamed:UNCHECK_IMAGE] retain];
	
	inImg = [[UIImage imageNamed:IN_IMAGE] retain];
	outImg = [[UIImage imageNamed:OUT_IMAGE] retain];
	
	addImg = [[UIImage imageNamed:@"out.png"] retain];
	
	
	if (currentList.age < 2 && currentList.sex == 0)
		self.navigationItem.leftBarButtonItem = doneButton;
	else{
		// self.navigationItem.leftBarButtonItem = self.editButtonItem;
		self.navigationItem.rightBarButtonItem = editButton;
		self.navigationItem.leftBarButtonItem = doneButton;
	}
	
	if (canEmail)
		emailButton.enabled=YES;
	else {
		emailButton.enabled=NO;
	}
	if(currentList.totalCount == 0)
		filterButton.enabled = NO;
	else {
		filterButton.enabled = YES;
	}

	
	statusLabel.textColor = COLOR_HEADER_TITLE;
	
	
	[self fontChanged:nil];
	
	statusLabel.backgroundColor = COLOR_STATUS_LABEL_BG;
	//statusLabel.alpha = 0.6;
	
	[self updateProgressBarAnimated:NO];

}

- (void) initPlayers {
	{
		NSString *soundFilePath =
		[[NSBundle mainBundle] pathForResource:CHECK_AUDIO_NAME
										ofType:CHECK_AUDIO_EXT];
		
		NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
		
		AVAudioPlayer *newPlayer =
		[[AVAudioPlayer alloc] initWithContentsOfURL: fileURL
											   error: nil];
		[fileURL release];
		newPlayer.volume = DEF_VOLUME;
		self.player1 = newPlayer;
		[newPlayer release];
		
	}
	
	{
		NSString *soundFilePath =
		[[NSBundle mainBundle] pathForResource:PROGRESS_AUDIO_NAME
										ofType:PROGRESS_AUDIO_EXT];
		
		NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
		
		AVAudioPlayer *newPlayer =
		[[AVAudioPlayer alloc] initWithContentsOfURL: fileURL
											   error: nil];
		[fileURL release];
		newPlayer.volume = DEF_VOLUME;
		self.player2 = newPlayer;
		[newPlayer release];
	}
	[self.player1 prepareToPlay];
	[self.player1 setDelegate: self];
	[self.player2 prepareToPlay];
	[self.player2 setDelegate: self];
	
}

- (IBAction) buttonAction:(id)sender {
	
	if (sender == doneButton) {
		if([self groupItems]){
			showUncheckedItem = NO;
			NSMutableArray *newlist = [[NSMutableArray alloc] initWithCapacity:0];
			for(int i = 0; i < ([sections count]/2); i++){
				for(PackingItem *item in currentList.itemArray){
					if([item.custom isEqualToString:[sections objectAtIndex:i*2]]){
						[newlist addObject:item];
						
					}
				}
				
			}
			currentList.itemArray = newlist;
			[newlist release];
		}
		if(self.move || self.moveGroup){
			[PackingItem deleteByListId:currentList.pk];
//			for (PackingItem *item in currentList.itemArray) {
//				[item deleteData];
//			}
			for (PackingItem *item in currentList.itemArray) {
				item.dirty = YES;
				[item insertData];
			}
		}
		
		for (PackingItem *item in currentList.itemArray) {
			item.listId	 = currentList.pk;
			[item saveData];
		}		
		currentList.update = [NSDate date];
		
		// will do nothing if this is a ready made list
		
		//NSLog(@"currentList updateData >> checkedCount= %d", currentList.checkedCount);
		[currentList updateItemsData];
		[currentList updateCheckStatus];
		
		[currentList updateDataWithoutIconAndPhoto];
		BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
		if (appDelegate.soundOn) {
			self.view.userInteractionEnabled = NO;
			[player2 play];
			[self updateProgressBarAnimated:YES];
		} else {
			if (self.homeController == nil)
				[self.navigationController popViewControllerAnimated:YES];
			else
				[self.navigationController popToViewController:homeController animated:YES];
		}
		
	} else if (sender == emailButton) {
		[self sendEmail];
	} else if (sender == resetButton) {
		for (PackingItem *item in currentList.itemArray) {
			[item setChecked:NO];
		}
		[myTableView reloadData];
		
		[self updateProgressBarAnimated:NO];
	} else if (sender == filterButton) {
		if (showUncheckedItem) {
			filterButton.title = TITLE_CLEAR_CHECK;
			showUncheckedItem = NO;
			self.uncheckedItemArray = nil;
		} else {
			filterButton.title = TITLE_KEEP_CHECK;
			showUncheckedItem = YES;
			self.uncheckedItemArray = [NSMutableArray arrayWithCapacity:[currentList.itemArray count]];
			for(PackingItem *item in currentList.itemArray) {
				if (!item.checked) {
					[uncheckedItemArray addObject:item];
				}
			}
		}
		[myTableView reloadData];
	}else if (sender == editButton){
		self.edit = ~edit;
		if(self.edit){
			[UIView beginAnimations:nil context:nil];
			[UIView setAnimationRepeatCount:0];
			editToolBar.alpha = 1;
			[UIView commitAnimations];
			//editButton.title = @"Hide";
			showUncheckedItem = NO;
			if(!showUncheckedItem)
				filterButton.title = TITLE_CLEAR_CHECK;
			[myTableView reloadData];
			filterButton.enabled = NO;

		}
		else{
			[UIView beginAnimations:nil context:nil];
			[UIView setAnimationRepeatCount:0];
			editToolBar.alpha = 0.0;
			[UIView commitAnimations];
			//editButton.title = @"More";
			if(!move && !moveGroup)
			filterButton.enabled = YES;
			
		}

	}else if (sender == addButton){
		
		AddItemViewController *controller = [[AddItemViewController alloc] initWithNibName:@"AddItemViewController" bundle:nil];
		controller.currentList = self.currentList;
		controller.isFromCheckView = YES;
		UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
		[self.navigationController presentModalViewController:navController  animated:YES];
		[controller release];
		[navController release];
		
	}else if (sender == reorderButton){
		self.move = ~move;
		[myTableView setEditing:move animated:YES];
		[super setEditing:move animated:YES];

	}else if (sender == reorderGroupButton){
		self.move = NO;
		self.moveGroup = ~moveGroup;
		[myTableView setEditing:moveGroup animated:YES];
		[super setEditing:moveGroup animated:YES];
		if(moveGroup){
			reorderButton.enabled = NO;
			ascButton.enabled = NO;
			descButton.enabled = NO;
		}
		else{
			reorderButton.enabled = YES;
			ascButton.enabled = YES;
			descButton.enabled = YES;
			
		}
		for(int i = 0; i<[sections count]; i += 2){
			NSString *hide = @"hide";
			[self.sections replaceObjectAtIndex:i+1 withObject:hide];
		}
		[myTableView reloadData];
		
	}else if (sender == descButton){
		
		[self reorderListByDesc];
		[myTableView reloadData];
		
	}else if (sender == ascButton){
		
		[self reorderListByAsc];
		[myTableView reloadData];
		
	}else if(sender == copyButton){
		NSString *meg = [NSString stringWithFormat:@"A new packing list called ' %@ copy' will be created.",currentList.name];
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
														message:meg
													   delegate:self
											  cancelButtonTitle:@"Cancel" otherButtonTitles: @"OK",nil];
		[alert show];	
		[alert release]; 
	}
	
}

//- (void)setEditing:(BOOL)editing animated:(BOOL)animate {
//	[super setEditing:editing animated:animate];
//	myTableView.editing = editing;
////	[myTableView reloadData];
//}

-(void)reorderListByDesc{

	NSMutableArray *items = currentList.itemArray;

	for(int m = 0 ; m < [items count]; m++){
		for(int n = 0 ; n < [items count]-m-1; n++){
			PackingItem*item1 = [items objectAtIndex:n];
			PackingItem*item2 = [items objectAtIndex:n+1];
			NSString *name1 = item1.name;
			NSString *name2 = item2.name;
			if([name1 compare: name2] == NSOrderedAscending){
				PackingItem *tempItem = [item1 retain];
				[currentList.itemArray replaceObjectAtIndex:n withObject:item2];
				[currentList.itemArray replaceObjectAtIndex:n+1 withObject:tempItem];

			}

		}
		
	}
}

-(void)reorderListByAsc{
	
	NSMutableArray *items = currentList.itemArray;
	
	for(int m = 0 ; m < [items count]; m++){
		for(int n = 0 ; n < [items count]-m-1; n++){
			PackingItem*item1 = [items objectAtIndex:n];
			PackingItem*item2 = [items objectAtIndex:n+1];
			NSString *name1 = item1.name;
			NSString *name2 = item2.name;
			if([name1 compare: name2] == NSOrderedDescending){
				PackingItem *tempItem = [item1 retain];
				[currentList.itemArray replaceObjectAtIndex:n withObject:item2];
				[currentList.itemArray replaceObjectAtIndex:n+1 withObject:tempItem];
				
			}
			
		}
		
	}
}

-(BOOL) groupItems{
	if(currentList.age < 2 && currentList.sex == 0)
		return FALSE;
	BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
	if([appDelegate.addItemsByGroup boolValue] && TARGET_VERSION != VERSION_BABY_PACKING)
		return TRUE;
	else
		return FALSE;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	for (PackingItem *item in currentList.itemArray) {
		item.listId	 = currentList.pk;
		[item saveData];
	}
	if(showUncheckedItem){
		[uncheckedItemArray removeAllObjects];
		for(PackingItem *item in currentList.itemArray) {
			if (!item.checked) {
				[uncheckedItemArray addObject:item];
			}
		}
	}
	
  	[myTableView reloadData];
	
}

/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	
	//remove from memory
	currentList.photo = nil;

	// save the order of cuttrent list
	
}

/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}
#pragma mark Alert methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 1) {
		PackingList *copyList = [currentList copyCurrentList:currentList];
		[copyList insertData];
		for(PackingItem *item in copyList.itemArray){
			item.listId	 = copyList.pk;
			item.dirty = YES;
			[item insertData];
		}
		BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
		NSMutableArray * coutemsList = [PackingList findAllWithoutPhoto];
		if([coutemsList count] == 7 || [coutemsList count] == 20)
		{
			NSString *meg = [NSString stringWithFormat:@"You have created %d custom packing lists. How do you like this app so far? If you are happy, please consider writing a (nice) review at the App Store. Also, feel free to send us an email for any feature request or issues.",[coutemsList count]];
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations" message:meg
														   delegate:appDelegate cancelButtonTitle:@"Close popup" otherButtonTitles: @"Write a review",@"Send an email",nil];
			[alert show];	
			[alert release]; 
		}
		[customListArray insertObject:copyList atIndex:0];
		if (self.homeController == nil)
			[self.navigationController popViewControllerAnimated:YES];
		else
			[self.navigationController popToViewController:homeController animated:YES];
	}

}
#pragma mark Table view methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	
	
	// update immediately
	BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
	if (!appDelegate.soundOn) {
		[self updateProgressBarAnimated:NO];
	} else {
		// update when click "done"
		// [self updateProgressBarAnimated:YES];
	}
	if(![self groupItems])
		return 1;
	else{
		if(moveGroup)
			return 1;
		else{
			for(PackingItem *item in currentList.itemArray){
				NSString *from = item.custom;
				if(![self.sections containsObject:from] ){
					[self.sections addObject:from];
					[self.sections addObject:@"rows"];
				}
			}
			if([sections count] > 0)
				return [sections count]/2;
			else
				return 1;
		}
	}
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	
	if(section != 0){
		UIView *header = [[[UIView alloc] initWithFrame:CGRectMake(0,0,0,-2)] autorelease];
		header.alpha = 0;
		return header;
	}
	else {
		return nil;
	}
	
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	return 10;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if(![self groupItems]){
		if (showUncheckedItem) {
			if (uncheckedItemArray) {
				return [uncheckedItemArray count];
			}
		} else {
			if (currentList) {
				return [currentList.itemArray count];
			}
		}
		return 0;
		
	}else{
		if (showUncheckedItem) {
			if (uncheckedItemArray) {
				int i = 0;
				for(PackingItem *item in uncheckedItemArray){
					if([item.custom isEqualToString:[self.sections objectAtIndex:section*2]]){
						if([[self.sections objectAtIndex:section*2+1] isEqualToString:@"hide"])
							return 1;
						i++;
					}
					
				}
				return i+1;
			}
		}
		else{
			if(moveGroup){
				return [sections count]/2;
			}
			if([currentList.itemArray count] > 0){
				int i = 0;
				for(PackingItem *item in currentList.itemArray){
					if([item.custom isEqualToString:[self.sections objectAtIndex:section*2]]){
						if([[self.sections objectAtIndex:section*2+1] isEqualToString:@"hide"])
							return 1;
						i++;
					}
					
				}
				return i+1;
			}
		}
		return 0;
	}
}

- (PackingItem*) itemAtIndexPath:(NSIndexPath*)indexPath {
	PackingItem *item = nil;
	if(![self groupItems]){
		if(showUncheckedItem)
			item = [uncheckedItemArray objectAtIndex:indexPath.row];
		else
			item = [currentList.itemArray objectAtIndex:indexPath.row];
	}else{
		
		if (showUncheckedItem) {
			int i = 0;
			for(PackingItem *item1 in uncheckedItemArray){
				if([item1.custom isEqualToString:[sections objectAtIndex:((indexPath.section)*2)]]){
					i++;
					if(i == indexPath.row){
						item = item1;
						break;
					}
					
				}
				
			}
		} 
		else {
			int i = 0;
			for(PackingItem *item1 in currentList.itemArray){
				if([item1.custom isEqualToString:[sections objectAtIndex:((indexPath.section)*2)]]){
					i++;
					if(i == indexPath.row){
						item = item1;
						break;
					}
				}
				
			}
		}
	}
	
	return item;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    static NSString *CellIdentifier = @"Cell";
	NSLog(@"cellForRowAtIndexPath");
    CSTableViewCell *cell = (CSTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	cell.hidden = NO;
    if (cell == nil) {
        cell = [[[CSTableViewCell alloc] initWithCSStyle:CSTableViewCellStyleValue1  reuseIdentifier:CellIdentifier] autorelease];
		cell.detailTextLabel.textColor = [UIColor darkGrayColor];
		cell.detailTextLabel.textAlignment = UITextAlignmentLeft;
		cell.backgroundColor = [UIColor clearColor];

    }

	PackingItem *item = nil;
	if(![self groupItems]) {
		if (showUncheckedItem) {
			item = [uncheckedItemArray objectAtIndex:indexPath.row];
		} else {
			item = [currentList.itemArray objectAtIndex:indexPath.row];
		}
		
	} else {
		
		if(moveGroup){
			NSString *s = [self.sections objectAtIndex:((indexPath.row)*2)];
			if([s isEqualToString:@"FromCustom"])
				s = @"Other Items";
			cell.textLabel.text = s;
			int n = 0;
			int m = 0;
			for(PackingItem *item1 in currentList.itemArray){
				if([item1.custom isEqualToString:[self.sections objectAtIndex:((indexPath.row)*2)]]){
					n++;
					if(item1.checked)
						m++;
				}
				
			}
			NSString *detail = [NSString stringWithFormat:@"(%d/%d)  ",m,n];
			cell.detailTextLabel.text = detail;
			if([[self.sections objectAtIndex:indexPath.row*2+1] isEqualToString: @"rows"])
				cell.image = outImg;
			else
				cell.image = inImg;
			
			cell.accessoryType = UITableViewCellAccessoryNone;
			return cell;
		}
			
		
		if(indexPath.row == 0){
			
			NSString *s = [self.sections objectAtIndex:((indexPath.section)*2)];
			if([s isEqualToString:@"FromCustom"])
				s = @"Other Items";
			cell.textLabel.text = s;
			int n = 0;
			int m = 0;
			for(PackingItem *item1 in currentList.itemArray){
				if([item1.custom isEqualToString:[self.sections objectAtIndex:((indexPath.section)*2)]]){
					n++;
					if(item1.checked)
						m++;
				}
				
			}
			NSString *detail = [NSString stringWithFormat:@"(%d/%d)  ",m,n];
			cell.detailTextLabel.text = detail;
			if([[self.sections objectAtIndex:indexPath.section*2+1] isEqualToString: @"rows"])
				cell.image = outImg;
			else
				cell.image = inImg;
			
			cell.accessoryType = UITableViewCellAccessoryNone;
			if(move)
				cell.editing = NO;
				return cell;		
		}
		if (showUncheckedItem) {
			int i = -1;
			for(PackingItem *item1 in uncheckedItemArray){
				if([item1.custom isEqualToString:[self.sections objectAtIndex:((indexPath.section)*2)]]){
					i++;
					if(i == indexPath.row-1){
						item = item1;
						break;
					}
				}
			}
		}
		else{
			int i = -1;
			for(PackingItem *item1 in currentList.itemArray){
				if([item1.custom isEqualToString:[self.sections objectAtIndex:((indexPath.section)*2)]]){
					i++;
					if(i == indexPath.row-1){
						item = item1;
						break;
					}
				}
			}
		}
		
	}
	
	
	cell.textLabel.text = item.name;
	[cell.imageView setFrame:CGRectMake(6,7,30,30)];
	if (currentList.age < 2 && currentList.sex == 0){
		if([item hasTip])
			cell.detailTextLabel.text = @"*";
		else
			cell.detailTextLabel.text = nil;
	}
	else{
		if ([item hasTip]){
			if (item.qty && [item.qty length] > 0)
				cell.detailTextLabel.text = [NSString stringWithFormat:@"*x%@",item.qty];
			else
				cell.detailTextLabel.text = @"*";
		}
		
		else if( item.qty && [item.qty length] > 0)
			cell.detailTextLabel.text = [NSString stringWithFormat:@"x%@", item.qty];
		else
			cell.detailTextLabel.text = nil;
	}
	
	
	if ([item isChecked]) {
		cell.image = checkImg;
	} else {
		cell.image = uncheckImg;
	}
	cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
	if([cell.textLabel.text isEqualToString:movedCell.textLabel.text] ){
		movedCell.hidden = YES;
		movedCell = nil;
	}
	cell.hidden = NO;
		return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if(moveGroup)
		return;
	
	BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
	if (appDelegate.soundOn) {
		//self.view.userInteractionEnabled = NO;
		[player1 play];
	}
	
	PackingItem *item = nil;
	if(![self groupItems]){
		if (showUncheckedItem) {
			item = [uncheckedItemArray objectAtIndex:indexPath.row];
		} else {
			item = [currentList.itemArray objectAtIndex:indexPath.row];
		}
	}
	else{
		if(indexPath.row == 0){
			
			if([[self.sections objectAtIndex:indexPath.section*2+1] isEqualToString: @"rows"]){
				[self.sections removeObjectAtIndex:indexPath.section*2+1];
				[self.sections insertObject:@"hide" atIndex:indexPath.section*2+1];
				
				
			}
			
			else{
				[self.sections removeObjectAtIndex:indexPath.section*2+1];
				[self.sections insertObject:@"rows" atIndex:indexPath.section*2+1];
			}
			[tableView reloadData];
			
		}
		//			return;
		if (showUncheckedItem) {
			int i = 0;
			for(PackingItem *item1 in uncheckedItemArray){
				if([item1.custom isEqualToString:[sections objectAtIndex:((indexPath.section)*2)]]){
					i++;
					if(i == indexPath.row){
						item = item1;
						break;
					}
					
				}
				
			}
		} 
		else {
			int i = 0;
			for(PackingItem *item1 in currentList.itemArray){
				if([item1.custom isEqualToString:[sections objectAtIndex:((indexPath.section)*2)]]){
					i++;
					if(i == indexPath.row){
						item = item1;
						break;
					}
				}
				
			}
		}
	}
	[item reverseCheckStatus];
	[tableView reloadData];
}

- (void) updateProgressBarAnimated:(BOOL) animated {
	[currentList updateCheckStatus];
	
	statusLabel.text = [NSString stringWithFormat:@"%d out of %d items checked", currentList.checkedCount, currentList.totalCount];
	CGRect rect = progressBar.bounds;
	if (currentList.totalCount == 0) {
		progressBar.progress = 0;
	} else {
		progressBar.progress = (float)currentList.checkedCount / currentList.totalCount;
	}
	rect.size.width = rect.size.width * progressBar.progress;
	if (animated) {
		[UIView beginAnimations:@"" context:nil];
		[UIView setAnimationDuration:2];
		[UIView setAnimationRepeatCount:1];
	}
	progressImageView.frame = rect;
	[progressBar setNeedsDisplay];
	if (animated) {
		[UIView commitAnimations];
	}
	
}
-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath*)indexPath{
	if (currentList.age < 2 && currentList.sex == 0)
		return NO;
	if([self groupItems]){
		if(indexPath.row == 0){
			if(move)
				return NO;
			else
				return YES;
		}
		else
			return YES;
	}
	else
		return YES;
	
}

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath{
	
	//if (!movedCell) movedCell = (id)[tableView cellForRowAtIndexPath:sourceIndexPath];
	if(proposedDestinationIndexPath.section == sourceIndexPath.section){
		if([self groupItems]){
			if(move && proposedDestinationIndexPath.row == 0)
				return sourceIndexPath;
		}
		return proposedDestinationIndexPath;
	}
	else {
		return sourceIndexPath;
	}
}               

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath{
	if(fromIndexPath == toIndexPath){
//		NSLog(@"movedCell 1 = 0x%x", movedCell);
//		if (![[tableView visibleCells] containsObject:movedCell]) {
//			NSLog(@"movedCell = 0x%x", movedCell);
//			movedCell.hidden = YES;
//			movedCell = nil;
//		} else {
//			NSArray *array = [tableView indexPathsForVisibleRows];
//			for (NSIndexPath* indexPath in array) {
//				NSLog(@"(%d, %d)", indexPath.section, indexPath.row);
//			}
//			
//			CGRect rect = [tableView rectForRowAtIndexPath:toIndexPath];
//			NSLog(@"cell rect: (%f, %f, %f, %f)", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
//		}
		[tableView scrollToRowAtIndexPath:fromIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
		return;
		
	}
	if(![self groupItems]){
		PackingItem *item = [[currentList.itemArray objectAtIndex:fromIndexPath.row] retain];
		[currentList.itemArray removeObjectAtIndex:fromIndexPath.row];
		[currentList.itemArray insertObject:item atIndex:toIndexPath.row];
		[item release];
	}
	else{
		if(move){
			int i = 0;
			int n = -1;
			for(PackingItem *item1 in currentList.itemArray){
				n++;
				if([item1.custom isEqualToString:[self.sections objectAtIndex:((fromIndexPath.section)*2)]]){
					i++;
					if(i == fromIndexPath.row){
						PackingItem *item = [item1 retain];
						NSMutableArray *items = [currentList.itemArray copy];
						[currentList.itemArray removeObjectAtIndex:n];
						int j = 0;
						int m = -1;
						for(PackingItem *item2 in items){
							m++;
							if([item2.custom isEqualToString:[self.sections objectAtIndex:((fromIndexPath.section)*2)]]){
								j++;
								if(j == toIndexPath.row){
									[currentList.itemArray insertObject:item atIndex:m];
									[item release];
									[items release];
									return;
								}
							}
						}
					}
				}
			}
		}
		
		else if(moveGroup){
			NSString *from = [sections objectAtIndex:fromIndexPath.row*2];
			NSMutableArray *tempArray = [[NSMutableArray alloc] initWithArray:0];
			for(PackingItem* item in currentList.itemArray){
				if([item.custom isEqualToString:from]){
					[tempArray addObject:item];
				}
			}			
			for(PackingItem *item in tempArray){
				[currentList.itemArray removeObject:item];
			}
			
			int j = -1;
			int k = -1;
			NSMutableArray *tempSectionArray = [[NSMutableArray alloc] initWithArray:0];
			for(PackingItem *item in currentList.itemArray){
				j++;
				NSString *kind = item.custom;
				if(![tempSectionArray containsObject:kind]){
					k++;
					[tempSectionArray addObject:kind];
				}

				if(k == toIndexPath.row){	
					int o = -1;
					for(PackingItem * item in tempArray){
						o++;
							[currentList.itemArray insertObject:item atIndex:j+o];
					}
					[tempArray release];
					[tempSectionArray release];
					
					NSString *group = [[sections objectAtIndex:fromIndexPath.row*2] retain];
					[sections removeObjectAtIndex:fromIndexPath.row*2];
					[sections removeObjectAtIndex:fromIndexPath.row*2];
					[sections insertObject:group atIndex:((toIndexPath.row)*2)];
					[sections insertObject:@"hide" atIndex:((toIndexPath.row)*2+1)];
					[group release];					
					break;
				}
				else if(toIndexPath.row == ([sections count]-1)/2){
						for(PackingItem * item in tempArray){
							[currentList.itemArray addObject:item];
						}
						[tempArray release];
						[tempSectionArray release];
						
						NSString *group = [[sections objectAtIndex:fromIndexPath.row*2] retain];
						[sections removeObjectAtIndex:fromIndexPath.row*2];
						[sections removeObjectAtIndex:fromIndexPath.row*2];
					    [sections insertObject:group atIndex:((toIndexPath.row)*2)];
						[sections insertObject:@"hide" atIndex:((toIndexPath.row)*2+1)];
						[group release];
						break;	
				}
			}
		}	
	}
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
	if (currentList.age < 2 && currentList.sex == 0){
		DetailItemViewController *controller = [[DetailItemViewController alloc] initWithNibName:@"DetailItemViewController" bundle:nil];
		controller.packingItem = [self itemAtIndexPath:indexPath];
		[self.navigationController pushViewController:controller  animated:YES];
		[controller release];
	}else{
		AddItemViewController *controller = [[AddItemViewController alloc] initWithNibName:@"AddItemViewController" bundle:nil];
		controller.editingItem = [self itemAtIndexPath:indexPath];
		controller.currentList = currentList;
		controller.isFromCheckView = YES;
		[self.navigationController pushViewController:controller  animated:YES];
		[controller release];
	}
	
}	



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
	if (currentList.age == 0 && currentList.sex == 0)
		return NO;
	if([self groupItems]){
		if(indexPath.row == 0){
			if(moveGroup)
				return YES;
			else
				return NO;
		}
		else
			return YES;
	}
	else
		return YES;
}


-(UITableViewCellEditingStyle)tableView: (UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{

	if(move){
		return UITableViewCellEditingStyleNone;
	}
	else{
		if(moveGroup)
			return UITableViewCellEditingStyleNone;
		return UITableViewCellEditingStyleDelete;
	}
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
	    PackingItem *item = [self itemAtIndexPath:indexPath];
		[item deleteData];
		// [currentList.itemArray removeObjectAtIndex:indexPath.row];
		if(showUncheckedItem){
			//			[uncheckedItemArray removeObjectAtIndex:indexPath.row];
			if([item.custom isEqualToString:@"FromCustom"]){
				int i=-1;
				for(PackingItem *item1 in currentList.itemArray){
					if(![item1.name isEqualToString:item.name]){
						i++;
					}
					else
						break;
				}
				[currentList.itemArray removeObjectAtIndex:i+1];
			}
			
			else
				[currentList.itemArray removeObject:item];
		}
		else{
			if([item.custom isEqualToString:@"FromCustom"]){
				int i=-1;
				for(PackingItem *item1 in currentList.itemArray){
					if(![item1.name isEqualToString:item.name]){
						i++;
					}
					else
						break;
				}
				[currentList.itemArray removeObjectAtIndex:i+1];
			}
			else
				[currentList.itemArray removeObject:item];
			
		}
		[myTableView reloadData];
		[self viewWillAppear:YES];
    }
	//	    
	// [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
	
}



/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

- (void) sendEmail {
	NSMutableString *email = [NSMutableString stringWithCapacity:1024];
	[email appendString:@"mailto:?"];
	if (TARGET_VERSION == VERSION_BABY_PACKING){
		NSString *subject = [NSString stringWithFormat:@"Baby Packing List - %@", currentList.name];
		[email appendString:[NSString stringWithFormat:@"subject=%@", [subject stringByReplacingOccurrencesOfString:@"&" withString:@"%26"]]];
	}		
	
	else{
		NSString *subject = [NSString stringWithFormat:@"Packing List - %@", currentList.name];
		[email appendString:[NSString stringWithFormat:@"subject=%@", [subject stringByReplacingOccurrencesOfString:@"&" withString:@"%26"]]];
	}
	
	NSMutableString *body = [NSMutableString stringWithCapacity:1024];
	
	//	if(![self groupItems]){
	[body appendString:[NSString stringWithFormat:@"Summary: %@\n\n",[currentList getAbstract]]];
	[body appendString:[NSString stringWithFormat:@"Unchecked Items(%d): \n\n", (currentList.totalCount - currentList.checkedCount)]];
	for (PackingItem *item in currentList.itemArray) {
		if (!item.checked) {
			[body appendString:@"--"];
			[body appendString:item.name];
			[body appendString:@"\n"];
		}
	}
	[body appendString:@"\n"];
	[body appendString:[NSString stringWithFormat:@"Checked Items(%d): \n\n", currentList.checkedCount]];
	for (PackingItem *item in currentList.itemArray) {
		if (item.checked) {
			[body appendString:@"--"];
			[body appendString:item.name];
			[body appendString:@"\n"];
		}
	}
	//	}
	//	else{
	//		[body appendString:[NSString stringWithFormat:@"Summary: %@\n\n",[currentList getAbstract]]];
	//		[body appendString:[NSString stringWithFormat:@"Unchecked Items(%d): \n\n", (currentList.totalCount - currentList.checkedCount)]];
	//		
	//		for(int i = 0; i<[sections count]; i++){
	//			[body appendString:[NSString stringWithFormat:@"%@ :",[sections objectAtIndex:i]]];
	//			[body appendString:@"\n"];
	//			for (PackingItem *item in currentList.itemArray) {
	//				if (!item.checked && [item.custom isEqualToString: [sections objectAtIndex:i]]) {
	//					[body appendString:@"--"];
	//					[body appendString:item.name];
	//					[body appendString:@"\n"];
	//				}
	//			}
	//			[body appendString:@"\n"];
	//		}
	//		
	//		[body appendString:@"\n"];
	//		[body appendString:[NSString stringWithFormat:@"Checked Items(%d): \n\n", currentList.checkedCount]];
	//		
	//		for(int i = 0; i<[sections count]; i++){
	//			[body appendString:[NSString stringWithFormat:@"%@ :",[sections objectAtIndex:i]]];
	//			[body appendString:@"\n"];
	//			for (PackingItem *item in currentList.itemArray) {
	//				if (item.checked && [item.custom isEqualToString:[sections objectAtIndex:i]]) {
	//					[body appendString:@"--"];
	//					[body appendString:item.name];
	//					[body appendString:@"\n"];
	//				}
	//			}
	//			[body appendString:@"\n"];
	//		}
	//	}
	
	[email appendString:[NSString stringWithFormat:@"&body=%@", [body stringByReplacingOccurrencesOfString:@"&" withString:@"%26"]]];
	
	[email setString:[email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	NSLog(@"email = %@", email);
	NSURL *url = [NSURL URLWithString:email];
	
    BOOL open = [[UIApplication sharedApplication] openURL:url];
	if(!open){
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" 
														message:@"Please wait a moment to open the e-mail!" 
													   delegate:self
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
		[alert show];
		return;	
	}
	
}

#pragma mark AVAudioPlayerDelegate
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
	//NSLog(@"audioPlayerDidFinishPlaying");
	self.view.userInteractionEnabled = YES;
	if ([player isEqual:player2]) {
		[self.navigationController popViewControllerAnimated:YES];
	} 
}

- (void)audioPlayerBeginInterruption:(AVAudioPlayer *)player {
	
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player {
	
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error {
	
}
#pragma mark -
@end


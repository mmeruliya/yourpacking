//
//  XMLParserBase.h
//  BabyPacking
//
//  Created by Jay Lee on 10/13/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

@interface XMLParserBase : NSObject
{
	NSMutableArray* mElementStack;
	NSMutableString* mCurrentString;
	
	NSMutableArray* mParsingStack;
	NSMutableArray *mResult;
}

- (id)initWithXMLData:(NSData*)xmlData;

/*
- (id)getDocumentRoot;
- (id)getNodesByName:(NSString *)name;
 */

// To be overridden in subclasses.
- (void)beforeParsing;
- (BOOL)afterParsing;
- (void)didStartElement:(NSString*)elementName namespaceURI:(NSString*)namespaceURI qualifiedName:(NSString*)qualifiedName attributes:(NSDictionary*)attributes;
- (void)didEndElement:(NSString*)elementName namespaceURI:(NSString*)namespaceURI qualifiedName:(NSString*)qualifiedName;

// For subclasses.
- (BOOL)xmlPathEndsWith:(NSString*)first, ...;
- (NSString*)trimmedString;
@end

//
//  HelpViewController.h
//  BabyPacking
//
//  Created by Gary he on 6/17/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestHandler.h"
//#import "LayoutManager.h"
@class BabyPackingAppDelegate;

@interface HelpViewController : UIViewController <RequestHandlerDelegate, UITableViewDelegate, UITableViewDataSource>
{	
	BabyPackingAppDelegate *appDelegate;
	//LayoutManager *layoutManager;
	IBOutlet UITableView *myTableView;
	IBOutlet UIBarButtonItem *doneButton;
	
	NSMutableArray *categoryArray;
	NSMutableArray *subCategoryArray;
	NSMutableArray *tempCategoryArray;
	NSMutableArray *tempSubCategoryArray;	
	BOOL isSubCategory;
	NSDictionary *categoryDict;
}

@property (nonatomic, retain) NSMutableArray *categoryArray;
@property (nonatomic, retain) NSMutableArray *subCategoryArray;
@property (nonatomic, retain) NSMutableArray *tempCategoryArray;
@property (nonatomic, retain) NSMutableArray *tempSubCategoryArray;	
@property (assign, readwrite) BOOL isSubCategory;
@property (nonatomic, retain) NSDictionary *categoryDict;

- (IBAction) buttonAction:(id) sender;
- (void) makeRequest;
- (void) getCategories;
- (NSMutableArray *) getSubCategories:(NSDictionary *)dict;

@end

//
//  AddViewController.h
//  BabyPacking
//
//  Created by Jay Lee on 10/13/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestHandler.h"
#import "syncTask.h"
#import <sqlite3.h>

@class BabyPackingAppDelegate;
@class SchedulePickerViewController;
@class CategoryViewController;
@class RepeatingViewController;
@class TaskNotesViewController;
@class TaskItem;
@class RecordItem;


@interface AddViewController : UIViewController<RequestHandlerDelegate> {
	
	BabyPackingAppDelegate* mAppDelegate;
	SchedulePickerViewController* mPickerController;
	CategoryViewController* mCategoryController;
	RepeatingViewController* mRepeatController;
	TaskNotesViewController* mNotesController;
	NSDateFormatter* mDateFormatter;
	TaskItem* mCurrentTask;
	CGRect mNotesFieldFrame;
	
	NSInteger mRemoteTaskID;
	RecordItem* mCurrentRecord;
		
	IBOutlet UITableView* mTableView;
	IBOutlet UITextField* mNotifyTitleField;
	//IBOutlet UILabel* mNotifyTextField;
	IBOutlet UILabel* mCategoryLabel;
	IBOutlet UILabel* mRepeatLabel;
	IBOutlet UILabel* mStartTimeLabel;
	IBOutlet UILabel* mEndTimeLabel;
	IBOutlet UITableViewCell* mTitleCell;
	IBOutlet UITableViewCell* mCategoryCell;
	IBOutlet UITableViewCell* mRepeatCell;
	//IBOutlet UITableViewCell* mNotesCell;
	IBOutlet UILabel* mNotesLabel;
	IBOutlet UILabel* mNotesLabelTemplate;
	IBOutlet UITableViewCell* mDateTimeCell;
    IBOutlet UITableViewCell* mEndDateTimeCell;
	IBOutlet UIBarButtonItem* mBtnBack;
	IBOutlet UIBarButtonItem* mBtnSave;
	IBOutlet UIView* mHeaderView;
	IBOutlet UIButton* mBtnDelete;
    
    //Change By MS
    sqlite3 *dbFamilyPacking;
    
    UILocalNotification *localNotif;
}

@property (strong) NSMetadataQuery *metadataQuery;
@property (strong) syncTask * doc;
@property (strong) NSMetadataQuery *query;

@property(nonatomic, assign) NSInteger remoteTaskID;
@property(nonatomic, retain) RecordItem* currentRecord;

- (IBAction)buttonSaveDidClick:(id)sender;
- (IBAction)buttonDeleteDidClick:(id)sender;

@end

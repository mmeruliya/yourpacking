//
//  Items.h
//  BabyPacking
//
//  Created by Hitesh on 12/15/10.
//  Copyright 2010 cmc limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface Items : NSObject 
{
	sqlite3 *database;
	
	NSInteger pk;
	NSInteger checked;
	NSString *name;
	NSString *tip;
	NSString *custom;
	NSString *qty;
	NSInteger deleted;
	NSInteger categoryId;
}

@property (assign, readwrite) NSInteger pk;
@property (assign, readwrite) NSInteger checked;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *tip;
@property (nonatomic, retain) NSString *custom;
@property (nonatomic, retain) NSString *qty;
@property (assign, readwrite) NSInteger deleted;
@property (assign, readwrite) NSInteger categoryId;

- (id) initWithPrimaryKey:(NSInteger)primaryKey database:(sqlite3 *)db;
- (void) addItem:(sqlite3 *)db;
- (void) updateItem:(sqlite3 *)db;
- (void) deleteItems:(sqlite3 *)db;
- (void) updateItemsForCategory:(sqlite3 *)db;

//New
- (void) updateItemsOldCategory:(NSString *)oldCat withNewCategory:(NSString *)newCat dbase:(sqlite3 *)db;

@end

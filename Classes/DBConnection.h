//
//  DBConn.h
//

#import <UIKit/UIKit.h>
// This includes the header for the SQLite library.
#import <sqlite3.h>


@interface DBConnection : NSObject {
    // Opaque reference to the SQLite database.
	@private
	sqlite3 *g_database;
}

@property (nonatomic,assign,readonly) sqlite3 *database;

+ (DBConnection *) sharedConnection;
// Init database
- (id)initConnection;
- (void)close;
+ (BOOL)upgradeDatabaseIfNeeded;

@end

//
//  welcomeViewController.h
//  BabyPacking
//
//  Created by eve on 12/15/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LayoutManager.h"

//#import "HelpViewController.h"
//#import "RootViewController.h"
@class HelpViewController;
@class RootViewController;
@interface WelcomeViewController : UIViewController {
	
    LayoutManager *layoutManager;
    IBOutlet UIButton* forHelpButton;
	IBOutlet UIButton* forListButton;
    IBOutlet UITextView *txtView;
	IBOutlet UISwitch *swidth;
	HelpViewController *helpController;
	RootViewController *homeController;
}
@property(nonatomic, retain) HelpViewController *helpController;
@property(nonatomic, retain) RootViewController *homeController;
- (IBAction) buttonAction:(id)sender;
- (void) valueChanged:(id) sender;
- (void) closeHelpPage;

@end

//
//  RepeatingViewController.h
//  BabyPacking
//
//  Created by Jay Lee on 10/14/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BabyPackingAppDelegate;

@interface RepeatingViewController : UIViewController {
	BabyPackingAppDelegate* mAppDelegate;
	NSArray* mRepeatList;
	NSString* mDefaultValue;
	
	IBOutlet UITableView* mTableView;
	IBOutlet UIPickerView* mPickerView;
}

@property(nonatomic, copy) NSString* defaultValue;

+ (NSString *)titleForValue:(NSString *)aValue;
+ (NSString *)valueForTitle:(NSString *)aTitle;

@end

//
//  SearchResultParser.h
//  BabyPacking
//
//  Created by Jay Lee on 10/13/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import "XMLParserBase.h"

@interface ResponseHandler : XMLParserBase
{
	int mErrorCode;
	BOOL mFound;
	NSString *mErrorMessage;
	NSInteger mTask_id;
}

@property (nonatomic, assign) int errorCode;
@property (nonatomic, assign) BOOL found;
@property (nonatomic, copy) NSString* errorMessage;
@property (nonatomic, assign) int task_id;

@end

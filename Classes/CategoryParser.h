//
//  CategoryParser.h
//  BabyPacking
//
//  Created by Hitesh Vaghasiya on 22/04/11.
//  Copyright 2011 Dev IT Solution pvt ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLParserBase.h"

@interface CategoryParser : XMLParserBase 
{
	BOOL mSuccessful;
	NSMutableDictionary* mData;
	NSMutableDictionary* mCurItem;
}

@property (nonatomic, assign) BOOL successful;
@property (nonatomic, retain) NSMutableDictionary* data;

@end

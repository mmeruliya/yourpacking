//
//  Categories.h
//  BabyPacking
//
//  Created by Hitesh on 12/15/10.
//  Copyright 2010 cmc limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface Categories : NSObject 
{
	sqlite3 *database;
	
	NSInteger pk;	
	NSString *name;
	NSInteger deleted;
	NSInteger type;
}

@property (assign, readwrite) NSInteger pk;
@property (nonatomic, retain) NSString *name;
@property (assign, readwrite) NSInteger deleted;
@property (assign, readwrite) NSInteger type;

- (id) initWithPrimaryKey:(NSInteger)primaryKey database:(sqlite3 *)db;
- (void) addCategory:(sqlite3 *)db;
- (void) updateCategory:(sqlite3 *)db;
- (void) deleteCategory:(sqlite3 *)db;

@end

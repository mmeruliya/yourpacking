//
//  Utils.h
//  BabyPacking
//
//  Created by Jay Lee on 10/13/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Utils : NSObject {

}

+ (NSString*) md5:(NSString*)str length:(int)len;
+ (NSString*) getCurrentTimeString;

+ (NSDate *)getTheBeginTimeOfDate:(NSDate *)fromDate;
+ (NSDate *)getTheBeginTimeOfMonth:(NSDate *)fromDate;
+ (NSDate *)getTheBeginTimeOfYear:(NSDate *)fromDate;
+ (NSDate *)getTheEndTimeOfDate:(NSDate *)fromDate;
+ (NSDate *)getTheEndTimeOfMonth:(NSDate *)fromDate;
+ (NSDate *)getTheEndTimeOfYear:(NSDate *)fromDate;
+ (NSDate *)getWeekDay:(NSInteger)ordinal ofDate:(NSDate *)theDate;
+ (NSDate *)getMonthDay:(NSInteger)ordinal ofDate:(NSDate *)theDate;
+ (NSInteger)getMinutesFromDate:(NSDate *)from toDate:(NSDate *)to;
+ (NSDate *)getDateWithDays:(NSInteger)days afterDate:(NSDate *)date;
+ (NSDate *)getDateWithHours:(NSInteger)hours afterDate:(NSDate *)date;

@end

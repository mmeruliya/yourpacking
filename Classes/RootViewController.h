//
//  RootViewController.h
//  BabyPacking
//
//  Created by Gary He on 5/18/09.
//  Copyright test 2009. All rights reserved.
//
#import "CSHeaderView.h"
#import "HelpViewController.h"
#import "WelcomeViewController.h"
#import "LayoutManager.h"
#import "syncTask.h"

@class BabyPackingAppDelegate;

@interface RootViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate> {

	BabyPackingAppDelegate *appDelegate;
    LayoutManager *layoutManager;
	
	IBOutlet UILabel *label;
    IBOutlet UILabel *labelLand;
	IBOutlet UITableView *myTableView;
    IBOutlet UITableView *myTableViewLand;
    IBOutlet UIView *brifcaseLoginView;
	
    //IBOutlet UIToolbar *toolbar;
	IBOutlet UIBarButtonItem *createBtItem;
    IBOutlet UIBarButtonItem *editButtonItem;
	IBOutlet UIBarButtonItem *settingsBtItem;
	IBOutlet UIBarButtonItem *infoBtItem;
	IBOutlet UIBarButtonItem *helpBtItem;
    
	IBOutlet UIView* mBlankView;
	
	IBOutlet UITabBar *mTabBar;
    IBOutlet UITabBar *mTabBarLand;
	
//	IBOutlet UITabBarItem *mMainTabBarItem;	
	
	UIAlertView	*mPasscodeAlertView;
	IBOutlet UITextField *mPasscodeTextView;
	IBOutlet UILabel *mLoginMessage;
	IBOutlet UITextField *code_seg_one;
	IBOutlet UITextField *code_seg_two;
	IBOutlet UITextField *code_seg_three;
	IBOutlet UITextField *code_seg_four;
    IBOutlet UITextField *txt_one;
	IBOutlet UITextField *txt_two;
	IBOutlet UITextField *txt_three;
	IBOutlet UITextField *txt_four;
	IBOutlet UINavigationController* mTaskAlertController;
	int mRetryCounter;
	BOOL ifLoginButtonClicked;
	UIImagePickerController *picker;
	
	CSHeaderView *statusHeaderView;
	CSHeaderView *customHeaderView;
	CSHeaderView *readyMadeHeaderView;
	
	NSMutableDictionary *customDict;
	NSMutableDictionary *readyMadeDict;
	NSMutableDictionary *statusDict;
	NSMutableArray *readyMadeListForDeluxeArray;
	NSMutableArray *allListArray;
	
	UIImage *pinkBearIcon;
	UIImage *blueBearIcon;
	UIImage *templateIcon;
	UIImage *boyIcon;
	UIImage *girlIcon;
	UIImage *manIcon;
	UIImage *womanIcon;
	UIImage *petIcon;
	UIImage *ShoppingIcon;
	UIImage *otherIcon;
	
	// when user click "Create", set YES
	BOOL isAddCustomList;
	
	HelpViewController *helpController;
	WelcomeViewController *welcomeController;
    
    
    
    //For Localization
    IBOutlet UILabel *lblRoottext;
    IBOutlet UIButton *btnAddNewList;
   
  }

@property (strong) NSMetadataQuery *metadataQuery;
@property (strong) syncTask * doc;
@property (strong) NSMetadataQuery *query;

@property(nonatomic,retain)IBOutlet UIView *landscapeVw,*portraitVw;
@property (nonatomic,retain) UIView *brifcaseLoginView;
@property(nonatomic, retain) NSMutableDictionary *customDict;
@property(nonatomic, retain) NSMutableDictionary *readyMadeDict;
@property(nonatomic, retain) NSMutableArray *readyMadeListForDeluxeArray;
@property(nonatomic, retain) NSMutableDictionary *statusDict;
@property(nonatomic, retain) NSMutableArray *allListArray;
@property(nonatomic) BOOL isAddCustomList;
@property(nonatomic, retain) HelpViewController *helpController;
@property(nonatomic, retain) WelcomeViewController *welcomeController;
@property (nonatomic,readonly) UIImagePickerController *picker;
@property(nonatomic,readonly) UITextField *txt_one;
@property(nonatomic,readonly) UITextField *txt_two;
@property(nonatomic,readonly) UITextField *txt_three;
@property(nonatomic,readonly) UITextField *txt_four;
@property(nonatomic,readonly) UIButton *btnlogin;

- (IBAction) buttonAction:(id) sender;
- (IBAction) buttonLogin:(id) sender;
- (void) closeHelpPage;
- (IBAction)valueDidChange:(id)sender;
- (int) hasInUseInReadyMadeList;
@end

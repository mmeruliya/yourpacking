//
//  TaskItem.h
//  BabyPacking
//
//  Created by Jay Lee on 10/14/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBModel.h"


@interface TaskItem : DBModel {
	NSInteger remote_id;
	NSString *title;
	NSString *category;
	NSString *repeat;
	NSInteger interval;
	NSDate* start_date;
	NSDate* end_date;
	NSString* text;
	NSString* device_token;
	NSDate* create_date;
	NSDate* update_date;
}

@property(nonatomic, assign) NSInteger remote_id;
@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *category;
@property(nonatomic, copy) NSString *repeat;
@property(nonatomic, assign) NSInteger interval;
@property(nonatomic, copy) NSDate* start_date;
@property(nonatomic, copy) NSDate* end_date;
@property(nonatomic, copy) NSString* text;
@property(nonatomic, copy) NSString* device_token;
@property(nonatomic, copy) NSDate* create_date;
@property(nonatomic, copy) NSDate* update_date;

//Change By MS
@property(nonatomic, copy) NSString* reminder_id;

- (id)initWithRemoteId:(NSInteger)remoteID;
+ (NSMutableArray*)findByCategory:(NSString *)category;
- (BOOL)isEqualToItem:(TaskItem *)item;
- (NSDateComponents *)calcOffsetDate;
- (NSDate *)getNextTaskTime;
- (void)deleteAllData;
+ (void)upgradeTableFromDatabase:(sqlite3 *)db ToDatabase:(sqlite3 *)newdb;
@end

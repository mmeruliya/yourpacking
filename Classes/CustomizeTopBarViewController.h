//
//  CustomizeCellViewController.h
//  BabyPacking
//
//  Created by Gary He on 6/8/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextPickerViewController.h"
#import "PasscodeViewController.h"
#import "DetailedSettingsViewController.h"
#import "ColorPickerViewController.h"
#import "LayoutManager.h"

@class BabyPackingAppDelegate;

@interface CustomizeTopBarViewController : UIViewController <ColorPickerViewControllerDelegate> {
	
    LayoutManager *layoutManager;
	BabyPackingAppDelegate *mAppDelegate;
	IBOutlet UIBarButtonItem *cancelButton;
	IBOutlet UIBarButtonItem *doneButton;
	
	IBOutlet UIView *selectColorView;
	
	IBOutlet UILabel *lblTextSize;
	
	int colorSelectIndex;
	
	int count;
    
    IBOutlet UIImageView *imgTopBGPreview;
    IBOutlet UIImageView *imgHeaderBGPreview;
    
    IBOutlet UILabel *lblTopTextPreview;
    IBOutlet UILabel *lblHeaderTextPreview;
    
    IBOutlet UIView *colorSwatch;

}

@property (readwrite,nonatomic,retain) IBOutlet UIView *colorSwatch;

-(IBAction) selectColor:(id)sender;

- (IBAction) buttonAction:(id) sender;

//- (IBAction) selectColor:(id)sender;
- (IBAction) colorButtonPressed:(id)sender;

- (IBAction) onMinusClicked:(id)sender;
- (IBAction) onPlusClicked:(id)sender;

- (IBAction) onSettingResetClicked:(id)sender;

@end

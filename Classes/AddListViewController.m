//
//  CreateItemViewController.m
//  BabyPacking
//
//  Created by Gary He on 6/6/09.
//  Copyright 2009 test. All rights reserved.
//

#import "RootViewController.h"
#import "AddListViewController.h"
#import "SelectItemViewController.h"
#import "CategoriseListViewController.h"
#import "AddItemViewController.h"
#import "BabyPackingAppDelegate.h"
#import "CustomeIconVC.h"
#import "Constant.h"
#import "DataManager.h"
#import "Items.h"

@interface AddListViewController(Private)

- (void) setSelectedSex:(BabySex) sex;
- (NSInteger) getSelectedSex;
- (void) setSelectedAge:(BabyAge) age; 
- (NSInteger) getSelectedAge;
- (UIImage *)scaleAndRotateImage:(UIImage *)image resolution:(int)resolution scaleFlag:(BOOL)aFlag;
- (void) openImagePickerController;
- (void) saveList;
- (void) loadPredefinedItems;
- (void) dismissPopOverController;
@end

@implementation AddListViewController
@synthesize homeController;
@synthesize editingList;
@synthesize customListArray,predefinedItems,babyPredefinedItems, imageName;

int selectedIdx, SegGenderIndex;

- (void)dealloc {
    [imageName release];
	[myTableView release];
	[tableHeaderView release];
	
	[photoView release];
	[nameField release];
	[addPhotoLabel release];
	[boyGirlLabel release];
	
	[boyControl release];
	[girlControl release];
	[babyControl release];
	[toddlerControl release];
	[infant_babyControl release];
	[childControl release];
	[adultControl release];
	[petControl release];
	[shoppingListControl release];
	
	[saveButton release];
	[cancelButton release];
	
	[homeController release];
	[editingList release];
	[customListArray release];
	[predefinedItems release];
	
	if (popoverController != nil)
	{
		[popoverController release];
		popoverController = nil;
	}
	
    [super dealloc];
}

/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if (self = [super initWithStyle:style]) {
    }
    return self;
}
*/



- (void)viewDidLoad
{
    [super viewDidLoad];
    selectedIdx = 104;
    SegGenderIndex = 100;
    [self setSelectedAge:Infant_baby];
    [self setSelectedSex:BabySexBoy];

   	appDelegate = (BabyPackingAppDelegate *)[[UIApplication sharedApplication] delegate];
	
    if (IOS_NEWER_THAN(7))  //Tejas
    {
     //   self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    if (!IOS_NEWER_THAN(7))
    {
        myTableView.frame = CGRectMake(myTableView.frame.origin.x, 0, myTableView.frame.size.width, myTableView.frame.size.height+65);
    }
    
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];

	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor whiteColor];
	NSString *customFontName = [appDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];		
	}
	else {
		label.font=[UIFont fontWithName:customFontName size:fontSize];		
	}
	
	[label sizeToFit];
	
	
	//label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
    label.textColor=[UIColor whiteColor];        
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label.textColor = theColour1;
	}
	// and finally set the colour of your label
	
	self.navigationItem.titleView = label;	
	label.text = TITLE_CREATE_NEW_LIST;
	[label sizeToFit];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 30, 30);
    btn.tag=1;
    [btn setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
    
    [btn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    cancelButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    self.navigationItem.leftBarButtonItem = cancelButton;

    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(0, 0, 30, 30);
    btn2.tag=1;
    [btn2 setImage:[UIImage imageNamed:@"saveNewList.png"] forState:UIControlStateNormal];
    
    [btn2 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    saveButton = [[UIBarButtonItem alloc] initWithCustomView:btn2];
    
    self.navigationItem.rightBarButtonItem = saveButton;
    
	
	photoView.contentMode = UIViewContentModeScaleAspectFit;
    
     
	photoView.action = @selector(buttonAction:);
    
	myTableView.tableHeaderView = tableHeaderView;

	if (TARGET_VERSION == VERSION_BABY_PACKING){
		familyItemView.alpha=1;
	}

	else if (TARGET_VERSION == VERSION_FAMILY_PACKING){
		familyItemView.alpha = 1;
		babyToddlerView.alpha = 0;
		nameField.placeholder = @"Enter Name...";
        if (IOS_NEWER_THAN(7))
        {
            nameField.borderStyle = UITextBorderStyleRoundedRect;
        }
       
	//	boyGirlLabel.text = TITLE_SELECT_SEX_FAMILY;
	}

	else if (TARGET_VERSION == VERSION_YOU_PACKING){
		familyItemView.alpha=0;
		babyToddlerView.alpha=0;
//		[babyControl setTitle:@"Adule" forSegmentAtIndex:0];
//		[toddlerControl setTitle:@"Other" forSegmentAtIndex:0];
		nameField.placeholder=@"Enter Name";
		[boyControl setTitle:@"Male" forSegmentAtIndex:0 ];
		[girlControl setTitle:@"Female" forSegmentAtIndex:0];
		boyGirlLabel.text = TITLE_SELECT_SEX_FAMILY;
	}
	
	if (editingList) {
		nameField.text = editingList.name;
		[self setSelectedSex:editingList.sex];
		[self setSelectedAge:editingList.age];
		photoView.image = editingList.photo;
		if (editingList.icon) {
			addPhotoLabel.alpha = 0;
		}
		
	} else {		
		
		//boyControl.tintColor = BOY_COLOR;
        [boyControl setBackgroundImage:[UIImage imageNamed:@"segmentSelected.png"]
                              forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
		if (TARGET_VERSION == VERSION_BABY_PACKING){
			babyControl.tintColor = BABY_COLOR;
		}else if (TARGET_VERSION == VERSION_FAMILY_PACKING){
		   //  infant_babyControl.tintColor=BABY_COLOR;
            [infant_babyControl setBackgroundImage:[UIImage imageNamed:@"segmentSelected.png"]
                                  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
		} else if (TARGET_VERSION == VERSION_YOU_PACKING) {
			adultControl.tintColor = BABY_COLOR;
		}
	}
    nameField.clearButtonMode = UITextFieldViewModeWhileEditing;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(actionSheet.tag == 101)
    {
        if (buttonIndex == 0)
        {
            [self openImagePickerController];
        }
        else if (buttonIndex == 3)
        {
            if(addPhotoLabel.hidden==TRUE)
            {
                addPhotoLabel.hidden=false;
            }
            addPhotoLabel.alpha = 1;
            photoView.image = [UIImage imageNamed:@"addPhoto.png"];
        }
        else if (buttonIndex == 1)
        {
            CustomeIconVC *controller = [[CustomeIconVC alloc] initWithNibName:@"CustomeIconVC" bundle:nil];
            [self.navigationController pushViewController:controller animated:YES];
        }
        else if (buttonIndex == 2)
        {
            @try
            {
                if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                {
                    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                    picker.delegate = self;
                    picker.navigationController.navigationBar.tintColor=[UIColor blackColor];
                    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                    picker.allowsEditing=YES;
                    [picker.delegate self];
                    // [self presentModalViewController:imgPicker animated:YES];
                    [self presentViewController:picker animated:YES completion:nil];
                }
            }
            @catch (NSException *exception)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Family Packing Alert" message:@"Camera is not available!!" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
        }
    }
    else if (actionSheet.tag == 102)
    {
        if (buttonIndex == 0) {
            [self openImagePickerController];
        } else if (buttonIndex == 1) {
           CustomeIconVC *controller = [[CustomeIconVC alloc] initWithNibName:@"CustomeIconVC" bundle:nil];
            [self.navigationController pushViewController:controller animated:YES];
            
        }
        else if (buttonIndex == 2) {
            @try
            {
                if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                {
                    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                    picker.delegate = self;
                    picker.navigationController.navigationBar.tintColor=[UIColor blackColor];
                    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                    picker.allowsEditing=YES;
                    [picker.delegate self];
                    // [self presentModalViewController:imgPicker animated:YES];
                    [self presentViewController:picker animated:YES completion:nil];
                }
            }
            @catch (NSException *exception)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Family Packing Alert" message:@"Camera is not available!!" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
        }

    }
}

- (IBAction) buttonAction:(id)sender {
	
	if (sender == photoView) {
		if (photoView.image != [UIImage imageNamed:@"addPhoto.png"]) {
			UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Choose Existing Photo", @"Choose custom icon",@"Capture Image",@"Delete Photo", nil];
			  actionSheet.tag = 101;
			[actionSheet showInView:self.view];
			[actionSheet release];
          
		} else {
            
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Choose Existing Photo", @"Choose custom icon",@"Capture Image", nil];
              actionSheet.tag = 102;
			[actionSheet showInView:self.view];
			[actionSheet release];
    }
	} else if (sender == cancelButton.customView) {
		[self.navigationController popViewControllerAnimated:YES];
	} else if(sender == saveButton.customView) {
		[self saveList];
	}
}

- (void) saveList {
	
	NSString *name = [nameField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	if (![name length] > 0) {
		BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
		[appDelegate showAlert:M_INPUT_NAME_TITLE message:M_INPUT_NAME];
		return;
	}
	
	BOOL isNewList = NO;
	
	if (!editingList) {
		isNewList = YES;
		PackingList *list = [[PackingList alloc] init];
		self.editingList = list;
		[list release];
	}
	
	editingList.name = name;
	
	editingList.sex = [self getSelectedSex];
	
	editingList.age = [self getSelectedAge];
	[self loadPredefinedItems];
	editingList.update = [NSDate date];
	if (photoView.image != [UIImage imageNamed:@"addPhoto.png"]) {
		editingList.icon = [self scaleAndRotateImage:photoView.image resolution:LIST_ICON_SIZE scaleFlag:YES];
		editingList.photo = [self scaleAndRotateImage:photoView.image resolution:PHOTO_SIZE scaleFlag:YES];

	} else {
		editingList.icon = nil;
		editingList.photo = nil;
	}
	
	if (isNewList) {
		[editingList insertData];
		// add this new custom list to home page
		self.homeController.isAddCustomList = YES;
		[customListArray insertObject:editingList atIndex:0];
	} else {
		[editingList updateData];
	}
	
	// release it.
	editingList.photo = nil;

	//NSLog(@"editingList insertData>>pk = %d", editingList.pk);
	
	if (editingList.age < Infant_baby){

        NSLog(@"BPNG");

		SelectItemViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[SelectItemViewController alloc] initWithNibName:@"SelectItemViewController" bundle:nil];
		else
			controller = [[SelectItemViewController alloc] initWithNibName:@"SelectItemViewController1" bundle:nil];
		
		//[[SelectItemViewController alloc] initWithNibName:@"SelectItemViewController" bundle:nil];
        controller.isEdit=NO;
		controller.homeController = self.homeController;
		controller.currentList = editingList;
		controller.babyPredefinedItems = babyPredefinedItems;
		controller.predefinedItems = predefinedItems;
		controller.isBabyPacking = YES;
		if (isNewList) {
			controller.editList = NO;			
		} else {
			controller.editList = YES;
		}
		[self.navigationController pushViewController:controller animated:YES];
		[controller release];

	}else{
		CategoriseListViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[CategoriseListViewController alloc] initWithNibName:@"CategoriseListViewController" bundle:nil];
		else
			controller = [[CategoriseListViewController alloc] initWithNibName:@"CategoriseListViewController1" bundle:nil];
		
		//[[CategoriseListViewController alloc] initWithNibName:@"CategoriseListViewController" bundle:nil];
		controller.homeController = self.homeController;
        controller.isEdit=NO;
		controller.currentList = editingList;
		if (isNewList) {
			controller.editList = NO;			
		} else {
			controller.editList = YES;
		}
		[self.navigationController pushViewController:controller animated:YES];
		[controller release];

	}		
}

- (IBAction) valueChanged:(id)sender
{
	UISegmentedControl *selectedControl = (UISegmentedControl*)sender;
	if (selectedControl == boyControl)
    {
        SegGenderIndex = 100;
		[self setSelectedSex:BabySexBoy];
	}
    else if (selectedControl == girlControl)
    {
         SegGenderIndex = 101;
		[self setSelectedSex:BabySexGirl];
	}
    else if (selectedControl == babyControl)
    {
         selectedIdx = 102;
		[self setSelectedAge:Baby];
	}
    else if (selectedControl == toddlerControl)
    {
         selectedIdx = 103;
		[self setSelectedAge:Toddler];
	}
    else if (selectedControl == infant_babyControl)
    {
         selectedIdx = 104;
		[self setSelectedAge:Infant_baby];
	}
    else if (selectedControl == childControl)
    {
         selectedIdx = 105;
		[self setSelectedAge:Child];
	}
    else if (selectedControl == adultControl || selectedControl == adultInFamilyControl)
    {
         selectedIdx = 106;
		[self setSelectedAge:Adult];
	}
    else if (selectedControl == petControl)
    {
         selectedIdx = 107;
		[self setSelectedAge:Pet];
	}
    else if (selectedControl == shoppingListControl)
    {
         selectedIdx = 108;
		[self setSelectedAge:ShoppingList];
	}
    else if (selectedControl == otherControl || selectedControl == otherInFamilyControl)
    {
         selectedIdx = 109;
		[self setSelectedAge:Other];
	}
}

- (void) setSelectedSex:(BabySex) sex {
	if (sex == BabySexBoy) {
		//boyControl.tintColor = BOY_COLOR;
		girlControl.tintColor = nil;
        [boyControl setBackgroundImage:[UIImage imageNamed:@"segmentSelected.png"]
                                   forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [girlControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                              forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
	} else if (sex == BabySexGirl) {
		//girlControl.tintColor = GIRL_COLOR;
		boyControl.tintColor = nil;
        [girlControl setBackgroundImage:[UIImage imageNamed:@"segmentSelected.png"]
                              forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [boyControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                               forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
	} else {
		girlControl.tintColor = nil;
		boyControl.tintColor = nil;
        [boyControl setBackgroundImage:[UIImage imageNamed:@"segmentSelected.png"]
                              forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [girlControl setBackgroundImage:[UIImage imageNamed:@"segmentSelected.png"]
                               forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
	}
}

- (NSInteger) getSelectedSex
{
/*	if (boyGirlView.hidden == YES)
    {
		return BabySexNone;
    }
	else if (boyControl.tintColor)
    {
		return BabySexBoy;
	}
    else if (girlControl.tintColor)
    {
		return BabySexGirl;
	}
    else
    {
		return BabySexNone;
	}*/
 
    NSLog(@"%d",childControl.tag);
    NSInteger gender;
    if (SegGenderIndex==boyControl.tag)
    {
        gender = BabySexBoy;
        //return Infant_baby;
    }
    else if (SegGenderIndex==girlControl.tag)
    {
        gender = BabySexGirl;
        //return Child;
    }
    else
    {
        gender = BabySexNone;
    }
    return gender;
}

- (void) setSelectedAge:(BabyAge) age{
	if (age == Baby) {
		//babyControl.tintColor = BABY_COLOR;
		toddlerControl.tintColor = nil;
        [babyControl setBackgroundImage:[UIImage imageNamed:@"segmentSelected.png"]
                              forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [toddlerControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                               forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
	
	} else if (age == Toddler) {
		//toddlerControl.tintColor = TODDLER_COLOR;
		babyControl.tintColor = nil;
        [toddlerControl setBackgroundImage:[UIImage imageNamed:@"segmentSelected.png"]
                               forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [babyControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];

	} else if (age == Infant_baby) {
		//infant_babyControl.tintColor = BABY_COLOR;
		childControl.tintColor = nil;
		adultControl.tintColor = nil;
		adultInFamilyControl.tintColor = nil;
		petControl.tintColor = nil;
		shoppingListControl.tintColor = nil;
		otherControl.tintColor = nil;
		otherInFamilyControl.tintColor = nil;
		[boyControl setTitle:@"Boy" forSegmentAtIndex:0 ];
		[girlControl setTitle:@"Girl" forSegmentAtIndex:0];
		boyGirlLabel.text = TITLE_SELECT_SEX_BABY;
		boyGirlView.hidden = NO;
        
        [infant_babyControl setBackgroundImage:[UIImage imageNamed:@"segmentSelected.png"]
                                  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [childControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                               forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [adultControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [adultInFamilyControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [petControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [shoppingListControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [otherControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [otherInFamilyControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
	}else if (age == Child) {
		infant_babyControl.tintColor = nil;
		//childControl.tintColor = TODDLER_COLOR;
		adultControl.tintColor = nil;
		adultInFamilyControl.tintColor = nil;
		petControl.tintColor = nil;
		shoppingListControl.tintColor = nil;
		otherControl.tintColor = nil;
		otherInFamilyControl.tintColor = nil;
		[boyControl setTitle:@"Boy" forSegmentAtIndex:0 ];
		[girlControl setTitle:@"Girl" forSegmentAtIndex:0];
		boyGirlLabel.text = TITLE_SELECT_SEX_BABY;
		boyGirlView.hidden = NO;
        
        [infant_babyControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                      forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [childControl setBackgroundImage:[UIImage imageNamed:@"segmentSelected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [adultControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [adultInFamilyControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                        forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [petControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                              forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [shoppingListControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                       forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [otherControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [otherInFamilyControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                        forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
	}else if (age == Adult) {
		infant_babyControl.tintColor = nil;
		childControl.tintColor = nil;
		//adultControl.tintColor = BABY_COLOR;
		//adultInFamilyControl.tintColor = BABY_COLOR;
		petControl.tintColor = nil;
		shoppingListControl.tintColor = nil;
		otherControl.tintColor = nil;
		otherInFamilyControl.tintColor = nil;
		boyGirlView.hidden = NO;
		boyGirlLabel.text = TITLE_SELECT_SEX_FAMILY;
		[boyControl setTitle:@"Male" forSegmentAtIndex:0 ];
		[girlControl setTitle:@"Female" forSegmentAtIndex:0];
        
        [infant_babyControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                      forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [childControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [adultControl setBackgroundImage:[UIImage imageNamed:@"segmentSelected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [adultInFamilyControl setBackgroundImage:[UIImage imageNamed:@"segmentSelected.png"]
                                        forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [petControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                              forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [shoppingListControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                       forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [otherControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [otherInFamilyControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                        forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        
	}else if (age == Pet){
		infant_babyControl.tintColor = nil;
		childControl.tintColor = nil;
		adultControl.tintColor = nil;
		adultInFamilyControl.tintColor = nil;
		//petControl.tintColor = TODDLER_COLOR;
		shoppingListControl.tintColor=nil;
		otherControl.tintColor = nil;
		otherInFamilyControl.tintColor = nil;
		boyGirlView.hidden = YES;
        [infant_babyControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                      forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [childControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [adultControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [adultInFamilyControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                        forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [petControl setBackgroundImage:[UIImage imageNamed:@"segmentSelected.png"]
                              forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [shoppingListControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                       forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [otherControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [otherInFamilyControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                        forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
	}else if (age==ShoppingList) {
		infant_babyControl.tintColor=nil;
		childControl.tintColor=nil;
		adultControl.tintColor=nil;
		adultInFamilyControl.tintColor = nil;
		petControl.tintColor=nil;
		//shoppingListControl.tintColor=BABY_COLOR;
		otherControl.tintColor = nil;
		otherInFamilyControl.tintColor = nil;
		boyGirlView.hidden = YES;
        
        [infant_babyControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                      forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [childControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [adultControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [adultInFamilyControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                        forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [petControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                              forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [shoppingListControl setBackgroundImage:[UIImage imageNamed:@"segmentSelected.png"]
                                       forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [otherControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [otherInFamilyControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                        forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        
	}else if (age ==Other){
		infant_babyControl.tintColor=nil;
		childControl.tintColor=nil;
		adultControl.tintColor=nil;
		adultInFamilyControl.tintColor = nil;
		petControl.tintColor=nil;
		shoppingListControl.tintColor=nil;
		//otherControl.tintColor = TODDLER_COLOR;
		//otherInFamilyControl.tintColor = TODDLER_COLOR;
		boyGirlView.hidden = YES;
        
        [infant_babyControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                      forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [childControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [adultControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [adultInFamilyControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                        forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [petControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                              forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [shoppingListControl setBackgroundImage:[UIImage imageNamed:@"segmentDeselected.png"]
                                       forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [otherControl setBackgroundImage:[UIImage imageNamed:@"segmentSelected.png"]
                                forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [otherInFamilyControl setBackgroundImage:[UIImage imageNamed:@"segmentSelected.png"]
                                        forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
	}

}

/**********In the following method enum is returned on the basis of tag of selected segmented control. In the upgrades for iOS 7 we have changed the code for Family packing target only. Code based on tint color will never work.***********/
- (NSInteger) getSelectedAge
{
	if (TARGET_VERSION == VERSION_BABY_PACKING)
    {
		if (babyControl.tintColor) {
			return Baby;
		} else if (toddlerControl.tintColor) {
			return Toddler;
		} 
		return Baby;
	}
    else if (TARGET_VERSION == VERSION_FAMILY_PACKING)
    {
        NSLog(@"%d",childControl.tag);
        NSInteger age;
        if (selectedIdx==infant_babyControl.tag)
        {
            age = Infant_baby;
			//return Infant_baby;
		}
        else if (selectedIdx==childControl.tag)
        {
            age = Child;
			//return Child;
		}
        else if ((selectedIdx==adultControl.tag) || (selectedIdx == adultInFamilyControl.tag))
        {
            age = Adult;
			//return Adult;
		}
        else if (selectedIdx==petControl.tag)
        {
            age = Pet;
			//return Pet;
		}
        else if (selectedIdx==shoppingListControl.tag)
        {
            age = ShoppingList;
			//return ShoppingList;
		}
         else if ((selectedIdx==otherControl.tag) || (selectedIdx == otherInFamilyControl.tag))
        {
            age = Other;
			//return Other;
		}
       else
        {
          age = Infant_baby;
        }
        return age;
	}
	else if (TARGET_VERSION == VERSION_YOU_PACKING)
		if(otherControl.tintColor)
			return Other;
		else
			return Adult;
}

- (void) loadPredefinedItems {
	
	BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
	NSMutableDictionary *appDict = appDelegate.appDict;
		if (TARGET_VERSION == VERSION_BABY_PACKING){
			self.predefinedItems = [NSMutableArray arrayWithCapacity:0];
			
			if (editingList.age == Baby) 
				babyPredefinedItems = [appDict objectForKey:K_BABY_ITEMS];
			else
				babyPredefinedItems = [appDict objectForKey:K_TODDLER_ITEMS];

			int i = 0;
			for(NSDictionary *babyItem in babyPredefinedItems) {
				i++;
				PackingItem *item = [[PackingItem alloc] init];
				item.name = [babyItem objectForKey:K_ITEM_NAME];
				item.tip = [babyItem objectForKey:K_ITEM_TIP];
				item.qty = [babyItem objectForKey:@"qty"];
				item.custom = [babyItem objectForKey:@"custom"];
				item.predefineId = i;
				item.predefineForAge = editingList.age;
				[self.predefinedItems addObject:item];
				[item release];	
			}
			
			/*[appDelegate.dataManager getItemByCategoty:[NSString stringWithFormat:@"BabyItem"]];
			
			for (int j = 0; j < appDelegate.itemsFromDB.count; j++)
			{
				Items *itemDB = [appDelegate.itemsFromDB objectAtIndex:j];
				i++;
				PackingItem *item = [[PackingItem alloc] init];
				item.pk = itemDB.pk;
				item.name = itemDB.name;
				item.tip = itemDB.tip;
				item.qty = itemDB.qty;
				item.custom = [NSString stringWithFormat:@"FromCustom"];
				item.predefineId = i;
				item.predefineForAge = editingList.age;
				
				BOOL flag = NO;
				for (int x = 0; x < self.predefinedItems.count; x++)
				{
					PackingItem *it1 = [self.predefinedItems objectAtIndex:x];
					if ([it1.name isEqualToString:item.name])
						flag = YES;
				}
				
				if (!flag)
					[self.predefinedItems addObject:item];
				
				[item release];	
			}*/
		}
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"This is  image name %@",appDelegate.ImageName);
    if(appDelegate.ImageName)
    {
        addPhotoLabel.hidden=TRUE;
        photoView.image = [UIImage imageNamed:appDelegate.ImageName];
        appDelegate.imageName=nil;
        
    }
  
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
        {
        
            [myTableView setFrame:CGRectMake(0,myTableView.frame.origin.y , myTableView.frame.size.width, myTableView.frame.size.height)];
        }
       else
        {
            [myTableView setFrame:CGRectMake(100,myTableView.frame.origin.y , myTableView.frame.size.width, myTableView.frame.size.height)];
        }
    }
    else
    {
        if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
        {
            
            [myTableView setFrame:CGRectMake(0,myTableView.frame.origin.y , myTableView.frame.size.width, myTableView.frame.size.height)];
        }
        else
        {

            [myTableView setFrame:CGRectMake(0,myTableView.frame.origin.y , myTableView.frame.size.width, myTableView.frame.size.height)];
            familyItemView.frame=CGRectMake(familyItemView.frame.origin.x+115, familyItemView.frame.origin.y, familyItemView.frame.size.width, familyItemView.frame.size.height);
            tableHeaderView.frame=CGRectMake(tableHeaderView.frame.origin.x+115, tableHeaderView.frame.origin.y, tableHeaderView.frame.size.width, tableHeaderView.frame.size.height);
            boyGirlView.frame=CGRectMake(boyGirlView.frame.origin.x+115, boyGirlView.frame.origin.y, boyGirlView.frame.size.width, boyGirlView.frame.size.height);
            babyToddlerView.frame=CGRectMake(babyToddlerView.frame.origin.x+115, babyToddlerView.frame.origin.y, babyToddlerView.frame.size.width, babyToddlerView.frame.size.height);
            youItemView.frame=CGRectMake(youItemView.frame.origin.x+115, youItemView.frame.origin.y, youItemView.frame.size.width, youItemView.frame.size.height);
            
             nameField.frame=CGRectMake(nameField.frame.origin.x+115, nameField.frame.origin.y, nameField.frame.size.width, nameField.frame.size.height);
            photoView.frame=CGRectMake(photoView.frame.origin.x+115, photoView.frame.origin.y, photoView.frame.size.width, photoView.frame.size.height);
             addPhotoLabel.frame=CGRectMake(addPhotoLabel.frame.origin.x+115, addPhotoLabel.frame.origin.y, addPhotoLabel.frame.size.width, addPhotoLabel.frame.size.height);
         
        }
    }
    
	[self dismissPopOverController];
}


//- (void)viewDidAppear:(BOOL)animated {
//    [super viewDidAppear:animated];
//	
//}

/*
- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if(indexPath.section == 1)
		return boyGirlView.frame.size.height;
	else if(indexPath.section == 0) {
		if (TARGET_VERSION == VERSION_BABY_PACKING)
			return babyToddlerView.frame.size.height;
		else if (TARGET_VERSION == VERSION_FAMILY_PACKING)	
			return familyItemView.frame.size.height;
		else 
			return youItemView.frame.size.height;
	}
	
	return 0;
}
/*
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	NSString *title;
	switch (section) {
		case 0:
			if (TARGET_VERSION == VERSION_BABY_PACKING)
			{
				title = TITLE_SELECT_SEX_BABY;
			}
			else if (TARGET_VERSION == VERSION_FAMILY_PACKING)
			{	
				title = TITLE_SELECT_SEX_FAMILY;
			}
			else if (TARGET_VERSION == VERSION_YOU_PACKING)
				title= TITLE_SELECT_SEX_FAMILY;
			break;
		case 1:
			
			if (TARGET_VERSION == VERSION_BABY_PACKING)
			{	
				title = TITLE_SELECT_AGE_BABY;
			}
			else if (TARGET_VERSION == VERSION_FAMILY_PACKING)
			{
				title = TITLE_SELECT_AGE_FAMILY;
			}
			else if (TARGET_VERSION == VERSION_YOU_PACKING)
				title = nil;
			
			break;
		default:
			break;
	}
	return title;
}
 */


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		//cell.alpha = 0;
    }
    if (indexPath.section == 1) {
		//boyGirlCell.backgroundColor = [UIColor clearColor];
		//return boyGirlCell;
		[cell.contentView addSubview:boyGirlView];
	} else if (indexPath.section == 0) {
		if (TARGET_VERSION == VERSION_BABY_PACKING)
			//return babyToddlerCell;
			[cell.contentView addSubview:babyToddlerView];
		else if (TARGET_VERSION == VERSION_FAMILY_PACKING)
			//return familyItemCell;		
			[cell.contentView addSubview:familyItemView];
		else
			[cell.contentView addSubview:youItemView];
	}

	
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	//cell.text = @"test";
    // Set up the cell...
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
	// AnotherViewController *anotherViewController = [[AnotherViewController alloc] initWithNibName:@"AnotherView" bundle:nil];
	// [self.navigationController pushViewController:anotherViewController];
	// [anotherViewController release];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	return YES;
}

- (void) dismissPopOverController
{
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
		if (popoverController != nil)
			[popoverController dismissPopoverAnimated:YES];
	} else {
		
	}
}

- (void) openImagePickerController {
	UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];		
	imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
	//imagePicker.navigationBar.barStyle = UIBarStyleBlackOpaque;
	imagePicker.delegate = self;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
		popoverController = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
		[popoverController presentPopoverFromRect:photoView.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
		[imagePicker release];	
	} else {
		[self presentModalViewController:imagePicker animated:YES];
		[imagePicker release];
	}
}

#pragma mark UIImagePickerControllerDelegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo 
{
	NSLog(@"didFinishPickingImage w = %f h = %f", image.size.width, image.size.height);
	
	photoView.image = image;
	//photoView.image = [self createPhotoIcon:image toSize:LIST_ICON_SIZE];
	//photoView.image = [self ClipImage:image ClipSize:CGSizeMake(40,40) ScaleToClip:YES Center:YES];
	//photoView.image = [self ClipImage:image ClipSize:CGSizeMake(40,40) ScaleToClip:YES Center:NO];
	//photoView.image = [self scaleImage:image toMaxSize:40];
	//photoView.image = [self scaleAndRotateImage:image scaleFlag:YES];
	//photoView.image = [self scaleAndRotateImage:image resolution:40 scaleFlag:YES];
	addPhotoLabel.alpha = 0;
	
	
    if (popoverController != nil)
        [popoverController dismissPopoverAnimated:YES];
    
    [picker dismissModalViewControllerAnimated:YES];
	
	[myTableView reloadData];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
	//NSLog(@"imagePickerControllerDidCancel");
	
    if (popoverController != nil)
        [popoverController dismissPopoverAnimated:YES];
    
    [picker dismissModalViewControllerAnimated:YES];
    
}

- (UIImage *)scaleAndRotateImage:(UIImage *)image resolution:(int)resolution scaleFlag:(BOOL)aFlag
{
	//int kMaxResolution = 40; // Or whatever
	
	CGImageRef imgRef = image.CGImage;
	
	CGFloat width = CGImageGetWidth(imgRef);
	CGFloat height = CGImageGetHeight(imgRef);
	
	CGAffineTransform transform = CGAffineTransformIdentity;
	CGRect bounds = CGRectMake(0, 0, width, height);
	
	if(aFlag==YES)
	{
		if (width > resolution || height > resolution) {
			CGFloat ratio = width/height;
			// for max resolution
//			if (ratio > 1) {
//				bounds.size.width = resolution;
//				bounds.size.height = bounds.size.width / ratio;
//			}
//			else {
//				bounds.size.height = resolution;
//				bounds.size.width = bounds.size.height * ratio;
//			}
			
			// for min resolution
			if (ratio > 1) {
				bounds.size.height = resolution;
				bounds.size.width = resolution * ratio;
			}
			else {
				bounds.size.width = resolution;
				bounds.size.height = resolution / ratio;
			}
		}
	}
	
	CGFloat scaleRatio = bounds.size.width / width;
	CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
	CGFloat boundHeight;
	UIImageOrientation orient = image.imageOrientation;
	NSLog(@"UIImageOrientation = %d", orient);
	switch(orient) {
			
		case UIImageOrientationUp: //EXIF = 1
			transform = CGAffineTransformIdentity;
			break;
			
		case UIImageOrientationUpMirrored: //EXIF = 2
			transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			break;
			
		case UIImageOrientationDown: //EXIF = 3
			transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
			transform = CGAffineTransformRotate(transform, M_PI);
			break;
			
		case UIImageOrientationDownMirrored: //EXIF = 4
			transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
			transform = CGAffineTransformScale(transform, 1.0, -1.0);
			break;
			
		case UIImageOrientationLeftMirrored: //EXIF = 5
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
			break;
			
		case UIImageOrientationLeft: //EXIF = 6
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
			break;
			
		case UIImageOrientationRightMirrored: //EXIF = 7
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeScale(-1.0, 1.0);
			transform = CGAffineTransformRotate(transform, M_PI / 2.0);
			break;
			
		case UIImageOrientationRight: //EXIF = 8
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
			transform = CGAffineTransformRotate(transform, M_PI / 2.0);
			break;
			
		default:
			[NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
			
	}
	
	UIGraphicsBeginImageContext(bounds.size);
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
		CGContextScaleCTM(context, -scaleRatio, scaleRatio);
		CGContextTranslateCTM(context, -height, 0);
	}
	else {
		CGContextScaleCTM(context, scaleRatio, -scaleRatio);
		CGContextTranslateCTM(context, 0, -height);
	}
	
	CGContextConcatCTM(context, transform);
	
	CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
	UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return imageCopy;
}

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{            return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
                toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
            {
                
                [myTableView setFrame:CGRectMake(0,myTableView.frame.origin.y , myTableView.frame.size.width, myTableView.frame.size.height)];
            }
            else
            {
                [myTableView setFrame:CGRectMake(100,myTableView.frame.origin.y , myTableView.frame.size.width, myTableView.frame.size.height)];
            }
        }
    else
    {
        if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
            toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        {
            
            [myTableView setFrame:CGRectMake(0,myTableView.frame.origin.y , myTableView.frame.size.width, myTableView.frame.size.height)];
        }
        else
        {
            [myTableView setFrame:CGRectMake(150,myTableView.frame.origin.y , myTableView.frame.size.width, myTableView.frame.size.height)];
        }
    }
    
    
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
            toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        {
            
            [myTableView setFrame:CGRectMake(0,myTableView.frame.origin.y , myTableView.frame.size.width, myTableView.frame.size.height)];
            
            familyItemView.frame=CGRectMake(familyItemView.frame.origin.x-115, familyItemView.frame.origin.y, familyItemView.frame.size.width, familyItemView.frame.size.height);
            tableHeaderView.frame=CGRectMake(tableHeaderView.frame.origin.x-115, tableHeaderView.frame.origin.y, tableHeaderView.frame.size.width, tableHeaderView.frame.size.height);
            boyGirlView.frame=CGRectMake(boyGirlView.frame.origin.x-115, boyGirlView.frame.origin.y, boyGirlView.frame.size.width, boyGirlView.frame.size.height);
            babyToddlerView.frame=CGRectMake(babyToddlerView.frame.origin.x-115, babyToddlerView.frame.origin.y, babyToddlerView.frame.size.width, babyToddlerView.frame.size.height);
            youItemView.frame=CGRectMake(youItemView.frame.origin.x-115, youItemView.frame.origin.y, youItemView.frame.size.width, youItemView.frame.size.height);
            
            nameField.frame=CGRectMake(nameField.frame.origin.x-115, nameField.frame.origin.y, nameField.frame.size.width, nameField.frame.size.height);
            photoView.frame=CGRectMake(photoView.frame.origin.x-115, photoView.frame.origin.y, photoView.frame.size.width, photoView.frame.size.height);
            addPhotoLabel.frame=CGRectMake(addPhotoLabel.frame.origin.x-115, addPhotoLabel.frame.origin.y, addPhotoLabel.frame.size.width, addPhotoLabel.frame.size.height);
        }
        else
        {
            
            [myTableView setFrame:CGRectMake(0,myTableView.frame.origin.y , myTableView.frame.size.width, myTableView.frame.size.height)];
            familyItemView.frame=CGRectMake(familyItemView.frame.origin.x+115, familyItemView.frame.origin.y, familyItemView.frame.size.width, familyItemView.frame.size.height);
            tableHeaderView.frame=CGRectMake(tableHeaderView.frame.origin.x+115, tableHeaderView.frame.origin.y, tableHeaderView.frame.size.width, tableHeaderView.frame.size.height);
            boyGirlView.frame=CGRectMake(boyGirlView.frame.origin.x+115, boyGirlView.frame.origin.y, boyGirlView.frame.size.width, boyGirlView.frame.size.height);
            babyToddlerView.frame=CGRectMake(babyToddlerView.frame.origin.x+115, babyToddlerView.frame.origin.y, babyToddlerView.frame.size.width, babyToddlerView.frame.size.height);
            youItemView.frame=CGRectMake(youItemView.frame.origin.x+115, youItemView.frame.origin.y, youItemView.frame.size.width, youItemView.frame.size.height);
            
            nameField.frame=CGRectMake(nameField.frame.origin.x+115, nameField.frame.origin.y, nameField.frame.size.width, nameField.frame.size.height);
            photoView.frame=CGRectMake(photoView.frame.origin.x+115, photoView.frame.origin.y, photoView.frame.size.width, photoView.frame.size.height);
            addPhotoLabel.frame=CGRectMake(addPhotoLabel.frame.origin.x+115, addPhotoLabel.frame.origin.y, addPhotoLabel.frame.size.width, addPhotoLabel.frame.size.height);
            
        }
    }

}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}

@end


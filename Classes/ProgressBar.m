//
//  ProgressBar.m
//  BabyPacking
//
//  Created by Gary He on 5/26/09.
//  Copyright 2009 test. All rights reserved.
//

#import "ProgressBar.h"
#import "Constant.h"


@implementation ProgressBar
@synthesize progress;

- (id)initWithCoder:(NSCoder *)decoder {
	if (self = [super initWithCoder:decoder]) {
		self.backgroundColor = [UIColor clearColor];
		self.progress = 0;
	}
	return self;
}

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Initialization code
    }
    return self;
}

//- (void)drawRect:(CGRect)rect {
//	
//	CGContextRef curContext = UIGraphicsGetCurrentContext();
//	
//	CGContextClearRect(curContext, rect);
//	
//	CGFloat radius = 5.0;
//	CGFloat minx,midx,maxx,miny,midy, maxy;
//	
//	CGRect borderRect = rect;
//	CGRect progressRect = rect;
//	progressRect.size.width = rect.size.width * progress;
//	
//	
//	
//	minx = CGRectGetMinX(borderRect); 
//	midx = CGRectGetMidX(borderRect);
//	maxx = CGRectGetMaxX(borderRect);
//	miny = CGRectGetMinY(borderRect);
//	midy = CGRectGetMidY(borderRect); 
//	maxy = CGRectGetMaxY(borderRect);
//	
//	CGContextMoveToPoint(curContext, minx, midy);
//	CGContextAddArcToPoint(curContext, minx, miny, midx, miny, radius);
//	CGContextAddArcToPoint(curContext, maxx, miny, maxx, midy, radius);
//	CGContextAddArcToPoint(curContext, maxx, maxy, midx, maxy, radius);
//	CGContextAddArcToPoint(curContext, minx, maxy, minx, midy, radius);
//	CGContextClosePath(curContext);
//	CGContextClip(curContext);
//	
//	CGContextSaveGState(curContext);
//	
//	minx = CGRectGetMinX(progressRect); 
//	midx = CGRectGetMidX(progressRect);
//	maxx = CGRectGetMaxX(progressRect);
//	miny = CGRectGetMinY(progressRect);
//	midy = CGRectGetMidY(progressRect); 
//	maxy = CGRectGetMaxY(progressRect);
//	
//	CGContextMoveToPoint(curContext, minx, midy);
//	CGContextAddArcToPoint(curContext, minx, miny, midx, miny, radius);
//	CGContextAddArcToPoint(curContext, maxx, miny, maxx, midy, radius);
//	CGContextAddArcToPoint(curContext, maxx, maxy, midx, maxy, radius);
//	CGContextAddArcToPoint(curContext, minx, maxy, minx, midy, radius);
//	CGContextClosePath(curContext);
//	CGContextClip(curContext);
//	CGContextSetInterpolationQuality(curContext, kCGInterpolationLow);
//	
//	
//	CGGradientRef myGradient;
//	CGColorSpaceRef myColorspace;
//	
//	size_t num_locations = 2;
//	CGFloat locations[2] = { 0.0, 1.0};
//	
//	CGFloat components[8] = {
//		COLOR_PROGRESS_BAR_TOP_R,
//		COLOR_PROGRESS_BAR_TOP_G,
//		COLOR_PROGRESS_BAR_TOP_B,
//		1.0,
//		COLOR_PROGRESS_BAR_BOTTOM_R,
//		COLOR_PROGRESS_BAR_BOTTOM_G,
//		COLOR_PROGRESS_BAR_BOTTOM_B,
//		1.0 
//	};
//	
//	myColorspace = CGColorSpaceCreateDeviceRGB();
//	myGradient = CGGradientCreateWithColorComponents (myColorspace, components,
//													  locations, num_locations);
//	
//	CGPoint myStartPoint, myEndPoint;
//	myStartPoint.x = 0.0;
//	myStartPoint.y = 0.0;
//	myEndPoint.x = 0.0;
//	myEndPoint.y = 25.0;
//	CGContextDrawLinearGradient (curContext, myGradient, myStartPoint, myEndPoint, 0);
//	
//	CGContextRestoreGState(curContext);
//
//	
//	CGContextMoveToPoint(curContext, minx, midy);
//	CGContextAddArcToPoint(curContext, minx, miny, midx, miny, radius);
//	CGContextAddArcToPoint(curContext, maxx, miny, maxx, midy, radius);
//	CGContextAddArcToPoint(curContext, maxx, maxy, midx, maxy, radius);
//	CGContextAddArcToPoint(curContext, minx, maxy, minx, midy, radius);
//	CGContextClosePath(curContext);
//	[[UIColor blackColor] set];
//	CGContextSetLineWidth(curContext, 2.0);
//	
//	CGContextStrokePath(curContext);
//	
//	//[[UIColor whiteColor] set];
//	[BOY_COLOR set];
//	
//	CGFloat fontSize = 15;
//	CGRect textRect = rect;
//	textRect.origin.y = (rect.size.height - fontSize) / 2 - 2;
//	//NSLog(@"textRect.origin.y = %f", textRect.origin.y);
//	textRect.size.height = fontSize;
//	NSString *text1 = [NSString stringWithFormat:@"%d%%", (int)(progress * 100)];
//	[text1 drawInRect:textRect withFont:[UIFont boldSystemFontOfSize:fontSize] lineBreakMode:UILineBreakModeTailTruncation alignment:UITextAlignmentCenter];
//	
//}

- (void)drawRect:(CGRect)rect {
	
	CGContextRef curContext = UIGraphicsGetCurrentContext();
	
	//CGContextClearRect(curContext, rect);
	CGContextSaveGState(curContext);

	CGRect borderRect = rect;
	CGRect progressRect = rect;
	progressRect.size.width = rect.size.width * progress;
	
	CGFloat radius = 5.0;
//	if (radius > progressRect.size.width) {
//		radius = progressRect.size.width;
//	}
	
	CGFloat minx = CGRectGetMinX(progressRect); 
	CGFloat midx = CGRectGetMidX(progressRect);
	CGFloat maxx = CGRectGetMaxX(progressRect);
	CGFloat miny = CGRectGetMinY(progressRect);
	CGFloat midy = CGRectGetMidY(progressRect); 
	CGFloat maxy = CGRectGetMaxY(progressRect);
	
	
	CGContextMoveToPoint(curContext, minx, midy);
	CGContextAddArcToPoint(curContext, minx, miny, midx, miny, radius);
	CGContextAddArcToPoint(curContext, maxx, miny, maxx, midy, radius);
	CGContextAddArcToPoint(curContext, maxx, maxy, midx, maxy, radius);
	CGContextAddArcToPoint(curContext, minx, maxy, minx, midy, radius);
	CGContextClosePath(curContext);
	CGContextClip(curContext);
	//CGContextSetInterpolationQuality(curContext, kCGInterpolationLow);
	
	
	CGGradientRef myGradient;
	CGColorSpaceRef myColorspace;
	
	size_t num_locations = 2;
	CGFloat locations[2] = { 0.0, 1.0};

	CGFloat components[8] = {
		COLOR_PROGRESS_BAR_TOP_R,
		COLOR_PROGRESS_BAR_TOP_G,
		COLOR_PROGRESS_BAR_TOP_B,
		1.0,
		COLOR_PROGRESS_BAR_BOTTOM_R,
		COLOR_PROGRESS_BAR_BOTTOM_G,
		COLOR_PROGRESS_BAR_BOTTOM_B,
		1.0 
	};
	
	myColorspace = CGColorSpaceCreateDeviceRGB();
	myGradient = CGGradientCreateWithColorComponents (myColorspace, components,
													  locations, num_locations);
	
	CGPoint myStartPoint, myEndPoint;
	myStartPoint.x = 0.0;
	myStartPoint.y = 0.0;
	myEndPoint.x = 0.0;
	myEndPoint.y = 25.0;
	//CGContextDrawLinearGradient (curContext, myGradient, myStartPoint, myEndPoint, 0);
	CGGradientRelease(myGradient);
	CGColorSpaceRelease(myColorspace);
	
	
	
	CGContextRestoreGState(curContext);
	minx = CGRectGetMinX(borderRect); 
	midx = CGRectGetMidX(borderRect);
	maxx = CGRectGetMaxX(borderRect);
	miny = CGRectGetMinY(borderRect);
	midy = CGRectGetMidY(borderRect); 
	maxy = CGRectGetMaxY(borderRect);
	
	CGContextMoveToPoint(curContext, minx, midy);
	CGContextAddArcToPoint(curContext, minx, miny, midx, miny, radius);
	CGContextAddArcToPoint(curContext, maxx, miny, maxx, midy, radius);
	CGContextAddArcToPoint(curContext, maxx, maxy, midx, maxy, radius);
	CGContextAddArcToPoint(curContext, minx, maxy, minx, midy, radius);
	CGContextClosePath(curContext);
	CGContextClip(curContext);
	
	CGContextMoveToPoint(curContext, minx, midy);
	CGContextAddArcToPoint(curContext, minx, miny, midx, miny, radius);
	CGContextAddArcToPoint(curContext, maxx, miny, maxx, midy, radius);
	CGContextAddArcToPoint(curContext, maxx, maxy, midx, maxy, radius);
	CGContextAddArcToPoint(curContext, minx, maxy, minx, midy, radius);
	CGContextClosePath(curContext);
	[[UIColor blackColor] set];
	CGContextSetLineWidth(curContext, 2.0);
	
	CGContextStrokePath(curContext);

	//[[UIColor whiteColor] set];
	[BOY_COLOR set];
	
	CGFloat fontSize = 15;
	CGRect textRect = rect;
	textRect.origin.y = (rect.size.height - fontSize) / 2 - 2;
	//NSLog(@"textRect.origin.y = %f", textRect.origin.y);
	textRect.size.height = fontSize;
	NSString *text1 = [NSString stringWithFormat:@"%d%%", (int)(progress * 100)];
	[text1 drawInRect:textRect withFont:[UIFont boldSystemFontOfSize:fontSize] lineBreakMode:UILineBreakModeTailTruncation alignment:UITextAlignmentCenter];
	
}


- (void)dealloc {
    [super dealloc];
}


@end

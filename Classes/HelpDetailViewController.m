//
//  HelpDetailViewController.m
//  BabyPacking
//
//  Created by Hitesh Vaghasiya on 25/04/11.
//  Copyright 2011 Dev IT Solution pvt ltd. All rights reserved.
//

#import "HelpDetailViewController.h"
#import "BabyPackingAppDelegate.h"
#import "Constant.h"
@implementation HelpDetailViewController

@synthesize categoryDict;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
//        layoutManager = [[LayoutManager alloc] initWithPortraitView:self.view andLandscapeNIB:@"HelpDetailViewController_L"];
        //self = [super initWithNibName:@"RecalliPhoneView" bundle:nibBundleOrNil];
        
    }
    return self;
}

//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
//    {
//        [layoutManager translateToPortraitForView:self.view withAnimation:NO];
//    }
//    else
//    {
//        [layoutManager translateToLandscapeForView:self.view withAnimation:NO];
//    }
//}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
	appDelegate = (BabyPackingAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSString *nameString = [self.categoryDict objectForKey:@"name"];
	//NSArray *componentArray = [nameString componentsSeparatedByString:@"."];
	

    
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor whiteColor];
    
	NSString *customFontName = [appDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];		
	}
	else {
		label.font=[UIFont fontWithName:customFontName size:fontSize];		
	}
	
	[label sizeToFit];
    
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(0, 0, 30, 30);
    doneBtn.tag=2;
    [doneBtn setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
    // [Editbtn setBackgroundColor:[UIColor blackColor]];
    [doneBtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    doneButton=[[UIBarButtonItem alloc] initWithCustomView:doneBtn];
	
	self.navigationItem.leftBarButtonItem = doneButton;
	
	
	//label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
            label.textColor=[UIColor whiteColor];
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label.textColor = theColour1;
	}
	// and finally set the colour of your label
	
	self.navigationItem.titleView = label;	
	label.text = nameString;
	[label sizeToFit];
	
	
	//[[componentArray objectAtIndex:1] stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
	if (![[self.categoryDict objectForKey:@"description"] isEqualToString:@""]) {
		[myWebView loadHTMLString:[self.categoryDict objectForKey:@"description"] baseURL:nil];
	} else {
		[myWebView loadHTMLString:[self.categoryDict objectForKey:@"shortdescription"] baseURL:nil];
	}
	
    [super viewDidLoad];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
}

- (IBAction) buttonAction:(id)sender
{
	if (sender == doneButton.customView) {
		[self.navigationController popViewControllerAnimated:YES];
	}
}

- (void)dealloc 
{
	[doneButton release];
	[myWebView release];
	
	[categoryDict release];
	
    [super dealloc];
}

//#pragma mark - Orientation Methods
//#ifdef IOS_OLDER_THAN_6
//
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
//{
//    return YES;
//    
//}
//
//#endif
//
//#ifdef IOS_NEWER_OR_EQUAL_TO_6
//
//- (BOOL)shouldAutorotate
//{
//    return YES;
//}
//
//- (NSUInteger)supportedInterfaceOrientations
//{
//    return (UIInterfaceOrientationMaskAll);
//}
//
//#endif
//
//- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
//{
//    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
//    //    if (IS_IPAD)
//    //    {
//    
//    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
//        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
//    {
//        // [layoutManager translateToPortraitForView:self.view withAnimation:YES];
//    }
//    else
//    {
//        // [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
//    }
//    // }
//}
//
//- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
//{
//    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
//}
//
//- (BOOL)isOrientationPortrait
//{
//    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
//    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
//    {
//        return YES;
//    }
//    else
//    {
//        return NO;
//    }
//    return NO;
//}


@end

//
//  AddCategoryViewController.h
//  BabyPacking
//
//  Created by Hitesh on 12/17/10.
//  Copyright 2010 cmc limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BabyPackingAppDelegate;
@class PackingList;

@interface AddCategoryViewController : UITableViewController 
{
	BabyPackingAppDelegate *appdelegate;
	IBOutlet UIBarButtonItem *saveButton;
	IBOutlet UIBarButtonItem *cancelButton;
	IBOutlet UITableViewCell *categoryNameCell;
	IBOutlet UITextField	*nameField;
	
	PackingList *currentList;
	PackingList *editingCategory;
	
	NSMutableArray *categoryListArray;
	NSString *strCategoryName;
	BOOL isNewCategory;
}
//New
@property (nonatomic, retain) NSString *strCategoryName;
//
@property (nonatomic, retain) PackingList *currentList;
@property (nonatomic, retain) PackingList *editingCategory;

@property (nonatomic, retain) NSMutableArray *categoryListArray;

- (IBAction) buttonAction:(id)sender;

@end

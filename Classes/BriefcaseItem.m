//
//  List.m
//  BabyPacking
//
//  Created by Jay Lee on 09/04/09.
//  Copyright 2009 test. All rights reserved.
//

#import "BriefcaseItem.h"
#import "BabyPacking_Prefix.pch"

static sqlite3_stmt *select_all_statement = nil;
static sqlite3_stmt *insert_statement = nil;
static sqlite3_stmt *init_statement = nil;
static sqlite3_stmt *delete_statement = nil;
static sqlite3_stmt *update_statement = nil;
static sqlite3_stmt *upgrade_check_table_statement = nil;
static sqlite3_stmt *upgrade_select_statement = nil;
static sqlite3_stmt *upgrade_insert_statement = nil;

@interface BriefcaseItem(Private)

- (void) deleteItemsFromDB;
- (void) loadItemsFromDB;

@end


@implementation BriefcaseItem

@synthesize title;
@synthesize passport;
@synthesize license;
@synthesize ssn;
@synthesize id_card;
@synthesize airline_name;
@synthesize flight;
@synthesize frequent_flyer;
@synthesize car_name;
@synthesize license_plate;
@synthesize hotel_name;
@synthesize hotel_phone;
@synthesize hotel_address;
@synthesize embassy_name;
@synthesize embassy_address;
@synthesize embassy_emergency;
@synthesize other;
@synthesize predefineId;
@synthesize briefcase;

- (id) init {
	if (self = [super init]) {
		self.title = @"";
		self.passport = @"";
		self.license = @"";
		self.ssn = @"";
		self.id_card = @"";
		self.airline_name = @"";
		self.flight = @"";
		self.frequent_flyer = @"";
		self.car_name = @"";
		self.license_plate = @"";
		self.hotel_name = @"";
		self.hotel_address = @"";
		self.hotel_phone = @"";
		self.embassy_name = @"";
		self.embassy_address = @"";
		self.embassy_emergency = @"";
		self.other = @"";
		self.pk = INVALID_PK;
		self.briefcase = [NSMutableArray arrayWithCapacity:16];
	}
	return self;
}

- (void)dealloc {
	[title release];
	[passport release];
	[license release];
	[ssn release];
	[id_card release];
	[airline_name release];
	[flight release];
	[frequent_flyer release];
	[car_name release];
	[license_plate release];
	[hotel_name release];
	[hotel_phone release];
	[hotel_address release];
    [super dealloc];
}

- (BOOL) editable {
	return (pk != INVALID_PK);
}

- (BOOL)isEqual:(id)anObject {
	//NSLog(@"isEqual >>>>>>>");
	if (anObject == self)
        return YES;
    if (!anObject || ![anObject isKindOfClass:[self class]])
        return NO;
    return [self isEqualToItem:anObject];
}

- (BOOL)isEqualToItem:(BriefcaseItem *)item {
	//NSLog(@"isEqualToItem >>>>>>>(%d =? %d)",predefineId ,item.predefineId);
    if (self.predefineId == item.predefineId) {
		return YES;
	}
    return NO;
}

/*
- (NSUInteger)hash {
    NSUInteger hash = 0;
    hash += [name hash];
    hash += [tip hash];
	hash += predefineId;
    return hash;
}
*/

- (id)initWithPK:(NSInteger)primaryKey {
	NSLog(@"BriefcaseItem initWithPK pk = %d ", primaryKey);
	if (self = [self init]) {
		self.pk = primaryKey;
		sqlite3 *database = conn.database;
		// Compile the query for retrieving book data. See insertNewBookIntoDatabase: for more detail.
        if (init_statement == nil) {
            // Note the '?' at the end of the query. This is a parameter which can be replaced by a bound variable.
            // This is a great way to optimize because frequently used queries can be compiled once, then with each
            // use new variable values can be bound to placeholders.
            const char *sql = "SELECT pk, title, passport, license, ssn, id_card, airline_name, flight, frequent_flyer, car_name, license_plate, hotel_name, hotel_phone, hotel_address, embassy_name, embassy_address, embassy_emergency, other FROM briefcase WHERE pk=?";
            if (sqlite3_prepare_v2(database, sql, -1, &init_statement, NULL) != SQLITE_OK) {
                NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));		
            }
        }
		// For this query, we bind the primary key to the first (and only) placeholder in the statement.
        // Note that the parameters are numbered from 1, not from 0.
        sqlite3_bind_int(init_statement, 1, pk);
		if (sqlite3_step(init_statement) == SQLITE_ROW) {
			int field = 1;
			char * val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.title = [NSString stringWithUTF8String:val];
			} else {
				self.title = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.passport = [NSString stringWithUTF8String:val];
			} else {
				self.passport = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.license = [NSString stringWithUTF8String:val];
			}else {
				self.license = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.ssn = [NSString stringWithUTF8String:val];
			}else {
				self.ssn = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.id_card = [NSString stringWithUTF8String:val];
			}else {
				self.id_card = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.airline_name = [NSString stringWithUTF8String:val];
			}else {
				self.airline_name = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.flight = [NSString stringWithUTF8String:val];
			}else {
				self.flight = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.frequent_flyer = [NSString stringWithUTF8String:val];
			}else {
				self.frequent_flyer = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.car_name = [NSString stringWithUTF8String:val];
			}else {
				self.car_name = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.license_plate = [NSString stringWithUTF8String:val];
			}else {
				self.license_plate = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.hotel_name = [NSString stringWithUTF8String:val];
			}else {
				self.hotel_name = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.hotel_phone = [NSString stringWithUTF8String:val];
			}else {
				self.hotel_phone = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.hotel_address = [NSString stringWithUTF8String:val];
			}else {
				self.hotel_address = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.embassy_name = [NSString stringWithUTF8String:val];
			}else {
				self.embassy_name = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.embassy_address = [NSString stringWithUTF8String:val];
			}else {
				self.embassy_address = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.embassy_emergency = [NSString stringWithUTF8String:val];
			}else {
				self.embassy_emergency = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.other = [NSString stringWithUTF8String:val];
			}else {
				self.other = @"";
			}
		} 
		// Reset the statement for future reuse.
        sqlite3_reset(init_statement);
        dirty = NO;
		hydrated = YES;
	}
	return self;
}

- (void)insertData {
	if (!dirty) return;
	NSLog(@"BriefcaseItem insertData pk = %d dirty = %d", pk, dirty);
	//if (pk == INVALID_PK) return;
	
    sqlite3 *database = conn.database;
    if (insert_statement == nil) {
        const char *sql = "INSERT INTO briefcase (title, passport, license, ssn, id_card, airline_name, flight, frequent_flyer, car_name, license_plate, hotel_name, hotel_phone, hotel_address, embassy_name, embassy_address, embassy_emergency, other) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        if (sqlite3_prepare_v2(database, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
	if (title == nil) self.title = @"";
	if (passport == nil) self.passport = @"";
	if (license == nil) self.license = @"";
	if (ssn == nil) self.ssn = @"";
	if (id_card == nil) self.id_card = @"";
	if (airline_name == nil) self.airline_name = @"";
	if (flight == nil) self.flight = @"";
	if (frequent_flyer == nil) self.frequent_flyer = @"";
	if (car_name == nil) self.car_name = @"";
	if (license_plate == nil) self.license_plate = @"";
	if (hotel_name == nil) self.hotel_name = @"";
	if (hotel_phone == nil) self.hotel_phone = @"";
	if (hotel_address == nil) self.hotel_address = @"";
	if (embassy_name == nil) self.embassy_name = @"";
	if (embassy_address == nil) self.embassy_address = @"";
	if (embassy_emergency == nil) self.embassy_emergency = @"";
	if (other == nil) self.other = @"";
	int field = 1;
    sqlite3_bind_text(insert_statement, field++, [title UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(insert_statement, field++, [passport UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [license UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [ssn UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [id_card UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [airline_name UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [flight UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [frequent_flyer UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [car_name UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [license_plate UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [hotel_name UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [hotel_phone UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [hotel_address UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [embassy_name UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [embassy_address UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [embassy_emergency UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [other UTF8String],-1, SQLITE_TRANSIENT);
	int success = sqlite3_step(insert_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(insert_statement);
    if (success == SQLITE_ERROR) {
        NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
    } else {
        // SQLite provides a method which retrieves the value of the most recently auto-generated primary key sequence
        // in the database. To access this functionality, the table should have a column declared of type 
        // "INTEGER PRIMARY KEY"
        self.pk = sqlite3_last_insert_rowid(database);
    }
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
}

- (void)updateData {
	if (!dirty) return;
	if (pk == INVALID_PK) return;
	NSLog(@"BriefcaseItem updateData pk = %d dirty = %d", pk, dirty);
    sqlite3 *database = conn.database;
    if (update_statement == nil) {
        const char *sql = "UPDATE briefcase SET title=?, passport=?, license=?, ssn=?, id_card=?, airline_name=?, flight=?, frequent_flyer=?, car_name=?, license_plate=?, hotel_name=?, hotel_phone=?, hotel_address=?, embassy_name=?, embassy_address=?, embassy_emergency=?, other=? WHERE pk=?";
        if (sqlite3_prepare_v2(database, sql, -1, &update_statement, NULL) != SQLITE_OK) {
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
	if (title == nil) self.title = @"";
	if (passport == nil) self.passport = @"";
	if (license == nil) self.license = @"";
	if (ssn == nil) self.ssn = @"";
	if (id_card == nil) self.id_card = @"";
	if (airline_name == nil) self.airline_name = @"";
	if (flight == nil) self.flight = @"";
	if (frequent_flyer == nil) self.frequent_flyer = @"";
	if (car_name == nil) self.car_name = @"";
	if (license_plate == nil) self.license_plate = @"";
	if (hotel_name == nil) self.hotel_name = @"";
	if (hotel_phone == nil) self.hotel_phone = @"";
	if (hotel_address == nil) self.hotel_address = @"";
	if (embassy_name == nil) self.embassy_name = @"";
	if (embassy_address == nil) self.embassy_address = @"";
	if (embassy_emergency == nil) self.embassy_emergency = @"";
	if (other == nil) self.other = @"";
	int field = 1;
	sqlite3_bind_text(update_statement, field++, [title UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [passport UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [license UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [ssn UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [id_card UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [airline_name UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [flight UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [frequent_flyer UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [car_name UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [license_plate UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [hotel_name UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [hotel_phone UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [hotel_address UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [embassy_name UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [embassy_address UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [embassy_emergency UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [other UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_int(update_statement, field++, pk);
    int success = sqlite3_step(update_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(update_statement);
    if (success == SQLITE_ERROR) {
        NSAssert1(0, @"Error: failed to update to the database with message '%s'.", sqlite3_errmsg(database));
    }
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
}

- (void)deleteData {
	if (pk == INVALID_PK) return;
	NSLog(@"BriefcaseItem deleteData pk = %d", pk);
	sqlite3 *database = conn.database;
    if (delete_statement == nil) {
        const char *sql = "DELETE FROM briefcase WHERE pk = ?";
        if (sqlite3_prepare_v2(database, sql, -1, &delete_statement, NULL) != SQLITE_OK) {
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
	sqlite3_bind_int(delete_statement, 1, pk);
    //int success = sqlite3_step(delete_statement);
	sqlite3_step(delete_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(delete_statement);
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
}

- (void)deleteAllData {
//	if (pk == INVALID_PK) return;
//	NSLog(@"BriefcaseItem deleteData pk = %d", pk);
	sqlite3 *database = conn.database;
    if (delete_statement == nil) {
        const char *sql = "DELETE FROM briefcase";
        if (sqlite3_prepare_v2(database, sql, -1, &delete_statement, NULL) != SQLITE_OK) {
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
//	sqlite3_bind_int(delete_statement, 1, pk);
    //int success = sqlite3_step(delete_statement);
	sqlite3_step(delete_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(delete_statement);
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
}

- (void)setTitle:(NSString *)aString {
    if ((!title && !aString) || (title && aString && [title isEqualToString:aString])) return;
    dirty = YES;
    [title release];
    title = [aString copy];
}

- (void)setPassport:(NSString *)aString {
    if ((!passport && !aString) || (passport && aString && [passport isEqualToString:aString])) return;
    dirty = YES;
    [passport release];
    passport = [aString copy];
}

- (void)setLicense:(NSString *)aString {
	if ((!license && !aString) || (license && aString && [license isEqualToString:aString])) return;
	dirty = YES;
	[license release];
	license = [aString copy];
}

- (void)setSsn:(NSString *)aString {
	if ((!ssn && !aString) || (ssn && aString && [ssn isEqualToString:aString])) return;
	dirty = YES;
	[ssn release];
	ssn = [aString copy];
}

- (void)setId_card:(NSString *)aString {
	if ((!id_card && !aString) || (id_card && aString && [id_card isEqualToString:aString])) return;
	dirty = YES;
	[id_card release];
	id_card = [aString copy];
}

- (void)setAirline_name:(NSString *)aString {
	if ((!airline_name && !aString) || (airline_name && aString && [airline_name isEqualToString:aString])) return;
	dirty = YES;
	[airline_name release];
	airline_name = [aString copy];
}

- (void)setFlight:(NSString *)aString {
	if ((!flight && !aString) || (flight && aString && [flight isEqualToString:aString])) return;
	dirty = YES;
	[flight release];
	flight = [aString copy];
}

- (void)setFrequent_flyer:(NSString *)aString {
	if ((!frequent_flyer && !aString) || (frequent_flyer && aString && [frequent_flyer isEqualToString:aString])) return;
	dirty = YES;
	[frequent_flyer release];
	frequent_flyer = [aString copy];
}

- (void)setCar_name:(NSString *)aString {
	if ((!car_name && !aString) || (car_name && aString && [car_name isEqualToString:aString])) return;
	dirty = YES;
	[car_name release];
	car_name = [aString copy];
}

- (void)setLicense_plate:(NSString *)aString {
	if ((!license_plate && !aString) || (license_plate && aString && [license_plate isEqualToString:aString])) return;
	dirty = YES;
	[license_plate release];
	license_plate = [aString copy];
}

- (void)setHotel_name:(NSString *)aString {
	if ((!hotel_name && !aString) || (hotel_name && aString && [hotel_name isEqualToString:aString])) return;
	dirty = YES;
	[hotel_name release];
	hotel_name = [aString copy];
}

- (void)setHotel_phone:(NSString *)aString {
	if ((!hotel_phone && !aString) || (hotel_phone && aString && [hotel_phone isEqualToString:aString])) return;
	dirty = YES;
	[hotel_phone release];
	hotel_phone = [aString copy];
}

- (void)seHotel_address:(NSString *)aString {
	if ((!hotel_address && !aString) || (hotel_address && aString && [hotel_address isEqualToString:aString])) return;
	dirty = YES;
	[hotel_address release];
	hotel_address = [aString copy];
}

- (void)setEmbassy_name:(NSString *)aString {
	if ((!embassy_name && !aString) || (embassy_name && aString && [embassy_name isEqualToString:aString])) return;
	dirty = YES;
	[embassy_name release];
	embassy_name = [aString copy];
}

- (void)setEmbassy_address:(NSString *)aString {
	if ((!embassy_address && !aString) || (embassy_address && aString && [embassy_address isEqualToString:aString])) return;
	dirty = YES;
	[embassy_address release];
	embassy_address = [aString copy];
}

- (void)setEmbassy_emergency:(NSString *)aString {
	if ((!embassy_emergency && !aString) || (embassy_emergency && aString && [embassy_emergency isEqualToString:aString])) return;
	dirty = YES;
	[embassy_emergency release];
	embassy_emergency = [aString copy];
}

- (void)setOther:(NSString *)aString {
	if ((!other && !aString) || (other && aString && [other isEqualToString:aString])) return;
	dirty = YES;
	[other release];
	other = [aString copy];
}

- (BOOL) isPredefine {
	return (predefineId != 0);
}

+ (NSMutableArray*) findAll {
	//NSLog(@"BriefcaseItem findAll");
	NSMutableArray *all = [NSMutableArray arrayWithCapacity:32];
	sqlite3 *database = [DBConnection sharedConnection].database;
	
	// Compile the delete statement if needed.
	if (select_all_statement == nil) {
		const char *sql = "SELECT pk, title, passport, license, ssn, id_card, airline_name, flight, frequent_flyer, car_name, license_plate, hotel_name, hotel_phone, hotel_address, embassy_name, embassy_address, embassy_emergency, other FROM briefcase";
		if (sqlite3_prepare_v2(database, sql, -1, &select_all_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			return all;
		}
	}
	
	// Execute the query.
	while (sqlite3_step(select_all_statement) == SQLITE_ROW) {
		// The second parameter indicates the column index into the result set.
		int field = 0;
		BriefcaseItem *briefcase = [[BriefcaseItem alloc] init];
		
		briefcase.pk = sqlite3_column_int(select_all_statement, field++);
		briefcase.title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		briefcase.passport = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		briefcase.license = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		briefcase.ssn = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		briefcase.id_card = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		briefcase.airline_name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		briefcase.flight = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		briefcase.frequent_flyer = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		briefcase.car_name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		briefcase.license_plate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		briefcase.hotel_name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		briefcase.hotel_phone = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		briefcase.hotel_address = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		briefcase.embassy_name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		briefcase.embassy_address = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		briefcase.embassy_emergency = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		briefcase.other = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		briefcase.dirty = NO;
		[all addObject:briefcase];
		[briefcase release];
	}
	// Reset the statement for future use.
	sqlite3_reset(select_all_statement);
	
	return all;
}

+ (void)upgradeTableFromDatabase:(sqlite3 *)db ToDatabase:(sqlite3 *)newdb {
	
	// Check if table exists
	if (upgrade_check_table_statement == nil) {
		const char *sql = "SELECT COUNT(*) FROM sqlite_master where type='table' and name='briefcase'";
		if (sqlite3_prepare_v2(db, sql, -1, &upgrade_check_table_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(db));
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(db));
		}
	}
	NSInteger count = 0;
	if (sqlite3_step(upgrade_check_table_statement) == SQLITE_ROW) {
		count = sqlite3_column_int(upgrade_check_table_statement, 0);
	}
	sqlite3_reset(upgrade_check_table_statement);
	sqlite3_finalize(upgrade_check_table_statement);
	upgrade_check_table_statement = nil;
	if (count <= 0) return;
	
	// Compile the delete statement if needed.
	if (upgrade_select_statement == nil) {
		const char *sql = "SELECT pk, title, passport, license, ssn, id_card, airline_name, flight, frequent_flyer, car_name, license_plate, hotel_name, hotel_phone, hotel_address, embassy_name, embassy_address, embassy_emergency, other FROM briefcase ORDER BY pk ASC";
		if (sqlite3_prepare_v2(db, sql, -1, &upgrade_select_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(db));
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(db));
		}
	}
	if (upgrade_insert_statement == nil) {
        const char *sql = "INSERT INTO briefcase (title, passport, license, ssn, id_card, airline_name, flight, frequent_flyer, car_name, license_plate, hotel_name, hotel_phone, hotel_address, embassy_name, embassy_address, embassy_emergency, other) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        if (sqlite3_prepare_v2(newdb, sql, -1, &upgrade_insert_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(newdb));
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(newdb));
        }
    }
	
	// Execute the query.
	NSInteger pk = -1;
	NSString* title;
	NSString* passport;
	NSString* license;
	NSString* ssn;
	NSString* id_card;
	NSString* airline_name;
	NSString* flight;
	NSString* frequent_flyer;
	NSString* car_name;
	NSString* license_plate;
	NSString* hotel_name;
	NSString* hotel_phone;
	NSString* hotel_address;
	NSString* embassy_name;
	NSString* embassy_address;
	NSString* embassy_emergency;
	NSString* other;
	while (sqlite3_step(upgrade_select_statement) == SQLITE_ROW) {
		// The second parameter indicates the column index into the result set.
		int field = 0;
		pk  = sqlite3_column_int(upgrade_select_statement, field++);
		char * val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			title = [NSString stringWithUTF8String:val];
		} else {
			title = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			passport = [NSString stringWithUTF8String:val];
		} else {
			passport = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			license = [NSString stringWithUTF8String:val];
		}else {
			license = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			ssn = [NSString stringWithUTF8String:val];
		}else {
			ssn = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			id_card = [NSString stringWithUTF8String:val];
		}else {
			id_card = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			airline_name = [NSString stringWithUTF8String:val];
		}else {
			airline_name = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			flight = [NSString stringWithUTF8String:val];
		}else {
			flight = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			frequent_flyer = [NSString stringWithUTF8String:val];
		}else {
			frequent_flyer = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			car_name = [NSString stringWithUTF8String:val];
		}else {
			car_name = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			license_plate = [NSString stringWithUTF8String:val];
		}else {
			license_plate = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			hotel_name = [NSString stringWithUTF8String:val];
		}else {
			hotel_name = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			hotel_phone = [NSString stringWithUTF8String:val];
		}else {
			hotel_phone = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			hotel_address = [NSString stringWithUTF8String:val];
		}else {
			hotel_address = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			embassy_name = [NSString stringWithUTF8String:val];
		}else {
			embassy_name = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			embassy_address = [NSString stringWithUTF8String:val];
		}else {
			embassy_address = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			embassy_emergency = [NSString stringWithUTF8String:val];
		}else {
			embassy_emergency = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			other = [NSString stringWithUTF8String:val];
		}else {
			other = @"";
		}
		
		int toField = 1;
		sqlite3_bind_text(upgrade_insert_statement, toField++, [title UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [passport UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [license UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [ssn UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [id_card UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [airline_name UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [flight UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [frequent_flyer UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [car_name UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [license_plate UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [hotel_name UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [hotel_phone UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [hotel_address UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [embassy_name UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [embassy_address UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(
						  upgrade_insert_statement, toField++, [embassy_emergency UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [other UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_step(upgrade_insert_statement);
		sqlite3_reset(upgrade_insert_statement);
	}
	// Reset the statement for future use.
	sqlite3_reset(upgrade_select_statement);
	sqlite3_finalize(upgrade_select_statement);
	upgrade_select_statement = nil;
	sqlite3_reset(upgrade_insert_statement);
	sqlite3_finalize(upgrade_insert_statement);
	upgrade_insert_statement = nil;	
}

+ (void)finalizeStatements {
	
	if (select_all_statement) {
		sqlite3_finalize(select_all_statement);
		select_all_statement = nil;
	}
    if (insert_statement) {
		sqlite3_finalize(insert_statement);
		insert_statement = nil;
	}
    if (init_statement) {
		sqlite3_finalize(init_statement);
		init_statement = nil;
	}
    if (delete_statement) {
		sqlite3_finalize(delete_statement);
		delete_statement = nil;
	}
	if (update_statement) {
		sqlite3_finalize(update_statement);
		update_statement = nil;
	}
	if (upgrade_check_table_statement) {
		sqlite3_finalize(upgrade_check_table_statement);
		upgrade_check_table_statement = nil;
	}
	if (upgrade_select_statement) {
		sqlite3_finalize(upgrade_select_statement);
		upgrade_select_statement = nil;
	}
	if (upgrade_insert_statement) {
		sqlite3_finalize(upgrade_insert_statement);
		upgrade_insert_statement = nil;
	}
}

@end

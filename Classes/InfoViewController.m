//
//  InfoViewController.m
//  BabyPacking
//
//  Created by Gary He on 5/25/09.
//  Copyright 2009 test. All rights reserved.
//

#import "InfoViewController.h"
#import "SupportViewController.h"
#import "Constant.h"
#import "BabyPackingAppDelegate.h"
#import "syncTask.h"
#import "NewsLinkController.h"

@implementation InfoViewController
bool isShowingLandscapeView;

- (void)dealloc {
    
    
    
	[headerView release];
	[doneButton release];
    // [doneButtonLand release];
	[lbVersion release];
    // [lbVersionLand release];
    [super dealloc];
}

/*
 - (id)initWithStyle:(UITableViewStyle)style {
 // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 if (self = [super initWithStyle:style]) {
 }
 return self;
 }
 */

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        layoutManager = [[LayoutManager alloc] initWithPortraitView:self.view andLandscapeNIB:@"InfoViewController_L"];
        //self = [super initWithNibName:@"RecalliPhoneView" bundle:nibBundleOrNil];
        
    }
    return self;
}


//#pragma mark Rotation
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    //self.view=UIInterfaceOrientationIsPortrait(interfaceOrientation);
//    return YES;
//}
//-(NSUInteger)supportedInterfaceOrientations{
//    return UIInterfaceOrientationMaskPortrait; // etc
//}

//#pragma mark orientation change methods
//
//- (void)orientationChanged:(NSNotification *)notification
//{
//    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
//    if (UIDeviceOrientationIsLandscape(deviceOrientation))
//    {
//        self.view=landscapeVw;
//        isShowingLandscapeView = YES;
//    }
//    else
//    {
//        self.view=portraitVw;
//        isShowingLandscapeView = NO;
//    }
//}


- (void)viewDidLoad {
    [super viewDidLoad];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
    
    // notification for changing in orientation change
    // [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    //	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:@"UIDeviceOrientationDidChangeNotification" object:nil];
    //    [self orientationChanged:nil];
    //
	appDelegate=(BabyPackingAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	if (TARGET_VERSION == VERSION_BABY_PACKING)
        
		imgIcon.image=[UIImage imageNamed:@"DishClips-72X72.png"];
    // imgIconLand.image=[UIImage imageNamed:@"DishClips-72X72.png"];}
	else if (TARGET_VERSION == VERSION_YOU_PACKING)
        
		imgIcon.image=[UIImage imageNamed:@"Pack&Go-72.png"];
    //  imgIconLand.image=[UIImage imageNamed:@"Pack&Go-72.png"];
    
	else if (TARGET_VERSION == VERSION_FAMILY_PACKING)
        
		imgIcon.image=[UIImage imageNamed:@"icon72.png"];
    // imgIconLand.image=[UIImage imageNamed:@"Pack-Go-72.png"];
    
    
    
	NSString *customFontName = [appDelegate getCustomFontName];
	
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor whiteColor];
    
	NSString *customFontName1 = [appDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];
	}
	else {
		//label.font=[UIFont fontWithName:customFontName size:fontSize];
	}
	
	[label sizeToFit];
	
    //
    //	label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
        label.textColor=[UIColor whiteColor];
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label.textColor = theColour1;
	}
	// and finally set the colour of your label
	
	self.navigationItem.titleView = label;
	label.text = @"Info";
	[label sizeToFit];
	
	
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    //	self.navigationItem.leftBarButtonItem = doneButton;
    //self.navigationItem.leftBarButtonItem = doneButtonLand;
	// set up the table's header view based on our UIView 'myHeaderView' outlet
	CGRect newFrame = CGRectMake(0.0, 0.0, self.tableView.bounds.size.width, headerView.frame.size.height);
	headerView.backgroundColor = [UIColor clearColor];
	headerView.frame = newFrame;
	self.tableView.tableHeaderView = headerView;	// note this will override UITableView's 'sectionHeaderHeight' property
    
    // headerViewLand.backgroundColor = [UIColor clearColor];
	//headerViewLand.frame = newFrame;
	//self.tableView.tableHeaderView = headerViewLand;
    
	NSBundle* bundle = [NSBundle mainBundle];
	NSString* version = [[bundle infoDictionary] objectForKey:@"CFBundleVersion"];
	
    lbVersion.text = [NSString stringWithFormat:@"Version %@", version];
    
    // lbVersionLand.text = [NSString stringWithFormat:@"Version %@", version];
    
	if ([customFontName1 isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	lbVersion.font=[UIFont fontWithName:customFontName1 size:fontSize];
	
	[lbVersion sizeToFit];
    
    
    UIButton *Editbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    Editbtn.frame = CGRectMake(0, 0, 30, 30);
    Editbtn.tag=2;
    [Editbtn setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
    // [Editbtn setBackgroundColor:[UIColor blackColor]];
    [Editbtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    doneButton=[[UIBarButtonItem alloc] initWithCustomView:Editbtn];
    
    self.navigationItem.leftBarButtonItem = doneButton;
    
    //lbVersionLand.font=[UIFont fontWithName:customFontName size:fontSize];
	
    //	[lbVersionLand sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
}

- (void) buttonAction:(id) sender {
	[self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [layoutManager translateToPortraitForView:self.view withAnimation:NO];
    }
    else
    {
        [layoutManager translateToLandscapeForView:self.view withAnimation:NO];
    }
}

/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure.png"]];
	}
    
	
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour12;
	// read the data back from the user defaults
	NSData *data112= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
	// check whether you got anything
	if(data112 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour12 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data112];
		cell.backgroundColor = theColour12;
	}
	// and finally set the colour of your label
	
	
	if (indexPath.row == 0) {
		cell.textLabel.text = TITLE_SHARE;
		NSString *customFontName = [appDelegate getCustomFontName];
		int fontSize = 18;
		if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
			fontSize -= 8;
		}
		cell.textLabel.font=[UIFont fontWithName:customFontName size:fontSize];
	} else if (indexPath.row == 1) {
		cell.textLabel.text = TITLE_SUPPORT;
		NSString *customFontName = [appDelegate getCustomFontName];
		int fontSize = 18;
		if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
			fontSize -= 8;
		}
		cell.textLabel.font=[UIFont fontWithName:customFontName size:fontSize];
	} else if (indexPath.row == 2) {
		cell.textLabel.text = TITLE_NEWS;
		NSString *customFontName = [appDelegate getCustomFontName];
		int fontSize = 18;
		if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
			fontSize -= 8;
		}
		cell.textLabel.font=[UIFont fontWithName:customFontName size:fontSize];
	}
    // Set up the cell...
	NSString *customFontName = [appDelegate getCustomFontName];
	
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
		cell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];
	}
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
		cell.textLabel.textColor = DEF_COLOR;
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		cell.textLabel.textColor = theColour1;
	}
	// and finally set the colour of your label
	
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
	// AnotherViewController *anotherViewController = [[AnotherViewController alloc] initWithNibName:@"AnotherView" bundle:nil];
	// [self.navigationController pushViewController:anotherViewController];
	// [anotherViewController release];
	if (indexPath.row == 0) {
		/*NSMutableString *email = [NSMutableString stringWithCapacity:1024];
		[email appendString:[NSString stringWithFormat:@"mailto:?subject=%@",[I_SHARE_SUBJECT stringByReplacingOccurrencesOfString:@"&" withString:@"%26"]]];
		[email setString:[email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
		
		NSString *body = [NSString stringWithFormat:@"%@\n",I_SHARE_MSG];

		body = [body stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		[email appendString:[NSString stringWithFormat:@"&body=%@", [body stringByReplacingOccurrencesOfString:@"&" withString:@"%26"]]];

		NSURL *url = [NSURL URLWithString:email];
		
		[[UIApplication sharedApplication] openURL:url];*/
       if ([MFMailComposeViewController canSendMail])
        {
        MFMailComposeViewController* controllerMail = [[MFMailComposeViewController alloc] init];
        controllerMail.mailComposeDelegate = self;
        [controllerMail setSubject:[NSString stringWithFormat:@"%@",[I_SHARE_SUBJECT stringByReplacingOccurrencesOfString:@"&" withString:@"%26"]]];
        NSString *body = [NSString stringWithFormat:@"%@\n",I_SHARE_MSG];
        [controllerMail setMessageBody:body isHTML:YES];
        if (controllerMail) [self presentModalViewController:controllerMail animated:YES];
        [controllerMail release];
        }
	} else if(indexPath.row ==1) {
		SupportViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[SupportViewController alloc] initWithNibName:@"SupportViewController" bundle:nil];
		else
			controller = [[SupportViewController alloc] initWithNibName:@"SupportViewController1" bundle:nil];
		
		//[[SupportViewController alloc] initWithNibName:@"SupportViewController" bundle:nil];
		[self.navigationController pushViewController:controller animated:YES];
		[controller release];
	} else if(indexPath.row ==2) {
		//UIApplication *app = [UIApplication sharedApplication];
		//[app openURL:[NSURL URLWithString:I_NEWS_LINK]];
        
        NewsLinkController *obj = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            obj= [[NewsLinkController alloc] initWithNibName:@"NewsLinkControlleriPad" bundle:nil];
        else
			obj= [[NewsLinkController alloc] initWithNibName:@"NewsLinkController" bundle:nil];
        [self.navigationController pushViewController:obj animated:YES];
		[obj release];
	}
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissModalViewControllerAnimated:YES];
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{            return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        [layoutManager translateToPortraitForView:self.view withAnimation:YES];
    }
    else
    {
        [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
    }
    // }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}

@end


//
//  CategoriseListViewController.h
//  BabyPacking
//
//  Created by eve on 9/16/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PackingList.h"
#import "RootViewController.h"
#import "LayoutManager.h"

@interface CategoriseListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,UIAlertViewDelegate> {
	IBOutlet UIImageView *bgImageView;
	//IBOutlet UIImageView *bgBorderView;
	LayoutManager *layoutManager;
	IBOutlet UITableView *myTableView;
	IBOutlet UIBarButtonItem *doneButton;
	IBOutlet UIBarButtonItem *resetButton;
	IBOutlet UIBarButtonItem *addButton;
	NSMutableArray *predefineListArry;
	NSMutableArray *categoryListArray;
	NSMutableArray *categoryListInAppData;
	BOOL editList;
	
	RootViewController *homeController;	
	
	UIImage *checkImg;
	UIImage *uncheckImg;
	UIImage *addImg;
	UIImage *removeImg;
	UIImage *selectImg;
	
	NSMutableArray *selectedListArray;
	NSMutableArray *listNum;
	
	PackingList *currentList;
	
	IBOutlet UIButton *delBtn;
	IBOutlet UIButton *sortBtn;
	IBOutlet UIButton *restoreBtn;
	
	IBOutlet UITableView *editTableView;
	
	BOOL allowDelete;
	int index;
	
	IBOutlet UIBarButtonItem *searchButton;
    
    BOOL isEdit;
}

@property(nonatomic, retain) RootViewController *homeController;
@property(nonatomic, retain) NSMutableArray *selectedListArray;
@property(nonatomic, retain) PackingList *currentList;
@property(nonatomic, retain) NSMutableArray *categoryListInAppData;
@property(nonatomic, retain) NSMutableArray *listNum;
@property(nonatomic, retain) NSMutableArray   *predefineListArry; 
@property (nonatomic, retain) NSMutableArray *categoryListArray;
@property(nonatomic) BOOL editList;
@property(nonatomic) BOOL isEdit;

- (IBAction) buttonAction:(id)sender;

- (void) loadData;

-(void) reorderListByDesc;
-(void)reorderListByAsc;

@end
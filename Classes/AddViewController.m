//
//  AddViewController.m
//  BabyPacking
//
//  Created by Jay Lee on 10/13/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import "Constant.h"
#import "Utils.h"
#import "AddViewController.h"
#import "SchedulePickerViewController.h"
#import "CategoryViewController.h"
#import "RepeatingViewController.h"
#import "TaskNotesViewController.h"
#import "BabyPackingAppDelegate.h"
#import "ResponseHandler.h"
#import "TaskItem.h"
#import "RecordItem.h"
#import "syncTask.h"

@implementation AddViewController

@synthesize remoteTaskID = mRemoteTaskID;
@synthesize currentRecord = mCurrentRecord;

NSNumber                   *startDateTimeStampObj;

- (void)dealloc {
	[mDateFormatter release];
	[mCurrentRecord release];
	[mCurrentTask release];
	[mCategoryController release];
	[mRepeatController release];
	[mNotesController release];
	[mTableView release];
	[mNotifyTitleField release];
	//[mNotifyTextField release];
	[mCategoryLabel release];
	[mRepeatLabel release];
	[mTitleCell release];
	[mCategoryCell release];
	[mRepeatCell release];
	[mNotesLabel release];
	[mDateTimeCell release];
	[mBtnBack release];
	[mBtnSave release];
	[mHeaderView release];
	[mBtnDelete release];
    [super dealloc];
}

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
		//mTableViewCells = [[NSArray alloc] initWithObjects:mTitleCell, mCategoryCell, mRepeatCell, mNotesCell, nil];
	}

    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

    mTableView.backgroundColor= [UIColor colorWithRed:0.92 green:0.92 blue:0.93 alpha:1.0];
	// Add 'Back' navigation button
//	UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:nil action:nil];
//    self.navigationItem.backBarButtonItem = backButton;
//    [backButton release];
    
     [mTableView setSeparatorColor:[UIColor darkGrayColor]];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 30, 30);
    btn.tag=1;
    [btn setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(goBck:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(0, 0, 30, 30);
    btn2.tag=1;
    [btn2 setImage:[UIImage imageNamed:@"saveNewList.png"] forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(buttonSaveDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    mBtnSave = [[UIBarButtonItem alloc] initWithCustomView:btn2];
    
//	self.navigationItem.rightBarButtonItem = mBtnCreate;
    
    
	self.navigationItem.rightBarButtonItem = mBtnSave;
    
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor whiteColor];
	NSString *customFontName = [mAppDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];		
	}
	else {
		label.font=[UIFont fontWithName:customFontName size:fontSize];		
	}
	
	[label sizeToFit];
	
	
//	label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
        label.textColor=[UIColor whiteColor];        
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label.textColor = theColour1;
	}
	// and finally set the colour of your label
	
	self.navigationItem.titleView = label;
	label.text = @"Add Task";
	[label sizeToFit];
	
	
	mDateFormatter = [[NSDateFormatter alloc] init];
	[mDateFormatter setDateStyle:NSDateFormatterMediumStyle];
	[mDateFormatter setTimeStyle:kCFDateFormatterShortStyle];
	
//	mNotesLabel.numberOfLines = 0;
//	mNotesLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0];
	mAppDelegate = (id)[UIApplication sharedApplication].delegate;
	
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		mPickerController = [[SchedulePickerViewController alloc] initWithNibName:@"SchedulePickerViewController" bundle:nil];
	else
		mPickerController = [[SchedulePickerViewController alloc] initWithNibName:@"SchedulePickerViewController1" bundle:nil];
	
	mPickerController.title = @"Task Schedule";
	mPickerController.startDate = [NSDate date];
	mPickerController.endDate = [Utils getDateWithHours:1 afterDate:[NSDate date]];
	
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		mCategoryController = [[CategoryViewController alloc] initWithNibName:@"CategoryViewController" bundle:nil];
	else
		mCategoryController = [[CategoryViewController alloc] initWithNibName:@"CategoryViewController1" bundle:nil];

	mCategoryController.title = @"Labels overview";
	mCategoryController.defaultValue = DEFAULT_NOTIFYING_CATEGORY;
	
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		mRepeatController = [[RepeatingViewController alloc] initWithNibName:@"RepeatingViewController" bundle:nil];
	else
		mRepeatController = [[RepeatingViewController alloc] initWithNibName:@"RepeatingViewController1" bundle:nil];
	
	mRepeatController.title = @"Repeat types";
	mRepeatController.defaultValue = DEFAULT_NOTIFYING_REPEAT;
	
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		mNotesController = [[TaskNotesViewController alloc] initWithNibName:@"TaskNotesViewController" bundle:nil];
	else
		mNotesController = [[TaskNotesViewController alloc] initWithNibName:@"TaskNotesViewController1" bundle:nil];
	
	mNotesController.title = @"Notes";
	/*
	mNotifyTextField.autoresizingMask = UIViewAutoresizingFlexibleHeight |
										UIViewAutoresizingFlexibleBottomMargin |
										UIViewAutoresizingFlexibleTopMargin;
	 */
	
	if (mRemoteTaskID > 0) {
		mCurrentTask = [[TaskItem alloc] initWithRemoteId:mRemoteTaskID];
		mPickerController.startDate = mCurrentTask.start_date;
		mPickerController.endDate = mCurrentTask.end_date;
		mCategoryController.defaultValue = [CategoryViewController valueForTitle:mCurrentTask.category];
		mRepeatController.defaultValue = mCurrentTask.repeat;
		mNotesController.notes = mCurrentTask.text;
		mNotifyTitleField.text = mCurrentTask.title;
		mNotesController.notes = mCurrentTask.text;
		mTableView.tableHeaderView = mHeaderView;
		mBtnDelete.hidden = NO;
	} else {
		mCurrentTask = [[TaskItem alloc] init];
		mBtnDelete.hidden = YES;
	}
    
    //Change By MS
    localNotif = [[UILocalNotification alloc] init];
    
    if (![mAppDelegate.globReminderId isEqualToString:@""])
        [self getReminderDetails];
    else
    {
        mCategoryController.defaultValue = DEFAULT_NOTIFYING_CATEGORY;
        mRepeatController.defaultValue = DEFAULT_NOTIFYING_REPEAT;
    }
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
}

- (IBAction)goBck:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    
	[super viewWillAppear:animated];
	
    
    
    //Change By MS
	mCategoryLabel.text = mCategoryController.defaultValue; //[CategoryViewController titleForValue:mCategoryController.defaultValue];
	mRepeatLabel.text = mRepeatController.defaultValue; //[RepeatingViewController titleForValue:mRepeatController.defaultValue];
	
	mStartTimeLabel.text = [mDateFormatter stringFromDate:mPickerController.startDate];
	mEndTimeLabel.text = [mDateFormatter stringFromDate:mPickerController.endDate];

	if (!mNotesController.notes || [@"" isEqualToString:mNotesController.notes]) {
		mNotesLabel.text = @"  Notes";
		mNotesLabel.textColor = [UIColor lightGrayColor];
	} else {
		mNotesLabel.textColor = [UIColor colorWithRed:0.22 green:0.329 blue:0.529 alpha:1.0];
		mNotesLabel.text = mNotesController.notes;
	}
	
	CGRect frame = mNotesLabelTemplate.frame;
	NSString *cellText = mNotesLabel.text;
	UIFont *cellFont = mNotesLabel.font;
	CGSize constraintSize = CGSizeMake(frame.size.width, MAXFLOAT);
	frame.size.height = [cellText sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap].height;
	mNotesLabel.frame = frame;
	/*
	if (!mNotesController.notes || [@"" isEqualToString:mNotesController.notes]) {
		mNotifyTextField.text = @"Notes";
		mNotifyTextField.textColor = [UIColor lightGrayColor];
		mNotifyTextField.hidden = NO;
	} else {
		mNotifyTextField.textColor = [UIColor colorWithRed:0.22 green:0.329 blue:0.529 alpha:1.0];
		mNotifyTextField.text = mNotesController.notes;
		[mNotifyTextField sizeToFit];
		mNotesFieldFrame.size.height = mNotifyTextField.frame.size.height;
	}
	 */
	
	[mTableView reloadData];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

#pragma mark - Change By MS

- (void)getReminderDetails
{
    const char *dbpath = [[mAppDelegate getDBPath] UTF8String];
    
    if (sqlite3_open(dbpath, &dbFamilyPacking) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"Select RemName, RemStartDate, RemEndDate, RemCategory, RemRepeat, RemNotes From tblReminder where RemId = '%@'", mAppDelegate.globReminderId];
        NSLog(@" ==> %@", querySQL);
        
        const char *sql = [querySQL UTF8String];
        
        sqlite3_stmt *searchStatement;
        
        if (sqlite3_prepare_v2(dbFamilyPacking, sql, -1, &searchStatement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(searchStatement) == SQLITE_ROW)
            {
                mNotifyTitleField.text = sqlite3_column_text(searchStatement, 0) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 0)] : @"";
                
                NSString* start_date = sqlite3_column_text(searchStatement, 1) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 1)] : @"";
                NSString* end_date = sqlite3_column_text(searchStatement, 2) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 2)] : @"";
                
                mPickerController.startDate = [mDateFormatter dateFromString:start_date];
                mPickerController.endDate = [mDateFormatter dateFromString:end_date];
                
                mStartTimeLabel.text = start_date;
                mEndTimeLabel.text = end_date;
                
                 mCategoryController.defaultValue = sqlite3_column_text(searchStatement, 3) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 3)] : @"To Do";
                 mRepeatController.defaultValue = sqlite3_column_text(searchStatement, 4) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 4)] : @"none";
                
                mCategoryLabel.text = mCategoryController.defaultValue;
                mRepeatLabel.text = mRepeatController.defaultValue;
                
                NSString *strNotes = sqlite3_column_text(searchStatement, 5) != nil ? [NSString stringWithUTF8String:(const char *) sqlite3_column_text(searchStatement, 5)] : @"";
                if ([@"" isEqualToString:strNotes]) {
                    mNotesLabel.text = @"Notes";
                    mNotesLabel.textColor = [UIColor lightGrayColor];
                } else {
                    mNotesLabel.textColor = [UIColor colorWithRed:0.22 green:0.329 blue:0.529 alpha:1.0];
                    mNotesLabel.text = strNotes;
                }
            }
        }
        sqlite3_finalize(searchStatement);
    }
    sqlite3_close(dbFamilyPacking);
    
    [mTableView reloadData];
}

- (IBAction)buttonSaveDidClick:(id)sender {
	
	if ([mNotifyTitleField canResignFirstResponder]) [mNotifyTitleField resignFirstResponder];
	
	NSString* category = mCategoryController.defaultValue; //[CategoryViewController titleForValue:mCategoryController.defaultValue];
	if (!mNotifyTitleField.text || [@"" isEqualToString:mNotifyTitleField.text] || 
		!category || [@"" isEqualToString:category] ||
		!mRepeatController.defaultValue || [@"" isEqualToString:mRepeatController.defaultValue] ||
		!mPickerController.startDate ||
		!mPickerController.endDate) {
		UIAlertView* alert = [[UIAlertView alloc] initWithTitle:M_INSUFFICIENT_TASK_INFO message:M_RETRY_TASK_INFO
													   delegate:self cancelButtonTitle:I_OK otherButtonTitles:nil];
		[alert show];
		[alert release];
		return;
	}
	
	//[mAppDelegate startSynchronizing];
	
	// Prepare task data
	mCurrentTask.title = mNotifyTitleField.text;
	mCurrentTask.category = category;
	mCurrentTask.repeat = mRepeatController.defaultValue;
	mCurrentTask.interval = 1;
	mCurrentTask.start_date = mPickerController.startDate;
	mCurrentTask.end_date = mPickerController.endDate;
	//mCurrentTask.text = mNotifyTextField.text;
	mCurrentTask.text = mNotesLabel.text;
	mCurrentTask.device_token = mAppDelegate.deviceToken;
	
	NSString* start_date = [mDateFormatter stringFromDate:mCurrentTask.start_date];
	NSString* end_date = [mDateFormatter stringFromDate:mCurrentTask.end_date];
	
	// Prepare form data to add task
	NSDate* now = [NSDate date];
	NSMutableDictionary* arguments = [NSMutableDictionary dictionaryWithCapacity:0];
	NSString* rem_id = mAppDelegate.deviceToken?mAppDelegate.deviceToken:@"Unknown";
	[arguments setObject:rem_id forKey:@"remId"];
	[arguments setObject:mCurrentTask.title forKey:@"remTitle"];
	[arguments setObject:mCurrentTask.text forKey:@"remText"];
	[arguments setObject:mCurrentTask.category forKey:@"remCategory"];
	NSString* rem_message = [NSString stringWithFormat:@"[%@] %@", mCurrentTask.category, mCurrentTask.title];
	[arguments setObject:rem_message forKey:@"remMessage"];
	[arguments setObject:start_date forKey:@"startDate"];
	[arguments setObject:end_date forKey:@"endDate"];
	[arguments setObject:mAppDelegate.taskSound forKey:@"remSound"];
	NSDate* next_time = [mCurrentTask getNextTaskTime];
	NSInteger duration = [Utils getMinutesFromDate:now toDate:next_time];
	NSString* remTime = [NSString stringWithFormat:@"%d", duration];
	[arguments setObject:remTime forKey:@"remTime"];
	duration = [Utils getMinutesFromDate:now toDate:mCurrentTask.end_date];
	NSString* remOverTime = [NSString stringWithFormat:@"%d", duration];
	[arguments setObject:remOverTime forKey:@"remOverTime"];
	NSString* clientTime = [now description];
	[arguments setObject:clientTime forKey:@"clientTime"];
	[arguments setObject:mCurrentTask.repeat forKey:@"loopflg"];
	NSString* interval = [NSString stringWithFormat:@"%d", mCurrentTask.interval];
	[arguments setObject:interval forKey:@"loopinterval"];
    
    NSTimeInterval currentTimeStamp = [mCurrentTask.start_date timeIntervalSince1970];
    startDateTimeStampObj = [NSNumber numberWithDouble: currentTimeStamp];
    
    //Change By MS
    
    sqlite3_stmt *statement;
    const char *dbpath = [[mAppDelegate getDBPath] UTF8String];
    
    if(sqlite3_open(dbpath, &dbFamilyPacking) == SQLITE_OK)
    {
        NSString *insertquery;
        
        if (![mAppDelegate.globReminderId isEqualToString:@""])
        {
            
            NSLog(@" ==> %@", localNotif);
            insertquery = [NSString stringWithFormat:@"Update tblReminder set RemName = '%@', RemStartDate = '%@', RemEndDate = '%@', RemCategory = '%@', RemRepeat = '%@', RemNotes = '%@', RemInterval = '%@', RemMessage = '%@', StartDateTimeStamp = '%@' Where RemId = '%@'", mCurrentTask.title, start_date, end_date, mCurrentTask.category, mCurrentTask.repeat, mCurrentTask.text, interval, rem_message, startDateTimeStampObj, mAppDelegate.globReminderId];
        }
        else
        {
            insertquery = [NSString stringWithFormat:@"Insert into tblReminder(RemName, RemStartDate, RemEndDate, RemCategory, RemRepeat, RemNotes, RemInterval, RemMessage, StartDateTimeStamp) VALUES('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')", mCurrentTask.title, start_date, end_date, mCurrentTask.category, mCurrentTask.repeat, mCurrentTask.text, interval, rem_message, startDateTimeStampObj];
        }
        
        NSLog(@"QUERY   -->%@", insertquery);
        
        const char *insert_query = [insertquery UTF8String];
        
        if (sqlite3_prepare_v2(dbFamilyPacking, insert_query, -1, &statement, NULL) == SQLITE_OK) {
            
            if(sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"Record Added Successfully...!!!");
                //[self sync_PDF_to_iCloud];
            }
            else
            {
                NSLog(@"Error :::::: '%s'.", sqlite3_errmsg(dbFamilyPacking));
            }
        }
        
        else
        {
            NSLog(@"Error :::::: '%s'.", sqlite3_errmsg(dbFamilyPacking));
        }
        
        sqlite3_finalize(statement);
        
    }
    sqlite3_close(dbFamilyPacking);

	NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];

	// Break the date up into components
	NSDateComponents *dateComponents = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
												   fromDate:mCurrentTask.start_date];
	NSDateComponents *timeComponents = [calendar components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit )
												   fromDate:mCurrentTask.start_date];
    
	// Set up the fire time
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[dateComponents day]];
    [dateComps setMonth:[dateComponents month]];
    [dateComps setYear:[dateComponents year]];
    
    [dateComps setHour:[timeComponents hour]];
	// Notification will fire in one minute
    [dateComps setMinute:[timeComponents minute]];
	[dateComps setSecond:[timeComponents second]];
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
    [dateComps release];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
	NSString *docDirectory = [paths objectAtIndex:0];
    
    
  /*  BOOL isDir=NO;
    NSArray *subpaths;
    NSString *exportPath = docDirectory;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:exportPath isDirectory:&isDir] && isDir){
        subpaths = [fileManager subpathsAtPath:exportPath];
    }
    
    NSString *archivePath = [docDirectory stringByAppendingString:@".zip"];
    
    ZipArchive *archiver = [[ZipArchive alloc] init];
    [archiver CreateZipFile2:archivePath];
    for(NSString *path in subpaths)
    {
        NSString *longPath = [exportPath stringByAppendingPathComponent:path];
        if([fileManager fileExistsAtPath:longPath isDirectory:&isDir] && !isDir)
        {
            [archiver addFileToZip:longPath newname:path];
        }
    }
    BOOL successCompressing = [archiver CloseZipFile2];
    
    if(successCompressing)
           {
               
               NSLog(@"Success");

        NSURL *ubiq = [[NSFileManager defaultManager]URLForUbiquityContainerIdentifier:nil];
        
        NSURL *ubiquitousPackage = [[ubiq URLByAppendingPathComponent:archivePath]  URLByAppendingPathComponent:archivePath];
        NSLog(@"%@",ubiquitousPackage);
        
       syncTask  *mydoc = [[syncTask alloc] initWithFileURL:ubiquitousPackage];
        //        NSArray *pdfs = [[NSBundle mainBundle] pathsForResourcesOfType:@"pdf" inDirectory:nil];
        //        NSString *filepath = [pdfs lastObject];
        NSArray *paths1 = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths1 objectAtIndex:0];
    
       
        //NSString *fileName = @"Report.pdf";
        //NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:archivePath];
        NSData *data = [NSData dataWithContentsOfFile:archivePath];
        NSLog(@"%u",data.length);
        //mydoc.pdfContent = data;
        
        NSString *docDir = archivePath;
        if (![[NSFileManager defaultManager] fileExistsAtPath:docDir])
            [[NSFileManager defaultManager] createDirectoryAtPath:docDir withIntermediateDirectories:YES attributes:nil error:nil];
        
        NSString *pdfFile = [docDir stringByAppendingPathComponent:archivePath];
        if(![[NSFileManager defaultManager] fileExistsAtPath:pdfFile])
            [[NSFileManager defaultManager] createFileAtPath:pdfFile contents:data attributes:nil];
        
        NSLog(@"Successfully loaded data from cloud file name %@", archivePath);
        
        [mydoc saveToURL:[mydoc fileURL] forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success)
         {
             if (success)
             {
                 NSLog(@"zipSynced with icloud");
                 
             }
             else
                 NSLog(@"zip Syncing FAILED with icloud");
         }];
    }
    else
    {
        NSLog(@"Fail zip");
    }*/
    
   
    
   /* if(successCompressing)
        return YES;
    else
        return NO; */
    
    
    if (localNotif == nil)
        return;
    
    localNotif.fireDate = itemDate;
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    
    NSLog(@"Notification Date : %@", localNotif.fireDate);
    
    // Notification details
    localNotif.alertBody = [NSString stringWithFormat:@"%@ ", mCurrentTask.text];
    // Set the action button
    localNotif.alertAction = @"View";
    
   // localNotif.soundName = UILocalNotificationDefaultSoundName;
    NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
    localNotif.soundName =  [userSettings objectForKey:K_SET_TASK_SOUND];
    NSLog(@"Notification Date : %@", [userSettings objectForKey:K_SET_TASK_SOUND]);
    
    localNotif.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    
    // Schedule the notification
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
    /*
	// Issue add task API
	RequestHandler *handler = [[RequestHandler alloc] init];
	handler.delegate = self;
	handler.method = @"POST";
	if (mCurrentTask.pk == INVALID_PK) {
		handler.url = API_ADD_TASK_URL;
	} else {
		handler.url = API_UPDATE_TASK_URL;
		NSString* remote_id = [NSString stringWithFormat:@"%d", mCurrentTask.remote_id];
		[arguments setObject:remote_id forKey:@"id"];
	}
	handler.requestArguments = arguments;
	handler.additionalArguments = nil;
	[handler startRequest];
	[handler release];
    */
    if(mAppDelegate.iCloudShow)
    {
    [self btnUpload];
    }
    else
    {
    [self.navigationController popViewControllerAnimated:YES];  
    }
}

#pragma mark - Database Methods

-(void) sync_PDF_to_iCloud
{
    //    if([self isIOS5iCloudAccessible] && [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil])
    //    {
    NSURL *ubiq = [[NSFileManager defaultManager]URLForUbiquityContainerIdentifier:nil];
    
    NSURL *ubiquitousPackage = [[ubiq URLByAppendingPathComponent:@"Documents"]  URLByAppendingPathComponent:@"FamilyPacking.sqlite3"];
    
    
    syncTask *mydoc = [[syncTask alloc] initWithFileURL:ubiquitousPackage];
    //        NSArray *pdfs = [[NSBundle mainBundle] pathsForResourcesOfType:@"pdf" inDirectory:nil];
    //        NSString *filepath = [pdfs lastObject];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fileName;
    
    fileName = @"FamilyPacking.sqlite3";
    
    //NSString *fileName = @"Report.pdf";
    NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    NSData *data = [NSData dataWithContentsOfFile:folderPath];
    NSLog(@"%u",data.length);
    
    mydoc.zipDataContent = data;
    
    
    NSString *docDir = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"Import/iCloud"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:docDir])
        [[NSFileManager defaultManager] createDirectoryAtPath:docDir withIntermediateDirectories:YES attributes:nil error:nil];
    
    NSString *pdfFile = [docDir stringByAppendingPathComponent:fileName];
    if(![[NSFileManager defaultManager] fileExistsAtPath:pdfFile])
        [[NSFileManager defaultManager] createFileAtPath:pdfFile contents:data attributes:nil];
    
    NSLog(@"Successfully loaded data from cloud file name %@", fileName);
    
    [mydoc saveToURL:[mydoc fileURL] forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success)
     {
         if (success)
         {
             NSLog(@"PDF: Synced with icloud");
         }
         else
             NSLog(@"PDF: Syncing FAILED with icloud");
     }];
    //    }
    
}

- (IBAction)buttonDeleteDidClick:(id)sender {
	
	[mAppDelegate startSynchronizing];
	
	// Prepare form data to add task
	NSMutableDictionary* arguments = [NSMutableDictionary dictionaryWithCapacity:0];
	NSString* remote_id = [NSString stringWithFormat:@"%d", mCurrentTask.remote_id];
	[arguments setObject:remote_id forKey:@"id"];
	
	// Issue add task API
	RequestHandler *handler = [[RequestHandler alloc] init];
	handler.delegate = self;
	handler.method = @"GET";
	handler.url = API_DELETE_TASK_URL;
	handler.requestArguments = arguments;
	handler.additionalArguments = nil;
	[handler startRequest];
	[handler release];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	
	return YES;
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	CGFloat height = 0;
	
	if (indexPath.section == 0)
		height = mTitleCell.contentView.frame.size.height;
	else if (indexPath.section == 1)
		height = mDateTimeCell.contentView.frame.size.height;
    else if (indexPath.section == 2)
		height = mEndDateTimeCell.contentView.frame.size.height;
	else if (indexPath.section == 3)
		height = mCategoryCell.contentView.frame.size.height;
	else if (indexPath.section == 4)
		height = mRepeatCell.contentView.frame.size.height;
	else if (indexPath.section == 5) {
		height = mNotesLabel.frame.size.height + 20;
	}
	
	return height;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";

	UITableViewCell* cell = nil;
	if (indexPath.section == 0) {
		cell = mTitleCell;
	} else if (indexPath.section == 1) {
		cell = mDateTimeCell;
	}else if (indexPath.section == 2) {
		cell = mEndDateTimeCell;
	} else if (indexPath.section == 3) {
		cell = mCategoryCell;
	} else if (indexPath.section == 4) {
		cell = mRepeatCell;
	} else if (indexPath.section == 5) {
		cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
           mNotesLabel.frame = CGRectMake(10.0, mNotesLabel.frame.origin.y, 280.0, mNotesLabel.frame.size.height);
        } else {
           mNotesLabel.frame = CGRectMake(10.0, mNotesLabel.frame.origin.y, 660.0, mNotesLabel.frame.size.height);
        }
		
        cell.backgroundColor=[UIColor clearColor];
        mNotesLabel.textColor= [UIColor darkGrayColor];
		[cell.contentView addSubview:mNotesLabel];
		//mNotesLabel.center = cell.contentView.center;
		
	} else {
		cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
	}

    return cell;
}

// Override to support row selection in the table view.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// Navigation logic may go here -- for example, create and push another view controller.
	if (indexPath.section == 1 || indexPath.section == 2) {
        if (indexPath.section == 1)
            mAppDelegate.startDate = YES;
        else
            mAppDelegate.startDate = NO;
        
		[self.navigationController pushViewController:mPickerController animated:YES];
	} else if (indexPath.section == 3) {
		[self.navigationController pushViewController:mCategoryController animated:YES];
	} else if (indexPath.section == 4) {
		[self.navigationController pushViewController:mRepeatController animated:YES];
	} else if (indexPath.section == 5) {
		[self.navigationController pushViewController:mNotesController animated:YES];
	}
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark <RequestHandlerDelegate> implementation

- (void)request:(RequestHandler *)request responseCompleteWithData:(NSData *)xmldata {
	
	[mAppDelegate stopSynchronizing];
	
	NSLog(@">>>>>>>>>>>>>> responseCompleteWithData begin");
	if ([API_ADD_TASK_URL isEqualToString:request.url] ||
		[API_UPDATE_TASK_URL isEqualToString:request.url]) {
		
		ResponseHandler* handler = [[ResponseHandler alloc] initWithXMLData:xmldata];
		if (!handler) {
			// Presend alert view
			UIAlertView* alert = [[UIAlertView alloc] initWithTitle:M_ERROR message: M_ERROR_NO_CONNECTION
														   delegate:self cancelButtonTitle:I_OK 
												  otherButtonTitles:nil];
			[alert show];
			[alert release];
			
			return;
		}
		
		if (!handler.found || handler.errorCode < 0) {
			// Remove data if failed
			[handler release];
			
			// Presend alert view
			NSString* message = [NSString stringWithFormat:M_ERROR_ADD_TASK, handler.errorCode];
			UIAlertView* alert = [[UIAlertView alloc] initWithTitle:M_ERROR message: message
															delegate:self cancelButtonTitle:I_OK 
												  otherButtonTitles:nil];
			[alert show];
			[alert release];
		} else {
			if (handler.task_id > 0) {
				mCurrentTask.remote_id = handler.task_id;
			}
			[mCurrentTask saveData];
			[handler release];
			[self.navigationController popViewControllerAnimated:YES];
		}
	} else if ([API_DELETE_TASK_URL isEqualToString:request.url]) {
		// Remove data locally if succeed
		ResponseHandler* handler = [[ResponseHandler alloc] initWithXMLData:xmldata];
		if (!handler) {
			// Presend alert view
			UIAlertView* alert = [[UIAlertView alloc] initWithTitle:M_ERROR message: M_ERROR_NO_CONNECTION
														   delegate:self cancelButtonTitle:I_OK 
												  otherButtonTitles:nil];
			[alert show];
			[alert release];
			
			return;
		}
		if (!handler.found || handler.errorCode < 0) {
			[handler release];
			
			// Presend alert view
			NSString* message = [NSString stringWithFormat:M_ERROR_DELETE_TASK, handler.errorCode];
			UIAlertView* alert = [[UIAlertView alloc] initWithTitle:M_ERROR message: message
														   delegate:self cancelButtonTitle:I_OK 
												  otherButtonTitles:nil];
			[alert show];
			[alert release];
		} else {
			[handler release];
			[mCurrentRecord deleteData];
			[RecordItem deleteAllWithTask:mCurrentTask.remote_id];
			[mCurrentTask deleteData];
			
			[self.navigationController popViewControllerAnimated:YES];
		}
	
	} else {
		
	}
	
	NSLog(@"responseCompleteWithData end <<<<<<<<<<<<<<<<");
}

- (void)request:(RequestHandler *)request responseFailedWithError:(NSError *)error {
	
	[mAppDelegate stopSynchronizing];
	
	NSLog(@">>>>>>>>>>>>>> responseFailedWithError begin");
	if ([API_ADD_TASK_URL isEqualToString:request.url] ||
		[API_UPDATE_TASK_URL isEqualToString:request.url]) {		
		// Presend alert view
		UIAlertView* alert = [[UIAlertView alloc] initWithTitle:M_ERROR message: M_ERROR_NO_CONNECTION
													   delegate:self cancelButtonTitle:I_OK 
											  otherButtonTitles:nil];
		[alert show];
		[alert release];
	} else {

	}
	
	NSLog(@"responseFailedWithError end <<<<<<<<<<<<<<<<");
}

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        // [layoutManager translateToPortraitForView:self.view withAnimation:YES];
    }
    else
    {
       //  [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
    }
    // }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}


#pragma mark - upload in iCloud

-(void)btnUpload
{
    NSURL *ubiq = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
    if (ubiq)
    {
        // Create XML and pass to iCloud
        NSString *xmlStr=@"<Family>";
        NSString *select_query;
        const char *select_stmt;
        sqlite3_stmt *compiled_stmt;
        sqlite3 *dbFamilyPacking2;
        if (sqlite3_open([[mAppDelegate getDBPath] UTF8String], &dbFamilyPacking2) == SQLITE_OK)
        {
            select_query = [NSString stringWithFormat:@"SELECT * FROM tblReminder"];
            select_stmt = [select_query UTF8String];
            if(sqlite3_prepare_v2(dbFamilyPacking2, select_stmt, -1, &compiled_stmt, NULL) == SQLITE_OK)
            {
                while(sqlite3_step(compiled_stmt) == SQLITE_ROW)
                {
                    NSString *addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,0)]];
                    xmlStr =[xmlStr stringByAppendingFormat:@"<Data><RemId>%@</RemId>",addr];
                    
                    addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,1)]];
                    xmlStr =[xmlStr stringByAppendingFormat:@"<RemName>%@</RemName>",addr];
                    
                    addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,2)]];
                    xmlStr =[xmlStr stringByAppendingFormat:@"<RemStartDate>%@</RemStartDate>",addr];
                    
                    
                    addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,3)]];
                    xmlStr =[xmlStr stringByAppendingFormat:@"<RemEndDate>%@</RemEndDate>",addr];
                    
                    addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,4)]];
                    xmlStr =[xmlStr stringByAppendingFormat:@"<RemCategory>%@</RemCategory>",addr];
                    
                    addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,5)]];
                    xmlStr =[xmlStr stringByAppendingFormat:@"<RemRepeat>%@</RemRepeat>",addr];
                    
                    addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,6)]];
                    xmlStr =[xmlStr stringByAppendingFormat:@"<RemNotes>%@</RemNotes>",addr];
                    
                    addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,7)]];
                    xmlStr =[xmlStr stringByAppendingFormat:@"<RemInterval>%@</RemInterval>",addr];
                    
                    addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,8)]];
                    xmlStr =[xmlStr stringByAppendingFormat:@"<RemMessage>%@</RemMessage>",addr];
                    
                    addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,9)]];
                    xmlStr =[xmlStr stringByAppendingFormat:@"<StartDateTimeStamp>%@</StartDateTimeStamp>",addr];
                    
                    
                    xmlStr =[xmlStr stringByAppendingString:@"</Data>"];
                }
                
                xmlStr =[xmlStr stringByAppendingString:@"</Family>"];
                
                sqlite3_finalize(compiled_stmt);
                
                
                mAppDelegate.notesTask = xmlStr;
                // Notify the previouse view to save the changes locally
                [[NSNotificationCenter defaultCenter] postNotificationName:@"New Note Task" object:self userInfo:[NSDictionary dictionaryWithObject:xmlStr forKey:@"NoteTask"]];
                  [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                NSLog(@"Error while creating detail view statement. '%s'", sqlite3_errmsg(dbFamilyPacking2));
            }
            
            NSLog(@"Generated XML : %@",xmlStr);
        }
    }
    else
    {
        NSLog(@"No iCloud access");
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end

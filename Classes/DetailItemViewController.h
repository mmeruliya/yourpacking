//
//  DetailItemViewController.h
//  BabyPacking
//
//  Created by Gary He on 5/21/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PackingItem.h"

@interface DetailItemViewController : UITableViewController {
	PackingItem *packingItem;
	IBOutlet UITextView *textView;
   //  IBOutlet UITextView *qtyTextView;
}

@property(nonatomic, retain) PackingItem *packingItem;

@end

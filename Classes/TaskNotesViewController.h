//
//  TaskNotesViewController.h
//  BabyPacking
//
//  Created by Jay Lee on 11/17/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BabyPackingAppDelegate;

@interface TaskNotesViewController : UIViewController {

	BabyPackingAppDelegate *appDelegate;
	
	NSString* mNotes;
	
	IBOutlet UITableView* mTableView;
	IBOutlet UITableViewCell* mNotesCell;
	IBOutlet UITextView* mNotesField;
	IBOutlet UIBarButtonItem* mBtnCancel;
	IBOutlet UIBarButtonItem* mBtnDone;
}

@property(nonatomic, copy) NSString* notes;

- (IBAction)buttonCancelDidClick:(id)sender;
- (IBAction)buttonDoneDidClick:(id)sender;

@end

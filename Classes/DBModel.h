//
//  DBModel.h
//  BabyPacking
//
//  Created by Gary He on 5/31/09.
//  Copyright 2009 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBConnection.h"

#define INVALID_PK -1

@interface DBModel : NSObject {
	DBConnection *conn;
	// Primary key in the database.
	NSInteger pk;
	// Internal state variables. Hydrated tracks whether attribute data is in the object or the database.
	BOOL hydrated;
	// Dirty tracks whether there are in-memory changes to data which have no been written to the database.
	BOOL dirty;
}

@property (nonatomic) NSInteger pk; 
@property (nonatomic) BOOL hydrated; 
@property (nonatomic) BOOL dirty; 

+ (void)finalizeStatements;
// Creates the object with primary key 

- (id)initWithPK:(NSInteger)primaryKey;
- (void)insertData;
- (void)updateData;
- (void)deleteData;
- (void)saveData;
+ (NSMutableArray*) findAll;

@end

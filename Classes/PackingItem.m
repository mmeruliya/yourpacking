//
//  PackingItem.m
//  BabyPacking
//
//  Created by Gary He on 5/20/09.
//  Copyright 2009 test. All rights reserved.
//

#import "PackingItem.h"
#import "BabyPackingAppDelegate.h"

static sqlite3_stmt *delete_by_listId_statement = nil;
static sqlite3_stmt *select_by_listId_statement = nil;
static sqlite3_stmt *select_all_statement = nil;
static sqlite3_stmt *upgrade_check_table_statement = nil;
static sqlite3_stmt *upgrade_select_statement = nil;
static sqlite3_stmt *upgrade_insert_statement = nil;
static sqlite3_stmt *insert_statement = nil;
static sqlite3_stmt *init_statement = nil;
static sqlite3_stmt *delete_statement = nil;
static sqlite3_stmt *update_statement = nil;
static sqlite3_stmt *hydrate_statement = nil;
static sqlite3_stmt *dehydrate_statement = nil;
static sqlite3_stmt *update_category_statement = nil;

@implementation PackingItem
@synthesize name;
@synthesize tip;
@synthesize qty;
@synthesize selected;
@synthesize checked;
@synthesize listId;
@synthesize predefineId;
@synthesize predefineForAge;
@synthesize custom;

- (BOOL)isEqual:(id)anObject {
	//NSLog(@"isEqual >>>>>>>");
	if (anObject == self)
        return YES;
    if (!anObject || ![anObject isKindOfClass:[self class]])
        return NO;
    return [self isEqualToItem:anObject];
}

- (BOOL)isEqualToItem:(PackingItem *)item {
	//NSLog(@"isEqualToItem >>>>>>>(%d =? %d)",predefineId ,item.predefineId);
    if (self.predefineId == item.predefineId) {
		return YES;
	}
    return NO;
}


- (NSUInteger)hash {
    NSUInteger hash = 0;
    hash += [name hash];
    hash += [tip hash];
	hash += [qty hash];
	hash += [custom hash];
	hash += predefineId;
    return hash;
}


- (id) init {
	if (self = [super init]) {
		name = @"";
		tip = @"";
		qty = @"";
		selected = NO;
		custom = @"";
		checked = NO;
		listId = 0;
		predefineId = 0;
		predefineForAge = 0;
		pk = INVALID_PK;
	}
	return self;
}

- (void)dealloc {
	[name release];
	[tip release];
	[qty release];
	[custom release];
    [super dealloc];
}

- (id)initWithPK:(NSInteger)primaryKey {
    
	NSLog(@"PackingItem initWithPK pk = %d ", primaryKey);
	if (self = [self init]) {
		self.pk = primaryKey;
		sqlite3 *database = conn.database;
		// Compile the query for retrieving book data. See insertNewBookIntoDatabase: for more detail.
        if (init_statement == nil) {

            // Note the '?' at the end of the query. This is a parameter which can be replaced by a bound variable.
            // This is a great way to optimize because frequently used queries can be compiled once, then with each
            // use new variable values can be bound to placeholders.
            const char *sql = "SELECT pk,name,tip,qty,selected,custom,checked,listId,predefineId,predefineForAge FROM item WHERE pk=?";
            if (sqlite3_prepare_v2(database, sql, -1, &init_statement, NULL) != SQLITE_OK) {
                NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));		
            }
        }
		// For this query, we bind the primary key to the first (and only) placeholder in the statement.
        // Note that the parameters are numbered from 1, not from 0.
        sqlite3_bind_int(init_statement, 1, pk);
		if (sqlite3_step(init_statement) == SQLITE_ROW) {
			int field = 1;
			char * val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.name = [NSString stringWithUTF8String:val];
			} else {
				self.name = @"";
			}

			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.tip = [NSString stringWithUTF8String:val];
			} else {
				self.tip = @"";
			}
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.qty = [NSString stringWithUTF8String:val];
			}else {
				self.qty = @"1";
			}
			self.selected = (BOOL)sqlite3_column_int(init_statement, field++);
			val = (char *)sqlite3_column_text(init_statement, field++);
			if (val != nil) {
				self.custom = [NSString stringWithUTF8String:val];
			}else {
				self.custom = @"";
			}
			self.checked = (BOOL)sqlite3_column_int(init_statement, field++);
			self.listId = sqlite3_column_int(init_statement, field++);
			self.predefineId = sqlite3_column_int(init_statement, field++);
			self.predefineForAge = sqlite3_column_int(init_statement, field++);
		} 
		// Reset the statement for future reuse.
        sqlite3_reset(init_statement);
        dirty = NO;
		hydrated = YES;
	}
	return self;
}
- (void)insertData {
	if (!dirty) return;
	NSLog(@"PackingItem insertData pk = %d dirty = %d", pk, dirty);
	//if (pk == INVALID_PK) return;
	
    sqlite3 *database = conn.database;
    if (insert_statement == nil) {
        const char *sql = "INSERT INTO item (name,tip,qty,selected,custom,checked,listId,predefineId,predefineForAge) VALUES(?,?,?,?,?,?,?,?,?)";
        if (sqlite3_prepare_v2(database, sql, -1, &insert_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
	if (name == nil) self.name = @"";
	if (tip == nil) self.tip = @"";
	if (qty == nil) self.qty = @"";
	if(custom == nil) self.custom = @"";
	int field = 1;
     sqlite3_bind_text(insert_statement, field++, [name UTF8String], -1, SQLITE_TRANSIENT);
     sqlite3_bind_text(insert_statement, field++, [tip UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insert_statement, field++, [qty UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_int(insert_statement, field++, (int)selected);
	sqlite3_bind_text(insert_statement, field++, [custom UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_int(insert_statement, field++, (int)checked);
	sqlite3_bind_int(insert_statement, field++, listId);
	sqlite3_bind_int(insert_statement, field++, predefineId);
	sqlite3_bind_int(insert_statement, field++, predefineForAge);
	int success = sqlite3_step(insert_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(insert_statement);
    if (success == SQLITE_ERROR) {
        NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
    } else {
        // SQLite provides a method which retrieves the value of the most recently auto-generated primary key sequence
        // in the database. To access this functionality, the table should have a column declared of type 
        // "INTEGER PRIMARY KEY"
        self.pk = sqlite3_last_insert_rowid(database);
    }
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
    
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    if(mAppDelegate.iCloudShow)
    {
        [self btnUpload];
    }
}
- (void)updateData {
	if (!dirty) return;
	if (pk == INVALID_PK) return;
	NSLog(@"PackingItem updateData pk = %d dirty = %d checked = %d", pk, dirty, checked);
    sqlite3 *database = conn.database;
    if (update_statement == nil) {
        const char *sql = "UPDATE item SET name=?, tip=?,qty=?, selected=?,custom=?,checked=?, listId=?, predefineId=?, predefineForAge=? WHERE pk=?";
        if (sqlite3_prepare_v2(database, sql, -1, &update_statement, NULL) != SQLITE_OK) {
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
	if (name == nil) self.name = @"";
	if (tip == nil) self.tip = @"";
	if (qty == nil) self.qty = @"1";
	if (custom == nil) self.custom = @"";
	int field = 1;
	sqlite3_bind_text(update_statement, field++, [name UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [tip UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_statement, field++, [qty UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_int(update_statement, field++, (int)selected);
	sqlite3_bind_text(update_statement, field++, [custom UTF8String],-1, SQLITE_TRANSIENT);
	sqlite3_bind_int(update_statement, field++, (int)checked);
	sqlite3_bind_int(update_statement, field++, listId);
	sqlite3_bind_int(update_statement, field++, predefineId);
	sqlite3_bind_int(update_statement, field++, predefineForAge);
	sqlite3_bind_int(update_statement, field++, pk);
	
    int success = sqlite3_step(update_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(update_statement);
    if (success == SQLITE_ERROR) {
        NSAssert1(0, @"Error: failed to update to the database with message '%s'.", sqlite3_errmsg(database));
    }
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
    
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    if(mAppDelegate.iCloudShow)
    {
        [self btnUpload];
    }
}
- (void)deleteData {
	if (pk == INVALID_PK) return;
	NSLog(@"PackingItem deleteData pk = %d", pk);
	sqlite3 *database = conn.database;
    if (delete_statement == nil) {
        const char *sql = "DELETE FROM item WHERE pk = ?";
        if (sqlite3_prepare_v2(database, sql, -1, &delete_statement, NULL) != SQLITE_OK) {
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
	sqlite3_bind_int(delete_statement, 1, pk);
    //int success = sqlite3_step(delete_statement);
	sqlite3_step(delete_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(delete_statement);
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
	delete_statement = nil;
    
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    if(mAppDelegate.iCloudShow)
    {
        [self btnUpload];
    }
}
- (void)deleteAllData {
//	if (pk == INVALID_PK) return;
//	NSLog(@"PackingItem deleteData pk = %d", pk);
	NSLog(@"DELETE ALL PACKINGITEM");
	sqlite3 *database = conn.database;
	if (delete_statement == nil) {
        const char *sql = "DELETE FROM item";
        if (sqlite3_prepare_v2(database, sql, -1, &delete_statement, NULL) != SQLITE_OK) {
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
   }
//	sqlite3_bind_int(delete_statement, 1, pk);
    //int success = sqlite3_step(delete_statement);
	sqlite3_step(delete_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(delete_statement);
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
	delete_statement = nil;
    
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    if(mAppDelegate.iCloudShow)
    {
        [self btnUpload];
    }
}

- (void) deleteByName
{
	sqlite3 *database = conn.database;
    if (delete_statement == nil) {
        const char *sql = "DELETE FROM item WHERE name = ?";
        if (sqlite3_prepare_v2(database, sql, -1, &delete_statement, NULL) != SQLITE_OK) {
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
	sqlite3_bind_text(delete_statement, 1, [name UTF8String], -1, SQLITE_TRANSIENT);
    //int success = sqlite3_step(delete_statement);
	sqlite3_step(delete_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(delete_statement);
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
	delete_statement = nil;
    
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    if(mAppDelegate.iCloudShow)
    {
        [self btnUpload];
    }
}

- (void) replaceCategoryName:(NSString *)str oldCategoryName:(NSString *)oldStr
{
	sqlite3 *database = conn.database;
    if (update_category_statement == nil) {
        const char *sql = "UPDATE item set custom = ? WHERE custom = ?";
        if (sqlite3_prepare_v2(database, sql, -1, &update_category_statement, NULL) != SQLITE_OK) {
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
	sqlite3_bind_text(update_category_statement, 1, [str UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(update_category_statement, 2, [oldStr UTF8String], -1, SQLITE_TRANSIENT);
    //int success = sqlite3_step(update_category_statement);
	sqlite3_step(update_category_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(update_category_statement);
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
	update_category_statement = nil;
    
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    if(mAppDelegate.iCloudShow)
    {
        [self btnUpload];
    }
}

- (void) replaceItemName:(NSString *)str
{
	sqlite3 *database = conn.database;
    if (delete_statement == nil) {
        const char *sql = "DELETE FROM item WHERE name = ?";
        if (sqlite3_prepare_v2(database, sql, -1, &delete_statement, NULL) != SQLITE_OK) {
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
	sqlite3_bind_text(delete_statement, 1, [str UTF8String], -1, SQLITE_TRANSIENT);
	//sqlite3_bind_text(delete_statement, 1, [name UTF8String], -1, SQLITE_TRANSIENT);
    //int success = sqlite3_step(delete_statement);
	sqlite3_step(delete_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(delete_statement);
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
	delete_statement = nil;
    
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    if(mAppDelegate.iCloudShow)
    {
        [self btnUpload];
    }
}

- (void) deleteByCategory:(NSString *)str
{
	sqlite3 *database = conn.database;
    if (delete_statement == nil) {
        const char *sql = "DELETE FROM item WHERE custom = ?";
        if (sqlite3_prepare_v2(database, sql, -1, &delete_statement, NULL) != SQLITE_OK) {
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
	sqlite3_bind_text(delete_statement, 1, [str UTF8String], -1, SQLITE_TRANSIENT);
    //int success = sqlite3_step(delete_statement);
	sqlite3_step(delete_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(delete_statement);
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    dirty = NO;
	hydrated = NO;
    
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    if(mAppDelegate.iCloudShow)
    {
        [self btnUpload];
    }
}

- (void)setName:(NSString *)aString {
    if ((!name && !aString) || (name && aString && [name isEqualToString:aString])) return;
    dirty = YES;
    [name release];
    name = [aString copy];
}

- (void)setTip:(NSString *)aString {
    if ((!tip && !aString) || (tip && aString && [tip isEqualToString:aString])) return;
    dirty = YES;
    [tip release];
    tip = [aString copy];
}

- (void)setQTY:(NSString *)aString {
	if ((!qty && !aString) || (qty && aString && [qty isEqualToString:aString])) return;
	dirty = YES;
	[qty release];
	qty = [aString copy];
}

- (BOOL) isChecked {
	return checked;
}

- (void) setChecked:(BOOL) aValue {
	if (aValue != checked) {
		dirty = YES;
		checked = aValue;
	}
}

- (void) setSelected:(BOOL) aValue {
	if (aValue != selected) {
		dirty = YES;
		selected = aValue;
	}
}

- (void)setCustom:(NSString *)aString {
	if ((!custom && !aString) || (custom && aString && [custom isEqualToString:aString])) return;
	dirty = YES;
	[custom release];
	custom = [aString copy];
}

- (void) setListId:(NSInteger) aListId {
	if (listId != aListId) {
		dirty = YES;
		listId = aListId;
	}
}

- (void) setPredefineId:(NSInteger) aId {
	if (predefineId != aId) {
		dirty = YES;
		predefineId = aId;
	}
}

- (void) setPredefineForAge:(NSInteger) newAge {
	if (predefineForAge != newAge) {
		dirty = YES;
		predefineForAge = newAge;
	}
}


//}
- (void) reverseCheckStatus {
	checked = !checked;
	dirty = YES;
}

- (void) reverseSelectStatus {
	selected = !selected;
	dirty = YES;
}

- (BOOL) isPredefine {
	return (predefineId != 0);
}

- (BOOL) hasTip {
	if (tip && [tip length] > 0) {
		NSString *s = tip;
		s = [s stringByReplacingOccurrencesOfString:@" " withString:@""];
		if ([s length] > 0)
			return YES;
		else
			return NO;
	} else {
		return NO;
	}
}

+ (void) deleteByListId:(NSInteger) listId {
	if (listId == 0) return;
	NSLog(@"PackingItem deleteByListId listId = %d", listId);
	sqlite3 *database = [DBConnection sharedConnection].database;
    if (delete_by_listId_statement == nil) {
        const char *sql = "DELETE FROM item WHERE listId = ?";
        if (sqlite3_prepare_v2(database, sql, -1, &delete_by_listId_statement, NULL) != SQLITE_OK) {
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
    }
	sqlite3_bind_int(delete_by_listId_statement, 1, listId);
    //int success = sqlite3_step(delete_statement);
	sqlite3_step(delete_by_listId_statement);
    // Because we want to reuse the statement, we "reset" it instead of "finalizing" it.
    sqlite3_reset(delete_by_listId_statement);
    // All data for the book is already in memory, but has not be written to the database
    // Mark as hydrated to prevent empty/default values from overwriting what is in memory
    
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    if(mAppDelegate.iCloudShow)
    {
        PackingItem *copy=[[PackingItem alloc]init];
        [copy btnUpload];
    }
}

+ (NSMutableArray*) findByListId:(NSInteger) listId {

    
	NSMutableArray *items = [NSMutableArray arrayWithCapacity:16];
	
	sqlite3 *database = [DBConnection sharedConnection].database;
	
	// Compile the delete statement if needed.
	if (select_by_listId_statement == nil) {
		const char *sql = "SELECT pk, name,tip,qty,selected,custom,checked,predefineId,predefineForAge FROM item WHERE listId = ? ";
		if (sqlite3_prepare_v2(database, sql, -1, &select_by_listId_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			return items;
		}
	}
	
	// Execute the query.
	sqlite3_bind_int(select_by_listId_statement, 1, listId);
	while (sqlite3_step(select_by_listId_statement) == SQLITE_ROW) {
		// The second parameter indicates the column index into the result set.
		PackingItem *item = [[PackingItem alloc] init];
		int field = 0;
		item.pk = sqlite3_column_int(select_by_listId_statement, field++);
		item.name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_by_listId_statement, field++)];
		item.tip = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_by_listId_statement, field++)];
		item.qty = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_by_listId_statement, field++)];
		item.selected = (BOOL)sqlite3_column_int(select_by_listId_statement, field++);
		item.custom = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_by_listId_statement, field++)];
		item.checked = (BOOL)sqlite3_column_int(select_by_listId_statement, field++);
		item.predefineId = sqlite3_column_int(select_by_listId_statement, field++);
		item.predefineForAge = sqlite3_column_int(select_by_listId_statement, field++);
		//NSLog(@"findByListId item.checked = %d", item.checked);
		item.listId = listId;
		item.dirty = NO;
		[items addObject:item];
		[item release];
	}
	// Reset the statement for future use.
	sqlite3_reset(select_by_listId_statement);
	
	return items;
}

+ (NSMutableArray*) findAll {
    
	//NSLog(@"PackingItem findAll");
	NSMutableArray *all = [NSMutableArray arrayWithCapacity:32];
	sqlite3 *database = [DBConnection sharedConnection].database;
	
	// Compile the delete statement if needed.
	if (select_all_statement == nil) {
		const char *sql = "SELECT pk,name,tip,qty,selected,custom,checked,listId,predefineId, predefineForAge FROM item ORDER BY name ASC";
		if (sqlite3_prepare_v2(database, sql, -1, &select_all_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
			return all;
		}
	}
	
	// Execute the query.
	while (sqlite3_step(select_all_statement) == SQLITE_ROW) {
		// The second parameter indicates the column index into the result set.
		int field = 0;
		PackingItem *item = [[PackingItem alloc] init];
		
		item.pk  = sqlite3_column_int(select_all_statement, field++);
		item.name  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		item.tip  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		item.qty  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement, field++)];
		NSLog(@"QUANTITY ==== %@",item.qty);
		item.selected = (BOOL)sqlite3_column_int(select_all_statement, field++);
		item.custom = [NSString stringWithUTF8String:(char *)sqlite3_column_text(select_all_statement,field++)];
		item.checked = (BOOL)sqlite3_column_int(select_all_statement, field++);
		item.listId = sqlite3_column_int(select_all_statement, field++);
		item.predefineId = sqlite3_column_int(select_all_statement, field++);
		item.predefineForAge = sqlite3_column_int(select_all_statement, field++);
		item.dirty = NO;
		[all addObject:item];
		[item release];
	}
	// Reset the statement for future use.
	sqlite3_reset(select_all_statement);
	
	return all;
}

+ (void)upgradeTableFromDatabase:(sqlite3 *)db ToDatabase:(sqlite3 *)newdb {
    
	// Check if table exists
	if (upgrade_check_table_statement == nil) {
		const char *sql = "SELECT COUNT(*) FROM sqlite_master where type='table' and name='item'";
		if (sqlite3_prepare_v2(db, sql, -1, &upgrade_check_table_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(db));
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(db));
		}
	}
	NSInteger count = 0;
	if (sqlite3_step(upgrade_check_table_statement) == SQLITE_ROW) {
		count = sqlite3_column_int(upgrade_check_table_statement, 0);
	}
	sqlite3_reset(upgrade_check_table_statement);
	sqlite3_finalize(upgrade_check_table_statement);
	upgrade_check_table_statement = nil;
	if (count <= 0) return;
	
	// Compile the delete statement if needed.
	if (upgrade_select_statement == nil) {
		const char *sql = "SELECT pk,name,tip,selected,checked,listId,predefineId,predefineForAge FROM item ORDER BY pk ASC";
		if (sqlite3_prepare_v2(db, sql, -1, &upgrade_select_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(db));
			NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(db));
		}
	}
	if (upgrade_insert_statement == nil) {
        const char *sql = "INSERT INTO item (name,tip,qty,selected,custom,checked,listId,predefineId,predefineForAge) VALUES(?,?,?,?,?,?,?,?,?)";
        if (sqlite3_prepare_v2(newdb, sql, -1, &upgrade_insert_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(newdb));
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(newdb));
        }
    }
	
	// Execute the query.
	NSInteger pk = -1;
	NSString* name;
	NSString* tip;
	NSString* qty;
	BOOL selected;
	NSString* custom;
	BOOL checked;
	NSInteger listId;
	NSInteger predefineId;
	NSInteger predefineForAge;
	while (sqlite3_step(upgrade_select_statement) == SQLITE_ROW) {
		// The second parameter indicates the column index into the result set.
		int field = 0;
		
		pk  = sqlite3_column_int(upgrade_select_statement, field++);
		char * val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			name = [NSString stringWithUTF8String:val];
		} else {
			name = @"";
		}
		val = (char *)sqlite3_column_text(upgrade_select_statement, field++);
		if (val != nil) {
			tip = [NSString stringWithUTF8String:val];
		} else {
			tip = @"";
		}
		qty  = @"1";
		selected = (BOOL)sqlite3_column_int(upgrade_select_statement, field++);
		custom = @"";
		checked = (BOOL)sqlite3_column_int(upgrade_select_statement, field++);
		listId = sqlite3_column_int(upgrade_select_statement, field++);
		predefineId = sqlite3_column_int(upgrade_select_statement, field++);
		predefineForAge = sqlite3_column_int(upgrade_select_statement, field++);
		
		int toField = 1;
		sqlite3_bind_text(upgrade_insert_statement, toField++, [name UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [tip UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [qty UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_int(upgrade_insert_statement, toField++, (int)selected);
		sqlite3_bind_text(upgrade_insert_statement, toField++, [custom UTF8String],-1, SQLITE_TRANSIENT);
		sqlite3_bind_int(upgrade_insert_statement, toField++, (int)checked);
		sqlite3_bind_int(upgrade_insert_statement, toField++, listId);
		sqlite3_bind_int(upgrade_insert_statement, toField++, predefineId);
		sqlite3_bind_int(upgrade_insert_statement, toField++, predefineForAge);
		sqlite3_step(upgrade_insert_statement);
		sqlite3_reset(upgrade_insert_statement);
	}
	// Reset the statement for future use.
	sqlite3_reset(upgrade_select_statement);
	sqlite3_finalize(upgrade_select_statement);
	upgrade_select_statement = nil;
	sqlite3_reset(upgrade_insert_statement);
	sqlite3_finalize(upgrade_insert_statement);
	upgrade_insert_statement = nil;
}

+ (void)finalizeStatements {
	//NSLog(@"finalizeStatements = ");
	if (delete_by_listId_statement) {
		sqlite3_finalize(delete_by_listId_statement);
		delete_by_listId_statement = nil;
	}
	if (select_by_listId_statement) {
		sqlite3_finalize(select_by_listId_statement);
		select_by_listId_statement = nil;
	}
	
	if (select_all_statement) {
		sqlite3_finalize(select_all_statement);
		select_all_statement = nil;
	}
	if (upgrade_check_table_statement) {
		sqlite3_finalize(upgrade_check_table_statement);
		upgrade_check_table_statement = nil;
	}
	if (upgrade_select_statement) {
		sqlite3_finalize(upgrade_select_statement);
		upgrade_select_statement = nil;
	}
	if (upgrade_insert_statement) {
		sqlite3_finalize(upgrade_insert_statement);
		upgrade_insert_statement = nil;
	}
    if (insert_statement) {
		sqlite3_finalize(insert_statement);
		insert_statement = nil;
	}
    if (init_statement) {
		sqlite3_finalize(init_statement);
		init_statement = nil;
	}
    if (delete_statement) {
		sqlite3_finalize(delete_statement);
		delete_statement = nil;
	}
	if (update_statement) {
		sqlite3_finalize(update_statement);
		update_statement = nil;
	}
	if (hydrate_statement) {
		sqlite3_finalize(hydrate_statement);
		hydrate_statement = nil;
	}
    if (dehydrate_statement) {
		sqlite3_finalize(dehydrate_statement);
		dehydrate_statement = nil;
	}
}

#pragma mark - download from iCloud

-(void)btnDownload
{
    NSURL *ubiq = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
    if (ubiq)
    {
        NSLog(@"iCloud access at %@", ubiq);
        [self loadiCloudData];
    }
    else
    {
        NSLog(@"No iCloud access");
    }
}

#pragma mark - upload in iCloud

-(void)btnUpload
{
    /*
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    
    // Create XML and pass to iCloud
    NSString *xmlStr=@"<Item>";
    NSString *select_query;
    const char *select_stmt;
    sqlite3_stmt *compiled_stmt;
    sqlite3 *dbFamilyPacking;
    if (sqlite3_open([[mAppDelegate getDBListPath] UTF8String], &dbFamilyPacking) == SQLITE_OK)
    {
        select_query = [NSString stringWithFormat:@"SELECT * FROM item"];
        select_stmt = [select_query UTF8String];
        if(sqlite3_prepare_v2(dbFamilyPacking, select_stmt, -1, &compiled_stmt, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiled_stmt) == SQLITE_ROW)
            {
                NSString *addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,0)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<Data><pk>%@</pk>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,1)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<name>%@</name>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,2)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<qty>%@</qty>",addr];
                
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,3)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<tip>%@</tip>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,4)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<selected>%@</selected>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,5)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<custom>%@</custom>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,6)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<checked>%@</checked>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,7)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<listId>%@</listId>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,8)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<predefineId>%@</predefineId>",addr];
                
                addr = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiled_stmt,9)]];
                xmlStr =[xmlStr stringByAppendingFormat:@"<predefineForAge>%@</predefineForAge>",addr];
                
                
                xmlStr =[xmlStr stringByAppendingString:@"</Data>"];
            }
            
            xmlStr =[xmlStr stringByAppendingString:@"</Item>"];
            
            sqlite3_finalize(compiled_stmt);
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"PropertiesItem.xml" ];
            NSData *file = [NSData dataWithBytes:[xmlStr UTF8String] length:strlen([xmlStr UTF8String])];
            [file writeToFile:path atomically:YES];
            
            NSString *fileName = [NSString stringWithFormat:@"PropertiesItem.xml"];
            NSURL *ubiq = [[NSFileManager defaultManager]URLForUbiquityContainerIdentifier:nil];
            NSURL *ubiquitousPackage = [[ubiq URLByAppendingPathComponent:@"Documents"]  URLByAppendingPathComponent:fileName];
            
            syncTask *mydoc = [[syncTask alloc] initWithFileURL:ubiquitousPackage];
            mydoc.noteContent = xmlStr;
            [mydoc saveToURL:[mydoc fileURL]forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success)
             {
                 
                 if (success)
                 {
                     NSLog(@"Sync succeeded");
                 }
                 else
                 {
                     NSLog(@"failed opening document from iCloud");
                 }
             }];
        }
        else
        {
            NSLog(@"Error while creating detail view statement. '%s'", sqlite3_errmsg(dbFamilyPacking));
        }
        
        NSLog(@"Generated XML : %@",xmlStr);
    }*/
    
}

#pragma mark - all other methods of iCloud

- (void)loadiCloudData
{
    @try
    {
        NSURL *ubiq = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
        
        if (ubiq) {
            
            self.metadataQuery = [[NSMetadataQuery alloc] init];
            [self.metadataQuery setSearchScopes:
             [NSArray arrayWithObject:
              NSMetadataQueryUbiquitousDocumentsScope]];
            
            NSPredicate *pred = [NSPredicate predicateWithFormat:
                                 @"%K like 'PropertiesItem.xml' ", NSMetadataItemFSNameKey];
            [self.metadataQuery setPredicate:pred];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(queryDidFinishGathering:)
                                                         name:NSMetadataQueryDidFinishGatheringNotification
                                                       object:self.metadataQuery];
            
            [self.metadataQuery startQuery];
            
        }
        else
        {
            NSLog(@"loadiCloudData : No iCloud access");
        }
    }
    @catch (NSException *exception) {
        NSLog(@"loadiCloudData : %@",(NSString *)exception);
    }
}

- (void)queryDidFinishGathering:(NSNotification *)notification
{
    @try
    {
        NSMetadataQuery *query = [notification object];
        [query disableUpdates];
        [query stopQuery];
        [self loadData:query];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:NSMetadataQueryDidFinishGatheringNotification
                                                      object:query];
        
        self.metadataQuery = nil;
    }
    @catch (NSException *exception)
    {
        NSLog(@"Error queryDidFinishGathering : %@",exception);
    }
}

- (void)loadData:(NSMetadataQuery *)queryData
{
    BabyPackingAppDelegate* mAppDelegate;
    mAppDelegate = [UIApplication sharedApplication].delegate;
    
    // Code for delete all existing records
    NSString *select_query;
    sqlite3 *database;
    if(sqlite3_open([[mAppDelegate getDBListPath] UTF8String], &database) == SQLITE_OK)
    {
        select_query = [NSString stringWithFormat:@"delete from item"];
        const char *sqlStatementShirts = [select_query UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatementShirts, -1, &compiledStatement, NULL) != SQLITE_OK)
        {
            NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
        else
        {
            sqlite3_step(compiledStatement);
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
    // Code for parse all the records of iCloud and store in database
    for (NSMetadataItem *item in [queryData results])
    {
        NSURL *url = [item valueForAttribute:NSMetadataItemURLKey];
        NSData *file = [NSData dataWithContentsOfURL:url];
        NSString *xmlFile = [[NSString alloc] initWithData:file encoding:NSASCIIStringEncoding];
        NSArray *arr=[xmlFile componentsSeparatedByString:@"<Data>"];
        if ([arr count]>1)
        {
            for(int i=1;i<[arr count];i++)
            {
                NSString *str=[arr objectAtIndex:i];
                NSArray *arr1=[str componentsSeparatedByString:@"<pk>"];
                NSString *data=[arr1 objectAtIndex:1];
                NSRange ranfrom=[data rangeOfString:@"</pk>"];
                NSString *val11=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<name>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</name>"];
                NSString *val1=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<qty>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</qty>"];
                NSString *val2=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<tip>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</tip>"];
                NSString *val3=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<selected>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</selected>"];
                NSString *val4=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<custom>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</custom>"];
                NSString *val5=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<checked>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</checked>"];
                NSString *val6=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<listId>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</listId>"];
                NSString *val7=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<predefineId>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</predefineId>"];
                NSString *val8=[data substringToIndex:ranfrom.location];
                
                arr1=[str componentsSeparatedByString:@"<predefineForAge>"];
                data=[arr1 objectAtIndex:1];
                ranfrom=[data rangeOfString:@"</predefineForAge>"];
                NSString *val9=[data substringToIndex:ranfrom.location];
                
                sqlite3 *database;
                if(sqlite3_open([[mAppDelegate getDBListPath] UTF8String], &database) == SQLITE_OK)
                {
                    select_query = [NSString stringWithFormat:@"INSERT INTO item (pk,name,qty,tip,selected,custom,checked,listId,predefineId,predefineForAge) VALUES(\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",val11,val1,val2,val3,val4,val5,val6,val7,val8,val9];
                    const char *sqlStatementShirts = [select_query UTF8String];
                    sqlite3_stmt *compiledStatement;
                    if(sqlite3_prepare_v2(database, sqlStatementShirts, -1, &compiledStatement, NULL) != SQLITE_OK)
                    {
                        NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
                    }
                    else
                    {
                        sqlite3_step(compiledStatement);
                    }
                    sqlite3_finalize(compiledStatement);
                }
                sqlite3_close(database);
            }
        }
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"iCloud downloading Done..." message:xmlFile
                                                       delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

@end

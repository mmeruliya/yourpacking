//
//  SupportViewController.m
//  BabyPacking
//
//  Created by Gary He on 5/25/09.
//  Copyright 2009 test. All rights reserved.
//

#import "SupportViewController.h"
#import "TextPickerViewController.h"
#import "BabyPackingAppDelegate.h"
#import "Constant.h"

@interface SupportViewController(Private) 

- (BOOL) checkEmpty:(NSString*)text;
- (void) sendEmail ;
@end


@implementation SupportViewController


- (void)dealloc {
	[nameCell release];
	[emailCell release];
	[requestTypeCell release];
	[subjectCell release];
	[messageCell release];
	
	[nameField release];
	[emailField release];
	[typeLabel release];
	[subjectField release];
	[messageView release];
	
    [super dealloc];
}



/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if (self = [super initWithStyle:style]) {
    }
    return self;
}
*/


- (void)viewDidLoad {
    [super viewDidLoad];
	appDelegate=(BabyPackingAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(0, 0, 30, 30);
    doneBtn.tag=2;
    [doneBtn setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
    // [Editbtn setBackgroundColor:[UIColor blackColor]];
    [doneBtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    doneButton=[[UIBarButtonItem alloc] initWithCustomView:doneBtn];
    
    self.navigationItem.leftBarButtonItem = doneButton;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.rightBarButtonItem = sendButton;
	

    
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor whiteColor];
    
	NSString *customFontName = [appDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];		
	}
	else {
		label.font=[UIFont fontWithName:customFontName size:fontSize];		
	}
	
	[label sizeToFit];
	
	
	//label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;
	
//////self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour11;
	// read the data back from the user defaults
	NSData *data111= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
	// check whether you got anything
	if(data111 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour11 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data111];
//		nameField.textColor = theColour11;
//		emailField.textColor = theColour11;
//		subjectField.textColor = theColour11;
//		messageView.textColor = theColour11;

		lblName.textColor = theColour11;
		lblEmail.textColor = theColour11;
		lblRequestType.textColor = theColour11;
		lblSubject.textColor = theColour11;
		lblMessage.textColor = theColour11;
	}
////// and finally set the colour of your label
	
	
	NSString *customFontName1 = [appDelegate getCustomFontName];
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
		nameField.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];			
		emailField.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];				
		subjectField.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];			
		typeLabel.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];			
		messageView.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];			
		
		lblName.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];			
		lblEmail.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];			
		lblRequestType.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];			
		lblSubject.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];					
	}
	
	
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
            label.textColor=[UIColor whiteColor];
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label.textColor = theColour1;
	}
	// and finally set the colour of your label
	
	self.navigationItem.titleView = label;	
	label.text = TITLE_SUPPORT;
	[label sizeToFit];
	
	typeLabel.text = @"General";
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
}


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (IBAction) buttonAction:(id) sender {
	if (sender == sendButton) {
		if (![self checkEmpty:nameField.text]) {
			BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
			[appDelegate showAlert:M_INPUT_NAME_TITLE message:M_INPUT_NAME];
			return;
		}
		
		if (![self checkEmpty:emailField.text]) {
			BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
			[appDelegate showAlert:M_INPUT_NAME_TITLE message:@"Please input your email!"];
			return;
		}
		
		if (![self checkEmpty:subjectField.text]) {
			BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
			[appDelegate showAlert:M_INPUT_NAME_TITLE message:@"Please input subject !"];
			return;
		}
		
		if (![self checkEmpty:messageView.text]) {
			BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
			[appDelegate showAlert:M_INPUT_NAME_TITLE message:@"Please input message!"];
			return;
		}
		
		[self sendEmail];
		
	} else if (sender == doneButton.customView) {
		[nameField resignFirstResponder];
		[emailField resignFirstResponder];
		[subjectField resignFirstResponder];
		[messageView resignFirstResponder];
        [self.navigationController popViewControllerAnimated:YES];
	}
}

- (BOOL) checkEmpty:(NSString*)text {
	NSString *trimedText = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	if (![trimedText length] > 0) {
		return NO;
	}
	return YES;
}

- (void) sendEmail {
	NSMutableString *email = [NSMutableString stringWithCapacity:1024];
	[email appendString:[NSString stringWithFormat:@"mailto:%@?",I_ADMIN_EMAIL ]];
	NSString *subject = [NSString stringWithFormat:@"%@ -- Request Type: %@", subjectField.text, typeLabel.text];
	
	[email appendString:[NSString stringWithFormat:@"subject=%@", [subject stringByReplacingOccurrencesOfString:@"&" withString:@"%26"]]];
	NSString *body = [NSString stringWithFormat:@"%@\n\n%@\n%@",messageView.text, emailField.text, nameField.text ];
	[email appendString:[NSString stringWithFormat:@"&body=%@", [body stringByReplacingOccurrencesOfString:@"&" withString:@"%26"]]];

	[email setString:[email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	//NSLog(@"email = %@", email);
	NSURL *url = [NSURL URLWithString:email];
	
    [[UIApplication sharedApplication] openURL:url];
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	NSString *title = @"";
	if (section == 0) {
		title = @"";
	} else if (section == 1){
		title = @"Message";
	}
	return title;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
		return 4;
	}
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == 1) {
		return 160;
	} else {
		return 44;
	}
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
    }
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour12;
	// read the data back from the user defaults
	NSData *data112= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
	// check whether you got anything
	if(data112 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour12 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data112];
		nameCell.backgroundColor = theColour12;
		emailCell.backgroundColor = theColour12;
		requestTypeCell.backgroundColor = theColour12;
		subjectCell.backgroundColor = theColour12;
		messageCell.backgroundColor = theColour12;
	}
	// and finally set the colour of your label
	
	
	
	NSString *customFontName = [appDelegate getCustomFontName];
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
		nameCell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];		
		emailCell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];		
		requestTypeCell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];		
		subjectCell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];		
		messageCell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];		
	}
	
	
	if (indexPath.section == 0) {
		if (indexPath.row == 0) {
			return nameCell;
		} else if (indexPath.row == 1) {
			return emailCell;
		} else if (indexPath.row == 2) {
			return requestTypeCell;
		} else if (indexPath.row == 3) {
			return subjectCell;
		}
	} else if(indexPath.section == 1) {
		return messageCell;
	}
    
    // Set up the cell...
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
	// AnotherViewController *anotherViewController = [[AnotherViewController alloc] initWithNibName:@"AnotherView" bundle:nil];
	// [self.navigationController pushViewController:anotherViewController];
	// [anotherViewController release];
	if (indexPath.row == 2) {
		TextPickerViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[TextPickerViewController alloc] initWithNibName:@"TextPickerViewController" bundle:nil];
		else
			controller = [[TextPickerViewController alloc] initWithNibName:@"TextPickerViewController1" bundle:nil];
		
		//[[TextPickerViewController alloc] initWithNibName:@"TextPickerViewController" bundle:nil];
		//controller.supportViewController = self;
		controller.title = @"Request Type";
		controller.selectedText = typeLabel.text;
		controller.delegate = self;
		controller.choiceArray = [NSArray arrayWithObjects:@"General", @"Feature Request", @"Product Inquiry", @"Feedback", @"Report a bug", nil];
		[self.navigationController pushViewController:controller animated:YES];
		[controller release];
	}
}

- (void) onSelectedFont:(NSString*) text {
	typeLabel.text = text;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if ([textField isEqual:nameField]) {
		[emailField becomeFirstResponder];
	} else if ([textField isEqual:emailField]) {
		[subjectField becomeFirstResponder];
	} else if ([textField isEqual:subjectField]) {
		[messageView becomeFirstResponder];
	} 
	return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
	self.navigationItem.rightBarButtonItem = doneButton;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	self.navigationItem.rightBarButtonItem = sendButton;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
	self.navigationItem.rightBarButtonItem = doneButton;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
	self.navigationItem.rightBarButtonItem = sendButton;
}

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{            return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
       // [layoutManager translateToPortraitForView:self.view withAnimation:YES];
    }
    else
    {
       // [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
    }
    // }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}

@end


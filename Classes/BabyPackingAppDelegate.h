//
//  BabyPackingAppDelegate.h
//  BabyPacking
//
//  Created by Gary He on 5/18/09.
//  Copyright test 2009. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>
#import "DBConnection.h"
#import "RequestHandler.h"
//#include <sys/xattr.h>
#import "syncTask.h"

@class WelcomeViewController;
@class RootViewController;
@class DataManager;
@class ViewController;

@interface BabyPackingAppDelegate : NSObject <UIApplicationDelegate> {
    
	UIWindow *window;
	UINavigationController *navigationController;
	IBOutlet UINavigationController *rootNavigationController;
	IBOutlet UINavigationController *settingsNavigationController;
	IBOutlet UINavigationController *infoNavigationController;
	IBOutlet UINavigationController *helpNavigationController;
	IBOutlet UINavigationController *briefcaseNavigationController;
	IBOutlet UITabBar *mTabBar;
	IBOutlet UITabBarItem *mMainTabBarItem;
	IBOutlet UIView *mSynchronizingView;
	IBOutlet UIActivityIndicatorView *mIndicatorView;
	
	RootViewController *homeViewController;
    ViewController *viewController;
	WelcomeViewController *welcomeViewController;
	
	DBConnection *dbConn;
	
	NSString *dataPath;
	NSMutableDictionary *appDict;
	NSMutableArray *settingBundle;
	NSMutableArray *readyMadeListArray;
	NSMutableArray *readyMadeListForDeluxeArray;
	NSMutableArray *babyPredefinedItems;
	NSMutableArray *toddlerPredefinedItems;
	NSMutableArray *userCreateItems;
	
	BOOL soundOn, startDate;
    BOOL soundProgressOn;
	BOOL colorBgOn;
	BOOL photoBgOn;
	BOOL firstTime;
	NSString * addItemsByGroup;
	BOOL readyMadeListShow;
    BOOL iCloudShow;
	NSString * welcomePageShow;
	NSString *password;
	NSString *fontType;
	NSString *taskSound;
	NSString *mDeviceToken;
	SystemSoundID mSoundID;
	
	NSMutableArray *categoryFromDB;
	NSMutableArray *itemsFromDB;
	NSMutableArray *restoreItems;
	NSMutableArray *restoreCategories;
	NSMutableArray *allItemsFromDB;
	
	NSMutableArray *allItemsList;
	NSMutableArray *allCategoriesList;
	
	DataManager *dataManager;
	
	BOOL isHelpDownloaded;
	NSMutableArray *helpArray;
	
	int loginStatus;
	int userID;
    
    //Change By MS
    NSString *dbPath;
    NSString *notesList,*notesItem,*notesTask;
}

@property (strong, nonatomic) NSString *notesList,*notesItem,*notesTask;
@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;
@property (nonatomic, retain) RootViewController *homeViewController;
@property (nonatomic, retain) WelcomeViewController *welcomeViewController;
@property (nonatomic, retain) NSString *dataPath;
@property (nonatomic, retain) NSMutableDictionary *appDict;
@property (nonatomic, retain) NSMutableArray *settingBundle;
@property (nonatomic, retain) NSMutableArray *readyMadeListArray;
@property (nonatomic, retain) NSMutableArray *readyMadeListForDeluxeArray;
@property (strong) NSMetadataQuery *metadataQuery;


@property (nonatomic, retain) NSMutableArray *babyPredefinedItems;
@property (nonatomic, retain) NSMutableArray *toddlerPredefinedItems;
@property (nonatomic, retain) NSMutableArray *userCreateItems;
@property (nonatomic, retain) NSString *password;
@property (nonatomic) BOOL soundOn, startDate;
@property (nonatomic) BOOL soundProgressOn;
@property (nonatomic) BOOL colorBgOn;
@property (nonatomic) BOOL photoBgOn;
@property (nonatomic) BOOL firstTime;
@property (nonatomic, retain) NSString *addItemsByGroup;
@property (nonatomic) BOOL readyMadeListShow;
@property (nonatomic) BOOL iCloudShow;
@property (nonatomic, retain) NSString *welcomePageShow;
@property (nonatomic, copy) NSString *fontType;
@property (nonatomic, copy) NSString *taskSound;
@property (nonatomic, copy) NSString *deviceToken;
@property (nonatomic, assign) SystemSoundID soundID;

@property (nonatomic, retain) NSMutableArray *categoryFromDB;
@property (nonatomic, retain) NSMutableArray *itemsFromDB;
@property (nonatomic, retain) NSMutableArray *restoreItems;
@property (nonatomic, retain) NSMutableArray *restoreCategories;
@property (nonatomic, retain) NSMutableArray *allItemsFromDB;

@property (nonatomic, retain) DataManager *dataManager;

@property (assign, readwrite) BOOL isHelpDownloaded;
@property (nonatomic, retain) NSMutableArray *helpArray;

@property int loginStatus;
@property int userID;

@property (nonatomic, retain) NSMutableArray *allItemsList;
@property (nonatomic, retain) NSMutableArray *allCategoriesList;

@property (nonatomic, retain) NSString *globReminderId;
@property (nonatomic,retain ) NSString *ImageName;

@property (strong) syncTask * doc;
@property (strong) NSMetadataQuery *query;

- (void)loadDocument;

- (void) showAlert:(NSString*) title message:(NSString*) message;
- (void) encodeMailBody:(NSMutableString*)str;
- (NSString*) getCustomFontName;

- (void) startSynchronizing;
- (void) stopSynchronizing;

- (void) prefetchSoundID:(NSString *)sound;

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

//Change By MS
//Database
- (NSString *) getDBPath ;
- (NSString *) getDBListPath;

@end


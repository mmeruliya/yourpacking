//
//  ReadyMadeListForDeluxe.m
//  BabyPacking
//
//  Created by eve on 9/21/09.
//  Copyright 2009 test. All rights reserved.
//

#import "ReadyMadeListForDeluxe.h"
#import "PackingList.h"
#import "BabyPackingAppDelegate.h"
#import "Constant.h"
#import "CheckListViewController.h"
#import "CSTableViewCell.h"
#import "PackingItem.h"

@implementation ReadyMadeListForDeluxe

@synthesize homeController;
@synthesize readyMadeListArray;
@synthesize isFromCheckList;
@synthesize titleName;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;

    label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor whiteColor];
    
	NSString *customFontName = [appDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
    //	label.font = [UIFont fontWithName:customFontName size:fontSize];
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];		
	}
	else {
		label.font=[UIFont fontWithName:customFontName size:fontSize];		
	}
	
//	label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;
    
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
    label.textColor=[UIColor whiteColor];        
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label.textColor = theColour1;
	}
	// and finally set the colour of your label
	
	label.text=[NSString stringWithFormat:@"%@",titleName];
    [label sizeToFit];
    
	self.navigationItem.titleView = label;

	bgImageView.alpha = BG_PHOTO_ALPHA;
	if (appDelegate.colorBgOn) {
			bgImageView.alpha = 1.0;
			if (TARGET_VERSION == VERSION_BABY_PACKING)
				bgImageView.image = [UIImage imageNamed:GRAY_BEAR];
			else
				bgImageView.image = [UIImage imageNamed:BACKGROUND_GRAY];
		}
		
	otherIcon = [[UIImage imageNamed:OTHER_ICON] retain];

	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 30, 30);
    [btn setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(resetCriteria:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *createBtItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    self.navigationItem.leftBarButtonItem = createBtItem;
    
   	/*if (isFromCheckList) {
		[self.navigationController popViewControllerAnimated:YES];
	}*/
}

-(IBAction)resetCriteria:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void) setFlag
{
	isFromCheckList = YES;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
    [titleName release];
	[bgImageView release];
	//[bgBorderView release];
	[myTableView release];
	[readyMadeListArray release];
	[otherIcon release];
	
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.readyMadeListArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

	static NSString *CellIdentifier = @"TemplateCell";
	UITableViewCell *cell;
	cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		cell = [[[CSTableViewCell alloc] initWithCSStyle:CSTableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		((CSTableViewCell*)cell).detailTextLabel.textColor = RED_COLOR;
		//((CSTableViewCell*)cell).imageView.contentMode = UIViewContentModeScaleAspectFill;
		UIImageView *inUseView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 35, 22)];
		inUseView.image = [UIImage imageNamed:IS_USE_ICON];
		((CSTableViewCell*)cell).csAccessoryView = inUseView;
		[inUseView release];
	}
	CSTableViewCell *csCell = (CSTableViewCell*) cell;
	// Configure the cell.
	PackingList *packingList = [readyMadeListArray objectAtIndex:indexPath.row];
	csCell.textLabel.text = packingList.name;

	csCell.image = otherIcon;
	if ([packingList inUse]) {
		[csCell showAccessoryView];
	} else {
		[csCell hideAccessoryView];
	}
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	
    
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
        tableView.backgroundColor = theColour1;
		cell.backgroundColor = theColour1;
        csCell.backgroundColor = theColour1;
	}
	// and finally set the colour of your label

    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour21;
	// read the data back from the user defaults
	NSData *data211= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
	// check whether you got anything
	if(data211 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour21 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data211];
        cell.textLabel.textColor = theColour21;
        csCell.textLabel.textColor = theColour21;
	}
	// and finally set the colour of your label
	
    
    NSString *customFontName = [appDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
		cell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];		
		csCell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];		
	}
	else {
		cell.textLabel.font=[UIFont fontWithName:customFontName size:fontSize];		
		csCell.textLabel.font=[UIFont fontWithName:customFontName size:fontSize];		
	}

    
	return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {	

	CheckListViewController *controller = nil;
	
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		controller = [[CheckListViewController alloc] initWithNibName:@"CheckListViewController" bundle:nil];
	else
		controller = [[CheckListViewController alloc] initWithNibName:@"CheckListViewController1" bundle:nil];
	
	//[[CheckListViewController alloc] initWithNibName:@"CheckListViewController" bundle:nil];
	PackingList *list = [readyMadeListArray objectAtIndex:indexPath.row];
    NSLog(@"name %@",list.name);
	controller.title = list.name;
    controller.controllerTitle = list.name;
	// will do nothing if this is a ready made list
	[list loadItemsIfNeed];
	controller.currentList = list;
	controller.homeController = self.homeController;
	if ([controller.currentList inUse])
		//controller.navigationItem.rightBarButtonItem.enabled=YES;
		controller.canEmail=YES;
	else 
		controller.canEmail=NO;
	//controller.navigationItem.rightBarButtonItem.enabled=NO;
	controller.rootController = self;
	[self.navigationController pushViewController:controller animated:YES];
	[controller release];
	
}

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        // [layoutManager translateToPortraitForView:self.view withAnimation:YES];
    }
    else
    {
        // [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
    }
    // }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}



@end

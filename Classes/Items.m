//
//  Items.m
//  BabyPacking
//
//  Created by Hitesh on 12/15/10.
//  Copyright 2010 cmc limited. All rights reserved.
//

#import "Items.h"

static sqlite3_stmt *statement = nil;
static sqlite3_stmt *add_statement = nil;
static sqlite3_stmt *delete_statement = nil;

@implementation Items

@synthesize pk,checked,name,tip,custom,qty,deleted,categoryId;

- (id) initWithPrimaryKey:(NSInteger)primaryKey database:(sqlite3 *)db
{
	if (self == [super init])
	{
		pk = primaryKey;
		database = db;
		
		if(statement == nil) 
		{
			const char *sql = "Select checked, name, tip, custom, qty, deleted, categoryId from Items Where pk = ?";
			if(sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK)
				NSAssert1(0, @"Error while creating detail view statement. '%s'", sqlite3_errmsg(database));
		}
		
		sqlite3_bind_int(statement, 1, pk);
		
		if(SQLITE_DONE != sqlite3_step(statement)) 
		{	
			self.checked = sqlite3_column_int(statement, 0);
			
			if (sqlite3_column_text(statement, 1) != nil)
				self.name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
			else 
				self.name = nil;
			
			if (sqlite3_column_text(statement, 2) != nil)
				self.tip = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
			else 
				self.tip = nil;
			
			if (sqlite3_column_text(statement, 3) != nil)
				self.custom = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
			else 
				self.custom = nil;
			
			if (sqlite3_column_text(statement, 4) != nil)
				self.qty = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
			else 
				self.qty = nil;
			
			self.deleted = sqlite3_column_int(statement, 5);
			
			self.categoryId = sqlite3_column_int(statement, 6);
		}
		else
			NSAssert1(0, @"Error while getting the progress record. '%s'", sqlite3_errmsg(database));
		
		//Reset the detail statement.
		sqlite3_reset(statement);
	}
	
	return self;	
}

- (void) addItem:(sqlite3 *)db
{
	database = db;
	if(add_statement == nil) 
	{
		const char *sql = "insert into Items(checked,name,tip,custom,qty,deleted,categoryId) values(?,?,?,?,?,?,?)";
		if(sqlite3_prepare_v2(database, sql, -1, &add_statement, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating insert statement. '%s'", sqlite3_errmsg(database));
	}
	
	sqlite3_bind_int(add_statement, 1, checked);
	sqlite3_bind_text(add_statement, 2, [name UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(add_statement, 3, [tip UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(add_statement, 4, [custom UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(add_statement, 5, [qty UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_int(add_statement, 6, deleted);
	sqlite3_bind_int(add_statement, 7, categoryId);
	
	if(SQLITE_DONE != sqlite3_step(add_statement))
		NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
	else
		//SQLite provides a method to get the last primary key inserted by using sqlite3_last_insert_rowid
		pk = sqlite3_last_insert_rowid(database);
	
	//Reset the add statement.
	sqlite3_reset(add_statement);	
}

- (void) updateItemsOldCategory:(NSString *)oldCat withNewCategory:(NSString *)newCat dbase:(sqlite3 *)db
{
    database = db;
	if(delete_statement == nil) 
	{
		const char *sql = "update Items set custom = ? where custom = ?";
		if(sqlite3_prepare_v2(database, sql, -1, &delete_statement, NULL) != SQLITE_OK){}
//			NSAssert1(0, @"Error while creating insert statement. '%s'", sqlite3_errmsg(database));
	}
	
	sqlite3_bind_text(delete_statement, 1, [newCat UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(delete_statement, 2, [oldCat UTF8String], -1, SQLITE_TRANSIENT);
	
	if (SQLITE_DONE != sqlite3_step(delete_statement))
//		NSAssert1(0,@"Error while deleting data with %s",sqlite3_errmsg(database));
	
	//Reset the add statement.
	sqlite3_reset(delete_statement);
}

- (void) updateItem:(sqlite3 *)db
{
	database = db;
	if(delete_statement == nil) 
	{
		const char *sql = "update Items set checked = ?, name = ?, tip = ?, qty = ?, deleted = ? where pk = ?";
		if(sqlite3_prepare_v2(database, sql, -1, &delete_statement, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating insert statement. '%s'", sqlite3_errmsg(database));
	}
	
	sqlite3_bind_int(delete_statement, 1, checked);
	sqlite3_bind_text(delete_statement, 2, [name UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(delete_statement, 3, [tip UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(delete_statement, 4, [qty UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_int(delete_statement, 5, deleted);
	sqlite3_bind_int(delete_statement, 6, pk);
	
	if (SQLITE_DONE != sqlite3_step(delete_statement))
		NSAssert1(0,@"Error while deleting data with %s",sqlite3_errmsg(database));
	
	//Reset the add statement.
	sqlite3_reset(delete_statement);
}

- (void) deleteItems:(sqlite3 *)db{
	database = db;
	if(delete_statement == nil) 
	{
		const char *sql = "delete from Items";
		if(sqlite3_prepare_v2(database, sql, -1, &delete_statement, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating insert statement. '%s'", sqlite3_errmsg(database));
	}

	if (SQLITE_DONE != sqlite3_step(delete_statement))
		NSAssert1(0,@"Error while deleting data with %s",sqlite3_errmsg(database));
	
	//Reset the add statement.
	sqlite3_reset(delete_statement);	
}

- (void) dealloc
{
	[name release];
	[tip release];
	[qty release];
	[custom release];
	[super dealloc];
}

@end

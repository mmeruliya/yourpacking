//
//  SupportViewController.h
//  BabyPacking
//
//  Created by Gary He on 5/25/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextPickerViewController.h"

@class BabyPackingAppDelegate;

@interface SupportViewController : UITableViewController<UITextFieldDelegate, UITextViewDelegate, TextPickerDelegate> {
	BabyPackingAppDelegate *appDelegate;
	IBOutlet UITableViewCell *nameCell;
	IBOutlet UITableViewCell *emailCell;
	IBOutlet UITableViewCell *requestTypeCell;
	IBOutlet UITableViewCell *subjectCell;
	IBOutlet UITableViewCell *messageCell;
	
	IBOutlet UITextField *nameField;
	IBOutlet UITextField *emailField;
	IBOutlet UILabel	 *typeLabel;
	IBOutlet UITextField *subjectField;
	IBOutlet UITextView  *messageView;
	
	IBOutlet UILabel *lblName;
	IBOutlet UILabel *lblEmail;
	IBOutlet UILabel *lblRequestType;
	IBOutlet UILabel *lblSubject;
	IBOutlet UILabel *lblMessage;
	
	IBOutlet UIBarButtonItem *sendButton;
	IBOutlet UIBarButtonItem *doneButton;
}

- (IBAction) buttonAction:(id) sender;

@end

//
//  SettingsViewController.m
//  BabyPacking
//
//  Created by Gary He on 6/8/09.
//  Copyright 2009 test. All rights reserved.
//

#import "Constant.h"
#import "CustomizeTopBarViewController.h"
#import "BabyPackingAppDelegate.h"

@implementation CustomizeTopBarViewController

@synthesize colorSwatch;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        layoutManager = [[LayoutManager alloc] initWithPortraitView:self.view andLandscapeNIB:@"CustomizeTopBarViewController_L"];
        //self = [super initWithNibName:@"RecalliPhoneView" bundle:nibBundleOrNil];
        
    }
    return self;
}



-(IBAction) selectColor:(id)sender {
    
    if ([sender tag]==1) {
		colorSelectIndex=1;
	}
	else if ([sender tag]==2) {
		colorSelectIndex=2;		
	}
	else if ([sender tag]==3) {
		colorSelectIndex=3;		
	}
	else if ([sender tag]==4) {
		colorSelectIndex=4;		
	}

    ColorPickerViewController *colorPickerViewController;
    
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){    
        colorPickerViewController = [[ColorPickerViewController alloc] initWithNibName:@"ColorPickerViewController_ipad" bundle:nil];
    }
    else{
        colorPickerViewController = [[ColorPickerViewController alloc] initWithNibName:@"ColorPickerViewController" bundle:nil];        
    }
    colorPickerViewController.delegate = self;
#ifdef IPHONE_COLOR_PICKER_SAVE_DEFAULT
    colorPickerViewController.defaultsKey = @"SwatchColor";
#else
    // We re-use the current value set to the background of this demonstration view
    colorPickerViewController.defaultsColor = colorSwatch.backgroundColor;
#endif
    [self presentModalViewController:colorPickerViewController animated:YES];
    [colorPickerViewController release];
}



- (void)colorPickerViewController:(ColorPickerViewController *)colorPicker didSelectColor:(UIColor *)color {
    
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:color];
//    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:colorPicker.defaultsKey];
    
	if (colorSelectIndex==1) {
		
		// write the data into the user defaults
		[[NSUserDefaults standardUserDefaults] setObject: colorData forKey: K_TOP_BAR_COLOR];
		
	}
	else if (colorSelectIndex==2) {
		
		// write the data into the user defaults
		[[NSUserDefaults standardUserDefaults] setObject: colorData forKey: K_TOP_TEXT_COLOR];
		
	}
	else if (colorSelectIndex==3) {
		
		// write the data into the user defaults
		[[NSUserDefaults standardUserDefaults] setObject: colorData forKey: K_SET_HEADER_COLOR];
		
	}
	else if (colorSelectIndex==4) {
        
		// write the data into the user defaults
		[[NSUserDefaults standardUserDefaults] setObject: colorData forKey: K_SET_HEADER_TEXT_COLOR];
        
	}
    
    [colorPicker dismissModalViewControllerAnimated:YES];
}

- (void)dealloc {
	
	[lblTextSize release];
	[cancelButton release];
	[doneButton release];
    [super dealloc];
}

/*
 - (id)initWithStyle:(UITableViewStyle)style {
 // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 if (self = [super initWithStyle:style]) {
 }
 return self;
 }
 */

- (void)viewDidLoad {
	
    [super viewDidLoad];
    
    mAppDelegate = (id)[UIApplication sharedApplication].delegate;
    
    //Custom Title	
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
   	
    self.navigationController.navigationBar.translucent = NO;
    
	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor blackColor];
    
	NSString *customFontName = [mAppDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
    //	label.font = [UIFont fontWithName:customFontName size:fontSize];
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];		
	}
	else {
		label.font=[UIFont fontWithName:customFontName size:fontSize];		
	}
	
//	label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;
    label.text = @"Customize Top Bar";
	[label sizeToFit];

    if (IOS_NEWER_THAN(7))
    {
       self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
    label.textColor=[UIColor whiteColor];        
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label.textColor = theColour1;
	}
	// and finally set the colour of your label
	
	
	count=0;
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]==nil) {
        lblTextSize.text = @"18";
    }
    else {
        lblTextSize.text = [[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE];        
    }

	
	NSLog(@"========%@",[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]);
	
	
	self.navigationItem.titleView = label;	
	
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.

	
	//	self.navigationItem.rightBarButtonItem = doneButton;
    
    UIButton *cancelButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton1.frame = CGRectMake(0, 0, 30, 30);
    [cancelButton1 setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
    [cancelButton1 addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    cancelButton1.tag=1;
    
    cancelButton = [[UIBarButtonItem alloc] initWithCustomView:cancelButton1];
    self.navigationItem.leftBarButtonItem = cancelButton;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
}


- (IBAction) buttonAction:(id) sender {
	if ([sender tag]== 1) {
		[self.navigationController popViewControllerAnimated:YES];
	} else if (sender == doneButton) {
		// save settings	
		
		NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
		[userSettings setObject:mAppDelegate.addItemsByGroup forKey:K_SET_ADDITEMSBYGROUP];
		[userSettings setObject:mAppDelegate.welcomePageShow forKey:K_SET_WELCOMEPAGESHOW];
		[userSettings setObject:mAppDelegate.fontType forKey:K_SET_FONT_TYPE];
		[userSettings synchronize];
		[self.navigationController popViewControllerAnimated:YES];
	}
}


- (void)viewWillAppear:(BOOL)animated {

     [super viewWillAppear:animated];
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [layoutManager translateToPortraitForView:self.view withAnimation:NO];
    }
    else
    {
        [layoutManager translateToLandscapeForView:self.view withAnimation:NO];
    }

	NSString *customFontName = [mAppDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
    //	label.font = [UIFont fontWithName:customFontName size:fontSize];
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		lblTopTextPreview.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];		
	}
	else {
		lblTopTextPreview.font=[UIFont fontWithName:customFontName size:fontSize];		
	}
	
//	lblTopTextPreview.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	lblTopTextPreview.textAlignment = UITextAlignmentCenter;
    
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		lblTopTextPreview.textColor = theColour1;
	}
	// and finally set the colour of your label

    
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour21;
	// read the data back from the user defaults
	NSData *data211= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_BAR_COLOR];
	// check whether you got anything
	if(data211 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour21 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data211];
		imgTopBGPreview.backgroundColor = theColour21;
	}
	// and finally set the colour of your label
    
    
    
    
    
    
    
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour31;
	// read the data back from the user defaults
	NSData *data311= [[NSUserDefaults standardUserDefaults] dataForKey: K_SET_HEADER_TEXT_COLOR];
	// check whether you got anything
	if(data311 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour31 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data311];
		lblHeaderTextPreview.textColor = theColour31;
	}
    
 //   lblHeaderTextPreview.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	lblHeaderTextPreview.textAlignment = UITextAlignmentCenter;

	// and finally set the colour of your label

    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour41;
	// read the data back from the user defaults
	NSData *data411= [[NSUserDefaults standardUserDefaults] dataForKey: K_SET_HEADER_COLOR];
	// check whether you got anything
	if(data411 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour41 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data411];
		imgHeaderBGPreview.backgroundColor = theColour41;
	}
	// and finally set the colour of your label

    
}

/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (IBAction) onSettingResetClicked:(id)sender{
    if ([sender tag]==6) {
        NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];

        [userSettings setObject:[NSKeyedArchiver archivedDataWithRootObject: [UIColor lightGrayColor]] forKey:K_TOP_BAR_COLOR];
       
         [userSettings setObject:[NSKeyedArchiver archivedDataWithRootObject: [UIColor whiteColor]] forKey:K_TOP_TEXT_COLOR];
        
        [userSettings setObject:nil forKey:K_SET_TOP_TEXT_SIZE];
        lblTopTextPreview.textColor=[UIColor clearColor];
        imgTopBGPreview.backgroundColor=[UIColor clearColor];
        lblTextSize.text=@"";
    }
    else {
        NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
        [userSettings setObject:[NSKeyedArchiver archivedDataWithRootObject: COLOR_STATUS_HEADER_BG] forKey:K_SET_HEADER_COLOR];
        [userSettings setObject:[NSKeyedArchiver archivedDataWithRootObject: [UIColor whiteColor]] forKey:K_SET_HEADER_TEXT_COLOR];
        lblHeaderTextPreview.textColor=[UIColor clearColor];
        imgHeaderBGPreview.backgroundColor=[UIColor clearColor];
    }
}

- (IBAction) onMinusClicked:(id)sender{
	NSLog(@"NUMBEAlkhf lhaslf a ===== %d",[lblTextSize.text intValue]);
	if ([lblTextSize.text intValue]-1>0) {
		int i=[lblTextSize.text intValue];
		i--;
		lblTextSize.text=[NSString stringWithFormat:@"%d",i];		

        [[NSUserDefaults standardUserDefaults] setObject:lblTextSize.text forKey:K_SET_TOP_TEXT_SIZE];				        
        
        NSString *customFontName = [mAppDelegate getCustomFontName];
		lblTopTextPreview.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];
	}
}

- (IBAction) onPlusClicked:(id)sender{
	if ([lblTextSize.text intValue]<30) {
		int i=[lblTextSize.text intValue];
		i++;
		lblTextSize.text=[NSString stringWithFormat:@"%d",i];			
		[[NSUserDefaults standardUserDefaults] setObject:lblTextSize.text forKey:K_SET_TOP_TEXT_SIZE];		
        
        NSString *customFontName = [mAppDelegate getCustomFontName];
		lblTopTextPreview.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];        
	}
}

- (IBAction) colorButtonPressed:(id)sender{
	if ([selectColorView superview]) {
		[selectColorView removeFromSuperview];
	}
	
	if (colorSelectIndex==1) {
		UIButton *btn=(UIButton *)sender;
		
		UIColor *selectedColor = btn.backgroundColor;
		
		NSData *data = [NSKeyedArchiver archivedDataWithRootObject: selectedColor];	
		
		// write the data into the user defaults
		[[NSUserDefaults standardUserDefaults] setObject: data forKey: K_TOP_BAR_COLOR];
		
	}
	else if (colorSelectIndex==2) {
		UIButton *btn=(UIButton *)sender;
		
		UIColor *selectedColor = btn.backgroundColor;
		
		NSData *data = [NSKeyedArchiver archivedDataWithRootObject: selectedColor];	
		
		// write the data into the user defaults
		[[NSUserDefaults standardUserDefaults] setObject: data forKey: K_TOP_TEXT_COLOR];
		
	}
	else if (colorSelectIndex==3) {
		UIButton *btn=(UIButton *)sender;
		
		UIColor *selectedColor = btn.backgroundColor;
		
		NSData *data = [NSKeyedArchiver archivedDataWithRootObject: selectedColor];	
		
		// write the data into the user defaults
		[[NSUserDefaults standardUserDefaults] setObject: data forKey: K_SET_HEADER_COLOR];
		
	}
	else if (colorSelectIndex==4) {
		UIButton *btn=(UIButton *)sender;
		
		UIColor *selectedColor = btn.backgroundColor;
		
		NSData *data = [NSKeyedArchiver archivedDataWithRootObject: selectedColor];	
		
		// write the data into the user defaults
		[[NSUserDefaults standardUserDefaults] setObject: data forKey: K_SET_HEADER_TEXT_COLOR];
	
	}
	
	
}
//- (IBAction) selectColor:(id)sender{
//	if ([sender tag]==1) {
//		colorSelectIndex=1;
//	}
//	else if ([sender tag]==2) {
//		colorSelectIndex=2;		
//	}
//	else if ([sender tag]==3) {
//		colorSelectIndex=3;		
//	}
//	else if ([sender tag]==4) {
//		colorSelectIndex=4;		
//	}
//	
//	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
//		[UIView beginAnimations:nil context:nil];
//		[selectColorView setFrame:CGRectMake(0, 768, 1024, 768)];
//		[self.view addSubview:selectColorView];
//		
//		[UIView setAnimationDuration:0.75f];
//		[selectColorView setFrame:CGRectMake(0, 0, 768, 1024)];
//		[UIView commitAnimations];	
//	}
//	else {
//		[UIView beginAnimations:nil context:nil];
//		[selectColorView setFrame:CGRectMake(0, 480, 320, 570)];
//		[self.view addSubview:selectColorView];
//		
//		
//		[UIView setAnimationDuration:0.75f];
//		[selectColorView setFrame:CGRectMake(0, 0, 320, 570)];
//		[UIView commitAnimations];	
//	}
//}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        [layoutManager translateToPortraitForView:self.view withAnimation:YES];
    }
    else
    {
        [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
    }
    // }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}


@end
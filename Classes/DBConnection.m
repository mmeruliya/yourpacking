//
//  DBConn.m
//

#import "DBConnection.h"
#import "PackingItem.h"
#import "PackingList.h"
#import "BriefcaseItem.h"
#import "TaskItem.h"
#import "RecordItem.h"

#define DB_NAME		@"baby1.sqlite"
#define DB_NAME_NEW @"baby1.temp.sqlite"

//Change By MS
#define DB_NAME_FAMILY		@"FamilyPacking.sqlite3"

static sqlite3_stmt *check_database_version_statement = nil;
sqlite3 * old_db = nil;
sqlite3 * new_db = nil;

// Private interface for AppDelegate - internal only methods.
@interface DBConnection (Private)
- (void) createEditableCopyOfDatabaseIfNeeded;
- (void) initializeDatabase;
- (void) createEditableCopyOfDatabaseIfNeededFamily;
- (void) initializeDatabaseFamily;
+ (float) getCurrentDatabaseVersion;
+ (float) getNewDatabaseVersion;
+ (void) prepareDatabase;
+ (void) switchDatabase;
+ (void) cleanupDatabase;
@end

@implementation DBConnection
static DBConnection *conn = NULL;

@synthesize database = g_database;

+ (DBConnection *) sharedConnection {
	if (!conn) {
		conn = [[DBConnection alloc] initConnection];
	}
	return conn;
}

- (id) initConnection {
	
	if (self = [super init]) {
		//database = g_database;
		if (g_database == nil) {
			// The application ships with a default database in its bundle. If anything in the application
			// bundle is altered, the code sign will fail. We want the database to be editable by users, 
			// so we need to create a copy of it in the application's Documents directory.     
			[self createEditableCopyOfDatabaseIfNeeded];
			// Call internal method to initialize database connection
			[self initializeDatabase];
            
            [self createEditableCopyOfDatabaseIfNeededFamily];
			// Call internal method to initialize database connection
			//[self initializeDatabaseFamily];
		}
	}
	
	return self;
}

- (void)dealloc {
	[super dealloc];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    // "dehydrate" all data objects - flushes changes back to the database, removes objects from memory
}

// Creates a writable copy of the bundled default database in the application Documents directory.
- (void)createEditableCopyOfDatabaseIfNeeded {
    // First, test for existence.
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:DB_NAME];
    success = [fileManager fileExistsAtPath:writableDBPath];
	if (success) return;
    // The writable database does not exist, so copy the default to the appropriate location.
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DB_NAME];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    if (!success) {
        NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
}

// Open the database connection and retrieve minimal information for all objects.
- (void)initializeDatabase {
    // The database is stored in the application bundle. 
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:DB_NAME];
	NSLog(@"SQLite Root: %s", [path UTF8String]);
	
    // Open the database. The database was prepared outside the application.
    if (sqlite3_open([path UTF8String], &g_database) != SQLITE_OK) {
        // Even though the open failed, call close to properly clean up resources.
        sqlite3_close(g_database);
		g_database = nil;
        NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(g_database));
        // Additional error handling, as appropriate...
    }
		
	//g_database = database;
}

- (void)createEditableCopyOfDatabaseIfNeededFamily {
    // First, test for existence.
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:DB_NAME_FAMILY];
    success = [fileManager fileExistsAtPath:writableDBPath];
	if (success) return;
    // The writable database does not exist, so copy the default to the appropriate location.
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DB_NAME_FAMILY];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    if (!success) {
        NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
}

// Open the database connection and retrieve minimal information for all objects.
- (void)initializeDatabaseFamily {
    // The database is stored in the application bundle.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:DB_NAME_FAMILY];
	NSLog(@"SQLite Root: %s", [path UTF8String]);
	
    // Open the database. The database was prepared outside the application.
    if (sqlite3_open([path UTF8String], &g_database) != SQLITE_OK) {
        // Even though the open failed, call close to properly clean up resources.
        sqlite3_close(g_database);
		g_database = nil;
        NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(g_database));
        // Additional error handling, as appropriate...
    }
    
	//g_database = database;
}

// Save all changes to the database, then close it.
- (void)close {
	
	if (g_database) {
		// Close the database.
		if (sqlite3_close(g_database) != SQLITE_OK) {
			NSAssert1(0, @"Error: failed to close database with message '%s'.", sqlite3_errmsg(g_database));
		}
		g_database = nil;
	}
}

+ (void) prepareDatabase {
	
	NSError *error;
	NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:DB_NAME];
	
	// Open the current database. The database was prepared outside the application.
    if (sqlite3_open([path UTF8String], &old_db) != SQLITE_OK) {
        // Even though the open failed, call close to properly clean up resources.
        sqlite3_close(old_db);
		old_db = nil;
        NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(old_db));
        // Additional error handling, as appropriate...
    }
	
    // The database is stored in the application bundle. 
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:DB_NAME_NEW];
	NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DB_NAME];
    // Remove the existed temp database file
	if ([fileManager fileExistsAtPath:writableDBPath]) {
		[fileManager removeItemAtPath:writableDBPath error:&error];
    }
	
	NSLog(@"Temp database: %@", writableDBPath);
	// Copy the new database file 
	if ([fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error] == NO) {
		NSAssert1(0, @"Failed to copy data with error message '%@'.", [error localizedDescription]);
	}
	
    // Open the database. The database was prepared outside the application.
    if (sqlite3_open([writableDBPath UTF8String], &new_db) != SQLITE_OK) {
        // Even though the open failed, call close to properly clean up resources.
        sqlite3_close(new_db);
		new_db = nil;
        NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(new_db));
        // Additional error handling, as appropriate...
    }
}

// Get current database version number
// -1 if no database file exists
// 0 if no 'version' table exists, this table was added since APP_VERSION_1_21
+ (float) getCurrentDatabaseVersion {
	
	float version = 0.0;
	BOOL success;
	NSFileManager *fileManager = [NSFileManager defaultManager];
    // The database is stored in the application bundle. 
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:DB_NAME_NEW];
	
	success = [fileManager fileExistsAtPath:path];
	if (!success || !old_db) {
		version = -1;
		return version;
	}
	
	if (check_database_version_statement == nil) {
		// Note the '?' at the end of the query. This is a parameter which can be replaced by a bound variable.
		// This is a great way to optimize because frequently used queries can be compiled once, then with each
		// use new variable values can be bound to placeholders.
		const char *sql = "SELECT id,version FROM version WHERE type = 'database' LIMIT 1 "; 
		
		if (sqlite3_prepare_v2(old_db, sql, -1, &check_database_version_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(old_db));
			sqlite3_finalize(check_database_version_statement);
			check_database_version_statement = nil;
			return version;
		}
	}
	
	if (sqlite3_step(check_database_version_statement) == SQLITE_ROW) {
		int field = 1;
		NSString* db_version = [NSString stringWithUTF8String:(char *)sqlite3_column_text(check_database_version_statement, field++)];
		version = [db_version floatValue];
	}
	sqlite3_finalize(check_database_version_statement);
	check_database_version_statement = nil;

	return version;
}

// Get new database version number
// 0 if no 'version' table exists, this table was added since APP_VERSION_1_21
+ (float) getNewDatabaseVersion {
	
	float version = 0.0;
	BOOL success;
	NSFileManager *fileManager = [NSFileManager defaultManager];
    // The database is stored in the application bundle. 
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:DB_NAME];
	
	success = [fileManager fileExistsAtPath:path];
	if (!success || !new_db) {
		version = -1;
		return version;
	}
	
	if (check_database_version_statement == nil) {
		// Note the '?' at the end of the query. This is a parameter which can be replaced by a bound variable.
		// This is a great way to optimize because frequently used queries can be compiled once, then with each
		// use new variable values can be bound to placeholders.
		const char *sql = "SELECT id,version FROM version WHERE type = 'database' LIMIT 1 "; 
		
		if (sqlite3_prepare_v2(new_db, sql, -1, &check_database_version_statement, NULL) != SQLITE_OK) {
			NSLog(@"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(new_db));
			sqlite3_finalize(check_database_version_statement);
			check_database_version_statement = nil;
			return version;
		}
	}
	
	if (sqlite3_step(check_database_version_statement) == SQLITE_ROW) {
		int field = 1;
		NSString* db_version = [NSString stringWithUTF8String:(char *)sqlite3_column_text(check_database_version_statement, field++)];
		version = [db_version floatValue];
	}
	sqlite3_finalize(check_database_version_statement);
	check_database_version_statement = nil;
	
	return version;
}

+ (void) switchDatabase {
	
	sqlite3_close(old_db);
	sqlite3_close(new_db);
	
	BOOL success;
	NSError *error;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:DB_NAME];
	
	// check if the target database file exists
	success = [fileManager fileExistsAtPath:writableDBPath];
	if (success) {
		// remove the target old database file
		success = [fileManager removeItemAtPath:writableDBPath error:&error];
		if (!success) {
			NSLog(@"Failed to remove the older version database file with message '%@'.", [error localizedDescription]);
		}
	}
    // copy the temp database file to the appropriate location.
    NSString *defaultDBPath = [documentsDirectory stringByAppendingPathComponent:DB_NAME_NEW];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    if (!success) {
        NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
	// remove temp database file
	success = [fileManager removeItemAtPath:defaultDBPath error:&error];
	if (!success) {
		NSLog(@"Failed to remove the older version database file with message '%@'.", [error localizedDescription]);
	}
}

+ (void) cleanupDatabase {
	
	sqlite3_close(old_db);
	sqlite3_close(new_db);

	BOOL success;
	NSError *error;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *defaultDBPath = [documentsDirectory stringByAppendingPathComponent:DB_NAME_NEW];
	
	// remove temp database file
	success = [fileManager removeItemAtPath:defaultDBPath error:&error];
	if (!success) {
		NSLog(@"Failed to remove the older version database file with message '%@'.", [error localizedDescription]);
	}
}

+ (BOOL) upgradeDatabaseIfNeeded {
	[DBConnection prepareDatabase];
	float current_version = [DBConnection getCurrentDatabaseVersion];
	float new_version = [DBConnection getNewDatabaseVersion];
	if (current_version >= 0 && current_version < new_version) {
		// Perform database upgrading
		NSLog(@"Database upgrading from %.04f to %.04f", current_version, new_version);
		[PackingItem upgradeTableFromDatabase:old_db ToDatabase:new_db];
		[PackingList upgradeTableFromDatabase:old_db ToDatabase:new_db];
		[BriefcaseItem upgradeTableFromDatabase:old_db ToDatabase:new_db];
		[TaskItem upgradeTableFromDatabase:old_db ToDatabase:new_db];
		[RecordItem upgradeTableFromDatabase:old_db ToDatabase:new_db];
		[DBConnection switchDatabase];
	} else {
		[DBConnection cleanupDatabase];
	}
	
	return YES;
}

@end


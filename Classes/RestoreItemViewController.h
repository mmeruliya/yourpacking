//
//  RestoreItemViewController.h
//  BabyPacking
//
//  Created by Hitesh on 12/17/10.
//  Copyright 2010 cmc limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BabyPackingAppDelegate;

@interface RestoreItemViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
{
	BabyPackingAppDelegate *appDelegate;
	
	UITableView *tableView1;
	
	int index;
}

@property (nonatomic, retain) IBOutlet UITableView *tableView1;

- (IBAction) onDoneclicked:(id)sender;

@end

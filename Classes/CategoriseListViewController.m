//
//  CategoriseListViewController.m
//  BabyPacking
//
//  Created by eve on 9/16/09.
//  Copyright 2009 test. All rights reserved.
//

#import "CategoriseListViewController.h"
#import "BabyPackingAppDelegate.h"
#import "CheckListViewController.h"
#import "SelectItemViewController.h"
#import "Constant.h"
#import "PackingItem.h"
#import "PackingList.h"
#import "CSTableViewCell.h"
#import "AddCategoryViewController.h"
#import "Categories.h"
#import "Items.h"
#import "DataManager.h"
#import "RestoreCategoryViewController.h"

@interface CategoriseListViewController(private)

-(PackingList*) ItemAtIndexpath: (NSIndexPath*)indexpath;
- (void) loadCategoryListFromAppData;
//- (void) saveCategoryListToAppData;
//- (void) saveSelectedItemsToDB;

@end

@implementation CategoriseListViewController

@synthesize selectedListArray,currentList;
@synthesize categoryListArray;
@synthesize homeController;
@synthesize predefineListArry;
@synthesize listNum;
@synthesize categoryListInAppData;
@synthesize editList;
@synthesize isEdit;

//@synthesize itemArray1,itemArray2,itemArray3,itemArray4,itemArray5,itemArray6,itemArray7,itemArray8;
//@synthesize itemArray9,itemArray10,itemArray12,itemArray13,itemArray14,itemArray15,itemArray16;


 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
        layoutManager = [[LayoutManager alloc] initWithPortraitView:self.view andLandscapeNIB:@"CategoriseListViewController_L"];
    }
    return self;
}*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];
	BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;	
    
    if (IOS_NEWER_THAN(7))
    {
      // self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    if (!IOS_NEWER_THAN(7))
    {
        myTableView.frame = CGRectMake(myTableView.frame.origin.x, 0, myTableView.frame.size.width, myTableView.frame.size.height);
    }
    
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor whiteColor];
	NSString *customFontName = [appDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];		
	}
	else {
		label.font=[UIFont fontWithName:customFontName size:fontSize];		
	}
	
	[label sizeToFit];
	
	//label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
            label.textColor=[UIColor whiteColor];
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		label.textColor = theColour1;
	}
	// and finally set the colour of your label
	
	self.navigationItem.titleView = label;	
	label.text = TITLE_CATEGORY_LIST;
	[label sizeToFit];
	
    
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour2;
	// read the data back from the user defaults
	NSData *data12= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
	// check whether you got anything
	if(data12 == nil) {
		// use this to set the colour the first time your app runs
        myTableView.backgroundColor = [UIColor whiteColor];
        editTableView.backgroundColor = [UIColor whiteColor];
        
	} else {
		// this recreates the colour you saved
		theColour2 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data12];
		myTableView.backgroundColor = theColour2;
        editTableView.backgroundColor = theColour2;        
	}
	// and finally set the colour of your label


	bgImageView.alpha = BG_PHOTO_ALPHA;
	
	if (currentList) {

		self.categoryListInAppData = [appDelegate.appDict objectForKey:K_CATEGORY_LIST];
		self.predefineListArry = [NSMutableArray arrayWithCapacity:0];
		self.listNum = [NSMutableArray arrayWithCapacity:0];
		if(currentList.age == Infant_baby)
			self.predefineListArry = [NSArray arrayWithObjects:@"11", nil];
		else if(currentList.age == Child){
			if(currentList.sex == BabySexBoy)
				self.predefineListArry =  [NSArray arrayWithObjects:@"3",@"19", @"4", @"5", @"6", @"7",@"8", @"9", @"10", @"12", @"14", @"15",nil];
			else
				self.predefineListArry =  [NSArray arrayWithObjects:@"18",@"19", @"4", @"5", @"6", @"7",@"8", @"9", @"10", @"12", @"14", @"15",nil];
		}
		else if(currentList.age == Adult){
			if (TARGET_VERSION == VERSION_FAMILY_PACKING){
				if(currentList.sex == BabySexBoy)
					self.predefineListArry = [NSArray arrayWithObjects:@"1",@"2",@"3",@"19", @"4", @"5", @"6", @"7",@"8", @"9", @"10", @"14", @"15", @"16",@"17", nil];
				else
					self.predefineListArry = [NSArray arrayWithObjects:@"1",@"2",@"18",@"19", @"4", @"5", @"6", @"7",@"8", @"9", @"10", @"14", @"15", @"16",@"17", nil];
			}
			else if (TARGET_VERSION == VERSION_YOU_PACKING){
				if(currentList.sex == BabySexBoy)
					self.predefineListArry = [NSArray arrayWithObjects:@"1",@"2",@"3",@"19", @"4", @"5", @"6", @"7",@"8", @"9",nil];
				else
					self.predefineListArry = [NSArray arrayWithObjects:@"1",@"2",@"18",@"19", @"4", @"5", @"6", @"7",@"8", @"9",nil];
			}
		}
		else if(currentList.age == Pet)
			self.predefineListArry = [NSArray arrayWithObjects:@"13",nil];
		else if(currentList.age == ShoppingList)
			self.predefineListArry = [NSArray arrayWithObjects:@"3",@"18",@"19", @"4", @"5", @"6", @"7",@"8", @"9", @"10",@"11",@"12",@"13", @"14", @"15", @"16",@"17", nil];
		else if(currentList.age == Other){
			if (TARGET_VERSION == VERSION_YOU_PACKING)
				self.predefineListArry = [NSArray arrayWithObjects:@"4",@"2",@"19", @"3", @"18", @"7", @"6",@"9", @"8", @"1", @"5", nil];
			else
				self.predefineListArry = [NSArray arrayWithObjects:@"1",@"2", @"3",@"18",@"19", @"4", @"5", @"6", @"7",@"8", @"9", @"10",@"11",@"12",@"13", @"14", @"15", @"16",@"17", nil];
		}
			if (appDelegate.colorBgOn) {
			bgImageView.alpha = 1.0;
			if(currentList.sex == BabySexBoy) {
				if (TARGET_VERSION == VERSION_BABY_PACKING)
					bgImageView.image = [UIImage imageNamed:BLUE_BEAR];
				else
					bgImageView.image = [UIImage imageNamed:BACKGROUND_BLUE];
			} else if(currentList.sex == BabySexGirl) {
				if (TARGET_VERSION == VERSION_BABY_PACKING)
					bgImageView.image = [UIImage imageNamed:PINK_BEAR];
				else
					bgImageView.image = [UIImage imageNamed:BACKGROUND_PINK];
			} else {
				if (TARGET_VERSION == VERSION_BABY_PACKING)
					bgImageView.image = [UIImage imageNamed:GRAY_BEAR];
				else
					bgImageView.image = [UIImage imageNamed:BACKGROUND_GRAY];
			}
		}
	}
    
    /*
     
     UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
     btn2.frame = CGRectMake(0, 0, 30, 30);
     btn2.tag=1;
     [btn2 setImage:[UIImage imageNamed:@"Done.png"] forState:UIControlStateNormal];
     [btn2 addTarget:self action:@selector(buttonDoneDidClick:) forControlEvents:UIControlEventTouchUpInside];
     
     mBtnDone = [[UIBarButtonItem alloc] initWithCustomView:btn2];
     
     */
	
	//myTableView.backgroundColor = BOY_BG_COLOR;
	checkImg = [[UIImage imageNamed:CHECK_IMAGE] retain];
	uncheckImg = [[UIImage imageNamed:UNCHECK_IMAGE] retain];
	addImg = [[UIImage imageNamed:ADD_IMAGE] retain];
	removeImg = [[UIImage imageNamed:REMOVE_IMAGE] retain];
	selectImg = [[UIImage imageNamed:@"select.png"] retain];
	
	[self loadCategoryListFromAppData];
	//    self.navigationItem.rightBarButtonItem = resetButton;
    
    UIButton *Editbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    Editbtn.frame = CGRectMake(0, 0, 30, 30);
    Editbtn.tag=2;
    [Editbtn setImage:[UIImage imageNamed:@"Done.png"] forState:UIControlStateNormal];
    // [Editbtn setBackgroundColor:[UIColor blackColor]];
    [Editbtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    doneButton=[[UIBarButtonItem alloc] initWithCustomView:Editbtn];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 30, 30);
    btn.tag=1;
    [btn setImage:[UIImage imageNamed:@"AddTop.png"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    addButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
	
    
	self.navigationItem.leftBarButtonItem = doneButton;
	self.navigationItem.rightBarButtonItem = addButton;
	
	for(int i = 0; i< [self.selectedListArray count]; i++){
		[listNum addObject:[NSNumber numberWithInt:0]];
		[listNum addObject:@"hide"];
	}
 // Split items into groups
//	BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
		for (PackingItem *item in currentList.itemArray) {
			NSString *from = item.custom;
			if (!from || [@"" isEqualToString:from]) {
				// the data may be from version older than 1.3.1
				// search the group from categories
				NSArray* categoryListInAppDataArray = [appDelegate.appDict objectForKey:K_CATEGORY_LIST];
				BOOL found = NO;
				for (NSDictionary* category in categoryListInAppDataArray) {
					NSArray* items = [category objectForKey:K_LIST_ITEMS];
					for (NSDictionary* each in items) {
						NSString* name = [each objectForKey:K_ITEM_NAME];
						if ([name isEqualToString:item.name]) {
							found = YES;
							item.custom = [category objectForKey:K_ITEM_NAME];
							break;
						}
					}
					
					if (found) break;
				}
				if (!found) {
					item.custom = @"FromCustom";
				}
			}
		}
	
	
//	editTableView.backgroundColor = [UIColor clearColor];
//	myTableView.backgroundColor = [UIColor clearColor];
    
    [sortBtn setImage:[UIImage imageNamed:@"btn-sort-down.png"] forState:UIControlStateNormal];
    
	editTableView.hidden = YES;
	allowDelete = NO;
	index = 0;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
}

- (void) getItemListForAllCategories
{
	BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
	
	if (appDelegate.allItemsFromDB)
	{
		[appDelegate.allItemsFromDB removeAllObjects];
		[appDelegate.allItemsFromDB release];
		appDelegate.allItemsFromDB = nil;
	}
	
	appDelegate.allItemsFromDB = [[NSMutableArray alloc] init];
	
	for (int i = 0; i < selectedListArray.count; i++)
	{
		PackingList *category = [selectedListArray objectAtIndex:i];
		[appDelegate.dataManager getItemByCategoty:category.pk];
		int y = 0;
		for (int k = 0; k < appDelegate.itemsFromDB.count; k++)
		{
			Items *itemDB = [appDelegate.itemsFromDB objectAtIndex:k];
			y++;
			PackingItem *item = [[PackingItem alloc] init];
			item.pk = itemDB.pk;
			item.name = itemDB.name;
			item.tip = itemDB.tip;
			item.qty = itemDB.qty;
			item.custom = itemDB.custom;//[NSString stringWithFormat:@"FromCustom"];
			item.predefineId = k * 1000 + y;
			item.predefineForAge = currentList.age;
			//item.category = itemDB.custom;
			
			[appDelegate.allItemsFromDB addObject:item];
			[item release];
		}
	}
	
	//NSLog(@"item count -- %d",appDelegate.allItemsFromDB.count);
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [layoutManager translateToPortraitForView:self.view withAnimation:NO];
       
        
    }
    else
    {
        [layoutManager translateToLandscapeForView:self.view withAnimation:NO];
       
    }

  	//[myTableView reloadData];
	[self.categoryListArray removeAllObjects];
	[self.selectedListArray removeAllObjects];
	[self loadCategoryListFromAppData];
	[self.listNum removeAllObjects];
	[self reorderListByAsc];
	for(int i = 0; i< [self.selectedListArray count]; i++){
		[listNum addObject:[NSNumber numberWithInt:0]];
		[listNum addObject:@"hide"];
	}
	
	if (currentList.itemArray.count > 0)
		[currentList.itemArray removeAllObjects];
	
	[currentList loadItemsIfNeed];
	for (int i = 0; i< [selectedListArray count]; i++){
		//NSInteger numInList = [[self.predefineListArry objectAtIndex:i] intValue];
		PackingList *list = [self.selectedListArray objectAtIndex:i];
		NSInteger j = 0;
		
		for(PackingItem *item in currentList.itemArray){
			if([item.custom isEqualToString:list.name]){
				
				j++;
			}
			
		}
		[self.listNum replaceObjectAtIndex:i*2 withObject:[NSNumber numberWithInt:j]];
	}
	
	[myTableView reloadData];
    /*NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(getItemListForAllCategories) object:nil];
	[queue addOperation:operation];
	[operation release];*/
	
	[self getItemListForAllCategories];
}
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void) loadData
{
	//[self.categoryListArray removeAllObjects];
	//[self.selectedListArray removeAllObjects];
	//[self loadCategoryListFromAppData];
	[self.listNum removeAllObjects];
	//[self reorderListByAsc];
	for(int i = 0; i< [self.selectedListArray count]; i++){
		[listNum addObject:[NSNumber numberWithInt:0]];
		[listNum addObject:@"hide"];
	}
	
	if (currentList.itemArray.count > 0)
		[currentList.itemArray removeAllObjects];
	
	[currentList loadItemsIfNeed];
	for (int i = 0; i< [selectedListArray count]; i++){
		//NSInteger numInList = [[self.predefineListArry objectAtIndex:i] intValue];
		PackingList *list = [self.selectedListArray objectAtIndex:i];
		NSInteger j = 0;
		
		for(PackingItem *item in currentList.itemArray){
			if([item.custom isEqualToString:list.name]){
				
				j++;
			}
			
		}
		[self.listNum replaceObjectAtIndex:i*2 withObject:[NSNumber numberWithInt:j]];
		
	}
	
	[myTableView reloadData];
	[editTableView reloadData];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
	//	[self saveCategoryListToAppData];
	
}

- (void)dealloc {
	
	[bgImageView release];
	//[bgBorderView release];
	[myTableView release];
	[editTableView release];
	[currentList release];
	[doneButton release];
	[resetButton release];
	[addButton release];
	[checkImg release];
	[uncheckImg release];
	[addImg release];
	[removeImg release];
	[selectImg release];
	[selectedListArray release];
	[categoryListArray release];
	[categoryListInAppData release];
	[listNum release];
	
	[delBtn release];
	[sortBtn release];
	[restoreBtn release];
	[searchButton release];
	
	[super dealloc];
	
}

- (void) loadCategoryListFromAppData {
	
	BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
    
	NSMutableDictionary *appDict = appDelegate.appDict;
	NSMutableArray *categoryListsInAppData = [appDict objectForKey:K_CATEGORY_LIST];
    
	self.categoryListArray = [NSMutableArray arrayWithCapacity:0];
	int i = 0;
	/*for(NSMutableDictionary *listDict in categoryListsInAppData) {
		
		i++;
		//create list
		NSLog(@"list name -- %@",[listDict objectForKey:K_LIST_NAME]);
		PackingList *tempList = [[PackingList alloc] init];
		tempList.name = [listDict objectForKey:K_LIST_NAME];
		tempList.sex = [[listDict objectForKey:K_LIST_SEX] intValue];
		tempList.age = [[listDict objectForKey:@"num"] intValue];
		tempList.update = [listDict objectForKey:K_LIST_UPDATE];
		tempList.totalCount = [[listDict objectForKey:K_LIST_TOTAL] intValue];
		tempList.checkedCount = [[listDict objectForKey:K_LIST_CHECKED] intValue];
		tempList.predefineId = i * 1000;
		NSMutableArray *items = [listDict objectForKey:K_LIST_ITEMS];
		//add item to list
		int j = 0;
		for(NSMutableDictionary *itemDict in items) {
			j++;
			PackingItem *tempItem = [[PackingItem alloc] init];
			tempItem.name = [itemDict objectForKey:K_ITEM_NAME];
			tempItem.tip = [itemDict objectForKey:K_ITEM_TIP];
			tempItem.qty = [itemDict objectForKey:@"qty"];
			tempItem.custom = [itemDict objectForKey:@"custom"];
			tempItem.checked = [[itemDict objectForKey:K_ITEM_CHECKED] boolValue];
			tempItem.predefineId = i * 1000 + j;
			//			tempItem.predefineForAge = tempList.predefineForAge;
			[tempList.itemArray addObject:tempItem];
			
			[tempItem release];
		}
		
		[categoryListArray addObject:tempList];
		
		[tempList release];
	}*/
	
	self.selectedListArray = [NSMutableArray arrayWithCapacity:0];
	
	[appDelegate.dataManager getCategoriesByList:currentList.pk];
	
    NSLog(@"FromCustom %d ",self.predefineListArry.count);
	for (int p = 0; p < self.predefineListArry.count; p++)
	{
		int index1 = [[self.predefineListArry objectAtIndex:p] intValue];
		if ((index1-1) < appDelegate.categoryFromDB.count)
		{
			Categories *categoryDB = [appDelegate.categoryFromDB objectAtIndex:(index1-1)];
			if (categoryDB.type == 0)
			{
				i++;
				PackingList *tempList = [[PackingList alloc] init];
				tempList.pk	= categoryDB.pk;
				tempList.name = categoryDB.name;
				tempList.custom = [NSString stringWithFormat:@"FromCustom"];
				tempList.predefineId = i * 1000;
				
				BOOL flag = NO;
				for (int x = 0; x < self.selectedListArray.count; x++)
				{
					PackingList *list1 = [self.selectedListArray objectAtIndex:x];
					if ([list1.name isEqualToString:categoryDB.name])
					{
						flag = YES;
					}
				}
				
				if (!flag)
				{
					[appDelegate.dataManager getItemByCategoty:tempList.pk];
					int y = 0;
					for (int k = 0; k < appDelegate.itemsFromDB.count; k++)
					{
						Items *itemDB = [appDelegate.itemsFromDB objectAtIndex:k];
						y++;
						PackingItem *item = [[PackingItem alloc] init];
						item.pk = itemDB.pk;
						item.name = itemDB.name;
						item.tip = itemDB.tip;
						item.qty = itemDB.qty;
						item.custom = itemDB.custom;//[NSString stringWithFormat:@"FromCustom"];
						item.predefineId = k * 1000 + y;
						item.predefineForAge = currentList.age;
						//item.category = itemDB.custom;
												
						[tempList.itemArray addObject:item];
						[item release];
					}
					
					[selectedListArray addObject:tempList];
					[tempList release];
				}
			}
		}
		//PackingList *list = [self.categoryListArray objectAtIndex:index-1];
		//[self.selectedListArray addObject:list];
	}
	
	for (int j = 0; j < appDelegate.categoryFromDB.count; j++)
	{
		Categories *categoryDB = [appDelegate.categoryFromDB objectAtIndex:j];
		
		if (categoryDB.type == 1)
		{
			i++;
			PackingList *tempList = [[PackingList alloc] init];
			tempList.pk	= categoryDB.pk;
			tempList.name = categoryDB.name;
			tempList.custom = [NSString stringWithFormat:@"FromCustom"];
			tempList.predefineId = i * 1000;
			
			BOOL flag = NO;
			for (int x = 0; x < self.selectedListArray.count; x++)
			{
				PackingList *list1 = [self.selectedListArray objectAtIndex:x];
				if ([list1.name isEqualToString:categoryDB.name])
				{
					flag = YES;
				}
			}
			
			if (!flag)
			{
				[appDelegate.dataManager getItemByCategoty:tempList.pk];
				int y = 0;
				for (int k = 0; k < appDelegate.itemsFromDB.count; k++)
				{
					Items *itemDB = [appDelegate.itemsFromDB objectAtIndex:k];
					y++;
					PackingItem *item = [[PackingItem alloc] init];
					item.pk = itemDB.pk;
					item.name = itemDB.name;
					item.tip = itemDB.tip;
					item.qty = itemDB.qty;
					item.custom = itemDB.custom;//[NSString stringWithFormat:@"FromCustom"];
					item.predefineId = k * 1000 + y;
					item.predefineForAge = currentList.age;
					//item.category = itemDB.custom;
					
					[tempList.itemArray addObject:item];
					[item release];
				}
				
				[selectedListArray addObject:tempList];
				[tempList release];
			}
		}
	}
}

#pragma mark Table view methods

-(PackingList*) ItemAtIndexpath: (NSIndexPath*)indexpath
{
	PackingList *item = nil;
	item = [self.selectedListArray objectAtIndex:indexpath.row];
	return item;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	//return 1;
	//[self reorderListByAsc];
	if (tableView == myTableView)
		return [self.selectedListArray count];
	else 
		return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	//return [self.predefineListArry count];
	//BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
	if (tableView == myTableView)
	{
		if([[self.listNum objectAtIndex:section*2+1] isEqualToString:@"hide"])
			return 1;
		else{
			int i = 0;
			//NSInteger numInList = [[self.predefineListArry objectAtIndex:section] intValue];
			PackingList *list = [self.selectedListArray objectAtIndex:section];
			for(PackingItem *item in currentList.itemArray){
				if([item.custom isEqualToString:list.name]){
					
					i++;
				}
				
			}
			return i+1;
		}
	}
	else 
		return [self.selectedListArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (tableView == myTableView)
	{
		static NSString *CellIdentifier = @"CSTableViewCell";
		
		CSTableViewCell *cell = (CSTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			cell = [[[CSTableViewCell alloc] initWithCSStyle:CSTableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
			cell.detailTextLabel.textColor = [UIColor darkGrayColor];
			cell.detailTextLabel.textAlignment = UITextAlignmentLeft;
		}
		
        //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
        UIColor *theColour1;
        // read the data back from the user defaults
        NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
        // check whether you got anything
        if(data11 == nil) {
            // use this to set the colour the first time your app runs
        } else {
            // this recreates the colour you saved
            theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
            cell.backgroundColor = theColour1;
        }
        // and finally set the colour of your label
        
        
        //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
        UIColor *theColour21;
        // read the data back from the user defaults
        NSData *data211= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
        // check whether you got anything
        if(data211 == nil) {
            // use this to set the colour the first time your app runs
        } else {
            // this recreates the colour you saved
            theColour21 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data211];
            cell.textLabel.textColor = theColour21;
        }
        // and finally set the colour of your label
        BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
        NSString *customFontName1 = [appDelegate getCustomFontName];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
            cell.textLabel.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];
        }
        else{
            cell.textLabel.font=[UIFont fontWithName:customFontName1 size:15];
        }
        
		//NSInteger numInList = [[self.predefineListArry objectAtIndex:indexPath.section] intValue];
		PackingList *list = [self.selectedListArray objectAtIndex:indexPath.section];
		if(indexPath.row == 0){
			//NSLog(@"item   predefineId = %d", item.predefineId);
			cell.textLabel.text = list.name;
			cell.accessoryType = UITableViewCellAccessoryDetailButton;
          //  cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure.png"]];
			if([currentList.itemArray count] == 0) {
				
				cell.image = addImg;
				cell.detailTextLabel.text = nil;
			} else {
				
				[currentList loadItemsIfNeed];
				NSInteger i = [[listNum objectAtIndex:((indexPath.section)*2)] intValue];
				if (i > 0){
					cell.image = removeImg;
					cell.detailTextLabel.text = [NSString stringWithFormat:@"%d",i];
				}
				
				else{
					cell.image = addImg;
					cell.detailTextLabel.text = nil;
				}
			}		
			
			return cell;
		}
		
		else{
			int i = -1;
			PackingItem *item = nil;
			for(PackingItem *item1 in currentList.itemArray){
				if([item1.custom isEqualToString:list.name]){
					i++;
					if(i == indexPath.row-1){
						item = item1;
						break;
					}
				}
			}
			
			cell.textLabel.text = item.name;
			if (currentList.age < 2 && currentList.sex == 0){
				if([item hasTip])
					cell.detailTextLabel.text = @"*";
				else
					cell.detailTextLabel.text = nil;
			}
			else{
				if ([item hasTip]){
					if (item.qty && [item.qty length] > 0)
                      //  cell.detailTextLabel.text = [NSString stringWithFormat:@"*x%@",item.qty];

						cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",item.qty];
					else
						cell.detailTextLabel.text = @"*";
				}
				
				else if( item.qty && [item.qty length] > 0)
                    //cell.detailTextLabel.text = [NSString stringWithFormat:@"*x%@",item.qty];
					cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", item.qty];
				else
					cell.detailTextLabel.text = nil;
			}
			cell.image = selectImg;
            
			//		if (item.selected) {
			//			cell.image = checkImg;
			//		} else {
			//			cell.image = uncheckImg;
			//		}
			cell.accessoryType = UITableViewCellAccessoryNone;
			return cell;
		}
	}
	else 
	{
		static NSString *CellIdentifier = @"CSTableViewCell1";
		
		UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		
		if (cell == nil)
		{
			cell = [[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier];
		}
		
        //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
        UIColor *theColour1;
        // read the data back from the user defaults
        NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
        // check whether you got anything
        if(data11 == nil) {
            // use this to set the colour the first time your app runs
        } else {
            // this recreates the colour you saved
            theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
            cell.backgroundColor = theColour1;
        }
        // and finally set the colour of your label
        
        
        //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
        UIColor *theColour21;
        // read the data back from the user defaults
        NSData *data211= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
        // check whether you got anything
        if(data211 == nil) {
            // use this to set the colour the first time your app runs
        } else {
            // this recreates the colour you saved
            theColour21 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data211];
            cell.textLabel.textColor = theColour21;
        }
        // and finally set the colour of your label
        BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
        NSString *customFontName1 = [appDelegate getCustomFontName];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
            cell.textLabel.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];
        }
        else{
            cell.textLabel.font=[UIFont fontWithName:customFontName1 size:15];
        }
        
		PackingList *list = [self.selectedListArray objectAtIndex:indexPath.row];
		cell.textLabel.text = list.name;
		
#ifdef __IPHONE_3_0
		if (editTableView.editing) {
			cell.editingAccessoryType = UITableViewCellAccessoryDetailButton;
		} else {
			cell.editingAccessoryType = UITableViewCellAccessoryNone;
		}
		
#else
		if (editTableView.editing) {
			cell.accessoryType = UITableViewCellAccessoryDetailButton;
            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure.png"]];
		} else {
			cell.accessoryType = UITableViewCellAccessoryNone;
		}
#endif
		return cell;
	}
	
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (tableView == editTableView)
		return YES;
	
	return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {	
	
	if (tableView == editTableView)
	{
        NSLog(@"EDIT TABLE VIEW");
		if (editTableView.editing)
		{
        NSLog(@"EDITING");            
			AddCategoryViewController *controller = nil;
			
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				controller = [[AddCategoryViewController alloc] initWithNibName:@"AddCategoryViewController" bundle:nil];
			else
				controller = [[AddCategoryViewController alloc] initWithNibName:@"AddCategoryViewController1" bundle:nil];
			
			//[[AddCategoryViewController alloc] initWithNibName:@"AddCategoryViewController" bundle:nil];
			controller.currentList = self.currentList;
			controller.editingCategory = [self.selectedListArray objectAtIndex:indexPath.row];
			controller.categoryListArray = self.selectedListArray;
			[self.navigationController pushViewController:controller animated:YES];
			[controller release];
		}		
	}
	else 
	{
                NSLog(@"ELSE EDIT TABLE VIEW");
		if(indexPath.row == 0){
			
			NSInteger num = [[self.listNum objectAtIndex:indexPath.section*2] intValue];
			if (num <= 0) {
                NSLog(@"<=0");
				[self tableView:tableView accessoryButtonTappedForRowWithIndexPath:indexPath];
			} else {
                NSLog(@"ELSE <=0");                
				if([[self.listNum objectAtIndex:indexPath.section*2+1] isEqualToString: @"rows"]){
                    NSLog(@"EQUAL TO STRING ");                                    
					[self.listNum removeObjectAtIndex:indexPath.section*2+1];
					[self.listNum insertObject:@"hide" atIndex:indexPath.section*2+1];				
				}
				else{
                    NSLog(@"ELSE EQUAL TO STRING ");                                                        
					[self.listNum removeObjectAtIndex:indexPath.section*2+1];
					[self.listNum insertObject:@"rows" atIndex:indexPath.section*2+1];
				}
				[tableView reloadData];
			}			
		}
	}	
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
	
	if (tableView.editing && tableView == editTableView)
	{
        NSLog(@"EDITING");
        
		AddCategoryViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[AddCategoryViewController alloc] initWithNibName:@"AddCategoryViewController" bundle:nil];
		else
			controller = [[AddCategoryViewController alloc] initWithNibName:@"AddCategoryViewController1" bundle:nil];
		
		//[[AddCategoryViewController alloc] initWithNibName:@"AddCategoryViewController" bundle:nil];

        UITableViewCell *cell=[tableView cellForRowAtIndexPath:indexPath];
        controller.strCategoryName=cell.textLabel.text;
		controller.currentList = self.currentList;
		controller.editingCategory = [self.selectedListArray objectAtIndex:indexPath.row];
		controller.categoryListArray = self.selectedListArray;
		[self.navigationController pushViewController:controller animated:YES];
		[controller release];
	}
	else 
	{

		if(indexPath.row == 0){
                NSLog(@"IF EDITING == 0");            
			SelectItemViewController *controller = nil;
			
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				controller = [[SelectItemViewController alloc] initWithNibName:@"SelectItemViewController" bundle:nil];
			else
				controller = [[SelectItemViewController alloc] initWithNibName:@"SelectItemViewController1" bundle:nil];
			
			           //[[SelectItemViewController alloc] initWithNibName:@"SelectItemViewController" bundle:nil];
			controller.homeController = self.homeController;
			           //NSInteger numInList = [[self.predefineListArry objectAtIndex:indexPath.section] intValue];
			PackingList *list = [self.selectedListArray objectAtIndex:indexPath.section];
			[currentList loadItemsIfNeed];
			controller.currentList = self.currentList;
			controller.predefinedItems = list.itemArray;
			controller.isFromCategory = YES;
			controller.editList = self.editList;
//New 26052012
            if (TARGET_VERSION == VERSION_BABY_PACKING){
                NSMutableDictionary *categoryInAppData = [self.categoryListInAppData objectAtIndex:indexPath.section];                
                controller.categoryListInAppData = categoryInAppData;
            }
//Removes crash issue            
			CSTableViewCell *cell = (CSTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
			controller.category = cell.textLabel.text;
			controller.currentCategory = list;
			[self.navigationController pushViewController:controller animated:YES];
			[controller release];
			for(PackingItem *item in list.itemArray ){
				if([currentList.itemArray containsObject:item])
					[currentList.itemArray removeObject:item];
				
			}
		}
        else{
                NSLog(@"ELSE EDITING ==0");			
		}
	}	
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (tableView == editTableView)
	{
		if (editingStyle == UITableViewCellEditingStyleDelete) {
			
			index = indexPath.row;
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"If you delete this category then items belonging to that category will also be deleted from your custom packing list(s). Do you want to delete?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO",nil];
			[alert show];
			[alert release];
			
			/*if (allowDelete == YES)
			{
				
				
				if (![[[UIDevice currentDevice].systemVersion substringToIndex:2] isEqualToString: @"2."])
					[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
			}*/
			
		}
	}	
}

- (IBAction) buttonAction:(id) sender {
	
	if (sender == doneButton.customView ) {
		//[self saveSelectedItemsToDB];
		BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
		NSMutableArray * coutemsList = [PackingList findAllWithoutPhoto];
		if (isEdit) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
            if([coutemsList count] == 25)
            {
                NSString *meg = [NSString stringWithFormat:@"You have created %d custom packing lists. How do you like this app so far? If you are happy, please consider writing a (nice) review at the App Store. Also, feel free to send us an email for any feature request or issues.",[coutemsList count]];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations" message:meg
                                                               delegate:appDelegate cancelButtonTitle:@"Close popup" otherButtonTitles: @"Write a review",@"Send an email",nil];
                [alert show];	
                [alert release]; 
            }

            [self.navigationController popToViewController:homeController animated:YES];            
        }

		
	}
	else if (sender == addButton.customView)
	{
		AddCategoryViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[AddCategoryViewController alloc] initWithNibName:@"AddCategoryViewController" bundle:nil];
		else
			controller = [[AddCategoryViewController alloc] initWithNibName:@"AddCategoryViewController1" bundle:nil];
		
		//[[AddCategoryViewController alloc] initWithNibName:@"AddCategoryViewController" bundle:nil];
		controller.categoryListArray = self.selectedListArray;
		controller.currentList = self.currentList;
		
		UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
		
		[self.navigationController presentModalViewController:navController  animated:YES];
		[controller release];
		[navController release];
	}
	else if (sender == delBtn)
	{
		if (delBtn.selected)
		{
			editTableView.hidden = YES;
			myTableView.hidden = NO;
			[self loadData];
			delBtn.selected = NO;
			[self setEditing:NO animated:YES];
		}
		else 
		{
			editTableView.hidden = NO;
			myTableView.hidden = YES;
			
			delBtn.selected = YES;
			[self setEditing:YES animated:YES];
		}
	}
	else if (sender == sortBtn)
	{
		if (sortBtn.selected)
		{
			sortBtn.selected = NO;
			[self reorderListByAsc];
			//[myTableView reloadData];
		}
		else 
		{
			sortBtn.selected = YES;
			[self reorderListByDesc];
			//[myTableView reloadData];
		}		
	}
	else if (sender == restoreBtn)
	{
		[self loadData];
		[currentList.itemArray removeAllObjects];
		BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
		if (!TARGET_VERSION == VERSION_BABY_PACKING){
			[appDelegate.dataManager getRestoreCategory];
		}
		/*else{
		 [appDelegate.dataManager getRestoreItemByCategory:[NSString stringWithFormat:@"BabyItem"]];
		 }*/
		
		RestoreCategoryViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[RestoreCategoryViewController alloc] initWithNibName:@"RestoreCategoryViewController" bundle:nil];
		else
			controller = [[RestoreCategoryViewController alloc] initWithNibName:@"RestoreCategoryViewController1" bundle:nil];
		
		//[[RestoreCategoryViewController alloc] initWithNibName:@"RestoreCategoryViewController" bundle:nil];
		
		//UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
		
		[self presentModalViewController:controller  animated:YES];
		[controller release];
		//[navController release];
	}
	else if (sender == searchButton)
	{
		BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
		SelectItemViewController *controller = nil;
		
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[SelectItemViewController alloc] initWithNibName:@"SelectItemViewController" bundle:nil];
		else
			controller = [[SelectItemViewController alloc] initWithNibName:@"SelectItemViewController1" bundle:nil];
		
		//[[SelectItemViewController alloc] initWithNibName:@"SelectItemViewController" bundle:nil];
		controller.homeController = self.homeController;
		controller.currentList = self.currentList;
		controller.isFromCategory = YES;
		controller.isSearchFromCategory = YES;
		controller.predefinedItems = appDelegate.allItemsFromDB;
		[self.navigationController pushViewController:controller animated:YES];
		[controller release];
	}
}

-(void)reorderListByDesc{
	
	NSMutableArray *items = selectedListArray;
	
	for(int m = 0 ; m < [items count]; m++){
		for(int n = 0 ; n < [items count]-m-1; n++){
			PackingList *item1 = [items objectAtIndex:n];
			PackingList *item2 = [items objectAtIndex:n+1];
			NSString *name1 = item1.name;
			NSString *name2 = item2.name;
			if([name1 compare: name2] == NSOrderedAscending){
				PackingItem *tempItem = [item1 retain];
				[selectedListArray replaceObjectAtIndex:n withObject:item2];
				[selectedListArray replaceObjectAtIndex:n+1 withObject:tempItem];
				
			}
			
		}
		
	}
	
	[self loadData];
}

-(void)reorderListByAsc{
	
	NSMutableArray *items = selectedListArray;
	
	for(int m = 0 ; m < [items count]; m++){
		for(int n = 0 ; n < [items count]-m-1; n++){
			PackingList *item1 = [items objectAtIndex:n];
			PackingList *item2 = [items objectAtIndex:n+1];
			NSString *name1 = item1.name;
			NSString *name2 = item2.name;
			if([name1 compare: name2] == NSOrderedDescending){
				PackingItem *tempItem = [item1 retain];
				[selectedListArray replaceObjectAtIndex:n withObject:item2];
				[selectedListArray replaceObjectAtIndex:n+1 withObject:tempItem];				
			}			
		}
		
	}
	
	[self loadData];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animate {
	[super setEditing:editing animated:animate];
	editTableView.editing = editing;
	[editTableView reloadData];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
	{
		allowDelete = YES;
		allowDelete = NO;
		BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
		// Delete the row from the data source		
		PackingList *list = [self.selectedListArray objectAtIndex:index];
		Categories *category = [[Categories alloc] init];
		//NSLog(@"category -- %@",nameField.text);
		category.pk = list.pk;
		category.name = list.name;
		category.deleted = 1;
		[appDelegate.dataManager updateCategory:category];
		
		PackingItem *item = [[PackingItem alloc] init];
		[item deleteByCategory:list.name];
		[self.selectedListArray removeObjectAtIndex:index];
		[editTableView reloadData];
	}
}

#pragma mark - Orientation Methods
#ifdef IOS_OLDER_THAN_6

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{            return YES;
    
}

#endif

#ifdef IOS_NEWER_OR_EQUAL_TO_6

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return (UIInterfaceOrientationMaskAll);
}

#endif

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if (IS_IPAD)
    //    {
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        [layoutManager translateToPortraitForView:self.view withAnimation:YES];
    }
    else
    {
        [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
    }
    // }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (BOOL)isOrientationPortrait
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}

@end

//
//  ExportParser.h
//  BabyPacking
//
//  Created by Mehul Bhuva on 07/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

//http://192.168.0.7/packngohelp/ws/import.php
//	Input - username, password, userType (1=>packing, 2=>baby), act (export)	

#import <Foundation/Foundation.h>


@class BabyPackingAppDelegate;

@interface ExportParser : NSObject <NSXMLParserDelegate>
{
	BOOL mSuccessful;
	BabyPackingAppDelegate *appDelegate;
	NSMutableDictionary* mData;
	NSMutableString* mCurrentString;
	NSMutableDictionary* mCurItem;
	NSMutableArray *mCurItemArray;
	NSMutableArray *userDetails;
	BOOL storingCharacters;
	id mDelegate;
}
@property (nonatomic, retain) NSMutableString* mCurrentString;
@property (nonatomic, assign) BOOL successful;
@property (nonatomic, retain) NSMutableDictionary* data;
@property (nonatomic, retain) NSMutableDictionary* mCurItem;
@property (nonatomic, retain) NSMutableArray *mCurItemArray;
@property (nonatomic, retain) NSMutableArray *userDetails;
- (void)initWithDBName:(NSString *)DBName withXMLString:(NSString *)xmlString;
//- (void)initFromStr:(NSString *)str;
- (void)displayInfo;
@end

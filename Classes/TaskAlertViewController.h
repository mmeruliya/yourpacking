//
//  TaskAlertViewController.h
//  BabyPacking
//
//  Created by Jay Lee on 10/13/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestHandler.h"
#import "LayoutManager.h"
#import "syncTask.h"
#import <sqlite3.h>

@class BabyPackingAppDelegate;

@interface TaskAlertViewController : UIViewController<RequestHandlerDelegate> {
	
	BabyPackingAppDelegate* mAppDelegate;
    LayoutManager *layoutManager;
	NSMutableDictionary* mData;
	NSInteger mTabBarState;
	NSMutableArray* mAllTasksOnline;
	NSMutableArray* mAllRecordsOnline;
	NSDateFormatter* mDateFormatter;
	BOOL mUpdatingOnline;
	
	IBOutlet UITableView* mTableView;
	IBOutlet UIView* mBlankView;
    IBOutlet UIView* mBlankView_L;
	IBOutlet UITabBar* mTabBar;
	IBOutlet UIBarButtonItem* mBtnCreate;
	IBOutlet UIButton* mBtnUpdate;
	IBOutlet UIImageView* mIconView;
	IBOutlet UILabel* mTitleLabel;
	IBOutlet UILabel* mBodyLabel;
	IBOutlet UILabel* mDateTimeLabel;
	IBOutlet UILabel* mRepeatLabel;
	IBOutlet UITableViewCell* mTplCell;
    
    //Change By MS
    sqlite3 *dbFamilyPacking;
    NSMutableArray *arrReminders;
    UILocalNotification *localNotif;
}

@property(nonatomic,retain)IBOutlet UIButton *btnAddtask;
@property(nonatomic,retain)IBOutlet UILabel *lblTit;

@property (strong) NSMetadataQuery *metadataQuery;
@property (strong) syncTask * doc;
@property (strong) NSMetadataQuery *query;

- (IBAction)buttonAddNewTaskDidClick:(id)sender;
- (IBAction)buttonCreateDidClick:(id)sender;
- (IBAction)buttonUpdateDidClick:(id)sender;

@end

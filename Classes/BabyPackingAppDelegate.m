//
//  BabyPackingAppDelegate.m
//  BabyPacking
//
//  Created by Gary He on 5/18/09.
//  Copyright test 2009. All rights reserved.
//

#import "BabyPackingAppDelegate.h"
#import "RootViewController.h"
#import "InfoViewController.h"
#import "SettingsViewController.h"
#import "HelpViewController.h"
#import "PackingList.h"
#import "PackingItem.h"
#import "BriefcaseItem.h"
#import "RecordItem.h"
#import "TaskItem.h"
#import "Constant.h"
#import "DataManager.h"
#import "syncTask.h"


#define kFILENAME @"FamilyPacking.sqlite3"

@interface BabyPackingAppDelegate(Private)

- (void) loadSettings;
- (void) loadAppData;
- (void) saveItemAsPreference:(NSDictionary *)items;
- (id) loadItemFromPreference:(NSString *)key;
- (void) loadReadyMadeListFromAppData;
- (void) loadReadyMadeListForDeluxeFromAppData;
- (void) loadPredefinedAndUserItems;

- (void) saveAppData;
- (void) saveReadyMadeListToAppData;
- (void) saveReadyMadeListForDeluxeToAppData;
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item;

- (void)uploadDeviceToken:(NSString *)deviceToken;
@end


@implementation BabyPackingAppDelegate

@synthesize window;
@synthesize navigationController,metadataQuery;

@synthesize homeViewController;
@synthesize welcomeViewController,ImageName;
@synthesize startDate;
@synthesize dataPath;
@synthesize appDict;
@synthesize settingBundle;
@synthesize deviceToken = mDeviceToken;
@synthesize readyMadeListArray;
@synthesize readyMadeListForDeluxeArray;
@synthesize babyPredefinedItems;
@synthesize toddlerPredefinedItems;
@synthesize userCreateItems;

@synthesize soundOn;
@synthesize soundProgressOn;
@synthesize colorBgOn;
@synthesize photoBgOn;
@synthesize addItemsByGroup;
@synthesize readyMadeListShow;
@synthesize iCloudShow;
@synthesize welcomePageShow;
@synthesize password;
@synthesize fontType;
@synthesize taskSound;
//@synthesize deviceToken = mDeviceToken;	//commented Hardik 12 Nov 10
@synthesize soundID = mSoundID;
@synthesize firstTime;
@synthesize categoryFromDB,itemsFromDB,dataManager,restoreCategories,restoreItems,allItemsFromDB;
@synthesize isHelpDownloaded,helpArray;

@synthesize loginStatus,userID,allItemsList,allCategoriesList;

@synthesize globReminderId,notesList,notesItem,notesTask;

#pragma mark -
#pragma mark Memory management

- (void)dealloc {
	[navigationController release];
	[window release];
	
	[homeViewController release];
	[mSynchronizingView release];
	[mIndicatorView release];
	
	[dbConn release];
	
	[dataPath release];
	[appDict release];
	[readyMadeListArray release];
	[readyMadeListForDeluxeArray release];
	
	[babyPredefinedItems release];
	[toddlerPredefinedItems release];
	[userCreateItems release];
	
	[password release];
	[fontType release];
	[taskSound release];
	[mDeviceToken release];
    
	if (allItemsList != nil)
	{
		[allItemsList removeAllObjects];
		[allItemsList release];
		allItemsList = nil;
	}
    
	if (allCategoriesList != nil)
	{
		[allCategoriesList removeAllObjects];
		[allCategoriesList release];
		allCategoriesList = nil;
	}
	
	if (itemsFromDB != nil)
	{
		[itemsFromDB removeAllObjects];
		[itemsFromDB release];
		itemsFromDB = nil;
	}
	
	if (categoryFromDB != nil)
	{
		[categoryFromDB removeAllObjects];
		[categoryFromDB release];
		categoryFromDB = nil;
	}
	
	if (restoreItems != nil)
	{
		[restoreItems removeAllObjects];
		[restoreItems release];
		restoreItems = nil;
	}
	
	if (restoreCategories != nil)
	{
		[restoreCategories removeAllObjects];
		[restoreCategories release];
		restoreCategories = nil;
	}
	
	if (allItemsFromDB != nil)
	{
		[allItemsFromDB removeAllObjects];
		[allItemsFromDB release];
		allItemsFromDB = nil;
	}
	
	[dataManager release];
	[helpArray release];
	
	[super dealloc];
}

#pragma mark -
#pragma mark Application lifecycle

- (void)applicationDidFinishLaunching:(UIApplication *)application {
	
   

    
    UIColor *selectedColor = [UIColor blackColor];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject: selectedColor];
    [[NSUserDefaults standardUserDefaults] setObject: data forKey: K_TOP_BAR_COLOR];
    
    UIColor *selectedColor2 = [UIColor whiteColor];
    NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject: selectedColor2];
    [[NSUserDefaults standardUserDefaults] setObject: data2 forKey: K_TOP_TEXT_COLOR];
    
    
	if (TARGET_VERSION == VERSION_BABY_PACKING)
		NSLog(@"Version baby packing started!");
	else if (TARGET_VERSION == VERSION_FAMILY_PACKING)
		NSLog(@"Version family packing started!");
	else if (TARGET_VERSION == VERSION_YOU_PACKING)
		NSLog(@"Version you packing started!");
	else
		NSLog(@"Version UNKNOWN started!");
	
	loginStatus=0;
	userID=0;
    startDate = YES;
	self.isHelpDownloaded = NO;
	// Upgrade database if needed
	[DBConnection upgradeDatabaseIfNeeded];
    
	//get home view controller from navigationController;
    self.homeViewController = (RootViewController*)navigationController.topViewController;
    
    //Change By MS
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"LocalNotification"]) {
        [self removeAllLocalNotification];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    }
	
	// Register APNS
    
    // last change by KA
    //comment for icloud
    //	self.deviceToken = [self loadItemFromPreference:@"DeviceToken"];
    //	if (!self.deviceToken) self.deviceToken = @"Unknown";
    //	UIApplication* app = [UIApplication sharedApplication];
    //	if ([app respondsToSelector:@selector(registerForRemoteNotificationTypes:)]) {
    //		[app registerForRemoteNotificationTypes:
    //										UIRemoteNotificationTypeBadge |
    //										UIRemoteNotificationTypeSound |
    //										UIRemoteNotificationTypeAlert];
    //	}
    // Override point for customization after app launch
	dbConn = [DBConnection sharedConnection];
	dataManager = [[DataManager alloc] init];
	[self loadSettings];
	[self loadAppData];
	if (self.taskSound != nil) [self prefetchSoundID:self.taskSound];
	[self loadReadyMadeListFromAppData];
	[self loadPredefinedAndUserItems];
	[self loadReadyMadeListForDeluxeFromAppData];
    //	[self loadCategoryListFromAppData];
    
    
    //    NSArray *docPaths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    //	NSString *docDir=[docPaths objectAtIndex:0];
    //
    //	NSString *dbPath =[docDir stringByAppendingPathComponent:@"Packing.sqlite"];
    //    [self addSkipBackupAttributeToItemAtURL:[NSURL URLWithString:dbPath]];
    //
    //	NSString *dbPath =[docDir stringByAppendingPathComponent:@"baby1.sqlite"];
    //    [self addSkipBackupAttributeToItemAtURL:[NSURL URLWithString:dbPath]];
    //
    //    NSString *dbPath =[docDir stringByAppendingPathComponent:@"AppData.plist"];
    //    [self addSkipBackupAttributeToItemAtURL:[NSURL URLWithString:dbPath]];
    
	//[window addSubview:[navigationController view]];
    [window setRootViewController:navigationController];
	[window makeKeyAndVisible];
    
    [self copyDatabaseIfNeeded];
  
	mSynchronizingView.hidden = YES;
	[window addSubview:mSynchronizingView];
    
}
- (void) copyDatabaseIfNeeded {
    //Using NSFileManager we can perform many file system operations.
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSString *dbPath = [self getDBPath];
    BOOL success = [fileManager fileExistsAtPath:dbPath];
    
    if(!success) {
        
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"FamilyPacking.sqlite"];
        success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
        
        if (!success)
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
}

//Change By MS

- (NSString *) getDBPath
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
	NSString *documentsDir = [paths objectAtIndex:0];
    
	return [documentsDir stringByAppendingPathComponent:@"FamilyPacking.sqlite3"];
}

- (NSString *) getDBListPath
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
	NSString *documentsDir = [paths objectAtIndex:0];
    
	return [documentsDir stringByAppendingPathComponent:@"baby1.sqlite"];
}

//- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
//{
//    const char* filePath = [[URL path] fileSystemRepresentation];
//
//    const char* attrName = "com.apple.MobileBackup";
//    u_int8_t attrValue = 1;
//
//    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
//    return result == 0;
//}


- (void) loadSettings {
	
	// load settings bundle
	NSString *pathStr = [[NSBundle mainBundle] bundlePath];
	NSString *settingsBundlePath = [pathStr stringByAppendingPathComponent:@"Settings.bundle"];
	NSString *finalPath = [settingsBundlePath stringByAppendingPathComponent:@"Root.plist"];
	NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithContentsOfFile:finalPath];
	self.settingBundle = [dict objectForKey:@"PreferenceSpecifiers"];
	
	// load settings in setting bundle
	NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
    
	self.soundOn = [userSettings boolForKey:K_SET_SOUND];
    self.soundProgressOn = [userSettings boolForKey:K_SET_PROGRESS_SOUND];
	self.colorBgOn = [userSettings boolForKey:K_SET_COLOR_BG];
	self.photoBgOn = [userSettings boolForKey:K_SET_PHOTO_BG];
	self.fontType = [userSettings stringForKey:K_SET_FONT_TYPE];
	self.password = [userSettings stringForKey:K_SET_PASSWORD];
	self.firstTime = [userSettings boolForKey:K_SET_FIRSTTIME];
	self.taskSound = [userSettings stringForKey:K_SET_TASK_SOUND];
	if (!taskSound || [taskSound length] == 0) {
		self.taskSound = [SettingsViewController defaultValueForSection:K_SET_TASK_SOUND];
	}
	self.addItemsByGroup = [userSettings stringForKey:K_SET_ADDITEMSBYGROUP];
	if (!addItemsByGroup || [addItemsByGroup length] == 0) {
		self.addItemsByGroup = @"YES";
	}
	self.readyMadeListShow = [userSettings boolForKey:K_SET_READYMADELISTSHOW];
    self.iCloudShow = [userSettings boolForKey:K_SET_ICLOUDSHOW];
  self.iCloudShow = @"YES";
    
	self.welcomePageShow = [userSettings stringForKey:K_SET_WELCOMEPAGESHOW];
	if (!welcomePageShow || [welcomePageShow length] == 0) {
		self.welcomePageShow = @"YES";
	}
    //	NSLog(@"font name = %@", fontType);
	if (!fontType || [fontType length] == 0) {
		self.fontType = FONT_TYPE_CLASSIC;
	}
}

- (void) loadAppData {
	// Check for data in Documents directory. Copy default appData.plist to Documents if not found.
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *pathToDefaultPlist = [[NSBundle mainBundle] pathForResource:@"AppData" ofType:@"plist"];
	self.dataPath = [documentsDirectory stringByAppendingPathComponent:@"AppData.plist"];
    
	//NSLog(@"dataPath = %@", dataPath);
    if (![fileManager fileExistsAtPath:dataPath]) {
		
		if ([fileManager copyItemAtPath:pathToDefaultPlist toPath:dataPath error:&error] == NO) {
            NSAssert1(0, @"Failed to copy data with error message '%@'.", [error localizedDescription]);
        }
    }
	NSDictionary* dict = [NSDictionary dictionaryWithContentsOfFile:dataPath];
	float version = [[dict objectForKey:@"version"] floatValue];
	NSDictionary* newDict = [NSDictionary dictionaryWithContentsOfFile:pathToDefaultPlist];
	float new_version = [[newDict objectForKey:@"version"] floatValue];
	if (version < new_version) {
		
		// Remove the old data plist
		if ([fileManager fileExistsAtPath:dataPath]) {
			NSLog(@"Remove the old plist file: %@.", dataPath);
			[fileManager removeItemAtPath:dataPath error:&error];
		}
		
		// Copy the new data plist
		if ([fileManager copyItemAtPath:pathToDefaultPlist toPath:dataPath error:&error] == NO) {
            NSAssert1(0, @"Failed to copy data with error message '%@'.", [error localizedDescription]);
        }
	}
	
	self.appDict = [NSMutableDictionary dictionaryWithContentsOfFile:dataPath];
}

// load ready made list from appdata, generate packing list and item instead of Dictionary and Array
- (void) loadReadyMadeListFromAppData {
	NSMutableArray *readyMadeListInAppData = [appDict objectForKey:K_READY_MADE];
    
	self.readyMadeListArray = [NSMutableArray arrayWithCapacity:[readyMadeListInAppData count]];
	int i = 0;
	for(NSMutableDictionary *listDict in readyMadeListInAppData) {
		i++;
		//create list
		PackingList *tempList = [[PackingList alloc] init];
		tempList.name = [listDict objectForKey:K_LIST_NAME];
		tempList.sex = [[listDict objectForKey:K_LIST_SEX] intValue];
		tempList.age = [[listDict objectForKey:K_LIST_AGE] intValue];
		tempList.update = [listDict objectForKey:K_LIST_UPDATE];
		tempList.totalCount = [[listDict objectForKey:K_LIST_TOTAL] intValue];
		tempList.checkedCount = [[listDict objectForKey:K_LIST_CHECKED] intValue];
		tempList.predefineId = i * 1000;
		if (i > 2) {
			tempList.predefineForAge = Toddler;
		} else {
			tempList.predefineForAge = Baby;
		}
		NSMutableArray *items = [listDict objectForKey:K_LIST_ITEMS];
		//add item to list
		int j = 0;
		for(NSMutableDictionary *itemDict in items) {
			j++;
			PackingItem *tempItem = [[PackingItem alloc] init];
			tempItem.name = [itemDict objectForKey:K_ITEM_NAME];
			tempItem.tip = [itemDict objectForKey:K_ITEM_TIP];
			tempItem.qty = [itemDict objectForKey:@"qty"];
			tempItem.checked = [[itemDict objectForKey:K_ITEM_CHECKED] boolValue];
			tempItem.predefineId = i * 1000 + j;
			tempItem.predefineForAge = tempList.predefineForAge;
			[tempList.itemArray addObject:tempItem];
			
			[tempItem release];
		}
		
		[readyMadeListArray addObject:tempList];
		
		[tempList release];
	}
    
	
	NSMutableDictionary *readyMadeDict = [NSMutableDictionary dictionaryWithObjectsAndKeys: readyMadeListArray, K_LIST, [NSNumber numberWithInt:ReadyMadeList], K_TYPE, nil];
	homeViewController.readyMadeDict = readyMadeDict;
	//[homeViewController.allListArray addObject:readyMadeDict];
	
}

- (void) loadReadyMadeListForDeluxeFromAppData {
	NSMutableArray *readyMadeListForDeluxeInAppData = [appDict objectForKey:K_READY_MADE_DELUXE];
	
	self.readyMadeListForDeluxeArray = [NSMutableArray arrayWithCapacity:[readyMadeListForDeluxeInAppData count]];
	int m =0;
    //	self.listArray = [NSMutableArray arrayWithCapacity:4];
	for(NSMutableArray *array in readyMadeListForDeluxeInAppData){
		m++;
		int i = 0;
		NSMutableArray *list = [NSMutableArray arrayWithCapacity:4];
		for(NSMutableDictionary *listDict in array) {
			i++;
			//create list
			PackingList *tempList = [[PackingList alloc] init];
			tempList.name = [listDict objectForKey:K_LIST_NAME];
			tempList.sex = [[listDict objectForKey:K_LIST_SEX] intValue];
			tempList.age = [[listDict objectForKey:K_LIST_AGE] intValue];
			tempList.update = [listDict objectForKey:K_LIST_UPDATE];
			tempList.totalCount = [[listDict objectForKey:K_LIST_TOTAL] intValue];
			tempList.checkedCount = [[listDict objectForKey:K_LIST_CHECKED] intValue];
			tempList.predefineId =m * 10000 + i * 1000;
            
			NSMutableArray *items = [listDict objectForKey:K_LIST_ITEMS];
			//add item to list
			int j = 0;
			for(NSMutableDictionary *itemDict in items) {
				j++;
				PackingItem *tempItem = [[PackingItem alloc] init];
				tempItem.name = [itemDict objectForKey:K_ITEM_NAME];
				tempItem.tip = [itemDict objectForKey:K_ITEM_TIP];
				tempItem.qty = [itemDict objectForKey:@"qty"];
				tempItem.checked = [[itemDict objectForKey:K_ITEM_CHECKED] boolValue];
				tempItem.predefineId =m *10000 + i * 1000 + j;
				tempItem.predefineForAge = tempList.predefineForAge;
				[tempList.itemArray addObject:tempItem];
				
				[tempItem release];
			}
            //			[array addObject:tempList];
            //			[self.listArray addObject:tempList];
			[list addObject:tempList];
			
			[tempList release];
			
		}
        //		[readyMadeListForDeluxeArray addObject:self.listArray];
        //		[readyMadeListForDeluxeArray addObject:array];
		[readyMadeListForDeluxeArray addObject:list ];
        
	}
    
    //
    //	NSMutableDictionary *readyMadeDict = [NSMutableDictionary dictionaryWithObjectsAndKeys: readyMadeListArray, K_LIST, [NSNumber numberWithInt:ReadyMadeList], K_TYPE, nil];
	homeViewController.readyMadeListForDeluxeArray = readyMadeListForDeluxeArray;
    //	[homeViewController.allListArray addObject:readyMadeDict];
	
}

- (void) loadPredefinedAndUserItems {
	babyPredefinedItems = [appDict objectForKey:K_BABY_ITEMS];
	toddlerPredefinedItems = [appDict objectForKey:K_TODDLER_ITEMS];
	userCreateItems = [appDict objectForKey:K_USER_ITEMS];
	
}

- (void)applicationWillTerminate:(UIApplication *)application {
	// Save data if appropriate
	
	//finalize statements before close db
	[PackingItem finalizeStatements];
	[PackingList finalizeStatements];
	[BriefcaseItem finalizeStatements];
	[RecordItem finalizeStatements];
	[TaskItem finalizeStatements];
	
	[self saveReadyMadeListToAppData];
	[self saveReadyMadeListForDeluxeToAppData];
	[self saveAppData];
	[dbConn close];
}


- (void) saveAppData {
	[appDict writeToFile:dataPath atomically:NO];
}

- (void) saveReadyMadeListToAppData {
	NSMutableArray *readyMadeListInAppData = [appDict objectForKey:K_READY_MADE];
	
	for (int i = 0; i < [readyMadeListArray count]; i++) {
		//get list
		PackingList *tempList = [readyMadeListArray objectAtIndex:i];
		NSMutableDictionary *listDict = [readyMadeListInAppData objectAtIndex:i];
		if (tempList.update) {
			[listDict setObject:tempList.update	forKey:K_LIST_UPDATE];
		}
		[listDict setObject:[NSNumber numberWithInt:tempList.totalCount] forKey:K_LIST_TOTAL];
		[listDict setObject:[NSNumber numberWithInt:tempList.checkedCount] forKey:K_LIST_CHECKED];
		NSMutableArray *itemsInListDict = [listDict objectForKey:K_LIST_ITEMS];
		for (int j = 0; j < [tempList.itemArray count]; j++) {
			PackingItem *tempItem = [tempList.itemArray objectAtIndex:j];
			NSMutableDictionary *itemDict = [itemsInListDict objectAtIndex:j];
			//NSLog(@"tempItem.checked = %d", tempItem.checked);
			[itemDict setObject:[NSNumber numberWithBool:tempItem.checked] forKey:K_ITEM_CHECKED];
		}
	}
}

- (void) saveReadyMadeListForDeluxeToAppData {
	
	NSMutableArray *readyMadeListForDeluxeInAppData = [appDict objectForKey:K_READY_MADE_DELUXE];
	for (int m = 0; m < [readyMadeListForDeluxeArray  count]; m++){
		NSMutableArray *readyMadeListArrays = [readyMadeListForDeluxeArray objectAtIndex:m];
		NSMutableArray *readyMadeListInAppData = [readyMadeListForDeluxeInAppData objectAtIndex:m];
		for (int i = 0; i < [readyMadeListArrays count]; i++) {
			//get list
			PackingList *tempList = [readyMadeListArrays objectAtIndex:i];
			NSMutableDictionary *listDict = [readyMadeListInAppData objectAtIndex:i];
			if (tempList.update) {
				[listDict setObject:tempList.update	forKey:K_LIST_UPDATE];
			}
			[listDict setObject:[NSNumber numberWithInt:tempList.totalCount] forKey:K_LIST_TOTAL];
			[listDict setObject:[NSNumber numberWithInt:tempList.checkedCount] forKey:K_LIST_CHECKED];
			NSMutableArray *itemsInListDict = [listDict objectForKey:K_LIST_ITEMS];
			for (int j = 0; j < [tempList.itemArray count]; j++) {
				PackingItem *tempItem = [tempList.itemArray objectAtIndex:j];
				NSMutableDictionary *itemDict = [itemsInListDict objectAtIndex:j];
				//NSLog(@"tempItem.checked = %d", tempItem.checked);
                [itemDict setObject:[NSNumber numberWithBool:tempItem.checked] forKey:K_ITEM_CHECKED];
                [itemDict setObject:tempItem.name forKey:K_LIST_NAME];
                [itemDict setObject:tempItem.qty forKey:@"qty"];
                
			}
			
		}
		
	}
}

- (void) saveItemAsPreference:(NSDictionary *)items {
	
	NSArray* all_keys = [items allKeys];
	
	NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
	for (NSString* key in all_keys) {
		[userSettings setObject:[items objectForKey:key] forKey:key];
	}
	[userSettings synchronize];
}

- (id) loadItemFromPreference:(NSString *)key {
	NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
	
	return [userSettings objectForKey:key];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
	
	if(buttonIndex == 1){
		NSURL *url = [NSURL URLWithString:REVIEW_URL];
		
		BOOL open = [[UIApplication sharedApplication] openURL:url];
		if(!open){
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Please wait a moment to open the e-mail!"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
			[alert show];
			return;
		}
		
	}else if(buttonIndex == 2){
		NSMutableString *email = [NSMutableString stringWithCapacity:1024];
		[email appendString:@"mailto:?"];
		NSString *to = @"redboxpro@gmail.com";
		[email appendString:[NSString stringWithFormat:@"to=%@", [to stringByReplacingOccurrencesOfString:@"&" withString:@"%26"]]];
		[email setString:[email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
		NSLog(@"email = %@", email);
		NSURL *url = [NSURL URLWithString:email];
		
		BOOL open = [[UIApplication sharedApplication] openURL:url];
		if(!open){
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Please wait a moment to open the e-mail!"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
			[alert show];
			return;
		}
	}
}

#pragma mark Public member function implementation

- (void) showAlert:(NSString*) title message:(NSString*) message {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message: message
												   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];
	[alert release];
}

- (NSString*) getCustomFontName; {
	NSString *fontName;
	if (!fontType) {
		return FONT_NAME_CLASSIC;
	} else {
		fontName = [SYSTEM_FONT_DICT objectForKey:fontType];
	}
	if (!fontName) {
		fontName = FONT_NAME_CLASSIC;
	}
	return fontName;
}

- (void)encodeMailBody:(NSMutableString*)str
{
#define REPLACE(from, to) \
[str replaceOccurrencesOfString: from withString: to \
options:NSLiteralSearch range:NSMakeRange(0, [str length])]
	
	//    // convert to HTML
	//    REPLACE(@"&", @"&amp;");
	//    REPLACE(@"<", @"&lt;");
	//    REPLACE(@">", @"&gt;");
	//    REPLACE(@"\"", @"&quot;");
	//    REPLACE(@" ", @"&nbsp;");
	//    REPLACE(@"\n", @"<br>");
	//    REPLACE(@"¥n", @"<br>");
	
    // URL encoding
    NSString *tmp = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [str setString:tmp];
	
    // encode for mail body
    REPLACE(@"&", @"%26");
}

- (void) startSynchronizing {
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];
	
	window.userInteractionEnabled = NO;
	[mIndicatorView startAnimating];
	mSynchronizingView.hidden = NO;
	
	[pool release];
}

- (void) stopSynchronizing {
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];
	
	window.userInteractionEnabled = YES;
	mSynchronizingView.hidden = YES;
	[mIndicatorView stopAnimating];
	
	[pool release];
}

- (void) prefetchSoundID:(NSString *)sound {
	
	if (mSoundID != 0) AudioServicesDisposeSystemSoundID(mSoundID);
	
	NSBundle *mainBundle = [NSBundle mainBundle];
	CFURLRef aFileURL = (CFURLRef)[NSURL fileURLWithPath:[mainBundle pathForResource:sound ofType:nil] isDirectory:NO];
	if (aFileURL != nil)  {
		AudioServicesCreateSystemSoundID(aFileURL, &mSoundID);
	}
}
- (void)applicationDidBecomeActive:(UIApplication *)application
{

}

#pragma mark - Change By MS

-(void)removeAllLocalNotification
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"LocalNotification"];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (NSString *)DocumentsDirectory {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

#pragma mark -
#pragma mark APNS (Apple Push Notification Services) delegate methods implementation

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
	NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken!");
	self.deviceToken = [[[[deviceToken description] stringByReplacingOccurrencesOfString:@"<"withString:@""]
						 stringByReplacingOccurrencesOfString:@">" withString:@""]
						stringByReplacingOccurrencesOfString: @" " withString: @""];
	
	NSDictionary *items = [NSDictionary dictionaryWithObject:mDeviceToken forKey:@"DeviceToken"];
	[self saveItemAsPreference:items];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
	NSLog(@"Error in registration. Error: %@", error);
	
	self.deviceToken = @"Unknown";
	NSDictionary *items = [NSDictionary dictionaryWithObject:mDeviceToken forKey:@"DeviceToken"];
	[self saveItemAsPreference:items];
	
#if TARGET_IPHONE_SIMULATOR
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc postNotificationName:M_DATA_NEEDS_UPDATE object:nil];
#endif
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSString *alertMsg;
    NSString *badge;
    NSString *sound;
	
    if( [[userInfo objectForKey:@"aps"] objectForKey:@"alert"] != NULL) {
        alertMsg = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
		
		NSString* title = @"";
		if (TARGET_VERSION == VERSION_BABY_PACKING)
			title = TITLE_APP_NAME_BABY;
		else if (TARGET_VERSION == VERSION_FAMILY_PACKING)
			title = TITLE_APP_NAME_FAMILY;
		else if (TARGET_VERSION == VERSION_YOU_PACKING)
			title = TITLE_APP_NAME_YOU;
		else
			title = TITLE_APP_NAME_UNKNOWN;
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(title, @"") message:alertMsg delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil];
		[alert show];
		[alert release];
    } else {
		alertMsg = @"{no alert message in dictionary}";
    }
    
	[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    if( [[userInfo objectForKey:@"aps"] objectForKey:@"badge"] != NULL) {
        badge = [[userInfo objectForKey:@"aps"] objectForKey:@"badge"];
		[UIApplication sharedApplication].applicationIconBadgeNumber = [badge intValue];
    } else {
		badge = @"{no badge number in dictionary}";
    }
    
    if( [[userInfo objectForKey:@"aps"] objectForKey:@"sound"] != NULL) {
        sound = [[userInfo objectForKey:@"aps"] objectForKey:@"sound"];
		AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    } else {
		sound = @"{no sound in dictionary}";
    }
	NSString* remote_id = [userInfo objectForKey:@"remote_id"];
	
	// Send message to Root view controller
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc postNotificationName:M_DATA_NEEDS_UPDATE object:nil];
	
	NSLog(@"Badge: %@; Alert Message: %@; Sound: %@; Task: %@", badge, alertMsg, sound, remote_id);
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
	
    NSBundle *mainBundle = [NSBundle mainBundle];
	CFURLRef aFileURL = (CFURLRef)[NSURL fileURLWithPath:[mainBundle pathForResource:notification.soundName ofType:nil] isDirectory:NO];
	if (aFileURL != nil)  {
		AudioServicesCreateSystemSoundID(aFileURL, &mSoundID);
	}
	
	AudioServicesPlaySystemSound(mSoundID);
    
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Task alert!" message:@"Check your task list." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
    
}

@end


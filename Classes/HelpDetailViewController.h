//
//  HelpDetailViewController.h
//  BabyPacking
//
//  Created by Hitesh Vaghasiya on 25/04/11.
//  Copyright 2011 Dev IT Solution pvt ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "LayoutManager.h"

@class BabyPackingAppDelegate;

@interface HelpDetailViewController : UIViewController
{
	BabyPackingAppDelegate *appDelegate;
	
	IBOutlet UIBarButtonItem *doneButton;
	IBOutlet UIWebView *myWebView;
	//LayoutManager *layoutManager;
	NSDictionary *categoryDict;
}

@property (nonatomic, retain) NSDictionary *categoryDict;

- (IBAction) buttonAction:(id)sender;

@end

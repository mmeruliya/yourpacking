//
//  CSHeaderView.m
//  BabyPacking
//
//  Created by Gary He on 5/26/09.
//  Copyright 2009 test. All rights reserved.
//

#import "CSHeaderView.h"
#import "Constant.h"
#import "BabyPackingAppDelegate.h"

@interface CSHeaderView(Private) 

- (void) load;
- (void) fontChanged:(NSNotification*)notification;

@end


@implementation CSHeaderView
@synthesize textLabel;

- (void)dealloc {
	[textLabel release];
	[[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:NOTIFY_FONT_CHANGED
                                                  object:nil];
    [super dealloc];
}

- (id)initWithCoder:(NSCoder *)decoder {
	if (self = [super initWithCoder:decoder]) {
		[self load];
	}
	return self;
}

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
		[[NSNotificationCenter defaultCenter] addObserver:self 
												 selector:@selector(fontChanged:) 
													 name:NOTIFY_FONT_CHANGED 
												   object:nil];
		[self load];
    }
    return self;
}

- (void) load {
	
	// Initialization code
	CGRect rect = self.bounds;
	rect.origin.x = rect.origin.x + 10;
	rect.size.height = rect.size.height - 4;
	rect.size.width = rect.size.width - 2 * 10;
	textLabel = [[UILabel alloc] initWithFrame:rect];
	textLabel.textColor = COLOR_HEADER_TITLE;
	
	textLabel.backgroundColor = [UIColor clearColor];    
	[self fontChanged:nil];
	
	[self addSubview:textLabel];
}

- (void) fontChanged:(NSNotification*)notification {
	//NSLog(@">>>>>>fontChanged");
	BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*) [UIApplication sharedApplication].delegate;
	NSString *customFontName = [appDelegate getCustomFontName];
	//NSLog(@"customFontName = %@", customFontName);
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	textLabel.font = [UIFont fontWithName:customFontName size:fontSize];
}

- (void)drawRect:(CGRect)rect {
    // Drawing code
}


@end

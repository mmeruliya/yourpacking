//
//  Constant.h
//  BabyPacking
//
//  Created by Gary He on 5/19/09.
//  Copyright 2009 test. All rights reserved.
//

#import "BabyPacking_Prefix.pch"

// Increase compatibility
#ifndef __IPHONE_3_0
typedef enum {
    UIRemoteNotificationTypeNone    = 0,
    UIRemoteNotificationTypeBadge   = 1 << 0,
    UIRemoteNotificationTypeSound   = 1 << 1,
    UIRemoteNotificationTypeAlert   = 1 << 2
} UIRemoteNotificationType;

@interface UIApplication (UIRemoteNotifications)

- (void)registerForRemoteNotificationTypes:(UIRemoteNotificationType)types;
// calls -registerForRemoteNotificationTypes with UIRemoteNotificationTypeNone
- (void)unregisterForRemoteNotifications;       
// returns the enabled types, also taking into account any systemwide settings; 
// doesn't relate to connectivity
- (UIRemoteNotificationType)enabledRemoteNotificationTypes;

@end

#endif

// ALPHA
#define BG_PHOTO_ALPHA 1.0

// Sound volume
#define DEF_VOLUME 0.6

//New
#define	K_TOP_BAR_COLOR @"K_TOP_BAR_COLOR"
#define K_TOP_TEXT_COLOR @"K_TOP_TEXT_COLOR"
#define K_SET_TOP_TEXT_SIZE @"K_SET_TOP_TEXT_SIZE"
#define K_SET_HEADER_COLOR @"K_SET_HEADER_COLOR"
#define K_SET_HEADER_TEXT_COLOR @"K_SET_HEADER_TEXT_COLOR"

#define	K_CELL_BG_COLOR @"K_CELL_BG_COLOR"
#define K_CELL_TEXT_COLOR @"K_CELL_TEXT_COLOR"
#define K_CELL_TEXT_SIZE @"K_CELL_TEXT_SIZE"

//New


// --------------Color-----------------
// Pink
#define HOT_PINK_COLOR [UIColor colorWithRed:1 green:(105.0/255.0) blue:(180.0/255) alpha:1.0]
#define DEEP_PINK_COLOR [UIColor colorWithRed:1 green:(20.0/255.0) blue:(147.0/255) alpha:1.0]
#define PINK_COLOR [UIColor colorWithRed:1 green:(192/255.0) blue:(203/255.0) alpha:1.0]
// Green
#define DARK_GREEN [UIColor colorWithRed:0 green:(100/255.0) blue:0 alpha:1.0]
#define LAWN_GREEN [UIColor colorWithRed:(124/255.0) green:(252/255.0) blue:0 alpha:1.0]
#define FOREST_GREEN [UIColor colorWithRed:(34/255.0) green:(139/255.0) blue:(34/255.0) alpha:1.0]
#define LIGHT_GREEN [UIColor colorWithRed:(102/255.0) green:(205/255.0) blue:(170/255.0) alpha:1.0]
// Blue
#define ROYAL_BLUE [UIColor colorWithRed:(66/255.0) green:(105/255.0) blue:(225/255.0) alpha:1.0]

#define RED_COLOR	[UIColor redColor]
#define DEF_COLOR	[UIColor blackColor]
//#define BOY_COLOR	[UIColor blueColor]
#define BOY_COLOR   ROYAL_BLUE

//#define GIRL_COLOR	[UIColor colorWithRed:0.9 green:(155.0/255.0) blue:1.0 alpha:1.0]
//#define GIRL_COLOR	[UIColor colorWithRed:(209.0/255) green:(73.0/255.0) blue:1.0 alpha:1.0]
#define GIRL_COLOR HOT_PINK_COLOR

#define BABY_COLOR LIGHT_GREEN
#define TODDLER_COLOR DARK_GREEN
#define IN_USE_COLOR [UIColor colorWithRed:1 green:(20/255.0) blue:(20/255.0) alpha:1]

#define GIRL_BG_COLOR	[UIColor colorWithRed:(243.0/255.0) green:(168.0/255.0) blue:1.0 alpha:1.0]
#define BOY_BG_COLOR	[UIColor colorWithRed:(179.0/255.0) green:(206.0/255.0) blue:(223.0/255.0) alpha:1.0]

//#define COLOR_HEADER_BG [UIColor darkGrayColor];

//#define COLOR_STATUS_HEADER_BG [UIColor colorWithRed:(116.0/255.0) green:(124/255.0) blue:(189/255.0) alpha:1.0]
#define COLOR_STATUS_HEADER_BG [UIColor colorWithRed:(111.0/255.0) green:(31/255.0) blue:(122/255.0) alpha:1.0]
#define COLOR_CUSTOM_HEADER_BG COLOR_STATUS_HEADER_BG
#define COLOR_READY_MADE_HEADER_BG COLOR_CUSTOM_HEADER_BG
#define COLOR_STATUS_LABEL_BG COLOR_STATUS_HEADER_BG

#define COLOR_HEADER_TITLE [UIColor whiteColor];

#define	COLOR_PROGRESS_BAR_TOP_R 1.0
#define	COLOR_PROGRESS_BAR_TOP_G 1.0
#define	COLOR_PROGRESS_BAR_TOP_B 0.0

#define	COLOR_PROGRESS_BAR_BOTTOM_R 0.0
#define	COLOR_PROGRESS_BAR_BOTTOM_G 1.0
#define	COLOR_PROGRESS_BAR_BOTTOM_B 0.0

// Version number
// Possible versions: v1.0, v1.20, v1.21, v1.30
#define APP_VERSION_1_0			1.0
#define APP_VERSION_1_20		1.20
#define APP_VERSION_1_21		1.21
#define APP_VERSION_1_30		1.30

// Title 
//Pack & Go - To Do, Task and Travel Packing List
//Pack & Go Deluxe - Family To Do, Tasks and Travel Packing List
//Baby Pack & Go - To Do, Task Log & Travel Packing List

#define TITLE_APP_NAME_BABY @"Baby Pack & Go"
#define TITLE_APP_NAME_FAMILY @"Pack & Go Deluxe"
#define TITLE_APP_NAME_YOU @"Pack & Go "

//#define TITLE_APP_NAME_BABY @"Baby Pack & Go - To Do, Task Log & Travel Packing List"
//#define TITLE_APP_NAME_FAMILY @"Pack & Go Deluxe - Family To Do, Tasks and Travel Packing List"
//#define TITLE_APP_NAME_YOU @"Pack & Go - To Do, Task and Travel Packing List"

#define TITLE_APP_NAME_UNKNOWN @"UNKNOWN VERSION"

#define TITLE_CREATE_NEW_LIST @"Create New List"
#define TITLE_READY_MADE1 @"Baby: Around Town"
#define TITLE_READY_MADE2 @"Baby: Out of Town"
#define TITLE_READY_MADE3 @"Toddler: Around Town"
#define TITLE_READY_MADE4 @"Toddler: Out of Town"

#define TITLE_STATUS_HEADER @"My Packing Status"
#define TITLE_CUSTOM_HEADER @"My Custom Packing List"
#define TITLE_READY_MAKE_HEADER @"Ready-Made Packing List"

#define TITLE_SELECT_ITEMS @"Select List Items"
#define TITLE_CATEGORY_LIST @"Select Category"



#define TITLE_ADD_NEW_ITEM @"Add New Item"
#define TITLE_EDIT_ITEM @"Edit The Item"
#define TITLE_ENTER_ITEM_NAME @"Enter Item Name"
#define TITLE_ADD_TIP @"Add a Tip!"

#define TITLE_ITEM_NAME @"Item Name"
#define TITLE_TIP @"Tip"
#define TITLE_QTY @"Item QTY"

#define TITLE_ADD_CHILD_NAME @"Enter Your Child's Name";
#define TITLE_SELECT_SEX_BABY @"Select Boy or Girl";
#define TITLE_SELECT_AGE_BABY @"Select Baby or Toddler";
#define TITLE_SELECT_SEX_FAMILY @"Select Male or Female";
#define TITLE_SELECT_AGE_FAMILY @"Select List Type";

#define TITLE_ADD_PHOTO @"Add a photo";

#define TITLE_CLEAR_CHECK @"Clear       "
#define TITLE_KEEP_CHECK @"Keep       "

#define TITLE_SHARE @"Share With Friends"
#define TITLE_SUPPORT @"Support"
#define TITLE_NEWS @"News"

#define TITLE_BRIEFCASE_NAME @"Person Name"
#define TITLE_BRIEFCASE_PASSPORT @"Passport"
#define TITLE_BRIEFCASE_LICENSE @"Driver's License"
#define TITLE_BRIEFCASE_SOCIAL_SECURITY @"Social Securty"
#define TITLE_BRIEFCASE_ID @"ID Card"
#define TITLE_BRIEFCASE_AIRLINE_NAME @"Airline Name"
#define TITLE_BRIEFCASE_FLIGHT @"Flight"
#define TITLE_BRIEFCASE_FREQUENT_FLYER @"Frequent Flyer"
#define TITLE_BRIEFCASE_CAR @"Car Name"
#define TITLE_BRIEFCASE_PLATE @"License Plate"
#define TITLE_BRIEFCASE_HOTEL_NAME @"Hotel Name"
#define TITLE_BRIEFCASE_HOTEL_PHONE @"Hotel Phone"
#define TITLE_BRIEFCASE_HOTEL_ADDRESS @"Hotel Address"
#define TITLE_BRIEFCASE_EMBASSY_NAME @"Embassy"
#define TITLE_BRIEFCASE_EMBASSY_ADDRESS @"Embassy Address"
#define TITLE_BRIEFCASE_EMERGENCY @"Emergency"
#define TITLE_BRIEFCASE_OTHER @"Other"



/*
American Typewriter
AppleGothic
Arial
Arial Rounded MT Bold
Arial Unicode MS
Courier
Courier New
DB LCD Temp
Georgia
Helvetica
Helvetica Neue
Hiragino Kaku Gothic ProN
Marker Felt
STHeiti J
STHeiti K
STHeiti SC
STHeiti TC
Times New Roman
Trebuchet MS
Verdana
Zapfino
*/
// Font type
#define FONT_TYPE_CLASSIC @"Helvetica (Classic)"

//
//// Font name
#define FONT_NAME_CLASSIC @"Helvetica-Bold"
#define BIG_FONT_NAME @"Zapfino" 
//#define FONT_NAME_COURIER_NEW @"CourierNewPS-BoldMT"
//#define FONT_NAME_VERDANA @"Verdana-Bold"
//#define FONT_NAME_GEORGIA @"Georgia-Bold"

// Font Dictionary
#define SYSTEM_FONT_DICT [NSDictionary dictionaryWithObjectsAndKeys: \
@"AmericanTypewriter-Bold", @"American Typewriter", \
@"AppleGothic", @"AppleGothic", \
@"Arial-BoldMT", @"Arial", \
@"ArialRoundedMTBold", @"Arial Rounded MT Bold", \
@"ArialUnicodeMS", @"Arial Unicode MS", \
@"Courier-Bold", @"Courier", \
@"CourierNewPS-BoldMT", @"Courier New", \
@"DBLCDTempBlack", @"DB LCD Temp", \
@"Georgia-Bold", @"Georgia", \
FONT_NAME_CLASSIC, FONT_TYPE_CLASSIC, \
@"HelveticaNeue-Bold", @"Helvetica Neue", \
@"HiraKakuProN-W3", @"Hiragino Kaku Gothic ProN", \
@"MarkerFelt-Thin", @"Marker Felt", \
@"STHeitiJ-Light", @"STHeiti J", \
@"STHeitiK-Light", @"STHeiti K", \
@"STHeitiSC-Light", @"STHeiti SC", \
@"STHeitiTC-Light", @"STHeiti TC", \
@"TimesNewRomanPS-BoldMT", @"Times New Roman", \
@"TrebuchetMS-Bold", @"Trebuchet MS", \
@"Verdana-Bold", @"Verdana", \
@"Zapfino", @"Zapfino", \
nil]

// KEY
#define K_TYPE @"type"
#define K_LIST @"list"

// KEYS in AppData.plist
#define K_READY_MADE @"ReadyMade"
#define K_BABY_ITEMS @"BabyItems"
#define K_TODDLER_ITEMS @"ToddlerItems"
#define K_USER_ITEMS @"UserItems"
#define K_CATEGORY_LIST @"CategoryList"
#define K_READY_MADE_DELUXE @"ReadyMadeForDeluxe"

#define K_LIST_NAME @"name"
#define K_LIST_SEX @"sex"
#define K_LIST_AGE @"age"
#define K_LIST_UPDATE @"update"
#define K_LIST_TOTAL @"totalCount"
#define K_LIST_CHECKED @"checkedCount"
#define K_LIST_PREDEFINE_ID @"predefineId"
#define K_LIST_ITEMS @"items"

#define K_ITEM_NAME @"name"
#define K_ITEM_TIP @"tip"
#define K_ITEM_CHECKED @"checked"
#define K_ITEM_PREDEFINE_ID @"predefineId"

#define K_SET_SOUND @"sound"
#define K_SET_PROGRESS_SOUND @"progress_sound"
#define K_SET_TASK_SOUND @"task alert sound"
#define K_SET_COLOR_BG @"colorbg"
#define K_SET_PHOTO_BG @"photobg"
#define K_SET_FONT_TYPE @"fonttype"
#define K_SET_PASSWORD @"password"
#define K_SET_FIRSTTIME @"firstTime"
#define K_SET_ADDITEMSBYGROUP @"addItemsByGroup"
#define K_SET_READYMADELISTSHOW @"readyMadeListShow"
#define K_SET_ICLOUDSHOW @"iCloudShow"
#define K_SET_WELCOMEPAGESHOW @"welcomePageShow"
// VALUEs in AppData.plist
#define TYPE_REPEAT_NONE				@"none"
#define TYPE_REPEAT_HOURLY				@"hourly"
#define TYPE_REPEAT_DAILY				@"daily"
#define TYPE_REPEAT_WEEKLY				@"weekly"
#define TYPE_REPEAT_MONTHLY				@"monthly"
#define TYPE_REPEAT_YEARLY				@"yearly"

// Database
// URL

#ifdef BABY_PACKING

// VALUEs in AppData.plist
#define DEFAULT_NOTIFYING_CATEGORY		@"bottle"
#define DEFAULT_NOTIFYING_REPEAT		@"none"

// Review URL
#define REVIEW_URL @"http://www.worldyoga.com/babypackandgo/Baby_Pack_And_Go/viewapp.php?version=baby"

// Tasks Alert API
#define API_ADD_TASK_URL @"http://www.worldyoga.com/babypackandgo/Baby_Pack_And_Go/reminder/baby/reminder.php?action=add&"
#define API_UPDATE_TASK_URL @"http://www.worldyoga.com/babypackandgo/Baby_Pack_And_Go/reminder/baby/reminder.php?action=update&"
#define API_DELETE_TASK_URL @"http://www.worldyoga.com/babypackandgo/Baby_Pack_And_Go/reminder/baby/reminder.php?action=delete&"
#define API_LIST_TASK_URL @"http://www.worldyoga.com/babypackandgo/Baby_Pack_And_Go/reminder/baby/reminder.php?action=list&"
#define API_SEARCH_RECORD_URL @"http://www.worldyoga.com/babypackandgo/Baby_Pack_And_Go/reminder/baby/search.php?"

// APNS configuration root in AppData.plist
#define APNS_SUBITEM_ROOT	@"BabyPacking"

#endif

#ifdef FAMILY_PACKING

// VALUEs in AppData.plist
#define DEFAULT_NOTIFYING_CATEGORY		@"todo"
#define DEFAULT_NOTIFYING_REPEAT		@"none"

#define REVIEW_URL @"http://www.worldyoga.com/babypackandgo/Baby_Pack_And_Go/viewapp.php?version=family"

// Tasks Alert API
#define API_ADD_TASK_URL @"http://www.worldyoga.com/babypackandgo/Baby_Pack_And_Go/reminder/family/reminder.php?action=add&"
#define API_UPDATE_TASK_URL @"http://www.worldyoga.com/babypackandgo/Baby_Pack_And_Go/reminder/family/reminder.php?action=update&"
#define API_DELETE_TASK_URL @"http://www.worldyoga.com/babypackandgo/Baby_Pack_And_Go/reminder/family/reminder.php?action=delete&"
#define API_LIST_TASK_URL @"http://www.worldyoga.com/babypackandgo/Baby_Pack_And_Go/reminder/family/reminder.php?action=list&"
#define API_SEARCH_RECORD_URL @"http://www.worldyoga.com/babypackandgo/Baby_Pack_And_Go/reminder/family/search.php?"

// APNS configuration root in AppData.plist
#define APNS_SUBITEM_ROOT	@"FamilyPacking"

#endif

#ifdef YOU_PACKING

// VALUEs in AppData.plist
#define DEFAULT_NOTIFYING_CATEGORY		@"todo"
#define DEFAULT_NOTIFYING_REPEAT		@"none"

#define REVIEW_URL @"http://www.worldyoga.com/babypackandgo/Baby_Pack_And_Go/viewapp.php?version=you"

// Tasks Alert API
#define API_ADD_TASK_URL @"http://www.worldyoga.com/babypackandgo/Baby_Pack_And_Go/reminder/you/reminder.php?action=add&"
#define API_UPDATE_TASK_URL @"http://www.worldyoga.com/babypackandgo/Baby_Pack_And_Go/reminder/you/reminder.php?action=update&"
#define API_DELETE_TASK_URL @"http://www.worldyoga.com/babypackandgo/Baby_Pack_And_Go/reminder/you/reminder.php?action=delete&"
#define API_LIST_TASK_URL @"http://www.worldyoga.com/babypackandgo/Baby_Pack_And_Go/reminder/you/reminder.php?action=list&"
#define API_SEARCH_RECORD_URL @"http://www.worldyoga.com/babypackandgo/Baby_Pack_And_Go/reminder/you/search.php?"

// APNS configuration root in AppData.plist
#define APNS_SUBITEM_ROOT	@"YouPacking"

#endif


// Information
#define I_NEWS_LINK @"http://babypackandgo.com/Baby_Pack_And_Go/News/News.html"
#define I_ADMIN_EMAIL @"redboxpro@gmail.com"
#define I_SHARE_SUBJECT @"Share with friend"
#define I_SHARE_MSG @"<body>Here's an excellent application for your iPhone or iPod Touch that will help remind you to pack everything you need the next time you or your family leaves the house or goes on a trip! \nClick the link below: <a href=\"https://itunes.apple.com/us/app/pack-go-deluxe-packing-list/id333267085?mt=8\">https://itunes.apple.com/us/app/pack-go-deluxe-packing-list/id333267085?mt=8</a>\nYou can download \"Baby Pack &amp; Go\",\"Pack &amp; Go\",\"Pack &amp; Go Deluxe\" from the iTunes App Store.</body>"

// Interface 
#define I_OK				@"OK"

// Size
#define LIST_ICON_SIZE 40
#define PHOTO_SIZE 320

// Message
#define M_INPUT_NAME_TITLE @"Warning"
#define M_INPUT_NAME @"Please input name!"
#define M_RENAME @"The item is already in custom list, please type another name."
#define M_RENAME_TOO @"The item is already in this list, please type another name."
#define M_RENAME_AGAIN @"The item already exists, plese edit it again"

#define M_INPUT_EMAIL @""

#define M_SELECT_ONE_TITLE @"Warning"
#define M_SELECT_ONE @"Please select at least one item!"

// Task alert
#define M_DATA_NEEDS_UPDATE			@"Task Data Needs Update"
#define M_INSUFFICIENT_TASK_INFO	@"Insufficient Task Info"
#define M_RETRY_TASK_INFO			@"Please complete the task information and retry."

#define M_ERROR						@"Error"
#define M_ERROR_ADD_TASK			@"Failed to add task. Errorcode:%d"
#define M_ERROR_DELETE_TASK			@"Failed to delete task. Errorcode:%d"
#define M_ERROR_NO_CONNECTION		@"Failed to connect to server."
#define M_ERROR_LOW_DEVICE_VERSION	@"iPhone OS 3.0 or later is required to use this functionality. Please update your device on iTunes with the latest OS to use this feature"

// Notificaton
#define NOTIFY_FONT_CHANGED @"FontChangedNotificaton" 

// Image Name
#define CHECK_IMAGE @"check.png"
#define UNCHECK_IMAGE @"uncheck.png"
#define ADD_IMAGE @"add.png"
#define REMOVE_IMAGE @"remove.png"
#define IN_IMAGE @"in.png"
#define OUT_IMAGE @"out.png"

#define BLUE_BEAR @"blueBearBg.png"
#define PINK_BEAR @"pinkBearBg.png"
#define GRAY_BEAR @"grayBearBg.png"
#define BACKGROUND_BORDER_BLUE @"background-1-1.png"
#define BACKGROUND_BLUE @"background-1.png"
#define BACKGROUND_BORDER_PINK @"background-2-1.png"
#define BACKGROUND_PINK @"background-2.png"
#define BACKGROUND_BORDER_GRAY @"background-3-1.png"
#define BACKGROUND_GRAY @"background-3.png"

#define BLUE_BORDER @"borderblue.png"
#define PINK_BORDER @"borderpink.png"

//#define PINK_BEAR_ICON @"pinkBearIcon.png"
//#define BLUE_BEAR_ICON @"blueBearIcon.png"
#define PINK_BEAR_ICON @"Girl.png"
#define BLUE_BEAR_ICON @"Boy.png"
#define TEMPLATE_ICON @"templateIcon.png"
#define BOY_ICON @"Boy.png"
#define GIRL_ICON @"Girl.png"
#define MAN_ICON @"Man.png"
#define WOMAN_ICON @"Woman.png"
#define PET_ICON @"Pet.png"
#define OTHER_ICON @"Other.png"
#define SHOPPING_ICON @"Shopping.png"

#define IS_USE_ICON @"inuse.png"

// Audio file
#define CHECK_AUDIO_NAME @"check"
#define CHECK_AUDIO_EXT @"wav"

#define PROGRESS_AUDIO_NAME @"progressbar"
#define PROGRESS_AUDIO_EXT @"wav"

#define CATEGORY_URL @"http://www.babypackandgo.com/packngohelp/ws/categories.php"

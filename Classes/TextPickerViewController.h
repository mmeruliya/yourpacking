//
//  CommonPickerViewController.h
//  BabyPacking
//
//  Created by Gary He on 6/8/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BabyPackingAppDelegate;

@protocol TextPickerDelegate<NSObject>

- (void) onSelectedFont:(NSString*) text;

@end

@class SupportViewController;
@interface TextPickerViewController : UITableViewController {
	
	BabyPackingAppDelegate *appDelegate;
	
	id<TextPickerDelegate> delegate;
	NSString *selectedText;
	NSArray *choiceArray;
	//SupportViewController *supportViewController;
}

@property(nonatomic, copy) NSString *selectedText;
@property(nonatomic, retain) NSArray *choiceArray;
//@property(nonatomic, retain) SupportViewController *supportViewController;
@property(nonatomic, retain) id<TextPickerDelegate> delegate;
@end

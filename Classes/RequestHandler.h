//
//  RequestHandler.h
//  BabyPacking
//
//  Created by Jay Lee on 10/13/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RequestHandler;

@protocol RequestHandlerDelegate
- (void)request:(RequestHandler *)request responseCompleteWithData:(NSData *)xmldata;
- (void)request:(RequestHandler *)request responseFailedWithError:(NSError *)error;
@end

@class IQ_EnginesAppDelegate;

@interface RequestHandler : NSObject {
	IQ_EnginesAppDelegate* mAppDelegate;
	id mDelegate;
	
	NSDictionary *mArguments;
	NSDictionary *mAdditionalArguments;
	NSString *mRequestMethod;
	NSString *mRequestURL;
	NSUInteger mTimeOut;
	NSURLConnection* mConnection;
	NSMutableData* mConnectionData;
	id mUserData;
}

@property (nonatomic, retain) id<RequestHandlerDelegate> delegate;
@property (nonatomic, retain) NSString* url;
@property (nonatomic, retain) NSDictionary* requestArguments;		// arguments will be calculated within api_sig
@property (nonatomic, retain) NSDictionary* additionalArguments;	// arguments will be calculated without api_sig
@property (nonatomic, retain) NSString *method;
@property (nonatomic, assign) NSUInteger timeout;
@property (nonatomic, retain) NSURLConnection* connection;
@property (nonatomic, retain) NSMutableData* connectionData;
@property (nonatomic, retain) id userData;

- (void)startRequest;

@end

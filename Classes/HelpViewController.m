//
//  HelpViewController.m
//  BabyPacking
//
//  Created by Gary he on 6/17/09.
//  Copyright 2009 test. All rights reserved.
//

#import "Constant.h"
#import "HelpViewController.h"
#import "RootViewController.h"
#import "BabyPackingAppDelegate.h"
#import "BabyPacking_Prefix.pch"
#import "CategoryParser.h"
#import "CSTableViewCell.h"
#import "HelpDetailViewController.h"

@implementation HelpViewController

@synthesize categoryArray,tempCategoryArray,tempSubCategoryArray;
@synthesize subCategoryArray;
@synthesize isSubCategory;
@synthesize categoryDict;

//#define HELP_IMAGES [NSArray arrayWithObjects:@"HelpP1.png",@"HelpP2.png",@"HelpP3.png",@"HelpP4.png", nil]

- (void)dealloc 
{
	[doneButton release];
	[categoryArray release];
	[subCategoryArray release];
	[myTableView release];
	
	[categoryDict release];
    [super dealloc];
}

- (IBAction) buttonAction:(id) sender {
	if (sender == doneButton.customView) {
		[self.navigationController popViewControllerAnimated:YES];
	}
}

- (void) makeRequest
{
	[appDelegate startSynchronizing];
	RequestHandler *handler = [[RequestHandler alloc] init];
	handler.delegate = self;
	handler.method = @"POST";
	handler.url = CATEGORY_URL;
	handler.requestArguments = nil;
	handler.additionalArguments = nil;
	[handler startRequest];
	[handler release];	
}

- (void) getCategories
{
	[self.categoryArray removeAllObjects];
	for (NSDictionary *dict in appDelegate.helpArray) {
		
		if ([[dict objectForKey:@"parentId"] isEqualToString:@"0"]) {
			
			if (!TARGET_VERSION == VERSION_BABY_PACKING){
				if ([[dict objectForKey:@"name"] isEqualToString:@"Pack And Go"]) {
					for (NSDictionary *cat in appDelegate.helpArray) {
						if ([[cat objectForKey:@"parentId"] isEqualToString:[dict objectForKey:@"id"]]) {
							[self.categoryArray addObject:cat];
						}
					}
				}
			} else {
				self.tempCategoryArray=[[NSMutableArray alloc] init];
				if ([[dict objectForKey:@"name"] isEqualToString:@"Baby Pack And Go"]) {
					
					for (NSDictionary *cat in appDelegate.helpArray) {
						if ([[cat objectForKey:@"parentId"] isEqualToString:[dict objectForKey:@"id"]]) {
							[self.categoryArray addObject:cat];
							//NSLog(@"%@",[cat objectForKey:@"name"]);
						}
					}
					//////////////////////////////////////////////////////////////////////////Chinab	
					//	 NSArray* arrayOfDictionaries; //the array we want to sort
					NSSortDescriptor* nameSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
					self.categoryArray = [self.categoryArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameSortDescriptor]];
					[nameSortDescriptor release];
					//////////////////////////////////////////////////////////////////////////	 
					
					for (int i=0;i< [self.categoryArray count];i++ ) {

						dict=[self.categoryArray objectAtIndex:i];
						NSString *s=[[NSString alloc] initWithFormat:@"%@",[dict objectForKey:@"name"]];
						[self.tempCategoryArray addObject:s];
//						NSLog(@"asdfgh:%@",[self.tempCategoryArray objectAtIndex:i]);
						NSLog(@"asdfgh:%@",s);
						[s release];
					}
					[self.tempCategoryArray sortUsingSelector:@selector(compare:)];
				}
			}
		}
	}
	[myTableView reloadData];
}

- (NSMutableArray *) getSubCategories:(NSDictionary *)category
{
 if (!TARGET_VERSION == VERSION_BABY_PACKING){
	NSLog(@"category dict -- %@",category);
	NSMutableArray *array = [NSMutableArray arrayWithCapacity:0];	
	for (NSDictionary *dict in appDelegate.helpArray) {
		if ([[dict objectForKey:@"parentId"] isEqualToString:[category objectForKey:@"id"]]) {
			[array addObject:dict];
		}
	}
	
	return array;
 }
 else {
	 NSLog(@"category dict -- %@",category);
	 NSMutableArray *array = [NSMutableArray arrayWithCapacity:0];
	 for (NSDictionary *dict in appDelegate.helpArray) {
		 if ([[dict objectForKey:@"parentId"] isEqualToString:[category objectForKey:@"id"]]) {
			 [array addObject:dict];
		 }
	 }
//////////////////////////////////////////////////////////////////////////Chinab		 
	 NSSortDescriptor* nameSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
	 array = [array sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameSortDescriptor]];
	 [nameSortDescriptor release];
//////////////////////////////////////////////////////////////////////////	 
	 return array;
 }

}

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self)
//    {
//        layoutManager = [[LayoutManager alloc] initWithPortraitView:self.view andLandscapeNIB:@"HelpDetailViewController_L"];
//        //self = [super initWithNibName:@"RecalliPhoneView" bundle:nibBundleOrNil];
//        
//    }
//    return self;
//}

//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
//    {
//        [layoutManager translateToPortraitForView:self.view withAnimation:NO];
//    }
//    else
//    {
//        [layoutManager translateToLandscapeForView:self.view withAnimation:NO];
//    }
//}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	appDelegate = (BabyPackingAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		myTableView.backgroundColor = theColour1;
	}
	// and finally set the colour of your label
	
	
	if (!isSubCategory) {
		self.categoryArray = [NSMutableArray arrayWithCapacity:0];
	} else {
		self.subCategoryArray = [NSMutableArray arrayWithCapacity:0];
	}	
		
	//Hitesh
	

    
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
	label.backgroundColor = [UIColor clearColor];
    label.textColor=[UIColor whiteColor];
    
	NSString *customFontName = [appDelegate getCustomFontName];
	int fontSize = 18;
	if ([customFontName isEqualToString:BIG_FONT_NAME]) { // this font is too big
		fontSize -= 8;
	}
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE]!=nil) {
		label.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_SET_TOP_TEXT_SIZE] doubleValue]];		
	}
	else {
		label.font=[UIFont fontWithName:customFontName size:fontSize];		
	}
	
	[label sizeToFit];
	
	
//	label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
	label.textAlignment = UITextAlignmentCenter;

	UIColor *theColour12;
	// read the data back from the user defaults
	NSData *data112= [[NSUserDefaults standardUserDefaults] dataForKey: K_TOP_TEXT_COLOR];
	// check whether you got anything
	if(data112 == nil) {
		// use this to set the colour the first time your app runs
            label.textColor=[UIColor whiteColor];
	} else {
		// this recreates the colour you saved
		theColour12 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data112];
		label.textColor = theColour12;
	}
	// and finally set the colour of your label
	
	self.navigationItem.titleView = label;	
	label.text = @"Help";
	[label sizeToFit];
    
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(0, 0, 30, 30);
    doneBtn.tag=2;
    [doneBtn setImage:[UIImage imageNamed:@"Back.png"] forState:UIControlStateNormal];
    // [Editbtn setBackgroundColor:[UIColor blackColor]];
    [doneBtn addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    doneButton=[[UIBarButtonItem alloc] initWithCustomView:doneBtn];
	
	self.navigationItem.leftBarButtonItem = doneButton;	
	if (!appDelegate.isHelpDownloaded) {
		[self makeRequest];
	} else {
		if (!isSubCategory) {
			[self getCategories];
		} else {
			self.subCategoryArray = [self getSubCategories:self.categoryDict];
		}	
	}
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"ipadAppBg.jpg"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed: @"appBg.jpg"]];
    }
}

#pragma mark 
#pragma mark UITableView Datasource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{	
	if (isSubCategory) {
		NSLog(@"subcategory count -- %d",subCategoryArray.count);
		return [self.subCategoryArray count];
	}
	return [self.categoryArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier = @"CSTableViewCell";
    
    CSTableViewCell *cell = (CSTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		cell = [[[CSTableViewCell alloc] initWithCSStyle:CSTableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		cell.detailTextLabel.textColor = [UIColor darkGrayColor];
		cell.detailTextLabel.textAlignment = UITextAlignmentLeft;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure.png"]];
    }
	
	NSDictionary *dict = nil;
	
	if (isSubCategory) {
		dict = [self.subCategoryArray objectAtIndex:indexPath.row];
//			cell.textLabel.text = [dict objectForKey:@"name"];
	} else {
		dict = [self.categoryArray objectAtIndex:indexPath.row];
//		cell.textLabel.text = [self.tempCategoryArray objectAtIndex:indexPath.row];
	}
	
	NSString *customFontName = [appDelegate getCustomFontName];
	
	if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
		cell.textLabel.font=[UIFont fontWithName:customFontName size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];		
	}
	//self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
	UIColor *theColour1;
	// read the data back from the user defaults
	NSData *data11= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
	// check whether you got anything
	if(data11 == nil) {
		// use this to set the colour the first time your app runs
		cell.textLabel.textColor = DEF_COLOR;
	} else {
		// this recreates the colour you saved
		theColour1 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data11];
		cell.textLabel.textColor = theColour1;
	}
	// and finally set the colour of your label
	
		cell.textLabel.text = [dict objectForKey:@"name"];

//	cell.textLabel.text = [self.tempCategoryArray objectAtIndex:indexPath.row];
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure.png"]];
	
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
    UIColor *theColour11;
    // read the data back from the user defaults
    NSData *data111= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_BG_COLOR];
    // check whether you got anything
    if(data111 == nil) {
        // use this to set the colour the first time your app runs
    } else {
        // this recreates the colour you saved
        theColour11 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data111];
        cell.backgroundColor = theColour11;
    }
    // and finally set the colour of your label
    
    
    //self.navigationController.navigationBar.tintColor=[UIColor cyanColor];
    UIColor *theColour21;
    // read the data back from the user defaults
    NSData *data211= [[NSUserDefaults standardUserDefaults] dataForKey: K_CELL_TEXT_COLOR];
    // check whether you got anything
    if(data211 == nil) {
        // use this to set the colour the first time your app runs
    } else {
        // this recreates the colour you saved
        theColour21 = (UIColor *)[NSKeyedUnarchiver unarchiveObjectWithData: data211];
        cell.textLabel.textColor = theColour21;
    }
    // and finally set the colour of your label
    BabyPackingAppDelegate *appDelegate = (BabyPackingAppDelegate*)[UIApplication sharedApplication].delegate;
    NSString *customFontName1 = [appDelegate getCustomFontName];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE]!=nil) {
        cell.textLabel.font=[UIFont fontWithName:customFontName1 size:[[[NSUserDefaults standardUserDefaults] objectForKey:K_CELL_TEXT_SIZE] doubleValue]];
    }
    else{
        cell.textLabel.font=[UIFont fontWithName:customFontName1 size:15];
    }
    
	return cell;
}

#pragma mark _
#pragma mark UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{	
	NSDictionary *dict = nil;
	if (!isSubCategory) {
			dict = [self.categoryArray objectAtIndex:indexPath.row];
	} else {
		dict = [self.subCategoryArray objectAtIndex:indexPath.row];
	}
	
	NSMutableArray *array = [self getSubCategories:dict];
	NSLog(@"subcategory count -- %d",array.count);
	if (array.count > 0) {
		HelpViewController *controller = nil;
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[HelpViewController alloc] initWithNibName:@"HelpViewController" bundle:nil];
		else
			controller = [[HelpViewController alloc] initWithNibName:@"HelpViewController1" bundle:nil];
		
		controller.isSubCategory = YES;
		controller.categoryDict = dict;
		[self.navigationController pushViewController:controller animated:YES];
		[controller release];
	} else {
		HelpDetailViewController *controller = nil;
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			controller = [[HelpDetailViewController alloc] initWithNibName:@"HelpDetailViewController" bundle:nil];
		else
			controller = [[HelpDetailViewController alloc] initWithNibName:@"HelpDetailViewController1" bundle:nil];
		
		controller.categoryDict = dict;
		[self.navigationController pushViewController:controller animated:YES];
		[controller release];
	}
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark _
#pragma mark RequestHandlerDelegate Methods

- (void)request:(RequestHandler *)request responseCompleteWithData:(NSData *)xmldata
{
	if ([CATEGORY_URL isEqualToString:request.url]) {
		NSLog(@"HelpViewController : responseCompleteWithData begin");
		NSString *str = [[NSString alloc] initWithData:xmldata encoding:NSASCIIStringEncoding];
		NSData * data = [str dataUsingEncoding:NSUTF8StringEncoding];
		CategoryParser* handler = [[CategoryParser alloc] initWithXMLData:data];
		if (handler) {
			NSMutableArray *response = [[NSMutableArray alloc] initWithCapacity:0];
			[response addObjectsFromArray:[handler.data objectForKey:@"Values"]];
			
			for (NSDictionary *dict in response) {
				NSLog(@"response dict -- %@",dict);
			}
			
			appDelegate.isHelpDownloaded = YES;
			appDelegate.helpArray = response;
			[handler release];
			[response release];
			[self getCategories];
			[appDelegate stopSynchronizing];
		} else {
			[appDelegate stopSynchronizing];
			// Present alert view
			//[appDelegate showAlertWithTitle:ERROR andMessage:ERROR_NO_CONNECTION];
		}
		
		NSLog(@"HelpViewController : responseCompleteWithData end");
	}
}

- (void)request:(RequestHandler *)request responseFailedWithError:(NSError *)error
{
	if ([CATEGORY_URL isEqualToString:request.url]) {
		[appDelegate stopSynchronizing];
		// Present alert view
		//[appDelegate showAlertWithTitle:ERROR andMessage:ERROR_NO_CONNECTION];
		NSLog(@"HelpViewController : responseFailedWithError end");
	}
}

//#pragma mark - Orientation Methods
//#ifdef IOS_OLDER_THAN_6
//
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
//{
//    return YES;
//    
//}
//
//#endif
//
//#ifdef IOS_NEWER_OR_EQUAL_TO_6
//
//- (BOOL)shouldAutorotate
//{
//    return YES;
//}
//
//- (NSUInteger)supportedInterfaceOrientations
//{
//    return (UIInterfaceOrientationMaskAll);
//}
//
//#endif
//
//- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
//{
//    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
//    //    if (IS_IPAD)
//    //    {
//    
//    if (toInterfaceOrientation == UIInterfaceOrientationPortrait ||
//        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
//    {
//        [layoutManager translateToPortraitForView:self.view withAnimation:YES];
//    }
//    else
//    {
//        [layoutManager translateToLandscapeForView:self.view withAnimation:YES];
//    }
//    // }
//}
//
//- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
//{
//    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
//}
//
//- (BOOL)isOrientationPortrait
//{
//    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
//    if (orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown)
//    {
//        return YES;
//    }
//    else
//    {
//        return NO;
//    }
//    return NO;
//}


@end

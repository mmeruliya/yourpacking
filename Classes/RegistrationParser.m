//
//  RegistrationParser.m
//  BabyPacking
//
//  Created by Mehul Bhuva on 07/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RegistrationParser.h"
#import "BabyPackingAppDelegate.h"

@implementation RegistrationParser

@synthesize successful = mSuccessful;
@synthesize data = mData;
@synthesize mCurItem,mCurItemArray,userDetails,mCurrentString;

- (void)dealloc
{
	//[mArray release];
	[mDelegate release];
	[mCurItem release];
	[mData release];
	[super dealloc];
}



- (void)beforeParsing
{
	mSuccessful = NO;
	self.data = [NSMutableDictionary dictionaryWithCapacity:0];
	[mData setObject:[NSMutableArray arrayWithCapacity:0] forKey:@"Values"];
}

- (void)initWithData:(NSString *)username1 withPassword:(NSString *)password withtype:(NSString *)type1 withEmail:(NSString *)email1{
	
	appDelegate=(BabyPackingAppDelegate *)[[UIApplication sharedApplication] delegate];
	storingCharacters=NO;
	mCurrentString=[[NSMutableString alloc] initWithString:@""];
	NSLog(@"initRegPARSER WITH URL");
	mCurItem = [[NSMutableDictionary alloc] initWithCapacity:0];
	mCurItemArray = [[NSMutableArray alloc] initWithCapacity:0];
	self.userDetails=[[NSMutableArray alloc] initWithCapacity:0];
	//	NSLog(@"URL STRING=%@",str);
	
	
	//##############################################	
	
//	NSString *urlString = @"http://192.168.0.7/packngohelp/ws/user_registration.php";
//	NSString *urlString = @"http://www.madhukantpatel.com/packngohelp/ws/user_registration.php";
//	http://babypackandgo.com/packngohelp/ws
	NSString *urlString = @"http://babypackandgo.com/packngohelp/ws/user_registration.php";	
	// setting up the request object now
	
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
	[request setURL:[NSURL URLWithString:urlString]];
	[request setHTTPMethod:@"POST"];
    
	/*
	 add some header info now
	 we always need a boundary when we post a file
	 also we need to set the content type
	 
	 You might want to generate a random boundary.. this is just the same 
	 as my output from wireshark on a valid html post
	 */
	NSString *boundary = [NSString stringWithString:@"---------------------------14737809831466499882746641449"];
	NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
	[request addValue:contentType forHTTPHeaderField: @"Content-Type"];
	
	/*
	 now lets create the body of the post
	 */
    NSLog(@"%@",username1);
    NSLog(@"%@",password);
    NSLog(@"%d",email1);
	   NSLog(@"%d",type1);
	NSMutableData *postBody = [NSMutableData data];
	
	[postBody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"username\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[[NSString stringWithFormat:@"%@",username1] dataUsingEncoding:NSUTF8StringEncoding]]; 	
	
	[postBody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[[NSString stringWithString:password] dataUsingEncoding:NSUTF8StringEncoding]];  

	[postBody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"email\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[[NSString stringWithString:email1] dataUsingEncoding:NSUTF8StringEncoding]]; 
	
	
	[postBody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"type\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[[NSString stringWithString:type1] dataUsingEncoding:NSUTF8StringEncoding]]; 
	
	// setting the body of the post to the reqeust
	[request setHTTPBody:postBody];
	
	// now lets make the connection to the web
	
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
	NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
	NSLog(@"DATA = %@",returnString);
	//##############################################		
	
	
	NSXMLParser *parse1=[[NSXMLParser alloc] initWithData:returnData];
	[parse1 setDelegate:self];
	[parse1 parse];
	[parse1 release];	
	
	
	
	
	//	NSURL *url=[[NSURL alloc] initWithString:str];
	//	NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
	//	NSString *dstr=[[NSString alloc] initWithContentsOfURL:url];
	//	NSLog(@"Response == %@", dstr);
	//	[parser setDelegate:self];
	//	[parser parse];
	//	[parser release];
	//	[url release];
	
	/*if (![self parseXml]) {
	 return nil;
	 }*/	
	//	[self displayInfo];
	//	if (appDelegate.nearUserDetails!=nil) {
	//		[appDelegate.nearUserDetails removeAllObjects];
	//		appDelegate.nearUserDetails=nil;
	//		[appDelegate release];
	//		appDelegate.nearUserDetails=[[NSMutableArray alloc] init];		
	//	}
	//	appDelegate.nearUserDetails=self.userDetails;
}

- (void) displayInfo{
	NSLog(@"initDIsplay Info");
	
	
	//		NSLog(@"ID=%@",[mCurItem objectForKey:@"id"]);
	NSLog(@"NAME=%@",[mCurItem objectForKey:@"status"]);	
	NSLog(@"NAME=%@",[mCurItem objectForKey:@"name"]);
	NSLog(@"REACHOUT ID=%@",[mCurItem objectForKey:@"reachoutid"]);
	NSLog(@"GENDER=%@",[mCurItem objectForKey:@"gender"]);
	NSLog(@"AGE=%@",[mCurItem objectForKey:@"age"]);
	NSLog(@"IMAGE URL=%@",[mCurItem objectForKey:@"imageurl"]);
	//		NSLog(@"RATING=%@",[dict1 objectForKey:@"rating"]);
	NSLog(@"MSTATUS=%@",[mCurItem objectForKey:@"marritalstatus"]);
	NSLog(@"LATITUDE=%@",[mCurItem objectForKey:@"latitude"]);
	NSLog(@"LONGITUDE=%@",[mCurItem objectForKey:@"longitude"]);
	//		NSLog(@"DISTANCE=%@",[mCurItem objectForKey:@"distance"]);
	
}

- (void)parser:(NSXMLParser*)parser didStartElement:(NSString*)elementName namespaceURI:(NSString*)namespaceURI qualifiedName:(NSString*)qualifiedName attributes:(NSDictionary*)attributes
{
	if ([elementName isEqualToString:@"response"]) {
		mCurItem = [[NSMutableDictionary alloc] initWithCapacity:0];
	}
	else if ([elementName isEqualToString:@"status"])
	{
		[mCurrentString setString:@""];
		storingCharacters = YES;		
	}		
	else if ([elementName isEqualToString:@"id"])
	{
		[mCurrentString setString:@""];
		storingCharacters = YES;		
	}		
//	else if ([elementName isEqualToString:@"username"])
//	{
//		[mCurrentString setString:@""];
//		storingCharacters = YES;		
//	}		
//	else if ([elementName isEqualToString:@"password"])
//	{
//		[mCurrentString setString:@""];
//		storingCharacters = YES;		
//	}		
	//	else if ([elementName isEqualToString:@"reachoutid"])
	//	{
	//		[mCurrentString setString:@""];
	//		storingCharacters = YES;
	//	}
	//	else if ([elementName isEqualToString:@"imageurl"])
	//	{
	//		[mCurrentString setString:@""];
	//		storingCharacters = YES;
	//	}
	//	else if ([elementName isEqualToString:@"name"])
	//	{
	//		[mCurrentString setString:@""];
	//		storingCharacters = YES;
	//	}
	//	else if ([elementName isEqualToString:@"age"])
	//	{
	//		[mCurrentString setString:@""];
	//		storingCharacters = YES;
	//	}
	//	else if ([elementName isEqualToString:@"gender"])
	//	{
	//		[mCurrentString setString:@""];
	//		storingCharacters = YES;
	//	}
	//	else if ([elementName isEqualToString:@"marritalstatus"])
	//	{
	//		[mCurrentString setString:@""];
	//		storingCharacters = YES;
	//	}
	//	else if ([elementName isEqualToString:@"latitude"])
	//	{
	//		[mCurrentString setString:@""];
	//		storingCharacters = YES;
	//	}
	//	else if ([elementName isEqualToString:@"longitude"])
	//	{
	//		[mCurrentString setString:@""];
	//		storingCharacters = YES;
	//	}
	//	
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
	//	NSLog(@"DID END");
	if ([elementName isEqualToString:@"status"])
	{
		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"status"];
		appDelegate.loginStatus=[mCurrentString intValue];
	}
	if ([elementName isEqualToString:@"id"])
	{
		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"id"];
		appDelegate.userID=[mCurrentString intValue];
	}
//	if ([elementName isEqualToString:@"username"])
//	{
//		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"username"];
//	}
//	if ([elementName isEqualToString:@"password"])
//	{
//		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"password"];
//	}
	//	else if ([elementName isEqualToString:@"imageurl"])
	//	{
	//		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"imageurl"];
	//	}
	//	else if ([elementName isEqualToString:@"name"])
	//	{
	//		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"name"];
	//	}
	//	else if ([elementName isEqualToString:@"age"])
	//	{
	//		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"age"];
	//	}
	//	else if ([elementName isEqualToString:@"gender"])
	//	{
	//		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"gender"];
	//	}
	//	else if ([elementName isEqualToString:@"marritalstatus"])
	//	{
	//		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"marritalstatus"];
	//	}
	//	else if ([elementName isEqualToString:@"latitude"])
	//	{
	//		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"latitude"];
	//	}
	//	else if ([elementName isEqualToString:@"longitude"])
	//	{
	//		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"longitutde"];
	//	}
	//	else if ([elementName isEqualToString:@"status"])
	//	{
	//		[mCurItem setObject:[mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"status"];
	//	}	
	//	else if ([elementName isEqualToString:@"response"])
	//	{
	//		//self.userDetails=mCurItemArray;
	//		//		[mCurItem removeAllObjects];
	//	}
	
	storingCharacters=NO;
}

- (void)parser:(NSXMLParser*)parser foundCharacters:(NSString*)string
{
		if (storingCharacters) {
			[mCurrentString appendString:string];		
		}
		
}

- (void) parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
	NSLog(@"RegParser Error : %@",[parseError localizedDescription]);
}


//- (void)parser:(NSXMLParser *)parser foundCDATA:(NSData *)CDATABlock
//{
//    NSString *someString = [[NSString alloc] initWithData:CDATABlock encoding:NSUTF8StringEncoding];
//	[mCurItem setObject:someString forKey:@"message"];
//}
@end
//
//  InfoViewController.h
//  BabyPacking
//
//  Created by Gary He on 5/25/09.
//  Copyright 2009 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LayoutManager.h"
#import "syncTask.h"
#import <MessageUI/MFMailComposeViewController.h>

@class BabyPackingAppDelegate;

@interface InfoViewController : UITableViewController<MFMailComposeViewControllerDelegate> {
	
	BabyPackingAppDelegate *appDelegate;
	IBOutlet UIView *headerView;
    //IBOutlet UIView *headerViewLand;
    
    IBOutlet UIView *portraitVw,*landscapeVw;
	
    IBOutlet UIBarButtonItem *doneButton;
   	IBOutlet UILabel *lbVersion;
	IBOutlet UITableView *myTableView;
    IBOutlet UIImageView *imgIcon;
    
//    IBOutlet UIBarButtonItem *doneButtonLand;
//    IBOutlet UILabel *lbVersionLand;
//	IBOutlet UITableView *myTableViewLand;
//    IBOutlet UIImageView *imgIconLand;
    
    LayoutManager *layoutManager;
}

@property (strong) NSMetadataQuery *metadataQuery;
@property (strong) syncTask * doc;
@property (strong) NSMetadataQuery *query;

- (void) buttonAction:(id) sender;
-(IBAction)btnDownload:(id)sender;
-(IBAction)btnUpload:(id)sender;

@end

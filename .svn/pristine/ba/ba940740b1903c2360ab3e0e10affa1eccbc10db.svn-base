//
//  XMLParserBase.m
//  BabyPacking
//
//  Created by Jay Lee on 10/13/09.
//  Copyright 2009 Sunrising Software. All rights reserved.
//

#import "XMLParserBase.h"
//#import "XMLNode.h"

@interface XMLParserBase (Private)

//- (BOOL)parsingNode:(XMLNode *)node byName:(NSString *)name;

@end

@implementation XMLParserBase

- (id)initWithXMLData:(NSData*)xmlData
{
	if (self = [super init]) {
		[self beforeParsing];
		mElementStack = [NSMutableArray new];
		mCurrentString = [NSMutableString new];
		NSXMLParser* parser = [[NSXMLParser alloc] initWithData:xmlData];
		NSLog(@"%s", [xmlData bytes]);
		[parser setDelegate:self];
		[parser parse];
		BOOL gotError = ([parser parserError] != nil);
		if (gotError || (![self afterParsing])) {
			[self release];
			NSLog(@"Parser = nil! Error: %@", [[parser parserError] description]);
			[parser release];
			return nil;
		}
		[parser release];
		
		mResult = [NSMutableArray new];
	}
	return self;
}

- (void)dealloc
{
	[mResult release];
	[mParsingStack release];
	[mCurrentString release];
	[mElementStack release];
	[super dealloc];
}

#pragma mark To be overridden in subclasses.

- (void)beforeParsing
{
}

- (BOOL)afterParsing
{
	return YES;
}

- (void)didStartElement:(NSString*)elementName namespaceURI:(NSString*)namespaceURI qualifiedName:(NSString*)qualifiedName attributes:(NSDictionary*)attributes;
{

}

- (void)didEndElement:(NSString*)elementName namespaceURI:(NSString*)namespaceURI qualifiedName:(NSString*)qualifiedName
{

}

#pragma mark NSXMLParser delegate implementation and support

- (void)parser:(NSXMLParser*)parser didStartElement:(NSString*)elementName namespaceURI:(NSString*)namespaceURI qualifiedName:(NSString*)qualifiedName attributes:(NSDictionary*)attributes
{
	[mElementStack addObject:elementName];
	[mCurrentString replaceCharactersInRange:NSMakeRange(0, [mCurrentString length]) withString:@""];
	[self didStartElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName attributes:attributes];
}

- (void)parser:(NSXMLParser*)parser didEndElement:(NSString*)elementName namespaceURI:(NSString*)namespaceURI qualifiedName:(NSString*)qualifiedName
{
	NSAssert([[mElementStack lastObject] isEqualToString:elementName], @"Bad stack");
	[self didEndElement:elementName namespaceURI:namespaceURI qualifiedName:qualifiedName];
	[mElementStack removeLastObject];
}

- (void)parser:(NSXMLParser*)parser foundCharacters:(NSString*)string
{
	[mCurrentString appendString:string];
}

- (BOOL)xmlPathEndsWith:(NSString*)first, ...
{
	// Count arguments.
	int count = 0;
	va_list ap1;
	va_start(ap1, first);
	NSString* x1 = first;
	while (x1) {
		count += 1;
		x1 = va_arg(ap1, NSString*);
	}
	va_end(ap1);
	if ([mElementStack count] < count) return NO;
	
	// Check path.
	int index = [mElementStack count] - count;
	va_list ap2;
	va_start(ap2, first);
	NSString* x2 = first;
	while (x2) {
		if (![[mElementStack objectAtIndex:index] isEqualToString:x2]) return NO;
		index += 1;
		x2 = va_arg(ap2, NSString*);
	}
	va_end(ap2);
	
	return YES;
}

- (NSString*)trimmedString
{
	return [mCurrentString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (void)parser:(NSXMLParser *)parser foundNotationDeclarationWithName:(NSString *)name publicID:(NSString *)publicID systemID:(NSString *)systemID {
	NSLog(@"foundNotationDeclarationWithName");
	NSLog(@"name: %@; publicID: %@; systemID: %@;", name, publicID, systemID);
}

- (void)parser:(NSXMLParser *)parser foundUnparsedEntityDeclarationWithName:(NSString *)name publicID:(NSString *)publicID systemID:(NSString *)systemID notationName:(NSString *)notationName {
	NSLog(@"foundUnparsedEntityDeclarationWithName");
	NSLog(@"name: %@; publicID: %@; systemID: %@; notationName: %@;", name, publicID, systemID, notationName);
}

- (void)parser:(NSXMLParser *)parser foundAttributeDeclarationWithName:(NSString *)attributeName forElement:(NSString *)elementName type:(NSString *)type defaultValue:(NSString *)defaultValue {
	NSLog(@"foundAttributeDeclarationWithName");
	NSLog(@"attributeName: %@; elementName: %@; type: %@; defaultValue: %@;", attributeName, elementName, type, defaultValue);
}

- (void)parser:(NSXMLParser *)parser foundElementDeclarationWithName:(NSString *)elementName model:(NSString *)model {
	NSLog(@"foundElementDeclarationWithName");
	NSLog(@"elementName: %@; model: %@;", elementName, model);
}

- (void)parser:(NSXMLParser *)parser foundInternalEntityDeclarationWithName:(NSString *)name value:(NSString *)value {
	NSLog(@"foundInternalEntityDeclarationWithName");
	NSLog(@"name: %@; value: %@;", name, value);
}

- (void)parser:(NSXMLParser *)parser foundExternalEntityDeclarationWithName:(NSString *)name publicID:(NSString *)publicID systemID:(NSString *)systemID {
	NSLog(@"foundExternalEntityDeclarationWithName");
	NSLog(@"name: %@; publicID: %@; systemID: %@;", name, publicID, systemID);
}


@end

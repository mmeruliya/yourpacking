
# SQLiteManager Dump
# Version: 1.2.0
# http://www.sqlitemanager.org/
#
# Host: localhost
# Generation Time: Thursday 11th 2009f June 2009 07:03 pm
# SQLite Version: 3.3.7
# PHP Version: 5.2.6
# Database: baby.sqlite
# --------------------------------------------------------

#
# Table structure for table: briefcase
#
CREATE TABLE briefcase ( pk INTEGER NOT NULL PRIMARY KEY, title TEXT NOT NULL, passport TEXT NOT NULL, license TEXT NOT NULL, ssn TEXT NOT NULL, id_card TEXT NOT NULL, airline_name TEXT NOT NULL, flight TEXT NOT NULL, frequent_flyer TEXT NOT NULL, car_name TEXT NOT NULL, license_plate TEXT NOT NULL, hotel_name TEXT NOT NULL, hotel_phone TEXT NOT NULL, hotel_address TEXT NOT NULL, embassy_name TEXT NOT NULL, embassy_address TEXT NOT NULL, embassy_emergency TEXT NOT NULL, other TEXT NOT NULL );

#
# Dumping data for table: briefcase
#
# --------------------------------------------------------


#
# Table structure for table: item
#
CREATE TABLE item ( pk INTEGER NOT NULL PRIMARY KEY, name TEXT NOT NULL DEFAULT '""', qty INTEGER NOT NULL DEFAULT '''1''', tip TEXT, selected INTEGER NOT NULL DEFAULT '''0''', custom CHAR(20) NOT NULL DEFAULT "", checked INTEGER NOT NULL DEFAULT '''0''', listId INTEGER NOT NULL DEFAULT '''0''', predefineId INTEGER NOT NULL DEFAULT '''0''', predefineForAge INTEGER NOT NULL DEFAULT '''0''' );

#
# Dumping data for table: item
#
# --------------------------------------------------------


#
# Table structure for table: list
#
CREATE TABLE list ( pk INTEGER NOT NULL PRIMARY KEY , name TEXT NOT NULL , sex INTEGER NOT NULL , age INTEGER NOT NULL , smallIcon BLOB , photo BLOB , lastUpdate DOUBLE NOT NULL DEFAULT '0' , totalCount INTEGER NOT NULL DEFAULT '0' , checkedCount INTEGER NOT NULL DEFAULT '0' , predefineId INTEGER NOT NULL DEFAULT '0' , predefineForAge INTEGER NOT NULL DEFAULT '0' );

#
# Dumping data for table: list
#
# --------------------------------------------------------


#
# Table structure for table: record
#
CREATE TABLE record ( id INTEGER NOT NULL PRIMARY KEY, remote_id INTEGER NOT NULL DEFAULT "0", tid INTEGER NOT NULL DEFAULT "0", title CHAR(50) NOT NULL DEFAULT "", category CHAR(50) NOT NULL DEFAULT "", repeat CHAR(20) NOT NULL DEFAULT "", interval INTEGER NOT NULL DEFAULT "0", start_date DOUBLE NOT NULL DEFAULT "0", text CHAR(200) NOT NULL DEFAULT "", device_token CHAR(50) NOT NULL DEFAULT "", create_date DOUBLE NOT NULL DEFAULT "0", update_date DOUBLE NOT NULL DEFAULT "0" );

#
# Dumping data for table: record
#
# --------------------------------------------------------


#
# Table structure for table: task
#
CREATE TABLE task ( id INTEGER NOT NULL PRIMARY KEY, remote_id INTEGER NOT NULL DEFAULT "0", title CHAR(50) NOT NULL DEFAULT "", category CHAR(50) NOT NULL DEFAULT "", repeat CHAR(20) NOT NULL DEFAULT "", interval INTEGER NOT NULL DEFAULT "0", start_date DOUBLE NOT NULL DEFAULT "0", text CHAR(200) NOT NULL DEFAULT "", device_token CHAR(50) NOT NULL DEFAULT "", create_date DOUBLE NOT NULL DEFAULT "0", update_date DOUBLE NOT NULL DEFAULT "0" );

#
# Dumping data for table: task
#
# --------------------------------------------------------


#
# Table structure for table: version
#
CREATE TABLE version ( id INTEGER NOT NULL PRIMARY KEY, version CHAR NOT NULL DEFAULT '1.0', type CHAR NOT NULL DEFAULT 'bundle' );

#
# Dumping data for table: version
#
INSERT INTO version VALUES ('1', '1.30', 'database');
# --------------------------------------------------------


#
# User Defined Function properties: md5rev
#
/*
function md5_and_reverse($string) { return strrev(md5($string)); }
*/

#
# User Defined Function properties: IF
#
/*
function sqliteIf($compare, $good, $bad){ if ($compare) { return $good; } else { return $bad; } }
*/